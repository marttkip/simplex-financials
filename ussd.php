<?php
session_start();
// Reads the variables sent via POST from our gateway
$sessionId   = $_POST["sessionId"];
$serviceCode = $_POST["serviceCode"];
$phoneNumber = $_POST["phoneNumber"];
$text        = $_POST["text"];


$ussd_string_exploded = explode ("*",$text);

$level = count($ussd_string_exploded);

$first_character = $ussd_string_exploded[0];

if ($text == "") {
    // This is the first request. Note how we start the response with CON
    // $response  = "CON What would you want to check \n";
    // $response .= "1. My Account \n";
    // $response .= "2. My phone number";

    display_menu($phoneNumber);

}
else if ($text == "1" OR $first_character == "1") {
    // Business logic for first level response


   	loan_request($ussd_string_exploded,$phoneNumber);
    
    // $response .= "1. Account number \n";
    // $response .= "2. Account balance";

} else if ($text == "2" OR $first_character == "2") {
    // Business logic for first level response
    // This is a terminal request. Note how we start the response with END
    my_loans($ussd_string_exploded,$phoneNumber);
    // $response = "END Your phone number is ".$phoneNumber;

}else
{
	$count = count($ussd_string_exploded);
	$one = $ussd_string_exploded[0];
	$two = $ussd_string_exploded[1];

	$ussd_text = "Choose a loan you wants to view $one $two \n";
        $ussd_text .= "1. Long Term Loan\n";
        $ussd_text .= "2. Short Term Loan\n";
        ussd_proceed($ussd_text);
}

function ussd_proceed($ussd_text){
    echo "CON $ussd_text";
}

/* This ussd_stop function appends END to the USSD response your application gives.
 * This informs Africa's Talking USSD gateway and consecuently Safaricom's
 * USSD gateway that the USSD session should end.
 * Use this when you to want the application session to terminate/end the application
*/
function ussd_stop($ussd_text){
    echo "END $ussd_text";
}



//This is the home menu function
function display_menu($phone)
{
	$url = 'https://simplex-financials.com/simplex-financials/microfinance/individual/get_individual_details';
	//Encode the array into JSON.
	$patient_details['phone_number'] = $phone;
	//The JSON data.

	$data_string = json_encode($patient_details);

	try{                                                                                                         

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string))
		);
		$result = curl_exec($ch);
		curl_close($ch);
		

		$result = (array)json_decode($result);
		
		$status = $result['status'];

		if($status == "success")
		{
			$response  = "Welcome to Simplex Financials ".$result['message'].",\n";
		    // $response .= "1. Enter passcode \n";
		    // $response = "Choose account information you want to view \n";
		    $response .= "1. Request short loan \n";
		    $response .= "2. My Loans";
		    // $response .= "2. Forgot password ";
		    	// var_dump($response);die();

		    $_SESSION['individual_id'] = $result['individual_id'];


		    ussd_proceed($response);
		}
		else
		{
			$ussd_text = "To Sign up for microfinance. Kindly call us through 0720465220";
			ussd_stop($ussd_text); 
		}

		 
	}
	catch(Exception $e)
	{
		
		$ussd_text = "Sorry could not process the request now. Please try again";
		ussd_stop($ussd_text); 
	}


  
}

function loan_request($details,$phone)
{
	if(count($details) == 1)
    {
    	$ussd_text = "Amount to borrow :";
        ussd_proceed($ussd_text); // ask user to enter registration details
    }
    if(count($details) == 2)
    {
        if (empty($details[1])){
                $ussd_text = "Sorry we do not accept blank values.";
                ussd_proceed($ussd_text);
        } else 
        {
	        $ussd_text = "Confirm you want to borrow : Ksh. ".$details[1]."\n";
	        $ussd_text .= "1. Confirm\n";
	        $ussd_text .= "2. Cancel\n";
	        ussd_proceed($ussd_text); // ask user to enter registration details
	    }
    }
    if(count($details)== 3)
    {
     //   if (empty($details[2])){
     //            $ussd_text = "Sorry we do not accept blank values.";
     //            ussd_proceed($ussd_text);
     //    } else 
     //    {
	    //    $amount_to_borrow = $details[1];

	    //    request_loan($phone,$amount_to_borrow);

	    // }
    	if($details[2] == "1")
    	{
    		$amount_to_borrow = $details[1];

	       request_loan($phone,$amount_to_borrow);
    	}
    	else
    	{
    		$ussd_text = "Confirm you want to borrow : Ksh. ".$details[1]."\n";
	        $ussd_text .= "1. Confirm\n";
	        ussd_proceed($ussd_text); // ask user to enter registration details

    	}
	        


	    
    }
    if(count($details)== 4)
    {
        if (empty($details[3])){
                $ussd_text = "Sorry we do not accept blank values";
                ussd_proceed($ussd_text);
        } else 
        {
         $ussd_text = "Passsword:";
         ussd_proceed($ussd_text); // ask user to enter registration details
        }
    }
    if(count($details)==5)
    {
        if (empty($details[3])){
                $ussd_text = "Sorry we do not accept blank values";
                ussd_proceed($ussd_text);
        } else 
        {
            $ussd_text = "Amount:";
            ussd_proceed($ussd_text); // ask user to enter registration details
        }
    }
}


function my_loans($details,$phone)
{
	if(count($details) == 1)
    {
    	$ussd_text = "Choose a loan you want to view \n";
        $ussd_text .= "1. Long Term Loan\n";
        $ussd_text .= "2. Short Term Loan\n";

        ussd_proceed($ussd_text); // ask user to enter registration details
    }
    else if(count($details) == 2)
    {
        if (empty($details[1])){
                $ussd_text = "Sorry we do not accept blank values.";
                ussd_proceed($ussd_text);
        } else 
        {

        	$checked_status = $details[1];
        	if($checked_status == "1")
        	{
        		get_my_loans($phone);
        	}
        	else if($checked_status == "2")
        	{
        		get_my_short_loans($phone);
        	}
	        
	    }
    }

    else if(count($details) == 3)
    {
        if (empty($details[2])){
                $ussd_text = "Sorry we do not accept blank values.";
                ussd_proceed($ussd_text);
        } else 
        {

        	$checked_status = $details[1];

        	if($checked_status == "1")
        	{
        		$loan_id = $details[2];
	        	// $response  = "Choose a loan ?\n";
		        get_loan_details($phone,$loan_id);
        	}
        	else if($checked_status == "2")
        	{
        		$loan_id = $details[2];
	        	// $response  = "Choose a loan ?\n";
		        get_short_loan_details($phone,$loan_id);
        	}
    	
	    }
    }

    else if(count($details) == 4)
    {
        if (empty($details[3])){
                $ussd_text = "Sorry we do not accept blank values.";
                ussd_proceed($ussd_text);
        } else 
        {
        	$loan_item = $_SESSION['individual_loan_id'];
	        $response  = "Amount :\n";

		    ussd_proceed($response);
    	
	    }
    }
    else if(count($details) == 5)
    {
        if (empty($details[4]) OR !is_numeric($details[4])){
                $ussd_text = "Sorry we do not accept blank values.";
                ussd_proceed($ussd_text);
        } else 
        {
        	$amount_to_pay = $details[4];
	        $response  = "You are about to make a payment of KES. ".number_format($amount_to_pay,2)."\n";
	        $response  .= "1. Continue\n";
	        $response  .= "2. Back to menu";

		    ussd_proceed($response);
    	
	    }
    }
    else if(count($details) == 6)
    {
        if ($details[5] == "1"){
                $amount_to_pay = $details[4];

                $checked_status = $details[1];
                if($checked_status == "1")
                {
                	submit_payment($phone,$amount_to_pay);
                }
                else if($checked_status == "2")
                {
                	submit_short_payment($phone,$amount_to_pay);
                }
                
        } else 
        {
        	$amount_to_pay = $details[4];
	        // $response  = "You are about to make a payment of KES. ".number_format($amount_to_pay,2)."\n";
	        $response  = "1. Continue\n";
	        $response  = "2. Back to menu";

		    ussd_proceed($response);
    	
	    }
    }
   
}

//This is the home menu function
function get_my_loans($phone)
{
	$url = 'https://simplex-financials.com/simplex-financials/microfinance/loans_application/get_individual_loans';
	//Encode the array into JSON.
	$patient_details['phone_number'] = $phone;
	$patient_details['individual_id'] = $_SESSION['individual_id'];
	//The JSON data.

	$data_string = json_encode($patient_details);

	try{                                                                                                         

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string))
		);
		$result = curl_exec($ch);
		curl_close($ch);
		

		$result = (array)json_decode($result);
		
		$status = $result['status'];

		if($status == "success")
		{
			$response  = "Choose a loan ?\n";
		    $response .= $result['loans'];
		    // $response .= "2. Forgot password ";
		    	// var_dump($response);die();


		    ussd_proceed($response);
		}
		else
		{

			$ussd_text = "Sorry you do not have loans under your account";
			ussd_stop($ussd_text); 
		}

		 
	}
	catch(Exception $e)
	{
		
		$ussd_text = "Sorry could not process the request now. Please try again";
		ussd_stop($ussd_text); 
	}


  
}

function get_loan_details($phone,$loan_id)
{
	$url = 'https://simplex-financials.com/simplex-financials/microfinance/loans_application/get_my_loan_details';
	//Encode the array into JSON.
	$patient_details['phone_number'] = $phone;
	$patient_details['individual_id'] = $_SESSION['individual_id'];
	$patient_details['loan_id'] = $loan_id;
	//The JSON data.

	$data_string = json_encode($patient_details);

	try{                                                                                                         

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string))
		);
		$result = curl_exec($ch);
		curl_close($ch);
		

		$result = (array)json_decode($result);
		
		$status = $result['status'];

		if($status == "success")
		{
			// $response  = "Choose a loan ?\n";
		    $response = $result['loan_detail'];

		    $_SESSION['individual_loan_id'] = $result['individual_loan_id'];
		    


		    ussd_proceed($response);
		}
		else
		{

			$ussd_text = "Sorry you do not have loanss under your account";
			ussd_stop($ussd_text); 
		}

		 
	}
	catch(Exception $e)
	{
		
		$ussd_text = "Sorry could not process the request now. Please try again";
		ussd_stop($ussd_text); 
	}


  
}

function submit_payment($phone,$amount_to_pay)
{

	$url = 'https://simplex-financials.com/simplex-financials/mpesa/pay_loan';
	//Encode the array into JSON.
	$patient_details['phone_number'] = $phone;
	// $patient_details['individual_loan_id'] = $_SESSION['individual_loan_id'];
	$patient_details['amount'] = $amount_to_pay;
	//The JSON data.

	$data_string = json_encode($patient_details);

	try{                                                                                                         

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string))
		);
		$result = curl_exec($ch);
		curl_close($ch);
		

		$result = (array)json_decode($result);
		
		// $status = $result['status'];

		// if($status == "success")
		// {
			$response  = "Your payment details is being processed. Kindly be patient";

		    ussd_stop($response);
		// }
		// else
		// {

		// 	$ussd_text = "Sorry you do not have loanss under your account";
		// 	ussd_stop($ussd_text); 
		// }

		 
	}
	catch(Exception $e)
	{
		
		$ussd_text = "Sorry could not process the request now. Please try again";
		ussd_stop($ussd_text); 
	}


}

function request_loan($phone,$amount)
{

	$url = 'https://simplex-financials.com/simplex-financials/mpesa/loan_request';
	//Encode the array into JSON.
	$patient_details['phone_number'] = $phone;
	$patient_details['amount'] = $amount;
	$patient_details['individual_id'] = $_SESSION['individual_id'];
	$patient_details['individual_loan_id'] = $_SESSION['individual_loan_id'];
	//The JSON data.

	$data_string = json_encode($patient_details);

	try{                                                                                                         

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string))
		);
		$result = curl_exec($ch);
		curl_close($ch);
		

		$result = (array)json_decode($result);
		
		$status = $result['status'];
		// if($status == "success")
		// {
			$response  = "Your request is being processed. Kindly be patient,\n";
		    ussd_stop($response);
		// }
		// else
		// {
		// 	$ussd_text = "Sorry your request could not be processed at the moment kindly try again";
		// 	ussd_stop($ussd_text); 
		// }
		 
	}
	catch(Exception $e)
	{
		
		$ussd_text = "Sorry could not process the request now. Please try again";
		ussd_stop($ussd_text); 
	}
}


//This is the home menu function
function get_my_short_loans($phone)
{
	$url = 'https://simplex-financials.com/simplex-financials/microfinance/loans_application/get_short_individual_loans';
	//Encode the array into JSON.
	$patient_details['phone_number'] = $phone;
	$patient_details['individual_id'] = $_SESSION['individual_id'];
	//The JSON data.

	$data_string = json_encode($patient_details);

	try{                                                                                                         

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string))
		);
		$result = curl_exec($ch);
		curl_close($ch);
		

		$result = (array)json_decode($result);
		
		$status = $result['status'];

		if($status == "success")
		{
			$response  = "Choose a loan ?\n";
		    $response .= $result['loans'];
		    // $response .= "2. Forgot password ";
		    	// var_dump($response);die();


		    ussd_proceed($response);
		}
		else
		{

			$ussd_text = "Sorry you do not have loans under your account";
			ussd_stop($ussd_text); 
		}

		 
	}
	catch(Exception $e)
	{
		
		$ussd_text = "Sorry could not process the request now. Please try again";
		ussd_stop($ussd_text); 
	}


  
}

function get_short_loan_details($phone,$loan_id)
{
	$url = 'https://simplex-financials.com/simplex-financials/microfinance/loans_application/get_short_loan_details';
	//Encode the array into JSON.
	$patient_details['phone_number'] = $phone;
	$patient_details['individual_id'] = $_SESSION['individual_id'];
	$patient_details['loan_id'] = $loan_id;
	//The JSON data.

	$data_string = json_encode($patient_details);

	try{                                                                                                         

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string))
		);
		$result = curl_exec($ch);
		curl_close($ch);
		

		$result = (array)json_decode($result);
		
		$status = $result['status'];

		if($status == "success")
		{
			// $response  = "Choose a loan ?\n";
		    $response = $result['loan_detail'];

		    $_SESSION['individual_loan_id'] = $result['individual_loan_id'];
		    


		    ussd_proceed($response);
		}
		else
		{

			$ussd_text = "Sorry you do not have loanss under your account";
			ussd_stop($ussd_text); 
		}

		 
	}
	catch(Exception $e)
	{
		
		$ussd_text = "Sorry could not process the request now. Please try again";
		ussd_stop($ussd_text); 
	}


}

function submit_short_payment($phone,$amount_to_pay)
{

	$url = 'https://simplex-financials.com/simplex-financials/mpesa/pay_short_loan';
	//Encode the array into JSON.
	$patient_details['phone_number'] = $phone;
	// $patient_details['individual_loan_id'] = $_SESSION['individual_loan_id'];
	$patient_details['amount'] = $amount_to_pay;
	//The JSON data.

	$data_string = json_encode($patient_details);

	try{                                                                                                         

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string))
		);
		$result = curl_exec($ch);
		curl_close($ch);
		

		$result = (array)json_decode($result);
		
		// $status = $result['status'];

		// if($status == "success")
		// {
			$response  = "Your payment details is being processed. Kindly be patient";

		    ussd_stop($response);
		// }
		// else
		// {

		// 	$ussd_text = "Sorry you do not have loanss under your account";
		// 	ussd_stop($ussd_text); 
		// }

		 
	}
	catch(Exception $e)
	{
		
		$ussd_text = "Sorry could not process the request now. Please try again";
		ussd_stop($ussd_text); 
	}


}

// Echo the response back to the API
header('Content-type: text/plain');
echo $response;

