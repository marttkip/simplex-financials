<?php

class Member_savings_model extends CI_Model 
{
	public function upload_image($path, $location, $resize, $name, $upload, $edit = NULL)
	{
		if(!empty($_FILES[$upload]['tmp_name']))
		{
			$image = $this->session->userdata($name);
			
			if((!empty($image)) || ($edit != NULL))
			{
				if($edit != NULL)
				{
					$image = $edit;
				}
				
				//delete any other uploaded image
				if($this->file_model->delete_file($path."\\".$image, $location))
				{
					//delete any other uploaded thumbnail
					$this->file_model->delete_file($path."\\thumbnail_".$image, $location);
				}
				
				else
				{
					$this->file_model->delete_file($path."/".$image, $location);
					$this->file_model->delete_file($path."/thumbnail_".$image, $location);
				}
			}
			//Upload image
			$response = $this->file_model->upload_file($path, $upload, $resize);
			if($response['check'])
			{
				$file_name = $response['file_name'];
				$thumb_name = $response['thumb_name'];
					
				//Set sessions for the image details
				$this->session->set_userdata($name, $file_name);
			
				return TRUE;
			}
		
			else
			{
				$this->session->set_userdata('upload_error_message', $response['error']);
				
				return FALSE;
			}
		}
		
		else
		{
			$this->session->set_userdata('upload_error_message', '');
			return FALSE;
		}
	}
	
	/*
	*	Retrieve all individual
	*
	*/
	public function all_branches()
	{
		$this->db->order_by('branch_name');
		$this->db->where('branch_status = 1');
		$query = $this->db->get('branch');
		
		return $query;
	}
	public function add_cheque_disbursement($individual_id,$individual_loan_id)
	{
		$data = array(
			'created'=> date('Y-m-d H:i:s'),
			'deleted' => 0,
			'deleted_on'=> date('Y-m-d H:i:s'),
			'individual_id'=>$individual_id,
			'individual_loan_id'=>$individual_loan_id,
			'created_by'=> $this->session->userdata('personnel_id'),
			'modified_by'=> $this->session->userdata('personnel_id'),
			'deleted_by'=> $this->session->userdata('personnel_id'),
			'cheque_number'=>$this->input->post('cheque_number'),
			'cheque_amount'=>$this->input->post('cheque_amount'),
			'disbursement_type' => $this->input->post('disbursement_type_id'),
			'dibursement_date'=>$this->input->post('disbursement_date')
			);
			if($this->db->insert('disbursement', $data))
			{
				//update loan status to disbursed
				$status_update = array(
									'individual_loan_status' => 2,
									'disbursed_amount'=>$this->input->post('cheque_amount'),
									'disbursed_date'=>$this->input->post('disbursement_date'),
									'disbursed_by'=>$this->session->userdata('personnel_id')
									);
				$this->db->where('individual_loan_id = '.$individual_loan_id);
				$this->db->update('individual_loan',$status_update);
				
				return $this->db->insert_id();
			}
			else{
				return FALSE;
			}
	}
	public function all_individual($individual_id = NULL)
	{
		$where = 'individual_status = 1';
		
		if($individual_id != NULL)
		{
			$where .= ' AND individual.individual_id <> '.$individual_id;
		}
		$this->db->where($where);
		$this->db->order_by('individual_fname');
		$query = $this->db->get('individual');
		
		// var_dump($query);
		return $query;
	}
	
	/*
	*	Retrieve all parent individual
	*
	*/
	public function all_parent_individual($order = 'individual_name')
	{
		$this->db->where('individual_status = 1 AND individual_parent = 0');
		$this->db->order_by($order, 'ASC');
		$query = $this->db->get('individual');
		
		return $query;
	}
	/*
	*	Retrieve all children individual
	*
	*/
	public function all_child_individual()
	{
		$this->db->where('individual_status = 1 AND individual_parent > 0');
		$this->db->order_by('individual_name', 'ASC');
		$query = $this->db->get('individual');
		
		return $query;
	}
	
	/*
	*	Retrieve all individual
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_individual($table, $where, $per_page, $page, $order, $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}

	
	/*
	*	Add a new individual
	*	@param string $image_name
	*
	*/
	public function add_individual()
	{
		$data = array(
			'individual_mname'=>ucwords(strtolower($this->input->post('individual_mname'))),
			'individual_lname'=>ucwords(strtolower($this->input->post('individual_lname'))),
			'individual_fname'=>ucwords(strtolower($this->input->post('individual_fname'))),
			'individual_dob'=>$this->input->post('individual_dob'),
			'individual_email'=>$this->input->post('individual_email'),
			'gender_id'=>$this->input->post('gender_id'),
			'individual_phone'=>$this->input->post('individual_phone'),
			'individual_phone2'=>$this->input->post('individual_phone2'),
			'civilstatus_id'=>$this->input->post('civil_status_id'),
			'branch_id'=>$this->input->post('branch_id'),
			'individual_address'=>$this->input->post('individual_address'),
			'individual_locality'=>$this->input->post('individual_locality'),
			'kra_pin'=>$this->input->post('kra_pin'),
			'title_id'=>$this->input->post('title_id'),
			'individual_number'=>$this->input->post('individual_number'),
			'individual_city'=>$this->input->post('individual_city'),
			'individual_post_code'=>$this->input->post('individual_post_code'),
			'individual_email2'=>$this->input->post('individual_email2'),
			'document_id'=>$this->input->post('document_id'),
			'document_number'=>$this->input->post('document_number'),
			'document_place'=>$this->input->post('document_place'),
			'individual_type_id'=>$this->input->post('individual_type_id')
		);
		
		if($this->db->insert('individual', $data))
		{
			return $this->db->insert_id();
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Update an existing individual
	*	@param string $image_name
	*	@param int $individual_id
	*
	*/
	public function edit_individual($individual_id, $image, $signature)
	{
		$data = array(
			'individual_mname'=>ucwords(strtolower($this->input->post('individual_mname'))),
			'individual_lname'=>ucwords(strtolower($this->input->post('individual_lname'))),
			'individual_fname'=>ucwords(strtolower($this->input->post('individual_fname'))),
			'individual_dob'=>$this->input->post('individual_dob'),
			'individual_email'=>$this->input->post('individual_email'),
			'gender_id'=>$this->input->post('gender_id'),
			'individual_phone'=>$this->input->post('individual_phone'),
			'individual_number'=>$this->input->post('individual_number'),
			'individual_phone2'=>$this->input->post('individual_phone2'),
			'civilstatus_id'=>$this->input->post('civil_status_id'),
			'individual_address'=>$this->input->post('individual_address'),
			'individual_locality'=>$this->input->post('individual_locality'),
			'kra_pin'=>$this->input->post('kra_pin'),
			'branch_id'=>$this->input->post('branch_id'),
			'title_id'=>$this->input->post('title_id'),
			'individual_city'=>$this->input->post('individual_city'),
			'image'=>$image,
			'signature'=>$signature,
			'individual_email2'=>$this->input->post('individual_email2'),
			'document_id'=>$this->input->post('document_id'),
			'document_number'=>$this->input->post('document_number'),
			'document_place'=>$this->input->post('document_place'),
			'individual_type_id'=>$this->input->post('individual_type_id'),
			'outstanding_loan'=>$this->input->post('outstanding_loan'),
			'total_savings'=>$this->input->post('total_savings')
		);
		
		$this->db->where('individual_id', $individual_id);
		if($this->db->update('individual', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}


	function upload_inividual_image($individual_id, $image)
	{
		$data = array(
			'document_name'=> $image,
			'created_by'=> $this->session->userdata('personnel_id'),
			'modified_by'=> $this->session->userdata('personnel_id'),
			'created'=> date('Y-m-d H:i:s'),
			'individual_id'=>$individual_id
		);
		
		if($this->db->insert('individual_identification', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	function upload_individual_documents($individual_id, $document)
	{
		$data = array(
			'document_name'=> $this->input->post('document_item_name'),
			'document_upload_name'=> $document,
			'created_by'=> $this->session->userdata('personnel_id'),
			'modified_by'=> $this->session->userdata('personnel_id'),
			'created'=> date('Y-m-d H:i:s'),
			'individual_id'=>$individual_id
		);
		
		if($this->db->insert('document_uploads', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function get_individual_name($individual_id)
	{


		$this->db->from('individual');
		$this->db->select('individual_fname, individual_mname, individual_lname');
		$this->db->where('individual_id ='.$individual_id);
		$query = $this->db->get();
		$total_paid = " ";
		$fname = " ";
        $lname = " ";
        $mname = " ";
		foreach ($query->result() as $key) {
			# code...
			$fname = $key->individual_fname;
			$lname = $key->individual_lname;
			$mname = $key->individual_mname;
			$total_paid = $fname.' '.$mname.' '.$lname;
		}
		return $total_paid;
	}
	public function get_withdrawal_charge($individual_savings_id)
	{


		$this->db->from('savings_plan,individual_savings');
		$this->db->select('savings_plan.charge_withdrawal as charge_withdrawal , savings_plan.withdrawal_charge as withdrawal_charge');
		$this->db->where('individual_savings.individual_savings_id = '.$individual_savings_id);
		$query = $this->db->get();
		$charge_withdrawal = 0;
		$withdrawal_charge = 0;
       
		foreach ($query->result() as $key) {
			# code...
			$charge_withdrawal = $key->charge_withdrawal;
			if($charge_withdrawal == 0)
			{
				$withdrawal_charge = 0;
			}
			else
			{
				$withdrawal_charge = $key->withdrawal_charge;

			}
			
		}
		return $withdrawal_charge;
	}

	function get_individual_identifications($individual_id)
	{
		$this->db->from('individual_identification');
		$this->db->select('*');
		$this->db->where('individual_id = '.$individual_id);
		$query = $this->db->get();
		
		return $query;
	}
	function get_document_uploads($individual_id)
	{
		$this->db->from('document_uploads');
		$this->db->select('*');
		$this->db->where('individual_id = '.$individual_id);
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	get a single individual's children
	*	@param int $individual_id
	*
	*/
	public function get_sub_individual($individual_id)
	{
		//retrieve all users
		$this->db->from('individual');
		$this->db->select('*');
		$this->db->where('individual_parent = '.$individual_id);
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	get a single individual's details
	*	@param int $individual_id
	*
	*/
	public function get_individual($individual_id)
	{
		//retrieve all users
		$this->db->from('individual');
		$this->db->select('*');
		$this->db->where('individual_id = '.$individual_id);
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	Delete an existing individual
	*	@param int $individual_id
	*
	*/
	public function delete_individual($individual_id)
	{
		//delete parent
		if($this->db->delete('individual', array('individual_id' => $individual_id)))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Activate a deactivated individual
	*	@param int $individual_id
	*
	*/
	public function activate_individual($individual_id)
	{
		$data = array(
				'individual_status' => 1
			);
		$this->db->where('individual_id', $individual_id);
		

		if($this->db->update('individual', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Deactivate an activated individual
	*	@param int $individual_id
	*
	*/
	public function deactivate_individual($individual_id)
	{
		$data = array(
				'individual_status' => 0
			);
		$this->db->where('individual_id', $individual_id);
		
		if($this->db->update('individual', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Retrieve gender
	*
	*/
	public function get_gender()
	{
		$this->db->order_by('gender_name');
		$query = $this->db->get('gender');
		
		return $query;
	}
	
	/*
	*	Retrieve title
	*
	*/
	public function get_title()
	{
		$this->db->order_by('title_name');
		$query = $this->db->get('title');
		
		return $query;
	}
	
	/*
	*	Retrieve civil_status
	*
	*/
	public function get_civil_status()
	{
		$this->db->order_by('civil_status_name');
		$query = $this->db->get('civil_status');
		
		return $query;
	}
	
	/*
	*	Retrieve religion
	*
	*/
	public function get_religion()
	{
		$this->db->order_by('religion_name');
		$query = $this->db->get('religion');
		
		return $query;
	}
	
	/*
	*	Retrieve relationship
	*
	*/
	public function get_relationship()
	{
		$this->db->order_by('relationship_name');
		$query = $this->db->get('relationship');
		
		return $query;
	}
	
	/*
	*	Select get_job_titles
	*
	*/
	public function get_job_titles()
	{
		$this->db->select('*');
		$this->db->order_by('job_title_name', 'ASC');
		$query = $this->db->get('job_title');
		
		return $query;
	}
	
	
	/*
	*	get a single individual's details
	*	@param int $individual_id
	*
	*/
	public function get_emergency_contacts($individual_id)
	{
		//retrieve all users
		$this->db->from('individual_emergency, relationship');
		$this->db->select('*');
		$this->db->where('individual_emergency.individual_id = '.$individual_id.' AND individual_emergency.relationship_id = relationship.relationship_id');
		$this->db->order_by('individual_emergency_fname');
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	get a single individual's details
	*	@param int $individual_id
	*
	*/
	public function get_individual_dependants($individual_id)
	{
		//retrieve all users
		$this->db->from('individual_dependant');
		$this->db->select('*');
		$this->db->where(array('individual_dependant.individual_id' => $individual_id, 'individual_dependant.relationship_id' => 'relationship.relationship_id'));
		$this->db->order_by('individual_dependant_fname');
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	get a single individual's details
	*	@param int $individual_id
	*
	*/
	public function get_individual_jobs($individual_id)
	{
		//retrieve all users
		$this->db->from('individual_job');
		$this->db->select('individual_job.*');
		$this->db->order_by('employment_date', 'DESC');
		$this->db->where(array('individual_job.individual_id' => $individual_id));
		$query = $this->db->get();
		
		return $query;
	}
	
	public function get_leave_types()
	{
		$table = "leave_type";
		$where = "leave_type_status = 0";
		$items = "leave_type_id, leave_type_name";
		$order = "leave_type_name";
		
		$this->db->where($where);
		$this->db->order_by($order);
		$result = $this->db->get($table);
		
		return $result;
	}
	
	/*
	*	get a single individual's leave details
	*	@param int $individual_id
	*
	*/
	public function get_individual_leave($individual_id)
	{
		//retrieve all users
		$this->db->from('leave_duration, leave_type');
		$this->db->select('leave_duration.*, leave_type.leave_type_name');
		$this->db->order_by('start_date', 'DESC');
		$this->db->where(array('leave_duration.individual_id' => $individual_id, 'leave_duration.leave_type_id' => 'leave_type.leave_type_id'));
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	get a single individual's roles
	*	@param int $individual_id
	*
	*/
	public function get_individual_roles($individual_id)
	{
		//retrieve all users
		$this->db->from('individual_section, section');
		$this->db->select('individual_section.*, section.section_name, section.section_position');
		$this->db->order_by('section_position', 'ASC');
		$this->db->where(array('individual_section.individual_id' => $individual_id, 'individual_section.section_id' => 'section.section_id'));
		$query = $this->db->get();
		
		return $query;
	}
	
	public function get_individual_savings_plans($individual_id)
	{
		
		$this->db->from('individual_savings');
		$this->db->select('*');
		$this->db->order_by('created', 'DESC');
		$this->db->where('individual_savings.individual_id = '.$individual_id);
		
		$query = $this->db->get();
		
		return $query;

	}
	
	/*
	*	Add a new individual
	*	@param string $image_name
	*
	*/
	public function add_individual_plan($individual_id) 
	{
		$data = array(
			'individual_id'=>$individual_id,
			'savings_plan_id'=>$this->input->post('savings_plan_id'),
			'individual_savings_status'=>$this->input->post('individual_savings_status'),
			'individual_savings_opening_balance'=>$this->input->post('individual_savings_opening_balance'),
			'start_date'=>$this->input->post('start_date'),
			'created'=>date('Y-m-d H:i:s'),
			'created_by'=>$this->session->userdata('personnel_id'),
			'modified_by'=>$this->session->userdata('personnel_id')
		);
		
		if($this->db->insert('individual_savings', $data))
		{
			return $this->db->insert_id();
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Activate a individual plan
	*	@param int $individual_savings_id
	*
	*/
	public function activate_individual_plan($individual_savings_id)
	{
		$data = array(
				'individual_savings_status' => 1
			);
		$this->db->where('individual_savings_id', $individual_savings_id);
		

		if($this->db->update('individual_savings', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Deactivate a individual plan
	*	@param int $individual_savings_id
	*
	*/
	public function deactivate_individual_plan($individual_savings_id)
	{
		$data = array(
				'individual_savings_status' => 0
			);
		$this->db->where('individual_savings_id', $individual_savings_id);
		

		if($this->db->update('individual_savings', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Add a new individual
	*	@param string $image_name
	*
	*/
	public function add_position($individual_id)
	{
		$data = array(
			'employer'=>$this->input->post('employer'),
			'job_title'=>$this->input->post('job_title'),
			'employment_date'=>$this->input->post('employment_date'),
			'individual_type_id'=>$this->input->post('individual_type_id'),
			'individual_id'=>$individual_id,
			'created'=>date('Y-m-d H:i:s'),
			'created_by'=>$this->session->userdata('personnel_id'),
			'modified_by'=>$this->session->userdata('personnel_id')
		);
		
		if($this->db->insert('individual_job', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Activate a individual plan
	*	@param int $individual_savings_id
	*
	*/
	public function activate_individual_position($individual_job_id)
	{
		$data = array(
				'individual_job_status' => 1
			);
		$this->db->where('individual_job_id', $individual_job_id);

		if($this->db->update('individual_job', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Deactivate a individual plan
	*	@param int $individual_savings_id
	*
	*/
	public function deactivate_individual_position($individual_job_id)
	{
		$data = array(
				'individual_job_status' => 0
			);
		$this->db->where('individual_job_id', $individual_job_id);

		if($this->db->update('individual_job', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Add a new individual
	*	@param string $image_name
	*
	*/
	public function add_emergency($individual_id)
	{
		$data = array(
			'individual_emergency_onames'=>ucwords(strtolower($this->input->post('individual_emergency_onames'))),
			'individual_emergency_fname'=>ucwords(strtolower($this->input->post('individual_emergency_fname'))),
			'individual_emergency_dob'=>$this->input->post('individual_emergency_dob'),
			'individual_emergency_email'=>$this->input->post('individual_emergency_email'),
			'individual_emergency_phone'=>$this->input->post('individual_emergency_phone'),
			'individual_emergency_phone2'=>$this->input->post('individual_emergency_phone2'),
			'individual_emergency_address'=>$this->input->post('individual_emergency_address'),
			'individual_emergency_city'=>$this->input->post('individual_emergency_city'),
			'individual_emergency_post_code'=>$this->input->post('individual_emergency_post_code'),
			'individual_emergency_email2'=>$this->input->post('individual_emergency_email2'),
			'share_percentage'=>$this->input->post('share_percentage'),
			'document_id'=>$this->input->post('document_id'),
			'document_number'=>$this->input->post('document_number'),
			'relationship_id'=>$this->input->post('relationship_id'),
			'individual_id'=>$individual_id
		);
		
		if($this->db->insert('individual_emergency', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	/*
	*	Add a new individual
	*	@param string $image_name
	*
	*/
	public function edit_emergency($individual_id,$individual_emergency_id)
	{
		$data = array(
			'individual_emergency_onames'=>ucwords(strtolower($this->input->post('individual_emergency_onames'))),
			'individual_emergency_fname'=>ucwords(strtolower($this->input->post('individual_emergency_fname'))),
			'individual_emergency_dob'=>$this->input->post('individual_emergency_dob'),
			'individual_emergency_email'=>$this->input->post('individual_emergency_email'),
			'individual_emergency_phone'=>$this->input->post('individual_emergency_phone'),
			'individual_emergency_phone2'=>$this->input->post('individual_emergency_phone2'),
			'individual_emergency_address'=>$this->input->post('individual_emergency_address'),
			'individual_emergency_city'=>$this->input->post('individual_emergency_city'),
			'individual_emergency_post_code'=>$this->input->post('individual_emergency_post_code'),
			'individual_emergency_email2'=>$this->input->post('individual_emergency_email2'),
			'share_percentage'=>$this->input->post('share_percentage'),
			'document_id'=>$this->input->post('document_id'),
			'document_number'=>$this->input->post('document_number'),
			'relationship_id'=>$this->input->post('relationship_id'),
			'individual_id'=>$individual_id
		);
		$this->db->where('individual_emergency_id = '.$individual_emergency_id);
		if($this->db->update('individual_emergency', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Delete an existing individual
	*	@param int $individual_id
	*
	*/
	public function delete_emergency($individual_emergency_id)
	{
		//delete children
		if($this->db->delete('individual_emergency', array('individual_emergency_id' => $individual_emergency_id)))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function get_loan_repayment_amount($individual_id)
	{
		$this->db->from('savings_withdrawal');
		$this->db->select('savings_withdrawal.*');
		$this->db->where('individual_id = '.$individual_id);
		$query = $this->db->get();
		
		return $query;
	}
	public function get_disbursments($individual_id)
	{
		$this->db->from('disbursement');
		$this->db->order_by('dibursement_date', 'ASC');
		$this->db->where('disbursement.individual_id = '.$individual_id);
		$query = $this->db->get();
		
		return $query;
	}
	
	public function get_individual_loans($individual_id)
	{
		$this->db->from('individual_loan');
		$this->db->select('individual_loan.*, loans_plan.loans_plan_name');
		$this->db->join('loans_plan', 'loans_plan.loans_plan_id = individual_loan.loans_plan_id', 'left');
		$this->db->order_by('disbursed_date', 'ASC');
		$this->db->where('individual_loan.individual_id = '.$individual_id);
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	Add a new individual
	*	@param string $image_name
	*
	*/
	public function add_individual_loan($individual_id) 
	{
		$loans_plan_id = $this->input->post('loans_plan_id');
		
		//get loan details
		$this->db->select('loans_plan.interest_id, loans_plan.interest_rate, installment_type.installment_type_duration');
		$this->db->where('loans_plan_id', $loans_plan_id);
		$this->db->join('installment_type', 'installment_type.installment_type_id = loans_plan.installment_type_id', 'left');
		$query = $this->db->get('loans_plan');
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$interest_id = $row->interest_id;
			$interest_rate = $row->interest_rate;
			$installment_type_duration = $row->installment_type_duration;
		}
		
		else
		{
			$interest_id = 0;
			$installment_type_duration = 0;
			$interest_rate = 0;
		}
		
		$data = array(
			'interest_rate'=>$interest_rate,
			'individual_id'=>$individual_id,
			'loans_plan_id'=>$loans_plan_id,
			'interest_rate'=>$interest_rate,
			'interest_id'=>$interest_id,
			'installment_type_duration'=>$installment_type_duration,
			'individual_loan_status'=>$this->input->post('individual_loan_status'),
			'individual_loan_opening_balance'=>$this->input->post('individual_loan_opening_balance'),
			'start_date'=>$this->input->post('start_date'),
			'created'=>date('Y-m-d H:i:s'),
			'created_by'=>$this->session->userdata('personnel_id'),
			'modified_by'=>$this->session->userdata('personnel_id')
		);
		
		if($this->db->insert('individual_loan', $data))
		{
			return $this->db->insert_id();
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Deactivate a individual plan
	*	@param int $individual_loan_id
	*
	*/
	public function deactivate_individual_loan($individual_loan_id)
	{
		$data = array(
				'individual_loan_status' => 0
			);
		$this->db->where('individual_loan_id', $individual_loan_id);
		

		if($this->db->update('individual_loan', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Add a new individual
	*	@param string $image_name
	*
	*/
	public function loan_application($individual_id) 
	{
		$loans_plan_id = $this->input->post('loans_plan_id');
		
		//get loan details
		$this->db->select('loans_plan.interest_id, loans_plan.interest_rate, installment_type.installment_type_duration');
		$this->db->where('loans_plan_id', $loans_plan_id);
		$this->db->join('installment_type', 'installment_type.installment_type_id = loans_plan.loans_plan_id', 'left');
		$query = $this->db->get('loans_plan');
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$interest_id = $row->interest_id;
			$interest_rate = $row->interest_rate;
			$installment_type_duration = $row->installment_type_duration;
		}
		
		else
		{
			$interest_id = 0;
			$installment_type_duration = 0;
			$interest_rate = 0;
		}
		$processing_fee_type = $this->input->post('processing_fee_type');
		if($processing_fee_type == 1)
		{
			$processing_fee = 500;
		}
		else
		{
			$processing_fee = $this->get_processing_fee_amount($this->input->post('proposed_amount'));
		}
		$individual_loan_number = $this->create_loan_number();
		$data = array(
			'interest_rate'=>$interest_rate,
			'individual_id'=>$individual_id,
			'loans_plan_id'=>$loans_plan_id,
			'individual_loan_number'=>$individual_loan_number,
			'installment_type_duration'=>$installment_type_duration,
			'individual_salary'=>$this->input->post('individual_salary'),
			'proposed_amount'=>$this->input->post('proposed_amount'),
			'processing_fee_type'=>$this->input->post('processing_fee_type'),
			'processing_fee'=>$processing_fee,
			'no_of_repayments'=>$this->input->post('no_of_repayments'),
			'grace_period'=>$this->input->post('grace_period'),
			'purpose'=>$this->input->post('purpose'),
			'application_date'=>$this->input->post('application_date'),
			'created'=>date('Y-m-d H:i:s'),
			'created_by'=>$this->session->userdata('personnel_id'),
			'modified_by'=>$this->session->userdata('personnel_id')
		);
		
		if($this->db->insert('individual_loan', $data))
		{
			$individual_loan_id = $this->db->insert_id();
			//save loan ammortization
			$no_of_repayments = $this->input->post('no_of_repayments');
			$start_balance = $loan_amount = $this->input->post('proposed_amount');
			
			for($p = 0; $p < $no_of_repayments; $p++)
			{
				$count = $p + 1;
				//straight line
				if($interest_id == 1)
				{
					//$interest_payment = ($loan_amount * ($interest_rate/100)) / $no_of_repayments;
					$interest_payment = ($loan_amount * ($interest_rate/100));
				}
				
				//reducing balance
				else
				{
					//$interest_payment = ($start_balance * ($interest_rate/100)) / $no_of_repayments;
					$interest_payment = ($start_balance * ($interest_rate/100));
				}
				$principal_payment = round(($loan_amount / $no_of_repayments),-3);
				
				if ($count == $no_of_repayments)
				{
					$principal_payment = $start_balance;
				}
				$end_balance = $start_balance - $principal_payment;
				$this->payments_model->update_amortization_table($count,$interest_payment,$principal_payment,$individual_loan_id);
				$start_balance -= $principal_payment;
			}
			return $individual_loan_id;
		}
		else{
			return FALSE;
		}
	}
	public function check_if_has_loan_balance($individual_id)
	{
		$loan_rs = $this->get_individual_loan($individual_loan_id);
		$row = $loan_rs->row();
		$individual_loan_id = $row->individual_loan_id;
		$proposed_amount = $row->proposed_amount;
		$approved_amount = $row->approved_amount;
		$disbursed_amount = $row->disbursed_amount;
		$purpose = $row->purpose;
		$installment_type_duration = $row->installment_type_duration;
		$no_of_repayments = $row->no_of_repayments;
		$interest_rate = $row->interest_rate;
		$interest_id = $row->interest_id;
		$grace_period = $row->grace_period;
		$individual_loan_status = $row->individual_loan_status;

		$loan_amount = $proposed_amount;
		$total_interest = 0;
		$start_balance = $loan_amount;
		$first_date = $row->application_date;

		for($r = 0; $r < $no_of_repayments; $r++)
		{
			$total_days = $installment_type_duration * $r;
			$count = $r+1;
			$payment_date = date('jS M Y', strtotime($first_date. ' + '.$total_days.' days'));
			
			//straight line
			if($interest_id == 1)
			{
				$total_interest += ($loan_amount * ($interest_rate/100));
			}
			
			//reducing balance
			else
			{
				$total_interest += ($start_balance * ($interest_rate/100));
			}
			$principal_payment = $loan_amount / $no_of_repayments;
			$start_balance -= $principal_payment;
		}

		return number_format((($total_interest + $loan_amount) - ($total_payment_interest + $total_payment_amount)), 0);
	}
	public function edit_loan_application($individual_loan_id)
	{
		$loans_plan_id = $this->input->post('loans_plan_id');
		
		//get loan details
		$this->db->select('loans_plan.interest_id, loans_plan.interest_rate, installment_type.installment_type_duration');
		$this->db->where('loans_plan_id', $loans_plan_id);
		$this->db->join('installment_type', 'installment_type.installment_type_id = loans_plan.loans_plan_id', 'left');
		$query = $this->db->get('loans_plan');
		
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$interest_id = $row->interest_id;
			$interest_rate = $row->interest_rate;
			$installment_type_duration = $row->installment_type_duration;
		}
		
		else
		{
			$interest_id = 0;
			$installment_type_duration = 0;
			$interest_rate = 0;
		}
		$processing_fee_type = $this->input->post('processing_fee_type');
		if($processing_fee_type == 1)
		{
			$processing_fee = 500;
		}
		else
		{
			$processing_fee = $this->get_processing_fee_amount($this->input->post('proposed_amount'));
		}
		$data = array(
			'interest_rate'=>$interest_rate,
			'individual_id'=>$individual_id,
			'loans_plan_id'=>$loans_plan_id,
			'interest_rate'=>$interest_rate,
			'interest_rate'=>$interest_rate,
			'installment_type_duration'=>$installment_type_duration,
			'individual_salary'=>$this->input->post('individual_salary'),
			'proposed_amount'=>$this->input->post('proposed_amount'),
			'processing_fee_type'=>$this->input->post('processing_fee_type'),
			'processing_fee'=>$processing_fee,
			'no_of_repayments'=>$this->input->post('no_of_repayments'),
			'grace_period'=>$this->input->post('grace_period'),
			'purpose'=>$this->input->post('purpose'),
			'application_date'=>$this->input->post('application_date'),
			'modified_by'=>$this->session->userdata('personnel_id')
		);
		$this->db->where('individual_loan_id',$individual_loan_id);
		if($this->db->update('individual_loan', $data))
		{
			return $this->db->insert_id();
		}
		else{
			return FALSE;
		}
	}
	public function get_processing_fee_amount($proposed_amount)
	{
		$where = '(processing_fee_from <= '.$proposed_amount.' AND processing_fee_to >= '.$proposed_amount.')';
		$this->db->from('processing_fee');
		$this->db->select('processing_fee_amount AS amount');
		$this->db->where($where);
		$query = $this->db->get();
		$rs = $query->result();

		if(!empty($rs[0]->amount))
		{
			$amount = $rs[0]->amount;
		}
		else
		{
			$amount = 0;
		}
		// var_dump($where); die();
		return $amount;
	}
	public function get_guarantors($individual_loan_id)
	{
		$this->db->from('individual, loan_guarantor');
		$this->db->select('individual.individual_fname, individual.individual_mname, individual.individual_lname, loan_guarantor.loan_guarantor_id, loan_guarantor.created_by, loan_guarantor.created, loan_guarantor.guaranteed_amount, loan_guarantor.individual_id, personnel.personnel_fname, personnel.personnel_onames, loan_guarantor.individual_loan_id');
		$this->db->order_by('individual_fname', 'DESC');
		$this->db->join('personnel', 'personnel.personnel_id = loan_guarantor.created_by', 'left');
		$this->db->where('loan_guarantor.loan_guarantor_delete <> 1 AND individual.individual_id = loan_guarantor.individual_id AND loan_guarantor.individual_loan_id = '.$individual_loan_id);
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	Add a new individual
	*	@param string $image_name
	*
	*/
	public function add_guarantors($individual_loan_id) 
	{
		$data = array(
			'individual_loan_id'=>$individual_loan_id,
			'individual_id'=>$this->input->post('individual_id'),
			'guaranteed_amount'=>$this->input->post('guaranteed_amount'),
			'created'=>date('Y-m-d H:i:s'),
			'created_by'=>$this->session->userdata('personnel_id'),
			'modified_by'=>$this->session->userdata('personnel_id')
		);
		
		if($this->db->insert('loan_guarantor', $data))
		{
			return $this->db->insert_id();
		}
		else{
			return FALSE;
		}
	}
	
	public function delete_loan_guarantor($loan_guarantor_id)
	{
		$data = array(
			'loan_guarantor_delete'=>1,
			'deleted_on'=>date('Y-m-d H:i:s'),
			'deleted_by'=>$this->session->userdata('personnel_id')
		);
		
		$this->db->where('loan_guarantor_id', $loan_guarantor_id);
		if($this->db->update('loan_guarantor', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function get_loan_details($individual_loan_id)
	{
		$where = 'individual_loan.individual_loan_id = '.$individual_loan_id;
		$this->db->select('loans_plan.*');
		$this->db->where($where);
		$query = $this->db->get('loans_plan, individual_loan');
		
		// var_dump($query);
		return $query;
	}
	
	public function get_loan_payments($individual_id)
	{
		$this->db->from('loan_payment');
		$this->db->select('loan_payment.*, personnel.personnel_fname, personnel.personnel_onames');
		$this->db->order_by('loan_payment.payment_date', 'ASC');
		$this->db->join('personnel', 'personnel.personnel_id = loan_payment.created_by', 'left');
		$this->db->where('loan_payment.loan_payment_delete <> 1 AND loan_payment.individual_id = '.$individual_id);
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	Add a new individual
	*	@param string $image_name
	*
	*/
	public function add_loan_payment($individual_id) 
	{
		$data = array(
			'individual_id'=>$individual_id,
			'payment_amount'=>$this->input->post('payment_amount'),
			'payment_interest'=>$this->input->post('payment_interest'),
			'payment_date'=>$this->input->post('payment_date'),
			'created'=>date('Y-m-d H:i:s'),
			'created_by'=>$this->session->userdata('personnel_id'),
			'modified_by'=>$this->session->userdata('personnel_id')
		);
		
		if($this->db->insert('loan_payment', $data))
		{
			return $this->db->insert_id();
		}
		else{
			return FALSE;
		}
	}
	
	public function delete_loan_payment($loan_payment_id)
	{
		$data = array(
			'loan_payment_delete'=>1,
			'deleted_on'=>date('Y-m-d H:i:s'),
			'deleted_by'=>$this->session->userdata('personnel_id')
		);
		
		$this->db->where('loan_payment_id', $loan_payment_id);
		if($this->db->update('loan_payment', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function get_individual_loan($individual_loan_id)
	{
		$this->db->from('individual_loan');
		$this->db->where('individual_loan.individual_loan_id = '.$individual_loan_id);
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	get a single individual's details
	*	@param int $individual_id
	*
	*/
	public function get_savings_plans()
	{
		//retrieve all users
		$this->db->from('savings_plan');
		$this->db->select('*');
		$this->db->where('savings_plan_delete = 0 AND savings_plan_status = 1');
		$this->db->order_by('savings_plan_name', 'DESC');
		$query = $this->db->get();
		
		return $query;
	}
	public function get_singular_savings_plans($savings_plan_id)
	{
		//retrieve all users
		$this->db->from('savings_plan');
		$this->db->select('*');
		$this->db->where('savings_plan_delete = 0 AND savings_plan_status = 1 AND savings_plan_id = '.$savings_plan_id);
		$this->db->order_by('savings_plan_name', 'DESC');
		$query = $this->db->get();
		
		return $query;
	}
	public function get_savings_plan_name($savings_plan_id)
	{
		//retrieve all users
		
		$this->db->select('savings_plan_name');
		$this->db->where('savings_plan_delete = 0 AND savings_plan_status = 1 AND savings_plan_id ='.$savings_plan_id);
		$this->db->order_by('savings_plan_name', 'DESC');
		$query = $this->db->get('savings_plan');
		
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $db_sp_name)
			{
				$sp_name = $db_sp_name->savings_plan_name;
			} 
		}
		return $sp_name; 
	}
	
	public function get_savings_withdrawals($individual_id)
	{
		//retrieve all users
		$this->db->from('savings_payment');
		$this->db->select('*');
		$this->db->where('savings_payment.individual_savings_id = '.$individual_id.' AND savings_payment.savings_payment_delete = 0 AND payment_type = 1');
		$this->db->order_by('payment_date', 'DESC');
		$query = $this->db->get();
		
		return $query;
	}
	
	public function get_savings_payments($individual_id)
	{
		//retrieve all users
		$this->db->from('savings_payment');
		$this->db->select('*');
		$this->db->where('savings_payment.individual_savings_id = '.$individual_id.' AND savings_payment.savings_payment_delete = 0 AND savings_payment.payment_type= 0');
		$this->db->order_by('payment_date', 'ASC');
		$query = $this->db->get();
		
		return $query;
	}
	public function get_all_savings_payments($individual_id)
	{
		//retrieve all users
		$this->db->from('savings_payment');
		$this->db->select('*');
		$this->db->where('savings_payment.individual_savings_id = '.$individual_id.' AND savings_payment.savings_payment_delete = 0');
		$this->db->order_by('payment_date', 'ASC');
		$query = $this->db->get();
		
		return $query;
	}
	
	public function get_individual_types()
	{
		//retrieve all users
		$this->db->from('individual_type');
		$this->db->select('*');
		$this->db->order_by('individual_type_name', 'ASC');
		$query = $this->db->get();
		
		return $query;
	}
	public function create_loan_number()
	{
		//select product code
		$this->db->from('individual_loan');
		$this->db->where("individual_loan_number LIKE '".$this->session->userdata('branch_code')."".date('y')."-%'");
		$this->db->select('MAX(individual_loan_number) AS number');
		$query = $this->db->get();
		
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$number++;//go to the next number
			
			if($number == 1){
				$number = "".$this->session->userdata('branch_code')."".date('y')."-0001";
			}
		}
		else{//start generating receipt numbers
			$number = "".$this->session->userdata('branch_code')."".date('y')."-0001";
		}
		
		return $number;
	}
	public function get_current_employement_date($individual_id)
	{
		$this->db->from('individual_job');
		$this->db->select('employment_date');
		$this->db->where('individual_id = '.$individual_id);
		$this->db->order_by('individual_job_id', 'DESC');
		$query = $this->db->get();
		return $query;
	}
	
	
	public function get_loan_amount($individual_loan_id)
	{
		$proposed_loan_amount  = 0;
		$this->db->select('proposed_amount');
		$this->db->where('individual_loan_id = '.$individual_loan_id);
		$query = $this->db->get('individual_loan');
		
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $loan_amount)
			{
				$proposed_loan_amount = $loan_amount->proposed_amount;
			} 
		}
		return $proposed_loan_amount; 
	}
	
	public function get_no_loans_guaranteed($individual_id)
	{
		$no_loans_guaranteed  = 0;
		$this->db->select('count(loan_guarantor.loan_guarantor_id) as loans_guaranteed');
		$this->db->where('loan_guarantor.loan_guarantor_delete = 0 AND loan_guarantor.individual_id = '.$individual_id);
		$query = $this->db->get('loan_guarantor');
		
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $loans_guaranteed)
			{
				$no_loans_guaranteed = $loans_guaranteed->loans_guaranteed;
			} 
		}
		return $no_loans_guaranteed;
	}
	
	public function get_total_sum_guaranteed($individual_id)
	{
		$total_sum_guaranteed  = 0;
		$this->db->select('sum(guaranteed_amount) as sum_guaranteed');
		$this->db->where('loan_guarantor_delete = 0 AND individual_id = '.$individual_id);
		$query = $this->db->get('loan_guarantor');
		
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $sums_guaranteed)
			{
				$total_sum_guaranteed = $sums_guaranteed->sum_guaranteed;
			} 
		}
		return $total_sum_guaranteed;
	}
	
	public function get_guaranteed_loans($individual_id)
	{
		$this->db->from('loan_guarantor, individual_loan, individual');
		$this->db->select('individual_loan.*, individual.*, loan_guarantor.guaranteed_amount');
		$this->db->where('individual_loan.individual_loan_id = loan_guarantor.individual_loan_id AND individual.individual_id = individual_loan.individual_id AND loan_guarantor.individual_id = '.$individual_id);
		$query = $this->db->get();
		
		return $query;
	}
	
	public function get_disbursement_types()
	{
		$this->db->from('disbursement_type');
		$this->db->select('*');
		$query = $this->db->get();
		
		return $query;
	}
	public function get_individual_disbursements($individual_id)
	{
		$this->db->from('disbursement');
		$this->db->select('disbursement.cheque_amount, disbursement.dibursement_date');
		//$this->db->join('loans_plan', 'loans_plan.loans_plan_id = individual_loan.loans_plan_id', 'left');
		//$this->db->order_by('disbursed_date', 'ASC');
		$this->db->where('disbursement.individual_id = '.$individual_id);
		$query = $this->db->get();
		
		return $query;
	}
	public function disburse_individual_loan($individual_loan_id,$proposed_amount)
	{
		$data = array(
				'individual_loan_status' => 2,
				'disbursed_amount'=>$proposed_amount,
				'disbursed_date'=>date('Y-m-d'),
				'disbursed_by'=>$this->session->set_userdata('personnel_id')
			);
		$this->db->where('individual_loan_id', $individual_loan_id);
		if($this->db->update('individual_loan', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	//uncleared loan to which disbursement is made to 
	public function get_uncleared_loan($individual_id)
	{
		$loan_id = 0;
		$this->db->select('individual_loan_id');
		$this->db->where('individual_loan_cleared = 0 and individual_id = '.$individual_id);
		$this->db->order_by('individual_loan_id','DESC');
		$this->db->limit(1);
		$query = $this->db->get('individual_loan');
		
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $individual_loans)
			{
				$loan_id = $individual_loans->individual_loan_id;
			}
			
			return $loan_id;
		}
		else
		{
			return $loan_id;
		}
	}
	
	/*
	*	Activate a individual plan
	*	@param int $individual_savings_id
	*
	*/
	public function activate_individual_loan($individual_loan_id)
	{
		$data = array(
				'individual_loan_status' => 1
			);
		$this->db->where('individual_loan_id', $individual_loan_id);
		if($this->db->update('individual_loan', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function check_if_loan_guaranteed($individual_loan_id)
	{
		//echo $individual_loan_id;die();
		$this->db->select('COUNT(loan_guarantor_id) as no_of_guarantors');
		$this->db->where('loan_guarantor_delete = 0 AND individual_loan_id = '.$individual_loan_id);
		$guarantors = $this->db->get('loan_guarantor');
		$t_guarantors = $guarantors->row();
		$total_guarantors = $t_guarantors->no_of_guarantors;//var_dump($total_guarantors);die();
		if($total_guarantors >= 3)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function get_total_guaranteed_amount($individual_loan_id)
	{
		//get total_guaranteed amount for the loan
		$this->db->select('SUM(guaranteed_amount) as total_guaranteed_amount');
		$this->db->where('loan_guarantor_delete = 0 AND individual_loan_id = '.$individual_loan_id);
		$amount = $this->db->get('loan_guarantor');
		$t_amount = $amount->row();
		$guaranteed_amount = $t_amount->total_guaranteed_amount;
		//get total repayment_amount for the loan (principal + interest)
		$this->db->select('proposed_amount,no_of_repayments,interest_rate,interest_id');
		$this->db->where('individual_loan_id = '.$individual_loan_id);
		$query = $this->db->get('individual_loan');
		$query_result = $query->row();
		$application_amount = $query_result->proposed_amount;
		$no_of_repayments = $query_result->no_of_repayments;
		$interest_rate = $query_result->interest_rate;
		$interest_id = $query_result->interest_id;
		$cummulative_interest = $cummulative_principal= 0;
		$start_balance = $application_amount;
		$cummulative_interest = $cummulative_principal = 0;

		$loan_ammortization = $this->individual_model->get_loan_ammortization($individual_loan_id);
		if($loan_ammortization->num_rows() > 0)
		{
			foreach($loan_ammortization->result() as $res)
			{
				$interest_payment = $res->interest_amount;
				$principal_payment = $res->principal_amount;
				$end_balance = $start_balance - $principal_payment;
				$cummulative_interest += $interest_payment;
				$cummulative_principal += $principal_payment;
			}
		}
		$total_repayment = $cummulative_interest + $cummulative_principal;//var_dump($total_repayment);die();
		if($guaranteed_amount >= $total_repayment)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function check_if_loan_approved($individual_loan_id)
	{
		$this->db->select('individual_loan_status');
		$this->db->where('individual_loan_id = '.$individual_loan_id);
		$loan = $this->db->get('individual_loan');
		$loan_status_id = $loan->row();
		$individual_loan_status = $loan_status_id->individual_loan_status;//var_dump($individual_loan_status);die();
		//if loan approved, then the status is 1
		if($individual_loan_status == 1)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function check_if_processing_fee_paid($individual_loan_id)
	{
		$this->db->select('individual_processing_fee_status,processing_amount');
		$this->db->where('individual_loan_id = '.$individual_loan_id);
		$loan_processing_fee = $this->db->get('individual_loan_processing_fee');
		$processing = $loan_processing_fee->row();
		$individual_processing_fee_status = $processing->individual_processing_fee_status;
		$processing_amount = $processing->processing_amount;//echo($individual_processing_fee_status.'-'.$processing_amount);die();
		//if procesing fee has been paid
		if(($individual_processing_fee_status == 1)&&($processing_amount > 0))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
		
	}
	public function add_processing_fee_payment($individual_loan_id)
	{
		$individual_loan_processing_fee = array(
						'transaction_code'=>$this->input->post('transaction_code'),
						'payment_date'=>$this->input->post('payment_date'),
						'payment_method_id'=>$this->input->post('payment_method_id'),
						'processing_amount'=>$this->input->post('fee_amount_paid'),
						'individual_loan_id'=>$individual_loan_id
						);
		if($this->db->insert('individual_loan_processing_fee', $individual_loan_processing_fee))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function get_all_saving_plans($order, $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from('saving_plan');
		$this->db->select('*');
		$this->db->where('');
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	
	public function get_loan_ammortization($individual_loan_id)
	{
		$this->db->where('individual_loan_id', $individual_loan_id);
		return $this->db->get('amortization');
	}
	public function get_phone_contact($mpesa_contact_id){
		$total_batch='0';
		$this->db->select('individual_phone');
		$this->db->where('individual_id = '.$mpesa_contact_id);
		$query = $this->db->get('individual');
		$total = $query->row();
		$total_batch = $total->individual_phone;

		return $total_batch;
	}
	public function get_phone_contact_name($mpesa_contact_id){
		$total_batch='0';
		$this->db->select('individual_fname');
		$this->db->where('individual_id = '.$mpesa_contact_id);
		$query = $this->db->get('individual');
		$total = $query->row();
		$total_batch = $total->individual_fname;

		return $total_batch;
	}
	public function clean_phone_number($phone_number)
	{
		//remove forward slash
		$numbers = explode("/",$phone_number);
		$phone_number = $numbers[0];

		//remove hyphens
		$phone_number = str_replace("-","",$phone_number);

		//remove spaces
		$phone_number = str_replace(" ","",$phone_number);

		if (substr($phone_number, 0, 1) === '0') 
		{
			$phone_number = ltrim($phone_number, '0');
		}
		
		if (substr($phone_number, 0, 3) === '254') 
		{
			$phone_number = ltrim($phone_number, '254');
		}
		
	
		return $phone_number;
	}
	public function send_mpesa($phone, $contact_name)
	{

        // This will override any configuration parameters set on the config file
		// max of 160 characters
		// to get a unique name make payment of 8700 to Africastalking/SMSLeopard
		// unique name should have a maximum of 11 characters
		//checks if first digit is 0.....
		
		$phone_number = '+'.$phone;
		// Africas Talking SMS KEY and Variables
		$username   = "robagits";
        $apikey     = "ff6488d8bac93f8608200b71a0dc5fd29d65f31c960892238ca7cdb94ac3afe4";
        $productName  = "Simplex Financials";



		// The 3-Letter ISO currency code for the checkout amount
		$currencyCode = "KES";

	    // var_dump($actual_message); die();
		// get the current branch code
		$params = array('username' => $username, 'apikey' => $apikey);  

		$this->load->library('mpesagateway', $params);
				// Provide the details of a mobile money recipient
		$recipient1   = array("phoneNumber" => $phone_number,
		                       "currencyCode" => "KES",
							   "amount"       => 10.50,
							   "metadata"     => array("name"   => $contact_name,
							                           "reason" => "Jan Test Salary")
				      );
		//print_r($recipient1); die();
		
		// Put the recipients into an array
		$recipients  = array($recipient1);
		try 
		{
			//$results = $this->africastalkinggateway->sendMessage($phone_number, $actual_message, $sms_from=22384);
			$results = $this->mpesagateway->mobilePaymentB2CRequest($productName, $recipients);

			
			//var_dump($results);die();
			foreach($responses as $response) {
		    // Parse the responses and print them out
		    echo "phoneNumber=".$response->phoneNumber;
		    echo ";status=".$response->status;
			
		    if ($response->status == "Queued") {
		      echo ";transactionId=".$response->transactionId;
		      echo ";provider=".$response->provider;
		      echo ";providerChannel=".$response->providerChannel;
		      echo ";value=".$response->value;
		      echo ";transactionFee=".$response->transactionFee."\n";
		    } else {
		      echo ";errorMessage=".$response->errorMessage."\n";
		    }
		  }
			return  json_encode($result->status);

		}
		
		catch(AfricasTalkingGatewayException $e)
		{
			// echo "Encountered an error while sending: ".$e->getMessage();
			return $e->getMessage();
		}
        
	    
	}
	public function all_savings_plan()
	{
		$this->db->where('savings_plan_status = 1');
		$query = $this->db->get('savings_plan');
		
		return $query;
	}
	function add_savings_plan($individual_id)
	{
		$data = array(
			'savings_plan_id'=> $this->input->post('savings_plan_id'),
			'individual_savings_amount'=> $this->input->post('starting_amount'),
			'individual_savings_status'=> 1,
			'created_by'=> $this->session->userdata('personnel_id'),
			'modified_by'=> $this->session->userdata('personnel_id'),
			'created'=> date('Y-m-d H:i:s'),
			'individual_id'=>$individual_id
		);
		
		if($this->db->insert('individual_savings', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	function add_member_savings_plan($individual_id)
	{
		$data = array(
			'savings_plan_id'=> $this->input->post('savings_plan_id'),
			'individual_savings_amount'=> $this->input->post('start_amount'),
			'start_date'=> $this->input->post('start_date'),
			'individual_savings_status'=> 1,
			'created_by'=> $this->session->userdata('personnel_id'),
			'modified_by'=> $this->session->userdata('personnel_id'),
			'created'=> date('Y-m-d H:i:s'),
			'individual_id'=>$individual_id
		);
		
		if($this->db->insert('individual_savings', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function get_saving_plan_payments($individual_savings_id,$individual_id)
	{

		$this->db->where('individual_id = '.$individual_id.' AND individual_savings_id = '.$individual_savings_id.' AND payment_status = 1 AND cancel = 0');
		
		return $this->db->get('savings_plan_payments');
	}
	public function get_saving_plan_withdrawals($individual_savings_id,$individual_id)
	{
		$this->db->where('individual_id = '.$individual_id.' AND individual_savings_id = '.$individual_savings_id.' AND payment_status = 1 AND cancel = 0');
		
		return $this->db->get('savings_plan_withdrawals');
	}
	public function get_saving_plan_dividends($individual_savings_id,$individual_id)
	{
		$this->db->where('individual_id = '.$individual_id.' AND individual_savings_id = '.$individual_savings_id.' AND payment_status = 1 AND cancel = 0');
		
		return $this->db->get('savings_plan_dividends');
	}
	public function get_saving_plan_pardons($individual_savings_id)
	{
		$this->db->where('individual_savings_id = '.$individual_savings_id.' AND pardon_status = 1 AND cancel = 0');
		
		return $this->db->get('savings_plan_pardons');
	}
	public function get_individual_savings_plans_details($individual_savings_id)
	{
		$this->db->where('individual_savings.individual_savings_id = '.$individual_savings_id.' AND individual_savings.individual_savings_status = 1 AND individual_savings.cancel = 0 AND individual.individual_id = individual_savings.individual_id');
		
		return $this->db->get('individual_savings,individual');
	}
	public function get_payment_methods()
	{
		$this->db->where('payment_method_id > 0');
		return $this->db->get('payment_method');
	}
	public function get_cancel_actions()
	{
		$this->db->where('cancel_action_status', 1);
		$this->db->order_by('cancel_action_name');
		
		return $this->db->get('cancel_action');
	}
	public function get_all_saving_plan_invoices($table, $where, $per_page, $page, $order, $order_method)
	{
		//retrieve all tenants
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$this->db->group_by('invoice.invoice_month,invoice.invoice_year');
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function get_all_invoice_month($individual_savings_id,$type= null)
	{
		// var_dump($individual_savings_id); die();
		if(!empty($type))
		{
			if($type == 1)
			{
				$add_where = ' AND invoice_type <> 2';
			}
			else
			{
				$add_where = ' AND invoice_type = 2';
			}
		}
		else
		{
			$add_where ='';
		}
		$this->db->from('savings_plan_invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice,invoice_month,invoice_year,invoice_date');
		$this->db->where('individual_savings_id = '.$individual_savings_id.' AND invoice_date <> "0000-00-00" '.$add_where);
		$this->db->order_by('invoice_date','ASC');
		$this->db->group_by('invoice_month,invoice_year');
		$query = $this->db->get();
		return $query;
	}
	public function get_all_withdrawals_month($individual_savings_id,$type= null)
	{
		// var_dump($individual_savings_id); die();
		
		$this->db->from('savings_withdrawal_item');
		$this->db->select('SUM(amount_paid) AS total_invoice,payment_month,payment_year,payment_item_created');
		$this->db->where('individual_savings_id = '.$individual_savings_id.' AND payment_item_created <> "0000-00-00" ');
		$this->db->order_by('payment_item_created','ASC');
		$this->db->group_by('payment_month,payment_year');
		$query = $this->db->get();
		return $query;
	}
	public function get_all_payments_savings($individual_savings_id,$type)
	{
		// var_dump($individual_savings_id); die();
		if(!empty($type))
		{
			if($type == 1)
			{
				$add_where = ' AND savings_payment_item.invoice_type_id <> 2';
			}
			else
			{
				$add_where = ' AND savings_payment_item.invoice_type_id = 2';
			}
		}
		else
		{
			$add_where ='';
		}

		$this->db->from('savings_plan_payments,savings_payment_item');
		$this->db->select('savings_plan_payments.*');
		$this->db->where('savings_plan_payments.individual_savings_id = '.$individual_savings_id.'  AND savings_plan_payments.payment_status = 1 AND cancel = 0 AND savings_plan_payments.payment_id = savings_payment_item.payment_id '.$add_where);
		$this->db->order_by('savings_plan_payments.payment_date','ASC');
		$this->db->group_by('savings_plan_payments.payment_id','ASC');
		$query = $this->db->get();
		return $query;
	}
	public function get_all_payments_withdrawals($individual_savings_id,$type)
	{
		
		$this->db->from('savings_plan_withdrawals,savings_withdrawal_item');
		$this->db->select('savings_plan_withdrawals.*');
		$this->db->where('savings_plan_withdrawals.individual_savings_id = '.$individual_savings_id.'  AND savings_plan_withdrawals.payment_status = 1 AND cancel = 0 AND savings_plan_withdrawals.payment_id = savings_withdrawal_item.payment_id ');
		$this->db->order_by('savings_plan_withdrawals.payment_date','ASC');
		$this->db->group_by('savings_plan_withdrawals.payment_id','ASC');
		$query = $this->db->get();
		return $query;
	}
	public function get_all_pardon_savings($individual_savings_id)
	{
		$this->db->from('savings_plan_pardons');
		$this->db->select('*');
		$this->db->where('individual_savings_id = '.$individual_savings_id.'  AND pardon_status = 1 AND pardon_delete = 0');
		$this->db->order_by('pardon_date','ASC');
		$query = $this->db->get();
		return $query;
	}
	public function get_individual_saving_statement($individual_savings_id)
	{
		$bills = $this->member_savings_model->get_all_withdrawals_month($individual_savings_id,1);
		$payments = $this->member_savings_model->get_all_payments_savings($individual_savings_id,1);
		$pardons = $this->member_savings_model->get_all_payments_withdrawals($individual_savings_id,1);

		// var_dump($payments); die();
		$x=0;

		$bills_result = '';
		$last_date = '';
		$current_year = date('Y');
		$total_invoices = $bills->num_rows();
		$invoices_count = 0;
		$total_invoice_balance = 0;
		$total_arrears = 0;
		$total_payment_amount = 0;
		$result = '';
		$total_pardon_amount = 0;
		if($bills->num_rows() > 0)
		{
			foreach ($bills->result() as $key_bills) {
				# code...
				$invoice_month = $key_bills->payment_month;
			    $invoice_year = $key_bills->payment_year;
				$invoice_date = $key_bills->payment_item_created;
				$invoice_amount = $key_bills->total_invoice;
				$invoices_count++;
				if($payments->num_rows() > 0)
				{
					foreach ($payments->result() as $payments_key) {
						# code...
						$payment_date = $payments_key->payment_date;
						$payment_year = $payments_key->year;
						$payment_month = $payments_key->month;
						$payment_amount = $payments_key->amount_paid;

						if(($payment_date <= $invoice_date) && ($payment_date > $last_date) && ($payment_amount > 0))
						{
							$total_arrears -= $payment_amount;
							// var_dump($payment_year); die();
							// if($payment_year >= $current_year)
							// {
								$result .= 
								'
									<tr>
										<td>'.date('d M Y',strtotime($payment_date)).' </td>
										<td>Payment</td>
										<td></td>
										<td>'.number_format($payment_amount, 2).'</td>
										<td>'.number_format($total_arrears, 2).'</td>
										<td></td>
									</tr> 
								';
							// }
							
							$total_payment_amount += $payment_amount;

						}
					}
				}
				
				//display disbursment if cheque amount > 0
				if($invoice_amount != 0)
				{
					$total_arrears += $invoice_amount;
					$total_invoice_balance += $invoice_amount;
						
					// if($invoice_year >= $current_year)
					// {
						$result .= 
						'
							<tr>
								<td>'.date('d M Y',strtotime($invoice_date)).' </td>
								<td>'.$invoice_month.' '.$invoice_year.' Invoice</td>
								<td>'.number_format($invoice_amount, 2).'</td>
								<td></td>
								<td>'.number_format($total_arrears, 2).'</td>
								<td><a href="'.site_url().'invoice/'.$individual_savings_id.'/'.$invoice_month.'/'.$invoice_year.'" target="_blank" class="btn btn-sm btn-warning">Invoice</a></td>
							</tr> 
						';
					// }
				}
						
				//check if there are any more payments
				if($total_invoices == $invoices_count)
				{
					//get all loan deductions before date
					if($payments->num_rows() > 0)
					{
						foreach ($payments->result() as $payments_key) {
							# code...
							$payment_date = $payments_key->payment_date;
							$payment_year = $payments_key->year;
							$payment_month = $payments_key->month;
							$payment_amount = $payments_key->amount_paid;

							if(($payment_date > $invoice_date) &&  ($payment_amount > 0))
							{
								$total_arrears -= $payment_amount;
								// if($payment_year >= $current_year)
								// {
									$result .= 
									'
										<tr>
											<td>'.date('d M Y',strtotime($payment_date)).' </td>
											<td>Payment</td>
											<td></td>
											<td>'.number_format($payment_amount, 2).'</td>
											<td>'.number_format($total_arrears, 2).'</td>
											<td></td>
										</tr> 
									';
								// }
								
								$total_payment_amount += $payment_amount;

							}
						}
					}

					
				}
						$last_date = $invoice_date;
			}
		}	
		else
		{
			//get all loan deductions before date
			if($payments->num_rows() > 0)
			{
				foreach ($payments->result() as $payments_key) {
					# code...
					$payment_date = $payments_key->payment_date;
					$payment_year = $payments_key->year;
					$payment_month = $payments_key->month;
					$payment_amount = $payments_key->amount_paid;

					if(($payment_amount > 0))
					{
						$total_arrears -= $payment_amount;
						// if($payment_year >= $current_year)
						// {
							$result .= 
							'
								<tr>
									<td>'.date('d M Y',strtotime($payment_date)).' </td>
									<td>Payment</td>
									<td></td>
									<td>'.number_format($payment_amount, 2).'</td>
									<td>'.number_format($total_arrears, 2).'</td>
									<td></td>
								</tr> 
							';
						// }
						
						$total_payment_amount += $payment_amount;

					}
				}
			}
			

		}
						
		//display loan
		$result .= 
		'
			<tr>
				<th colspan="2">Total</th>
				<th>'.number_format($total_invoice_balance, 2).'</th>
				<th>'.number_format($total_payment_amount, 2).'</th>
				<th>'.number_format($total_arrears, 2).'</th>
				<td></td>
			</tr> 
		';

		$invoice_date = $this->get_max_invoice_date($individual_savings_id);

		$account_invoice_amount = $this->member_savings_model->get_savings_plan_invoice_brought_forward($individual_savings_id,$invoice_date);
		$account_paid_amount = $this->member_savings_model->get_payment_savings_brought_forward($individual_savings_id,$invoice_date);
		$account_todays_payment =  $this->member_savings_model->get_todays_savings_current_payments($individual_savings_id,$invoice_date);
		$total_brought_forward_account = $account_invoice_amount - $account_paid_amount;
	


		$response['total_arrears'] = $total_arrears;
		$response['invoice_date'] = $invoice_date;
		$response['result'] = $result;
		$response['total_invoice_balance'] = $total_brought_forward_account;
		$response['total_payment_amount'] = $total_payment_amount;
		$response['total_pardon_amount'] = $total_pardon_amount;

		// var_dump($response); die();

		return $response;
	}
	public function get_max_invoice_date($individual_savings_id,$invoice_month = NULL,$invoice_year=NULL)
	{
		if($invoice_month != NULL || $invoice_year != NULL)
		{
			$add = 'AND invoice_month = "'.$invoice_month.'" AND invoice_year = '.$invoice_year.'';
		}
		else
		{
			$add = '';
		}
		$where = 'individual_savings_id = '.$individual_savings_id.' '.$add;

		$this->db->from('savings_plan_invoice');
		$this->db->select('MAX(invoice_date) AS invoice_date');
		$this->db->where($where);
		$query = $this->db->get();
		$invoice_date = '';
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$invoice_date = $row->invoice_date;
		}
		return $invoice_date;
	}
	public function get_savings_plan_invoice_brought_forward($individual_savings_id, $invoice_date=NULL,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND invoice_type = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}
		// $date = $invoice_year.'-'.$invoice_month.'-30';

		if(!empty($invoice_date))
		{

			$date_add = ' AND invoice_date < "'.$invoice_date.'"';	
					
		}
		else
		{
			$date_add = '';
		}

		$where = 'individual_savings_id = '.$individual_savings_id.' '.$date_add.' '.$add;
		// var_dump($where); die();

		
		$this->db->from('savings_plan_invoice');
		$this->db->select('SUM(invoice_amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;
	}
	public function get_payment_savings_brought_forward($individual_savings_id,$invoice_date=NULL,$invoice_type_id = NULL,$confirm =null)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND savings_plan_payments.payment_id = savings_payment_item.payment_id AND savings_payment_item.invoice_type_id = '.$invoice_type_id;
			$table_add = 'savings_plan_payments,savings_payment_item';
			$amount_cal = 'SUM(savings_payment_item.amount_paid) AS total_paid';
		}
		else
		{
			$add = ' AND savings_plan_payments.payment_status = 1 AND cancel = 0 ';
			$table_add = 'savings_plan_payments';
			$amount_cal = 'SUM(savings_plan_payments.amount_paid) AS total_paid';
		}

		if(!empty($invoice_date))
		{
			$date_add = '  AND (savings_plan_payments.payment_date < "'.$invoice_date.'")';
		}
		else
		{
			$date_add = '';
		}
		if($confirm == 1)
		{
			$status = 'AND savings_plan_payments.confirmation_status = 0';
		}
		else
		{
			$status = '';
		}
		

		$where = 'savings_plan_payments.individual_savings_id = '.$individual_savings_id.'   '.$date_add.' '.$add.' '.$status;

		

		$this->db->from($table_add);
		$this->db->select($amount_cal);
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}
	public function get_todays_savings_current_payments($individual_savings_id,$invoice_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND savings_payment_item.invoice_type_id = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}

		// $date = $invoice_year.'-'.$invoice_month.'-30';
		// $date = $invoice_year.'-08-30';
		// var_dump($date); die();
		$where = 'savings_plan_payments.individual_savings_id = '.$individual_savings_id.' AND savings_plan_payments.payment_id = savings_payment_item.payment_id AND savings_plan_payments.payment_status = 1 AND cancel = 0 AND (savings_plan_payments.payment_date = "'.$invoice_date.'")'.$add;

		// var_dump($where); die();

		$this->db->from('savings_payment_item,savings_plan_payments');
		$this->db->select('SUM(savings_payment_item.amount_paid) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}
	function check_transaction_code($transaction_code)
	{
		$this->db->where('transaction_code = "'.$transaction_code.'" AND payment_status <= 2 AND cancel = 0');
		$query = $this->db->get('savings_plan_payments');

		if($query->num_rows() > 0)
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	public function receipt_payment($individual_id, $individual_savings_id,$personnel_id = NULL){
		$payment_method_id = $this->input->post('payment_method_id');
		$total_amount_paid = $this->input->post('total_amount_paid');
		$net_amount_paid = $this->input->post('net_amount_paid');
		$legal_fee = $this->input->post('legal_fee');
		$payment_date = $this->input->post('payment_date');
		$amount_paid = $this->input->post('amount_paid');
		$paid_by = $this->input->post('paid_by');

		$receipt_number = $this->create_receipt_number();
		
		
		
		if($payment_method_id == 1)
		{
			$transaction_code = $this->input->post('bank_name');
		}
		else if($payment_method_id == 5)
		{
			//  check for mpesa code if inserted
			$transaction_code = $this->input->post('mpesa_code');
		}
		else
		{
			$transaction_code = '';
		}

		// calculate the points to get 
		$payment_date = $this->input->post('payment_date');


		// $this->get_points_to_award($individual_savings_id,$payment_date);
		// end of point calculation
		$date_check = explode('-', $payment_date);
		$month = $date_check[1];
		$year = $date_check[0];

		
		
		$data = array(
			'individual_id'=>$individual_id,
			'payment_method_id'=>$payment_method_id,
			'amount_paid'=>$total_amount_paid,
			'personnel_id'=>$this->session->userdata("personnel_id"),
			'transaction_code'=>$transaction_code,
			'payment_date'=>$this->input->post('payment_date'),
			'receipt_number'=>$receipt_number,
			'paid_by'=>$this->input->post('paid_by'),
			'payment_created'=>date("Y-m-d"),
			'year'=>$year,
			'month'=>$month,
			'confirm_number'=> $receipt_number,
			'payment_created_by'=>$this->session->userdata("personnel_id"),
			'approved_by'=>$personnel_id,'date_approved'=>date('Y-m-d')
		);

		$type_of_account = 1;

		if($type_of_account == 1)
		{
			$data['individual_savings_id'] = $individual_savings_id;

			if($this->db->insert('savings_plan_payments', $data))
			{

				$payment_id = $this->db->insert_id();

				// send email and sms for receipting
				// $this->send_receipt_notification($individual_savings_id,$confirm_number,$payment_id,$amount);

				$net_amount_paid = $this->input->post('net_amount_paid');
				$legal_fee = $this->input->post('legal_fee');


				$invoice_number = $this->get_invoice_number(); 

				if(!empty($net_amount_paid))
				{
					// insert for service charge
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $net_amount_paid,
										'invoice_type_id' => 1,
										'payment_item_status' => 1,
										'individual_savings_id' => $individual_savings_id,
										'individual_id' => $individual_id,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('savings_payment_item',$service);
				}

				if(!empty($legal_fee))
				{
					$service_two = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $legal_fee,
										'invoice_type_id' => 2,
										'payment_item_status' => 1,
										'individual_savings_id' => $individual_savings_id,
										'individual_id' => $individual_id,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('savings_payment_item',$service_two);
					// update the invoice table 
					// using invoice_type_id
					//$invoice_id = $this->get_last_invoice_id(2);

					

				}
				

				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		
		
	}
function create_receipt_number()
	{
		//select product code
		$preffix = "SF-SAV-PAY-";
		$this->db->from('savings_plan_payments');
		$this->db->where("receipt_number LIKE '".$preffix."%' AND payment_status = 1");
		$this->db->select('MAX(receipt_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();
		
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number);
			$real_number++;//go to the next number
			$number = $preffix.sprintf('%03d', $real_number);
		}
		else{//start generating receipt numbers
			$number = $preffix.sprintf('%03d', 1);
		}
		
		return $number;
	}
public function get_invoice_number()
	{
		//select product code
		$preffix = $this->session->userdata('branch_code');
		$this->db->from('savings_plan_invoice');
		$this->db->where("invoice_number LIKE '".$preffix."%'");
		$this->db->select('MAX(invoice_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();
		
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number);
			$real_number++;//go to the next number
			$number = $preffix.sprintf('%03d', $real_number);
		}
		else{//start generating receipt numbers
			$number = $preffix.sprintf('%03d', 1);
		}
		
		return $number;
	}
public function get_total_individual_savings_payments($individual_id)
{
		$number = '';
		$this->db->from('savings_plan_payments');
		$this->db->where('cancel = 0 AND individual_id = '.$individual_id);
		$this->db->select('SUM(amount_paid) AS amount');
		$query = $this->db->get();
		$total_paid = 0;
		//echo $query->num_rows(); die();
		
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
			$total_paid = $key->amount;
		   }
			
		}
   	if(empty($total_paid))
   	{
   		$total_paid =0;
   	}
		
		return $total_paid;
}
public function get_total_individual_savings_withdrawals($individual_id)
{
		$number = '';
		$this->db->from('savings_plan_withdrawals');
		$this->db->where('withdrawal_delete = 0 AND individual_id = '.$individual_id);
		$this->db->select('SUM(amount_paid) AS amount');
		$query = $this->db->get();
		$total_paid = 0;
		//echo $query->num_rows(); die();
		
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
			$total_paid = $key->amount;
		   }
			
		}
   	if(empty($total_paid))
   	{
   		$total_paid =0;
   	}
		
	return $total_paid;
}
public function get_total_individual_savings_dividends($individual_id)
{
		$number = '';
		$this->db->from('savings_plan_dividends');
		$this->db->where('dividend_delete = 0 AND individual_id = '.$individual_id);
		$this->db->select('SUM(amount_paid) AS amount');
		$query = $this->db->get();
		$total_paid = 0;
		//echo $query->num_rows(); die();
		
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
			$total_paid = $key->amount;
		   }
			
		}
   	if(empty($total_paid))
   	{
   		$total_paid =0;
   	}
		
	return $total_paid;
}
public function get_actual_individual_savings_payments($individual_savings_id)
{
		$number = '';
		$this->db->from('savings_plan_payments');
		$this->db->where('cancel = 0 AND individual_savings_id = '.$individual_savings_id);
		$this->db->select('SUM(amount_paid) AS amount');
		$query = $this->db->get();
		$total_paid = 0;
		//echo $query->num_rows(); die();
		
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
			$total_paid = $key->amount;
		   }
			
		}
   	if(empty($total_paid))
   	{
   		$total_paid =0;
   	}
		
		return $total_paid;
}
public function get_actual_individual_savings_withdrawals($individual_savings_id)
{
		$number = '';
		$this->db->from('savings_plan_withdrawals');
		$this->db->where('withdrawal_delete = 0 AND individual_savings_id = '.$individual_savings_id);
		$this->db->select('SUM(amount_paid) AS amount');
		$query = $this->db->get();
		$total_paid = 0;
		//echo $query->num_rows(); die();
		
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
			$total_paid = $key->amount;
		   }
			
		}
   	if(empty($total_paid))
   	{
   		$total_paid =0;
   	}
		
	return $total_paid;
}
public function get_actual_individual_savings_dividends($individual_savings_id)
{
		$number = '';
		$this->db->from('savings_plan_dividends');
		$this->db->where('dividend_delete = 0 AND individual_savings_id = '.$individual_savings_id);
		$this->db->select('SUM(amount_paid) AS amount');
		$query = $this->db->get();
		$total_paid = 0;
		//echo $query->num_rows(); die();
		
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key) {
			$total_paid = $key->amount;
		   }
			
		}
   	if(empty($total_paid))
   	{
   		$total_paid =0;
   	}
		
	return $total_paid;
}
public function receipt_withdrawals($individual_id, $individual_savings_id,$personnel_id = NULL){
		$total_withdrawal_amount = $this->input->post('total_withdrawal_amount');
		$withdrawal_reason = $this->input->post('withdrawal_reason');
		$withdrawal_date = $this->input->post('withdrawal_date');
		

		$payment_method_id = 1;
		$transaction_code = 'Cash';

		$withdrawal_number = $this->create_receipt_number();
		$paid_by = $this->get_individual_name($individual_id);
		
		

		// $this->get_points_to_award($individual_savings_id,$payment_date);
		// end of point calculation
		$date_check = explode('-', $withdrawal_date);
		$month = $date_check[1];
		$year = $date_check[0];
		$withdrawal_charge = $this->get_withdrawal_charge($individual_savings_id);
		if($withdrawal_charge>0)
		{
			$new_total_withdrawal_amount = $total_withdrawal_amount + $withdrawal_charge;
		}
		else
		{
			$new_total_withdrawal_amount = $total_withdrawal_amount;

		}
		
		$data = array(
			'individual_id'=>$individual_id,
			'payment_method_id'=>$payment_method_id,
			'amount_paid'=>$new_total_withdrawal_amount,
			'personnel_id'=>$this->session->userdata("personnel_id"),
			'transaction_code'=>$transaction_code,
			'payment_date'=>$this->input->post('withdrawal_date'),
			'receipt_number'=>$withdrawal_number,
			'paid_by'=>$paid_by,
			'payment_created'=>date("Y-m-d"),
			'year'=>$year,
			'month'=>$month,
			'confirm_number'=> $withdrawal_number,
			'payment_created_by'=>$this->session->userdata("personnel_id"),
			'approved_by'=>$personnel_id,'date_approved'=>date('Y-m-d')
		);

		$type_of_account = 1;

		if($type_of_account == 1)
		{
			$data['individual_savings_id'] = $individual_savings_id;

			if($this->db->insert('savings_plan_withdrawals', $data))
			{

				$payment_id = $this->db->insert_id();

				// send email and sms for receipting
				// $this->send_receipt_notification($individual_savings_id,$confirm_number,$payment_id,$amount);

				$total_withdrawal_amount = $this->input->post('total_withdrawal_amount');
				$paid_by = $this->get_individual_name($individual_id);
				
				


				$invoice_number = $this->get_invoice_number(); 

				if(!empty($total_withdrawal_amount))
				{
					// insert for service charge
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $total_withdrawal_amount,
										'invoice_type_id' => 1,
										'payment_item_status' => 1,
										'individual_savings_id' => $individual_savings_id,
										'individual_id' => $individual_id,
										'payment_month' => $month,
										'payment_year' => $year,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('savings_withdrawal_item',$service);
				}

				if($withdrawal_charge>0)
				{
					$service_two = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $withdrawal_charge,
										'invoice_type_id' => 2,
										'payment_item_status' => 1,
										'individual_savings_id' => $individual_savings_id,
										'individual_id' => $individual_id,
										'payment_month' => $month,
										'payment_year' => $year,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('savings_withdrawal_item',$service_two);
					// update the invoice table 
					// using invoice_type_id
					//$invoice_id = $this->get_last_invoice_id(2);

					

				}
				

				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		
		
	}
function create_withdrawal_number()
	{
		//select product code
		$preffix = "SF-SAV-WIT-";
		$this->db->from('savings_plan_withdrawals');
		$this->db->where("receipt_number LIKE '".$preffix."%' AND payment_status = 1");
		$this->db->select('MAX(receipt_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();
		
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number);
			$real_number++;//go to the next number
			$number = $preffix.sprintf('%03d', $real_number);
		}
		else{//start generating receipt numbers
			$number = $preffix.sprintf('%03d', 1);
		}
		
		return $number;
	}
	public function receipt_dividends($individual_id, $individual_savings_id,$personnel_id = NULL)
	{
		$dividend_amount = $this->input->post('dividend_amount');
		$dividend_date = $this->input->post('dividend_date');
		

		$payment_method_id = 1;
		$transaction_code = 'Cash';

		$withdrawal_number = $this->create_dividend_number();
		$paid_by = $this->get_individual_name($individual_id);
		
		

		// $this->get_points_to_award($individual_savings_id,$payment_date);
		// end of point calculation
		$date_check = explode('-', $dividend_date);
		$month = $date_check[1];
		$year = $date_check[0];
		$withdrawal_charge = $this->get_withdrawal_charge($individual_savings_id);
		if($withdrawal_charge>0)
		{
			$new_total_withdrawal_amount = $total_withdrawal_amount + $withdrawal_charge;
		}
		else
		{
			$new_total_withdrawal_amount = $total_withdrawal_amount;

		}
		
		$data = array(
			'individual_id'=>$individual_id,
			'payment_method_id'=>$payment_method_id,
			'amount_paid'=>$dividend_amount,
			'personnel_id'=>$this->session->userdata("personnel_id"),
			'transaction_code'=>$transaction_code,
			'payment_date'=>$dividend_date,
			'receipt_number'=>$withdrawal_number,
			'paid_by'=>$paid_by,
			'payment_created'=>date("Y-m-d"),
			'year'=>$year,
			'month'=>$month,
			'confirm_number'=> $withdrawal_number,
			'payment_created_by'=>$this->session->userdata("personnel_id"),
			'approved_by'=>$personnel_id,'date_approved'=>date('Y-m-d')
		);

		$type_of_account = 1;

		if($type_of_account == 1)
		{
			$data['individual_savings_id'] = $individual_savings_id;

			if($this->db->insert('savings_plan_dividends', $data))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		
		
	}
function create_dividend_number()
	{
		//select product code
		$preffix = "SF-SAV-DIV-";
		$this->db->from('savings_plan_dividends');
		$this->db->where("receipt_number LIKE '".$preffix."%' AND payment_status = 1");
		$this->db->select('MAX(receipt_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();
		
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number);
			$real_number++;//go to the next number
			$number = $preffix.sprintf('%03d', $real_number);
		}
		else{//start generating receipt numbers
			$number = $preffix.sprintf('%03d', 1);
		}
		
		return $number;
	}
}
?>