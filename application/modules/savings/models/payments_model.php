<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payments_model extends CI_Model {

	// public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}
	public function all_individual()
	{
		// $this->db->where('individual_status = 0');
		$query = $this->db->get('individual');
		
		// var_dump($query);
		return $query;
	}
	public function add_individual_payment($individual_id)
	{
		$data = array(
			'individual_id'  => $individual_id,
			'payment_amount' => $this->input->post('payment_amount'),
			'payment_date'   => $this->input->post('payment_date'),
			'created'      => date('Y-m-d H:i:s'),
			'created_by' => $this->session->userdata('personnel_id'), 
			'modified_by' => $this->session->userdata('personnel_id'), 
		);
		if($this->db->insert('savings_payment', $data))
		{
			$individual_loan_id = $this->db->insert_id();
			return $individual_loan_id;
		}
		else{
			return FALSE;
		}
	}
	public function add_loan_payment($individual_id)
	{
		//get the oldest loan for that indvidual 
		$individual_loan_id = $this->get_oldest_loan($individual_id);//var_dump($individual_loan_id);die();
		
		//get the payment is for which month (no of repayment)
		$repayment_no = $this->get_repayment_number($individual_loan_id);
		
		//select the amount expected for that repayment for that loan
		$expected_interest_return = $this->get_interest_expected($individual_loan_id, $repayment_no);
		
		$payment_amount = $this->input->post('payment_amount');
		$principal_payment = $this->calculate_principal_payment($payment_amount,$individual_loan_id,$expected_interest_return);
		$interest_payment = $expected_interest_return;
		$data = array(
			'individual_id'  => $individual_id,
			'payment_amount' => $principal_payment,
			'payment_interest' => $interest_payment,
			'payment_date'   => $this->input->post('payment_date'),
			'created'      => date('Y-m-d H:i:s'),
			'created_by' => $this->session->userdata('personnel_id'), 
			'modified_by' => $this->session->userdata('personnel_id'),
			'individual_loan_id' => $individual_loan_id 
		);
		if($this->db->insert('loan_payment', $data))
		{
			return $this->db->insert_id();
		}
		else{
			return FALSE;
		}
	}
	public function all_individual_payment($table, $where, $per_page, $page, $order = 'date_paid', $order_method = 'ASC')
	{
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function add_group_payment()
	{
		$data = array(
			'group_id'  => $this->input->post('customer_id'),
			'amount'         => $this->input->post('payment_amount'),
			'slip_number'    => $this->input->post('slip_number'),
			'date_paid'      => $this->input->post('date_paid'),
			'month_paid_for' => $this->input->post('payment_month'), 
		);
		if($this->db->insert('group_payments', $data))
		{
			return $this->db->insert_id();
		}
		else{
			return FALSE;
		}
	}
	public function get_individual_payment_details($id)
	{
		$this->db->from('individual_payments');
		$this->db->select('*');
		$this->db->where('id = '.$id);
		$query = $this->db->get();
		
		return $query;
	}
	public function edit_individual_payment()
	{
		$data = array(
			'id'=>$this->input->post('pid'),
			'individual_id'=>$this->input->post('customer_id'),
			'amount'=>$this->input->post('payment_amount'),
			'slip_number'=>$this->input->post('slip_number'),
			'date_paid'=>$this->input->post('date_paid'),
			'month_paid_for' => $this->input->post('payment_month'),
		);
		print_r($data);
		$this->db->where('id', $data['id']);
		if($this->db->update('individual_payments', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function delete_individual_payment($id)
	{
		$this->db->where(array('savings_payment_id' => $id));
		if($this->db->delete('savings_payment'))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function get_individual_id_by_paymentsid($id)
	{
		$this->db->from('individual_payments');
		$this->db->select('individual_id');
		$this->db->where('id = '.$id);
		$query = $this->db->get();
		$pid = $query->first_row('array');
		// var_dump($pid['group_id']);
		return $pid;
	}
	public function all_groups_payment($table, $where, $per_page, $page, $order = 'date_paid', $order_method = 'ASC')
	{
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}

	public function delete_group_payment($id)
	{	
		$group_id = $this->get_group_id_by_paymentsid($id);
		if($this->db->delete('group_payments', array('id' => $id)))
		{
			return $group_id;
		}
		else{
			return FALSE;
		}
	}
	public function get_payment_details($id)
	{
		$this->db->from('group_payments');
		$this->db->select('*');
		$this->db->where('id = '.$id);
		$query = $this->db->get();
		
		return $query;
	}
	public function get_group_id_by_paymentsid($id)
	{
		$this->db->from('group_payments');
		$this->db->select('group_id');
		$this->db->where('id = '.$id);
		$query = $this->db->get();
		$pid = $query->first_row('array');
		// var_dump($pid['group_id']);
		return $pid;
	}

	public function edit_group_payment()
	{
		$data = array(
			'id'=>$this->input->post('pid'),
			'group_id'=>$this->input->post('customer_id'),
			'amount'=>$this->input->post('payment_amount'),
			'slip_number'=>$this->input->post('slip_number'),
			'date_paid'=>$this->input->post('date_paid'),
			'month_paid_for' => $this->input->post('payment_month'),
		);
		// print_r($data);
		$this->db->where('id', $data['id']);
		if($this->db->update('group_payments', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function get_oldest_loan($individual_id)
	{
		//loan must not be cleared and it must be approved
		$this->db->select('individual_loan_id,application_date,COUNT(individual_loan_id) as total_loans');
		$this->db->where('individual_loan_cleared = 0 AND individual_loan_status >= 1 AND individual_id = '.$individual_id);
		$query = $this->db->get('individual_loan');
		if($query->num_rows() > 0)
		{
			$individual_loan_id = 0;
			foreach($query->result() as $oldest_loan)
			{
				$individual_loan_id = $oldest_loan->individual_loan_id;
				$application_date = $oldest_loan->application_date;
				$total_loans = $oldest_loan->total_loans;
				for($r=0;$r<$total_loans;$r++)
				{
					$current_application_date = $application_date;
					$current_individual_loan_id = $individual_loan_id;
					$application_date = $oldest_loan->application_date;
					$individual_loan_id = $oldest_loan->individual_loan_id;
					
					if($current_application_date < $application_date)
					{
						$individual_loan_id  = $current_individual_loan_id;
					}
					else
					{
						$individual_loan_id  = $individual_loan_id;
					}
				}
			}
		}
		return $individual_loan_id;
	}
	public function calculate_principal_payment($payment_amount,$individual_loan_id,$expected_interest_return)
	{
		$principal_payment_amount = $payment_amount - $expected_interest_return;
		return $principal_payment_amount;
	}
	public function get_interest_expected($individual_loan_id, $repayment_no)
	{
		$this->db->select('interest_amount');
		$this->db->where('individual_loan_id = '.$individual_loan_id.' AND repayment = '.$repayment_no);
		$interest_due = $this->db->get('amortization');
		$interest = 0;
		if($interest_due->num_rows()>0)
		{
			$interest_expected = $interest_due->row();
			$interest = $interest_expected->interest_amount;
		}
		return $interest;
	}
	public function get_principal_expected($individual_loan_id, $repayment_no)
	{
		$this->db->select('principal_amount');
		$this->db->where('individual_loan_id = '.$individual_loan_id.' AND repayment = '.$repayment_no);
		$principal_due = $this->db->get('amortization');
		$principal = 0;
		if($principal_due->num_rows()>0)
		{
			$principal_expected = $principal_due->row();
			$principal = $principal_expected->principal_amount;
		}
		return $principal;
	}
	public function get_repayment_number($individual_loan_id)
	{
		$this->db->select('COUNT(loan_payment_id) AS payments_made');
		$this->db->where('individual_loan_id = '.$individual_loan_id);
		$query = $this->db->get('loan_payment');
		$payment_number = $payment_to_be_made = 0;
		if($query->num_rows>0)
		{
			$number = $query->row();
			$payment_number = $number-> payments_made;
			$payment_to_be_made = $payment_number + 1;
		}
		else
		{
			$payment_to_be_made = $payment_number + 1;
		}
		return $payment_to_be_made;
	}
	public function update_amortization_table($count,$interest_payment,$principal_payment,$individual_loan_id)
	{
		//check if for that loan repayment schedule has been added
		//var_dump($this->check_if_recovery_schedule_exists($individual_loan_id,$count));die();
		if(($this->check_if_recovery_schedule_exists($individual_loan_id,$count))==FALSE)
		{
			$repayment_schedule = array(
										'repayment'=>$count,
										'interest_amount'=>$interest_payment,
										'principal_amount'=>$principal_payment,
										'individual_loan_id'=>$individual_loan_id
										);
			if($this->db->insert('amortization',$repayment_schedule))
			{
				//var_dump(1);die();
				$response['result'] = 1;
				$response['message'] = "Amortization Update done";
			}
			else
			{
				$response['result'] = 0;
				$response['message'] = "Amortization Update not Done";
			}

		}

		
			return $response;
	}

	public function check_if_recovery_schedule_exists($individual_loan_id,$count)
	{
		$this->db->select('*');
		$this->db->where('individual_loan_id = '.$individual_loan_id.' AND repayment = '.$count);
		$query = $this->db->get('amortization');
		if($query->num_rows()>0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function get_loan_interest_amoount($individual_loan_id)
	{
		$this->db->select('interest_rate,no_of_repayments,proposed_amount');
		$this->db->where('individual_loan_id = '.$individual_loan_id);
		$query = $this->db->get('individual_loan');
		$loan_details = $query->row();
		
		$interest_rate = $loan_details->interest_rate;
		$no_of_repayments = $loan_details->no_of_repayments;
		$proposed_amount = $loan_details->proposed_amount;
		$start_balance = $proposed_amount;
		$cummulative_interest = 0;
		
		for($r = 0; $r < $no_of_repayments; $r++)
		{
			$interest_payment = ($start_balance * ($interest_rate/100));
			$cummulative_interest += $interest_payment;
			$principal_payment = round(($start_balance / $no_of_repayments),-3);
			$end_balance = $start_balance - $principal_payment;
			$start_balance = $end_balance;
		}
		return $cummulative_interest;
	}
	public function add_guarantor_loan_payment($guarantor_loan_id)
	{
		$guarantor_loan_details = array(
						'guarantor_loan_id'=>$guarantor_loan_id,
						'guarantor_loan_payment_date'=>$this->input->post('payment_date'),
						'guarantor_loan_payment_amount'=>$this->input->post('fee_amount_paid'),
						'transaction_code'=>$this->input->post('transaction_code')
										);
		if($this->db->insert('guarantor_loan_payment', $guarantor_loan_details))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function get_total_guarantor_loan_amount_repaid($guarantor_loan_id)
	{
		$this->db->select('SUM(guarantor_loan_payment_amount) as total_repaid');
		$this->db->where('guarantor_loan_id = '.$guarantor_loan_id);
		$query = $this->db->get('guarantor_loan_payment');
		
		$total_paid = 0;
		$rows = $query->row();
		$total_paid = $rows->total_repaid;
		
		return $total_paid;
	}
	public function create_apex_payment_id($api_key, $transaction_id)
	{
		$data = array(
			"transaction_id" => $transaction_id,
			"api_key" => $api_key,
			"created" => date('Y-m-d H:i:s')
		);
		
		if($this->db->insert('apex_payment', $data))
		{
			return $this->db->insert_id();
		}
		
		else
		{
			return FALSE;
		}
	}
	public function disburse_payment($amount, $payment_ref, $apex_payment_id)
	{
		//$service_url = 'https://104.155.25.147:8282/TestBed/testrequest';
		$service_url = 'https://104.155.25.147:8282/B2CMpesa/fcgrequest';
		$username = '308';
		$raw_password = 'Omn!s2016';
		$timestamp = date('YmdHis');
		$password = md5($username.$raw_password.$timestamp);//echo $password; die();
		$OrgShtDesc = 'OMNIS';
		$ShortCode = '852471';
		//$ShortCode = '570761';
		$phone = $payment_ref;
		if (substr($phone, 0, 1) === '0') 
		{
			$phone = ltrim($phone, '0');
		}
		
		$phone_number = '254'.$phone;
		
		//account number null
		//$fields_string = '{"OrgShtDesc":"'.$OrgShtDesc.'","ShortCode":"'.$ShortCode.'","PaymentRef":"'.$phone_number.'","AccountNumber":"null","PaymentType":"BusinessPayment","Amount":"'.$amount.'","AuthDetails":[{"UserId":"'.$username.'","TimeStamp":"'.$timestamp.'","Password":"'.$password.'"}],"TimeOutURL":"'.site_url().'payment-timeout","TransactionId":"'.$apex_payment_id.'","ResultURL":"'.site_url().'payment-result"}';
		
		//account number left out
		$fields_string = '{"OrgShtDesc":"'.$OrgShtDesc.'","ShortCode":"'.$ShortCode.'","PaymentRef":"'.$phone_number.'","PaymentType":"BusinessPayment","Amount":"'.$amount.'","AuthDetails":[{"UserId":"'.$username.'","TimeStamp":"'.$timestamp.'","Password":"'.$password.'"}],"TimeOutURL":"'.site_url().'payment-timeout","TransactionId":"'.$apex_payment_id.'","ResultURL":"'.site_url().'payment-result"}';
		//echo $fields_string;die();
		try
		{
			$ch = curl_init();
			
			// b. set the options, including the url
			curl_setopt($ch, CURLOPT_URL, $service_url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			//curl_setopt($ch, CURLOPT_PORT, 8282);
			
			// c. execute and fetch the resulting HTML output
			$output = curl_exec($ch);
			$errmsg  = curl_error( $ch );
			$err     = curl_errno( $ch );
			$header  = curl_getinfo( $ch );
			//var_dump($output); die();
			//in the case of an error save it to the database
			if ($output === FALSE) 
			{
				$response['result'] = 0;
				$response['message'] = curl_error($ch);
			}
			
			else
			{
				$result = json_encode($output);
				$result_array2 = json_decode($output, TRUE);
				$result_array = $result_array2[0];
				//var_dump($result_array); die();
				/*if($result_array['responseCode'] == 500)
				{
					echo $result_array['responseMessage'];
				}
				
				else if($result_array['responseCode'] == 404)
				{
					echo $result_array['responseMessage'];
				}
				
				else
				{
					echo $result_array['responseMessage'];
				}*/
				$response['result'] = $result_array['responseCode'];
				$response['message'] = $result_array['responseMessage'];
			}
		}
		
		//in the case of an exceptions save them to the database
		catch(Exception $e)
		{
			$response['result'] = 0;
			$response['message'] = $e->getMessage();
		}
		
		return $response;
	}
	public function validateApi_Key($api_key){
    	// Select from Db 
    	$this->db->where(array("apikey_code" =>$api_key));
    	$result = $this->db->get("api_key");
    	if($result->num_rows() > 0)
		{
    	 return TRUE;
    	}
		else
		{
    		return FALSE;
    	}




    }
	
	
}

/* End of file payments_model.php */
/* Location: ./application/models/payments_model.php */