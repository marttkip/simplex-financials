<?php
		
		$result = '';
		if($defaulters != NULL)
		{
			//if users exist display them
			if ($defaulters['query']->num_rows() > 0)
			{
				$count_individual = 0;
				
				$result .= 
				'
				<table class="table table-bordered table-striped table-condensed">
					<thead>
						<tr>
							<th>#</th>
							<th>Member Number</th>
							<th>Member Name</th>
							<th>Loan Number</th>
							<th>Disbursements</th>
							<th>Payments</th>
							<th>Defaulted Amount</th>
							<th>Actions</th>
	
						</tr>
					</thead>
					  <tbody>
					  
				';
				foreach ($defaulters['query']->result() as $row)
				{
					$individual_id = $row->individual_id;
					$individual_fname = $row->individual_fname;
					$individual_mname = $row->individual_mname;
					$individual_lname = $row->individual_lname;
					$individual_number = $row->individual_number;
					$individual_name = $individual_fname.' '.$individual_lname;
					$individual_loan_id = $row->individual_loan_id;
	
					$loan_number = $this->reports_model->get_individual_loan_number($individual_loan_id);
					$defaulters_balance = $defaulters['totals_disbursed'] - $defaulters['total_payments'];
					//get total disbursements
					
					$count_individual++;
					$result .= 
					'
						<tr>
							<td>'.$count_individual.'</td>
							<td>'.$individual_number.'</td>
							<td>'.$individual_lname.' '.$individual_fname.' '.$individual_mname.'</td>
							<td>'.$loan_number.'</td>
							<td>'.$defaulters['totals_disbursed'].'</td>
							<td>'.$defaulters['total_payments'].'</td>
							<td>'.$defaulters_balance.'</td>
							<td><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#more'.$individual_loan_id.'">
						<i class="fa fa-plus"></i>
						</button>
					<!-- Modal -->
					<div class="modal fade" id="more'.$individual_loan_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						<div class="modal-dialog modal-dialog-wide" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
									<h4 class="modal-title"> Guarantors'.$loan_number.'</h4>
								</div>
								<div class="modal-body">
								';
									$result2 = '';
									$guarantors = $this->individual_model->get_guarantors($individual_loan_id);
									$source_individual_id = $individual_id;
									$individual_loan_id = $individual_loan_id;
									$individuals = $this->individual_model->all_individual($individual_id);
									$title = 'Loan guarantor';	
									//var_dump($guarantors->num_rows()); die();
									if($guarantors->num_rows() > 0)
									{
										$count = 0;
											
										$result .= 
										'
										<table class="table table-bordered table-striped table-condensed">
											<thead>
												<tr>
													<th>#</th>
													<th>Guarantor name</th>
													<th>Guaranteed amount</th>
													<th>Added on</th>
													<th>Added by</th>
													<th>Deduction Amount</th>
													<th>Action</th>
												</tr>
											</thead>
											  <tbody>
												<tr><strong>Defaulted Amount :'.$defaulters_balance.'</strong></tr> 
											  
										';
										$total_guarnateed_amount = 0;
										$count = 0;
										
										foreach ($guarantors->result() as $row)
										{
											$guaranteed_amount = $row->guaranteed_amount;
											$total_guarnateed_amount += $guaranteed_amount;
										}
										foreach ($guarantors->result() as $row)
										{
											$loan_guarantor_id = $row->loan_guarantor_id;
											$individual_fname = $row->individual_fname;
											$individual_lname = $row->individual_lname;
											$individual_mname = $row->individual_mname;
											$personnel_fname = $row->personnel_fname;
											$personnel_onames = $row->personnel_onames;
											$guaranteed_amount = $row->guaranteed_amount;
											$defaulters_share = ($guaranteed_amount/$total_guarnateed_amount)*100;
											$guaranteed_amount = number_format($guaranteed_amount, 2);
											$created = date('jS M Y H:i:s',strtotime($row->created));
											
											$count++;
											$deduction_amount = $defaulters_balance*($defaulters_share/100);
											$result .= 
											'
												<tr>
													<td>'.$count.'</td>
													<td>'.$individual_fname.' '.$individual_mname.' '.$individual_lname.'</td>
													<td>'.$guaranteed_amount.'</td>
													<td>'.$created.'</td>
													<td>'.$personnel_fname.' '.$personnel_onames.'</td>
													<td>'.$deduction_amount.'</td>
													<td><a class="btn btn-info" href="'.site_url().'microfinance/reports/create-guarantor-loan/'.$loan_guarantor_id.'/'.$deduction_amount.'/'.$individual_loan_id.'" onclick="return confirm(\'Do you want to create a non-interest earning loan for '.$individual_fname.'?\');" title="Create Non Interest Earning Loan for '.$individual_fname.'"><i class="fa fa-money"></i></a></td>
												</tr> 
											';
										}
										$result .= 
										'
											<tr>
												<td></td>
												<td></td>
												<th>'.number_format($total_guarnateed_amount, 2).'</th>
												<td></td>
												<td></td>
											</tr> 
											</tbody>
										</table>
										';
										
										/*$result .= 
										'
											<tr>
												<th colspan="3">Deduction Amount</th>
												<th>'.number_format($deduction_amount,2).'</th>
													  </tbody>
													</table>
										';*/
									}
									
									else
									{
										$result .= "No guarantors were added for this loan";
									}
									
									$result .='</div>
																	
								</div>
							</div></td>
						</tr> 
													';
												}
												
												$result .= 
												'
															  </tbody>
															</table>
												';
											}
											
											else
											{
												$result .= "There are no defaulters";
											}
										}
										
										else
										{
											$result .= "There are no defaulters";
										}
?>


<section class="panel">
	<header class="panel-heading">						
		<h2 class="panel-title"><?php echo $title;?></h2>
	</header>
	<div class="panel-body">
    	<?php
        $success = $this->session->userdata('success_message');

		if(!empty($success))
		{
			echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
			$this->session->unset_userdata('success_message');
		}
		
		$error = $this->session->userdata('error_message');
		
		if(!empty($error))
		{
			echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
			$this->session->unset_userdata('error_message');
		}
		?>
    	<div class="row " style="margin-bottom:20px;">
            <div class="col-lg-2 col-lg-offset-8 pull-right">
                <a href="<?php echo site_url();?>export-defaulters" class="btn btn-sm btn-success pull-right">Export Defaulters</a>
            </div>
        </div>
		<div class="table-responsive">
        	
			<?php echo $result;?>
	
        </div>
	</div>
    <div class="panel-footer">
    	<?php if(isset($links)){echo $links;}?>
    </div>
</section>