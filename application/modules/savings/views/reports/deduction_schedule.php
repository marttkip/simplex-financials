<?php
$branch_id = set_value('branch_id');
$repayment_no = set_value('repayment_no');
?>
<section class="panel">
	<header class="panel-heading">						
		<h2 class="panel-title"><?php echo $title;?></h2>
	</header>
	<div class="panel-body">
		<?php echo form_open('microfinance/reports/generate_schedule', array("class" => "form-horizontal", "role" => "form"));?>
		<div class="row">
        	<div class="col-md-6">
            	<div class = "form-group">
					<?php $branches = $this->individual_model->all_branches();
                    ?>
                    <label class="col-lg-5 control-label">Site <span class="required">*</span></label>
                    <div class="col-lg-7">
                        <select name="branch_id" id="branch_id" class="form-control">
                            <?php
                            echo '<option value="">No Branch </option>';
                            if($branches->num_rows() > 0)
                            {
                                $result = $branches->result();
                                foreach($result as $res)
                                {
                                    if($res->branch_id == $branch_id)
                                        {
                                        echo '<option value="'.$res->branch_id.'" selected>'.$res->branch_name.' '.$res->branch_id.'</option>';
                                        }
                                        else
                                        {
                                        echo '<option value="'.$res->branch_id.'">'.$res->branch_name.' </option>';
                                        }
                                 }
                            }
                           ?>
                        </select>
                    </div>
                </div>
            </div>
           <!-- <div class="col-md-6">
                <div class="form-group">
                        <label class="col-lg-5 control-label">Repayment *: </label>
                        
                        <div class="col-lg-7">
                            <input type="text" class="form-control" name="repayment_no" placeholder="Repayment" value="<?php //echo $repayment_no;?>">
                        </div>
                </div>
            </div>-->
        </div>
        <div class="row" style="margin-top:10px;">
            <div class="col-md-12">
                <div class="form-actions center-align">
                    <button class="submit btn btn-primary" type="submit" formtarget="_blank">
                        Generate
                    </button>
                </div>
            </div>
        </div>
				<?php echo form_close();?>
		
        
    </div>
</section>