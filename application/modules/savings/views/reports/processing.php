<?php
$result = '';
if($all_fees->num_rows()>0)
{
	$count = 0;
	$result.='
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Individual</th>
						<th>Applied Amount</th>
						<th>Expected Fee</th>
						<th>Processing Fee Paid</th>
						<th>Transaction code</th>
						<th colspan="4">Actions</th>
					</tr>
				</thead>
				<tbody>';
	foreach($all_fees->result() as $processing_fee_details)
	{
		$count++;
		$individual_loan_processing_fee_id = $processing_fee_details->individual_loan_processing_fee_id;
		$individual_fname = $processing_fee_details->individual_fname;
		$individual_mname = $processing_fee_details->individual_mname;
		$individual_lname = $processing_fee_details->individual_lname;
		$application_amount = $processing_fee_details->proposed_amount;
		$processing_fee_amount = $processing_fee_details->processing_amount;
		$processing_fee = $processing_fee_details->processing_fee;
		$transaction_code = $processing_fee_details->transaction_code;
		$individual_processing_fee_status = $processing_fee_details->individual_processing_fee_status;
		$individual_name = $individual_fname.' '.$individual_mname.' '.$individual_lname;
		
		if($individual_processing_fee_status==0)
		{
			$status = '<span class="label label-default">Pending verification</span>';
			$edit_button = '<a class="btn btn-info" href="'.site_url().'microfinance/edit-processing-fee/'.$individual_loan_processing_fee_id.'" onclick="return confirm(\'Do you want to edit the fees\');" title="Edit"><i class="fa fa-edit"></i></a>';
			$button = '<a class="btn btn-warning" href="'.site_url().'microfinance/veirfy-processing-fee/'.$individual_loan_processing_fee_id.'" onclick="return confirm(\'Do you want to approve the fees\');" title="Approve Fee Paymnent"><i class="fa fa-thumbs-up"></i></a>';
		}
		if($individual_processing_fee_status==1)
		{
			$status = '<span class="label label-default">Verified</span>';
			$edit_button = '';
			$button = '';
		}
		$result.='
				<tr>
					<td>'.$count.'</td>
					<td>'.$individual_name.'</td>
					<td>'.$application_amount.'</td>
					<td>'.$processing_fee.'</td>
					<td>'.$processing_fee_amount.'</td>
					<td>'.$transaction_code.'</td>
					<td>'.$status.'</td>
					<td>'.$button.'</td>
					<td>'.$edit_button.'</td>
				</tr>';
	}
}
else
{
	$result .='No fees have been paid';
}
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Processing Fees</h2>
    </header>
    <div class="panel-body">
    <?php
	$success = $this->session->userdata('success_message');

	if(!empty($success))
	{
		echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
		$this->session->unset_userdata('success_message');
	}
	
	$error = $this->session->userdata('error_message');
	
	if(!empty($error))
	{
		echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
		$this->session->unset_userdata('error_message');
	} 
        echo $result;
    ?>  
    </div>
 </section>