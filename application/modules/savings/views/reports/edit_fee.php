<?php
$row = $processing_fee_details->row();
$processing_amount = $row->processing_amount;
$transaction_code = $row->transaction_code;
$individual_processing_fee_status = $row->individual_processing_fee_status;
$individual_loan_id = $row->individual_loan_id;
$individual_id  = $this->reports_model->get_individual_id($individual_loan_id);
$individual_details = $this->individual_model->get_individual($individual_id);
$individual_data = $individual_details->row();
$individual_lname = $individual_data->individual_lname;
$individual_fname = $individual_data->individual_fname;
$individual_mname = $individual_data->individual_mname;

$validation_error = validation_errors();
				
if(!empty($validation_error))
{
	$processing_amount = set_value('processing_amount');
	$individual_processing_fee_status = set_value('individual_processing_fee_status');
}
?>
 <section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Processing fee for  <?php echo $individual_lname.' '.$individual_fname.' '.$individual_mname;?></h2>
    </header>
    <div class="panel-body">
        <!-- Adding Errors -->
        <?php
        if(isset($error)){
            echo '<div class="alert alert-danger"> Oh snap! Change a few things up and try submitting again. </div>';
        }
        if(!empty($validation_errors))
        {
            echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
        }
        
        ?>
        
        <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-lg-5 control-label">Processing fee amount *: </label>
                    
                    <div class="col-lg-7">
                        <input type="text" class="form-control" name="processing_amount" placeholder="Processing Amount" value="<?php echo $processing_amount;?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-5 control-label">Transaction code *: </label>
                    
                    <div class="col-lg-7">
                        <input type="text" class="form-control" name="transaction_code" placeholder="Transaction Code" value="<?php echo $transaction_code;?>">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-actions center-align">
                    <button class="btn btn-primary" type="submit">
                        Edit fee
                    </button>
                </div>
            </div>
        </div>
        <?php echo form_close();?>
    </div>
</section>
	