<?php
$guarantor_loan_result = '';
$count = 0;
if($query->num_rows() > 0)
{
	$guarantor_loan_result .='
					<table class="table table-bordered table-striped table-condensed">
						<thead>
							<tr>
								<th>#</th>
								<th>Individual Name</th>
								<th>Guaranteed Loan Number</th>
								<th>Guaranteed Member</th>
								<th>Repayment AMount</th>
								<th>Paid</th>
								<th>Balance</th>
								<th>Status</th>
								<th colspan="3">Actions</th>
							</tr>
						</thead>
						<tbody>';
	foreach($query->result() as $guarantor_loan)
	{
		$payment_button = '';
		$individual_fname = $guarantor_loan->individual_fname;
		$individual_mname = $guarantor_loan->individual_mname;
		$individual_lname = $guarantor_loan->individual_lname;
		$loan_number = $guarantor_loan->individual_loan_number;
		$individual_name = $individual_fname.' '.$individual_mname.' '.$individual_lname;
		$defaulted_loan_amount = $guarantor_loan->loan_amount;
		$guarantor_loan_id = $guarantor_loan->guarantor_loan_id;
		$guarantor_loan_status_id = $guarantor_loan->guarantor_loan_status;
		
		$total_guarantor_loan_amount_repaid = $this->payments_model->get_total_guarantor_loan_amount_repaid($guarantor_loan_id);
		
		$individual_id = $guarantor_loan->individual_id;
		
		$guaranteed_member = $this->individual_model->get_individual($individual_id);
		$guaranteed_member_details = $guaranteed_member->row();
		$guaranteed_fname = $guaranteed_member_details->individual_fname;
		$guaranteed_mname = $guaranteed_member_details->individual_mname;
		$guaranteed_lname = $guaranteed_member_details->individual_lname;
		$guaranteed_name = $guaranteed_fname.' '.$guaranteed_mname.' '.$guaranteed_lname;
		
		$balance =$defaulted_loan_amount - $total_guarantor_loan_amount_repaid;
		 
		if($guarantor_loan_status_id==1)
		{
			$status = '<span class="label label-success">Activated</span>';
			$button = '<a class="btn btn-default" href="'.site_url().'mfi-reports/guarantor-loans/deactivate-individual-loan/'.$guarantor_loan_id.'" onclick="return confirm(\'Do you want to deactivate the non interest loan for '.$individual_name.'?\');" title="Deactivate '.$individual_name.'"><i class="fa fa-thumbs-down"></i></a>';
		}
		else
		{
			$status = '<span class="label label-warning">Dectivated</span>';
			$button = '<a class="btn btn-default" href="'.site_url().'mfi-reports/guarantor-loans/activate-individual-loan/'.$guarantor_loan_id.'" onclick="return confirm(\'Do you want to activate the non interest loan for '.$individual_name.'?\');" title="Activate '.$individual_name.'"><i class="fa fa-thumbs-up"></i></a>';
		}
		$count++;	
		
		$payment_button.='<button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#make_payment'.$guarantor_loan_id.'">
										<i class="fa fa-money"></i>
										</button></td>
										<!-- Modal -->
									<div class="modal fade" id="make_payment'.$guarantor_loan_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
										<div class="modal-dialog modal-dialog-wide" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
													<h4 class="modal-title">'.$individual_name.' Guarantor Payment</h4>
												</div>
												<div class="modal-body">'.form_open('mfi-reports/guarantor-loans/make-payment/'.$guarantor_loan_id, array("class" => "form-horizontal", "role" => "form")).'<div class="row">
															
														<div class="col-md-6">
															<div class="form-group">
																<label class="col-lg-5 control-label">Amount Paid: </label>
																
																<div class="col-lg-7">
																	<input type="text" class="form-control" name="fee_amount_paid" placeholder="Amount Paid" value="">
																</div>
															</div>
															
															
														</div>
													   
														<div class="col-md-6">
															
															<div class="form-group">
																<label class="col-lg-5 control-label">Transaction Code: </label>
																
																<div class="col-lg-7">
																	<input type="text" class="form-control" name="transaction_code" placeholder="Transaction Code" value="">
																</div>
															</div>
															<div class="form-group">
																<label class="col-lg-5 control-label">Payment date: </label>
																
																<div class="col-lg-7">
																	<div class="input-group">
																		<span class="input-group-addon">
																			<i class="fa fa-calendar"></i>
																		</span>
																		<input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="payment_date" placeholder="Payment Date" value="">
																	</div>
																</div>
															</div>
															
														</div>
													</div>
													<div class="row" style="margin-top:10px;">
														<div class="col-md-12">
															<div class="form-actions center-align">
																<button class="btn btn-primary" type="submit">
																	 Add payment
																</button>
															</div>
														</div>
													</div>'.form_close();
												$payment_button .='
													
												</div>
											</div>
										</div>
									</div>
								';
		
		$guarantor_loan_result.='
							<tr>
								<td>'.$count.'</td>
								<td>'.$individual_name.'</td>
								<td>'.$loan_number.'</td>
								<td>'.$guaranteed_name.'</td>
								<td>'.number_format($defaulted_loan_amount,2).'</td>
								<td>'.number_format($total_guarantor_loan_amount_repaid,2).'</td>
								<td>'.number_format($balance,2).'</td>
								<td>'.$status.'</td>
								<td>'.$button.'</td>
								<td>'.$payment_button.'</td>
								<td><a class="btn btn-primary" href="'.site_url().'mfi-reports/guarantor-loans/clear-non-interst-loan/'.$guarantor_loan_id.'" title="Clear Loan '.$individual_name.'"><i class="fa fa-plus"></i></a></td>
							</tr>';
	}
}
else
{
	$guarantor_loan_result .='No guarantor loans as no loan was defaulted';
}
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title"><?php echo $title;?></h2>
    </header>
    <div class="panel-body">
    	<?php
        $success = $this->session->userdata('success_message');

		if(!empty($success))
		{
			echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
			$this->session->unset_userdata('success_message');
		}
		
		$error = $this->session->userdata('error_message');
		
		if(!empty($error))
		{
			echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
			$this->session->unset_userdata('error_message');
		}
		echo $guarantor_loan_result;?>
    </div>
</section>