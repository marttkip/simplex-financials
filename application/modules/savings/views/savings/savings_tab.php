<?php
//individual data
$row = $individual->row();

$individual_lname = $row->individual_lname;
$individual_mname = $row->individual_mname;
$individual_fname = $row->individual_fname;
$individual_email = $row->individual_email;
$individual_phone = $row->individual_phone;
$individual_id = $row->individual_id;
$individual_number = $row->individual_number;
// $indidual_type_id = $row->indidual_type_id;


$v_data['outstanding_loan'] = $row->outstanding_loan;
$v_data['total_savings'] = $row->total_savings;

$employement_date_rs = $this->individual_model->get_current_employement_date($individual_id);

if($employement_date_rs->num_rows() > 0)
{
	$res = $employement_date_rs->result();
	$employment_date = $res[0]->employment_date;
	$current_date = date('Y-m-d');
	$diff = abs(strtotime($current_date) - strtotime($employment_date));

	$years = floor($diff / (365*60*60*24));
	$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
	$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

	$working_time = 'Years '.$years.', Months '.$months.' Days '.$days;
}
else
{
	$working_time =  '';
}
$v_data['working_time'] = $working_time;
?>
      	<div class="row">
        
          <section class="panel">

                <header class="panel-heading">
                	<div class="row">
	                	<div class="col-md-6">
		                    <h2 class="panel-title">Edit <?php echo $individual_fname.' '.$individual_mname.' '.$individual_lname;?></h2>
		                    <i class="fa fa-user"/></i>
		                    <span id="work_email"><?php echo $individual_number;?></span>
		                    <i class="fa fa-phone"/></i>
		                    <span id="mobile_phone"><?php echo $individual_phone;?></span>
		                    <i class="fa fa-envelope"/></i>
		                    <span id="work_email"><?php echo $individual_email;?></span>

		                </div>
		                <div class="col-md-6">
		                		<a href="<?php echo site_url();?>microfinance/individual" class="btn btn-sm btn-info pull-right">Back to individuals</a>
		                </div>
	                </div>
                </header>
                <div class="panel-body">
                    <form action="<?php echo site_url().'microfinance/individual/search-member-numer/'.$individual_id;?>" style="margin-bottom:20px;" method="post">
                        <div class="row">
                            
                            <div class="col-md-6">
                            	<input type="text" class="form-control" placeholder="Search member number" name="individual_number" />
                            </div>
                            <div class="col-md-3">
                                    <button type="submit" class="btn btn-info btn-sm">Search</button>
                            </div>
                        </div>
                    </form>
                	<?php
					$validation_errors = validation_errors();
					if(!empty($validation_errors))
					{
						echo '<div class="alert alert-danger"> '.$validation_errors.' </div>';
					}
					
					$success = $this->session->userdata('success_message');
		
					if(!empty($success))
					{
						echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
						$this->session->unset_userdata('success_message');
					}
					
					$error = $this->session->userdata('error_message');
					
					if(!empty($error))
					{
						echo '<div class="alert alert-danger"> '.$error.' </div>';
						$this->session->unset_userdata('error_message');
					}
					?>
                    
                    <div class="row">
                    	<div class="col-md-12">
                        	<div class="tabs">
								<ul class="nav nav-tabs nav-justified">
									<li class="active">
										<a class="text-center" data-toggle="tab" href="#general"><i class="fa fa-user"></i> General details</a>
									</li>
									<li>
										<a class="text-center" data-toggle="tab" href="#loans"><i class="fa fa-balance-scale"></i> Loans</a>
									</li>
									<li>
										<a class="text-center" data-toggle="tab" href="#account"><i class="fa fa-money"></i> Savings</a>
									</li>
									
									<li>
										<a class="text-center" data-toggle="tab" href="#history"><i class="fa fa-file"></i>  Approval</a>
									</li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="general">
										<?php echo $this->load->view('savings/savings/saving_tab/about', '', TRUE);?>
									</div>
									<div class="tab-pane" id="job">
										<?php echo $this->load->view('savings/savings/saving_tab/job', '', TRUE);?>
									</div>
									<div class="tab-pane" id="loans">
										<?php echo $this->load->view('savings/savings/saving_tab/loan',  $v_data, TRUE);?>
									</div>
									<div class="tab-pane" id="account">
										<?php echo $this->load->view('savings/savings/saving_tab/saving', $v_data, TRUE);?>
									</div>
									<div class="tab-pane" id="history">
										<?php echo $this->load->view('savings/savings/saving_tab/history', $v_data,  TRUE);?>
									</div>
								</div>
							</div>
                        </div>
                    </div>
                </div>
            </section>