<?php
        $count = 0; 
        $result = '';
        $withdrawal_charge = 0;

        
        //if users exist display them
        if ($individual_savings->num_rows() > 0)

        {   
            $result .= 
            '
            <table class="table table-bordered table-striped table-condensed">
                <thead>
                    <tr>
                        <th>#</th>
                        <th> Savings Plan Name</th>
                        <th> Application Date</th>
                        <th> Opening Balance</th>
                        <th> Monthly Payments</th>
                        <th> Withdrawal Charges</th>
                        <th> Processing Fees</th>
                        <th> Interest</th>
                        <th> Approval Status</th>
                        <th> Actions</th>
                    </tr>
                </thead>
                  <tbody>
                  
            ';
            
           
            foreach ($individual_savings->result() as $row)
            {
                $individual_savings_id = $row->individual_savings_id;
                $individual_id = $row->individual_id;

                $savings_plan_id = $row->savings_plan_id;
                $savings_plan_name = $this->member_savings_model->get_savings_plan_name($savings_plan_id);
                
                $individual_savings_status = $row->individual_savings_status;
                $monthly_payments = $row->individual_savings_amount;
                $savings_start_date = $row->start_date;
                $opening_balance = $row->opening_balance;
                $approval_status = $row->approval_status;
                $charge_withdrawal = $row->charge_withdrawal;
                $processing_fees = $row->processing_fees;
                $estimated_interest = $row->interest;

                if ($charge_withdrawal == 0)
                {
                  $withdrawal_charge = 0;

                }
                else
                {
                    $withdrawal_charge = $row->withdrawal_charge;

                }
                
                
                
                //status
                if($approval_status == 1)
                {
                     $status = '<span class="label label-success">Approved</span>';
                     $approved = ' ';
                }
                else
                {
                    $status = '<span class="label label-default">Pending Approval</span>';
                    $approved = '<a  class="btn btn-xs btn-primary" id="open_individual_savings_id'.$individual_savings_id.'" onclick="get_individual_savings_id('.$individual_savings_id.')" ><i class="fa fa-folder"></i> Approve</a>
							<a  class="btn btn-xs btn-warning" id="close_individual_savings_id'.$individual_savings_id.'" style="display:none;" onclick="close_individual_savings_id('.$individual_savings_id.')" ><i class="fa fa-folder"></i> Close Approval</a>';

                }

                
                //create deactivated status display
                if($individual_savings_status == 0)
                {
                   
                    $button = '<a class="btn btn-info" href="'.site_url().'microfinance/activate-individual/'.$individual_savings_id.'" onclick="return confirm(\'Do you want to activate '.$savings_plan_name.'?\');" title="Activate '.$savings_plan_name.'"><i class="fa fa-thumbs-up"></i></a>';
                }
                //create activated status display
                else if($individual_savings_status == 1)
                {
                   
                    $button = '<a class="btn btn-default" href="'.site_url().'microfinance/deactivate-individual/'.$individual_savings_id.'" onclick="return confirm(\'Do you want to deactivate '.$savings_plan_name.'?\');" title="Deactivate '.$savings_plan_name.'"><i class="fa fa-thumbs-down"></i></a>';
                }
                
                $count++;
                $balance = '0';
                $member_payments = $this->member_savings_model->get_actual_individual_savings_payments($individual_savings_id);
                $member_withdrawals = $this->member_savings_model->get_actual_individual_savings_withdrawals($individual_savings_id);
                $member_balances = $member_payments - $member_withdrawals;
                $member_dividends = $this->member_savings_model->get_actual_individual_savings_dividends($individual_savings_id);
                $result .= 
                '
                    <tr>
                        <td>'.$count.'</td>
                        <td>'.$savings_plan_name.'</td>
                        <td>'.$savings_start_date.'</td>
                        <td>'.$opening_balance.'</td>
                        <td>'.$monthly_payments.'</td>
                        <td>'.$withdrawal_charge.'</td>
                        <td>'.$processing_fees.'</td>
                        <td>'.$estimated_interest.'</td>
                        <td>'.$status.'</td>
                        <td>'.$approved.'</td>
                       
                        
                    
                    </tr> 
                ';

                $v_data['individual_savings_id'] = $individual_savings_id;
                $v_data['individual_id'] = $individual_id;
				$v_data['savings_plan_id'] = $savings_plan_id;
                $result .= '<tr id="individual_savings_ids'.$individual_savings_id.'" style="display:none;">
						<td colspan="13">
							'.$this->load->view("approval_list", $v_data, TRUE).'
						</td>
					</tr>';
            }
            
            $result .= 
            '
                          </tbody>
                        </table>
            ';
        }
        
        else
        {
            $result .= "There are no savings plans";
        }

?>                    


<?php

$savings_plan_id = set_value('savings_plan_id');
$actual_savings_plan_id = set_value('actual_savings_plan_id');
$opening_balance = set_value('opening_balance');
$monthly_payments = set_value('monthly_payments');
$application_date = set_value('application_date');
$first_month_payment = set_value('first_month_payment');
$actual_opening_balance = set_value('actual_opening_balance');
$transaction_code = set_value('transaction_code');
$payment_method_id = set_value('payment_method_id');




?>
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title">Savings Plan Approval Process</h2>
        </header>
     
        <div class="col-md-12 center-align ">
            
            
            <div class="panel-body">
                <div class="table-responsive">
               
                    <?php echo $result;?>
            
                </div>
             </div>

        </div>
    </section>

    <script type="text/javascript">
		function get_individual_savings_id(individual_savings_id){

			var myTarget2 = document.getElementById("individual_savings_ids"+individual_savings_id);
			var button = document.getElementById("open_individual_savings_id"+individual_savings_id);
			var button2 = document.getElementById("close_individual_savings_id"+individual_savings_id);

			myTarget2.style.display = '';
			button.style.display = 'none';
			button2.style.display = '';
		}
		function close_individual_savings_id(individual_savings_id){

			var myTarget2 = document.getElementById("individual_savings_ids"+individual_savings_id);
			var button = document.getElementById("open_individual_savings_id"+individual_savings_id);
			var button2 = document.getElementById("close_individual_savings_id"+individual_savings_id);

			myTarget2.style.display = 'none';
			button.style.display = '';
			button2.style.display = 'none';
		}

  </script>

