<?php
        $count = 0; 
        $result = '';
        $withdrawal_charge = 0;

        
        //if users exist display them
        if ($individual_savings->num_rows() > 0)

        {   
            $result .= 
            '
            <table class="table table-bordered table-striped table-condensed">
                <thead>
                    <tr>
                        <th>#</th>
                        <th> Savings Plan Name</th>
                        <th> Application Date</th>
                        <th> Opening Balance</th>
                        <th> Monthly Payments</th>
                        <th> Withdrawal Charges</th>
                        <th> Processing Fees</th>
                        <th> Approval Status</th>
                    </tr>
                </thead>
                  <tbody>
                  
            ';
            
           
            foreach ($individual_savings->result() as $row)
            {
                $individual_savings_id = $row->individual_savings_id;

                $savings_plan_id = $row->savings_plan_id;
                $savings_plan_name = $this->member_savings_model->get_savings_plan_name($savings_plan_id);
                
                $individual_savings_status = $row->individual_savings_status;
                $monthly_payments = $row->individual_savings_amount;
                $savings_start_date = $row->start_date;
                $opening_balance = $row->opening_balance;
                $approval_status = $row->approval_status;
                $charge_withdrawal = $row->charge_withdrawal;
                $processing_fees = $row->processing_fees;

                if ($charge_withdrawal == 0)
                {
                  $withdrawal_charge = 0;

                }
                else
                {
                    $withdrawal_charge = $row->withdrawal_charge;

                }
                
                
                
                //status
                if($approval_status == 1)
                {
                     $status = '<span class="label label-success">Approved</span>';
                }
                else
                {
                    $status = '<span class="label label-default">Pending Approval</span>';
                }

                
                //create deactivated status display
                if($individual_savings_status == 0)
                {
                   
                    $button = '<a class="btn btn-info" href="'.site_url().'microfinance/activate-individual/'.$individual_savings_id.'" onclick="return confirm(\'Do you want to activate '.$savings_plan_name.'?\');" title="Activate '.$savings_plan_name.'"><i class="fa fa-thumbs-up"></i></a>';
                }
                //create activated status display
                else if($individual_savings_status == 1)
                {
                   
                    $button = '<a class="btn btn-default" href="'.site_url().'microfinance/deactivate-individual/'.$individual_savings_id.'" onclick="return confirm(\'Do you want to deactivate '.$savings_plan_name.'?\');" title="Deactivate '.$savings_plan_name.'"><i class="fa fa-thumbs-down"></i></a>';
                }
                
                $count++;
                $balance = '0';
                $member_payments = $this->member_savings_model->get_actual_individual_savings_payments($individual_savings_id);
                $member_withdrawals = $this->member_savings_model->get_actual_individual_savings_withdrawals($individual_savings_id);
                $member_balances = $member_payments - $member_withdrawals;
                $member_dividends = $this->member_savings_model->get_actual_individual_savings_dividends($individual_savings_id);
                $result .= 
                '
                    <tr>
                        <td>'.$count.'</td>
                        <td>'.$savings_plan_name.'</td>
                        <td>'.$savings_start_date.'</td>
                        <td>'.$opening_balance.'</td>
                        <td>'.$monthly_payments.'</td>
                        <td>'.$withdrawal_charge.'</td>
                        <td>'.$processing_fees.'</td>
                        <td>'.$status.'</td>
                        
                    
                    </tr> 
                ';
            }
            
            $result .= 
            '
                          </tbody>
                        </table>
            ';
        }
        
        else
        {
            $result .= "There are no savings plans";
        }

?>                    


<?php

$savings_plan_id = set_value('savings_plan_id');
$actual_savings_plan_id = set_value('actual_savings_plan_id');
$opening_balance = set_value('opening_balance');
$monthly_payments = set_value('monthly_payments');
$application_date = set_value('application_date');
$first_month_payment = set_value('first_month_payment');
$actual_opening_balance = set_value('actual_opening_balance');
$transaction_code = set_value('transaction_code');
$payment_method_id = set_value('payment_method_id');




?>
    <section class="panel">
        <header class="panel-heading">
            <h2 class="panel-title">Member Savings Plan Application Process</h2>
        </header>
        <div class="panel-body">
        <!-- Adding Errors -->
            <div class="row">
                        
                    <div class="col-md-12 center-align ">
                        <h4><strong> Step 1: Identify the Member Saving Plan Required</strong></h4>
                    </div>
            </div>
            <?php echo form_open('savings-management/calculate-dividend/'.$individual_id, array("class" => "form-horizontal", "role" => "form"));?>
                <div class="row">
                        
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-lg-5 control-label">Savings Plan type: </label>
                            
                            <div class="col-lg-7">
                                <select class="form-control" name="savings_plan_id" id="update_savings_plan">
                                    <option value="">--Select Savings Plan--</option>
                                    <?php
                                        if($savings_plans->num_rows() > 0)
                                        {
                                            $savings_plan = $savings_plans->result();
                                            
                                            foreach($savings_plan as $res)
                                            {
                                                $db_savings_plan_id = $res->savings_plan_id;
                                                $savings_plan_name = $res->savings_plan_name;
                                                
                                                if($db_savings_plan_id == $savings_plan_id)
                                                {
                                                    echo '<option value="'.$db_savings_plan_id.'" selected>'.$savings_plan_name.'</option>';
                                                }
                                                
                                                else
                                                {
                                                    echo '<option value="'.$db_savings_plan_id.'">'.$savings_plan_name.'</option>';
                                                }
                                            }
                                        }
                                    ?>
                                </select>
                                <br/>
                                <div id="saving_plan_details"></div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-lg-5 control-label">Opening Balance: </label>
                            
                            <div class="col-lg-7">
                                <input type="text" class="form-control" name="opening_balance" id="opening_balance"placeholder="Opening Balance" value="<?php echo $opening_balance;?>">
                            </div>
                        </div>
                     
                        
                    </div>
                   
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-lg-5 control-label">Monthly Payments: </label>
                            
                            <div class="col-lg-7">
                                <input type="text" class="form-control" name="monthly_payments" id="monthly_payments" placeholder="Monthly Payments" value="<?php echo $monthly_payments;?>">
                            </div>
                        </div>      
                      

                        
                        
                        
                    </div>
                </div>
                <div class="row" style="margin-top:10px;">
                    <div class="col-md-12">
                        <div class="form-actions center-align">
                            <a class="btn btn-primary" onclick="calculate_dividend()">
                                Get Saving Plan Figures
                            </a>
                        </div>
                    </div>
                </div>
            <?php 
                echo form_close();
            ?>
        </div>
        <div class="col-md-12 center-align ">
            <h4><strong> Step 2: Get Back the Calculated Expected Annual Dividends</strong></h4>
            <br/>
            <div id="saving_plan_calculations"></div>

        </div>

        <section class="panel">
        <header class="panel-heading">
            <h4><strong> Step 3: Record the Actual Member Saving Plan Payments</strong></h4>
        </header>
        <div class="panel-body">
        <!-- Adding Errors -->
           
            <?php echo form_open('savings-management/actual-savings-record/'.$individual_id, array("class" => "form-horizontal", "role" => "form"));?>
                <div class="row">
                        
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-lg-5 control-label">Savings Plan type: </label>
                            
                            <div class="col-lg-7">
                                <select class="form-control" name="actual_savings_plan_id" >
                                    <option value="">--Select Savings Plan--</option>
                                    <?php
                                        if($savings_plans->num_rows() > 0)
                                        {
                                            $savings_plan = $savings_plans->result();
                                            
                                            foreach($savings_plan as $res)
                                            {
                                                $db_savings_plan_id = $res->savings_plan_id;
                                                $savings_plan_name = $res->savings_plan_name;
                                                
                                                if($db_savings_plan_id == $savings_plan_id)
                                                {
                                                    echo '<option value="'.$db_savings_plan_id.'" selected>'.$savings_plan_name.'</option>';
                                                }
                                                
                                                else
                                                {
                                                    echo '<option value="'.$db_savings_plan_id.'">'.$savings_plan_name.'</option>';
                                                }
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-lg-5 control-label">Actual Opening Balance: </label>
                            
                            <div class="col-lg-7">
                                <input type="text" class="form-control" name="actual_opening_balance" placeholder="Actual Opening Balance" value="<?php echo $actual_opening_balance;?>">
                            </div>
                        </div>
                       
                     
                        
                    </div>
                   
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-lg-5 control-label">Proposed Monthly Payments: </label>
                            
                            <div class="col-lg-7">
                                <input type="text" class="form-control" name="first_month_payment"  placeholder="Proposed Monthly Payments" value="<?php echo $first_month_payment;?>">
                            </div>
                        </div>      
                       <div class="form-group">
                            <label class="col-lg-5 control-label">Application date: </label>
                            
                            <div class="col-lg-7">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="application_date" placeholder="Application date" value="<?php echo $application_date;?>">
                                </div>
                            </div>
                        </div>
                        

                        
                        
                        
                    </div>
                </div>
                <div class="row" style="margin-top:10px;">
                    <div class="col-md-12">
                        <div class="form-actions center-align">
                            <button class="btn btn-primary" type="submit">
                                Add Actual Saving Plan Details
                            </button>
                        </div>
                    </div>
                </div>
            <?php 
                echo form_close();
            ?>
        </div>
        <div class="col-md-12 center-align ">
            <h4><strong> Step 4: Member Savings History</strong></h4>
            
            <div class="panel-body">
                <div class="table-responsive">
                    
                    <?php echo $result;?>
            
                </div>
             </div>

        </div>
            
        </section>
    </section>


   



<script type="text/javascript">
    $(document).on("change","select#update_savings_plan",function(e)
    {
        $( "#saving_plan_details" ).html('');
        var savings_plan_id = $(this).val();
        
        //get department services
        $.get( "<?php echo site_url();?>savings/savings_application/get_savings_plan_details/"+savings_plan_id, function( data ) 
        {
            $( "#saving_plan_details" ).html( data );
        });
    });
    function calculate_dividend()
    {
        

         // $( "#loan_details" ).html('');

        var savings_plan_id = document.getElementById("update_savings_plan").value;
        var opening_balance =  document.getElementById("opening_balance").value;
        var monthly_payments = document.getElementById("monthly_payments").value;
        //alert(savings_plan_id);
       
        //get department services
        $.get( "<?php echo site_url();?>savings/savings_application/calculate_dividend/"+savings_plan_id+"/"+opening_balance+"/"+monthly_payments, function( data ) 
        {
            $( "#saving_plan_calculations" ).html( data );
        });

    }
</script>