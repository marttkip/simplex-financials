<?php

$all_leases =  $this->individual_model->get_individual_savings_plans($individual_id);
	foreach ($all_leases->result() as $leases_row)
	{
		$individual_savings_id = $leases_row->individual_savings_id;
	}
?>
<section class="panel panel-featured panel-featured-info">
    <header class="panel-heading">
    	<h2 class="panel-title pull-right"></h2>
    	<h2 class="panel-title">Approve Member Savings Plan</h2>

    </header>             

  <!-- Widget content -->
        <div class="panel-body">
	<?php
    echo form_open("record-savings-plan-approval/".$individual_id."/".$individual_savings_id, array("class" => "form-horizontal"));
    ?>
    <div class="row">
     <div class="col-md-4">
            <input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
            <div class="form-group">
                <div class="form-group">
					<label class="col-md-4 control-label">Approval Date: </label>
				  
					<div class="col-md-7">
						 <div class="input-group">
	                        <span class="input-group-addon">
	                            <i class="fa fa-calendar"></i>
	                        </span>
	                        <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="approved_date" placeholder="Approval Date" required>
	                    </div>
					</div>
				</div>
            </div>
            
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="col-lg-4 control-label">Approved Interest: </label>
                
                <div class="col-lg-8">
                	<div class="col-lg-12">
                	<input type="text" class="form-control" name="approved_interest" placeholder="Actual Approved Interest" >
                </div>
                </div>
            </div>
        </div>

        
 
    </div>
    <br>
    <div class="row">
        <div class="form-group">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="center-align">
                        <button type="submit" class="btn btn-info">Record</button>
                    </div>
                </div>
            </div>
    </div>
    
    
    <?php
    echo form_close();
    ?>

  </div>
</section>