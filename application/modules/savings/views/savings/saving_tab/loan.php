<?php
        $count = 0; 
        $result = '';

        
        //if users exist display them
        if ($individual_loan->num_rows() > 0)

        {   
            $result .= 
            '
            <table class="table table-bordered table-striped table-condensed">
                <thead>
                    <tr>
                        <th>#</th>
                        <th> Loans Plan Name</th>
                        <th> Start Date</th>
                        <th> End Date</th>
                        <th> No of Repayments</th>
                        <th> Disbursed Amount</th>
                        <th> Disbursed Date</th>
                        <th> Loan Cleared</th>
                        <th> Loan Status</th>
                       
                    </tr>
                </thead>
                  <tbody>
                  
            ';
            
           
            foreach ($individual_loan->result() as $row)
            {
                $individual_loan_id = $row->individual_loan_id;

                $individual_id = $row->individual_id;
                $loans_plan_name = $row->loans_plan_name;
                $start_date = $row->start_date;
                $end_date = $row->end_date;
                $disbursed_amount = $row->disbursed_amount;
                $no_of_repayments = $row->no_of_repayments;
                $disbursed_date = $row->disbursed_date;
                $loan_cleared = $row->individual_loan_cleared;
                $loan_status = $row->individual_loan_status;

                
                
                
                
                //loan status
                if($loan_status == 1)
                {
                    $status = '<span class="label label-success">Active</span>';
                }
                else
                {
                    $status = '<span class="label label-default">Deactivated</span>';
                }
                //loan cleared status
                if($loan_cleared == 1)
                {
                    $loan_cleared_status = '<span class="label label-success">Cleared</span>';
                }
                else
                {
                    $loan_cleared_status = '<span class="label label-default">Pending</span>';
                }
                
                
                
                $count++;
                $balance = '0';
               
                $result .= 
                '
                    <tr>
                        <td>'.$count.'</td>
                        <td>'.$loans_plan_name.'</td>
                        <td>'.$start_date.'</td>
                        <td>'.$end_date.'</td>
                        <td>'.$no_of_repayments.'</td>
                        <td>'.$disbursed_amount.'</td>
                        <td>'.$disbursed_date.'</td>
                        <td>'.$loan_cleared_status.'</td>
                        <td>'.$status.'</td>
                    
                    
                    </tr> 
                ';
            }
            
            $result .= 
            '
                          </tbody>
                        </table>
            ';
        }
        
        else
        {
            $result .= "There Are No Member Loan Plans";
        }

?>                    

 <section class="panel">
        <header class="panel-heading">
            <h4><strong> Current Member Applied Loans</strong></h4>
        </header>
        <div class="col-md-12 center-align ">
            
            <div class="panel-body">
                <div class="table-responsive">
                    
                    <?php echo $result;?>
            
                </div>
             </div>

        </div>
            
        </section>