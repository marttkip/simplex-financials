<?php
//savings_plan data
$row = $savings_plan->row();

$savings_plan_id = $row->savings_plan_id;
$savings_plan_name = $row->savings_plan_name;
$savings_plan_min_opening_balance =$row->savings_plan_opening_balance;
$savings_plan_min_interest_balance =$row->savings_plan_interest_balance;
$savings_plan_total_period =$row->period;
$interest_percentage =$row->interest;

$charge_withdrawal =$row->charge_withdrawal;
$interest_period_id =$row->compounding_period_id;
$transactional_withdrawal_charge =$row->withdrawal_charge;
$interest_type_id =$row->interest_type;


//repopulate data if validation errors occur
$validation_error = validation_errors();
				
if(!empty($validation_error))
{
$savings_plan_name = set_value('savings_plan_name');
$savings_plan_min_opening_balance = set_value('savings_plan_min_opening_balance');
$savings_plan_min_interest_balance = set_value('savings_plan_min_interest_balance');
$savings_plan_total_period = set_value('savings_plan_total_period');
$interest_percentage = set_value('interest_percentage');

$charge_withdrawal = set_value('charge_withdrawal');
$transactional_withdrawal_charge = set_value('transactional_withdrawal_charge');
$compounding_period_id = set_value('compounding_period_id');
$interest_type_id = set_value('interest_type_id');
}

?>          
            <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                	<div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <a href="<?php echo site_url();?>savings-management/setup-savings-plan" class="btn btn-info pull-right">Back to Savings Plan</a>
                        </div>
                    </div>
                        
                    <!-- Adding Errors -->
                    <?php
                    if(isset($error)){
                        echo '<div class="alert alert-danger"> Oh snap! Change a few things up and try submitting again. </div>';
                    }
                    
                    $validation_errors = validation_errors();
                    
                    if(!empty($validation_errors))
                    {
                        echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
                    }
                    ?>
                    
                    <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
<div class="row">
	<div class="col-md-6">
        <div class="form-group">
            <label class="col-lg-5 control-label">Savings Plan Name: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="savings_plan_name" placeholder="Savings Plan Name" value="<?php echo $savings_plan_name;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Mininum Opening Balance: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="savings_plan_min_opening_balance" placeholder="Mininum Opening Balance" value="<?php echo $savings_plan_min_opening_balance;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Mininum Interest Earning Balance: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="savings_plan_min_interest_balance" placeholder="Mininum Interest Earning Balance" value="<?php echo $savings_plan_min_interest_balance;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Total Period in Months: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="savings_plan_total_period" placeholder="Total Sales Plan Period" value="<?php echo $savings_plan_total_period;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Interest Percentage: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="interest_percentage" placeholder=" Interest Percentage" value="<?php echo $interest_percentage;?>">
            </div>
        </div>
        
        
	</div>
    
    <div class="col-md-6">
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Charge withdrawal: </label>
            
            <div class="col-lg-7">
            	<div class="radio">
                    <label>
                        <input type="radio" checked="checked" value="1" id="charge_withdrawal" name="charge_withdrawal">
                        Yes
                    </label>
                </div>
                
                <div class="radio">
                    <label>
                        <input type="radio" checked="" value="0" id="charge_withdrawal" name="charge_withdrawal">
                        No
                    </label>
                </div>
            </div>
        </div>
         <div class="form-group">
            <label class="col-lg-5 control-label">Transactional Withdrawal Charge: </label>
            
            <div class="col-lg-7">
                <input type="text" class="form-control" name="transactional_withdrawal_charge" placeholder="Transactional Withdrawal Charge" value="<?php echo $transactional_withdrawal_charge;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Interest compounding period: </label>
            
            <div class="col-lg-7">
                <select class="form-control" name="compounding_period_id">
                	<?php
                    	if($compounding_period->num_rows() > 0)
						{
							foreach($compounding_period->result() as $res)
							{
								$db_compounding_period_id = $res->compounding_period_id;
								$compounding_period_name = $res->compounding_period_name;
								
								if($db_compounding_period_id == $compounding_period_id)
								{
									echo '<option value="'.$db_compounding_period_id.'" selected>'.$compounding_period_name.'</option>';
								}
								
								else
								{
									echo '<option value="'.$db_compounding_period_id.'">'.$compounding_period_name.'</option>';
								}
							}
						}
					?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-5 control-label">Interest Type: </label>
            
            <div class="col-lg-7">
                <select class="form-control" name="interest_type_id">
                    <?php
                        if($interest->num_rows() > 0)
                        {
                            foreach($interest->result() as $res)
                            {
                                $db_interest_type_id = $res->interest_id;
                                $interest_type_name = $res->interest_name;
                                
                                if($db_interest_type_id == $interest_type_id)
                                {
                                    echo '<option value="'.$db_interest_type_id.'" selected>'.$interest_type_name.'</option>';
                                }
                                
                                else
                                {
                                    echo '<option value="'.$db_interest_type_id.'">'.$interest_type_name.'</option>';
                                }
                            }
                        }
                    ?>
                </select>
            </div>
        </div>
        
    </div>
</div>
<div class="row" style="margin-top:10px;">
	<div class="col-md-12">
        <div class="form-actions center-align">
            <button class="submit btn btn-primary" type="submit">
                Edit savings plan
            </button>
        </div>
    </div>
</div>
                    <?php echo form_close();?>
                </div>
            </section>