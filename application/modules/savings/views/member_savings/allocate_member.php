 <?php
               if($individual_other_documents->num_rows() > 0)
                {
                    $count = 0;
                        
                    $identification_result = 
                    '
                    <table class="table table-bordered table-striped table-condensed">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Document Name</th>
                                <th>Download Link</th>
                                <th colspan="3">Actions</th>
                            </tr>
                        </thead>
                          <tbody>
                          
                    ';
                    
                    foreach ($individual_other_documents->result() as $row)
                    {
                        $document_upload_id = $row->document_upload_id;
                        $document_name = $row->document_name;
                        $document_upload_name = $row->document_upload_name;
                        $document_status = $row->document_status;
                        
                        //create deactivated status display
                        if($document_status == 0)
                        {
                            $status = '<span class="label label-default">Deactivated</span>';
                            $button = '<a class="btn btn-info" href="'.site_url().'microfinance/activate-individual-identification/'.$document_upload_id.'/'.$individual_id.'" onclick="return confirm(\'Do you want to activate?\');" title="Activate "><i class="fa fa-thumbs-up"></i></a>';
                        }
                        //create activated status display
                        else if($document_status == 1)
                        {
                            $status = '<span class="label label-success">Active</span>';
                            $button = '<a class="btn btn-default" href="'.site_url().'microfinance/deactivate-individual-identification/'.$document_upload_id.'/'.$individual_id.'" onclick="return confirm(\'Do you want to deactivate ?\');" title="Deactivate "><i class="fa fa-thumbs-down"></i></a>';
                        }
                        
                        $count++;
                        $identification_result .= 
                        '
                            <tr>
                                <td>'.$count.'</td>
                                <td>'.$document_name.'</td>
                                <td><a href="'.$this->document_upload_location.''.$document_upload_name.'" target="_blank" >Download Here</a></td>
                                <td>'.$status.'</td>
                                
                            </tr> 
                        ';
                    }
                    
                    $identification_result .= 
                    '
                                  </tbody>
                                </table>
                    ';
                }
                
                else
                {
                    $identification_result = "<p>No plans have been added</p>";
                }
               
               ?>


<?php
    if($jobs->num_rows() > 0)
    {
        $count = 0;
            
        $result_jobs = 
        '
        <table class="table table-bordered table-striped table-condensed">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Place of work</th>
                    <th>Position</th>
                    <th>Employment date</th>
                    <th>Status</th>
                </tr>
            </thead>
              <tbody>
              
        ';
        
        foreach ($jobs->result() as $row)
        {
            $individual_job_id = $row->individual_job_id;
            $job = $row->job_title;
            $employer = $row->employer;
            $individual_job_status = $row->individual_job_status;
            $employment_date = date('jS M Y',strtotime($row->employment_date));
            
            //create deactivated status display
            if($individual_job_status == 0)
            {
                $status = '<span class="label label-default">Former</span>';
                $button = '<a class="btn btn-info" href="'.site_url().'microfinance/activate-position/'.$individual_job_id.'/'.$individual_id.'" onclick="return confirm(\'Do you want to activate '.$job.'?\');" title="Activate '.$job.'"><i class="fa fa-thumbs-up"></i></a>';
            }
            //create activated status display
            else if($individual_job_status == 1)
            {
                $status = '<span class="label label-success">Employed</span>';
                $button = '<a class="btn btn-default" href="'.site_url().'microfinance/deactivate-position/'.$individual_job_id.'/'.$individual_id.'" onclick="return confirm(\'Do you want to deactivate '.$job.'?\');" title="Deactivate '.$job.'"><i class="fa fa-thumbs-down"></i></a>';
            }
            
            $count++;
            $result_jobs .= 
            '
                <tr>
                    <td>'.$count.'</td>
                    <td>'.$employer.'</td>
                    <td>'.$job.'</td>
                    <td>'.$employment_date.'</td>
                    <td>'.$status.'</td>
                </tr> 
            ';
        }
        
        $result_jobs .= 
        '
                      </tbody>
                    </table>
        ';
    }
    
    else
    {
        $result_jobs = "No positions have been assigned";
    }

$employer = '';
$job_title = '';
$employment_date = '';

//repopulate data if validation errors occur
$validation_error = validation_errors();
                
if(!empty($validation_error))
{
    $employer = set_value('employer');
    $job_title = set_value('job_title');
    $employment_date = set_value('employment_date');
}
?>







<?php


//individual data
$row = $individual->row();

$individual_lname = $row->individual_lname;
$individual_mname = $row->individual_mname;
$individual_fname = $row->individual_fname;
$individual_dob = $row->individual_dob;
$individual_email = $row->individual_email;
$individual_phone = $row->individual_phone;
$individual_phone2 = $row->individual_phone2;
$individual_address = $row->individual_address;
$civil_status_id = $row->civilstatus_id;
$individual_civil_status = $this->individual_model->get_civil_status_name($civil_status_id);
$individual_locality = $row->individual_locality;
$title_id = $row->title_id;
$individual_title = $this->individual_model->get_loan_individual_title($title_id);
$branch_id = $row->branch_id;
$gender_id = $row->gender_id;
$individual_gender = $this->loans_plan_model->get_loan_gender_name($gender_id);
$individual_city = $row->individual_city;
$individual_number = $row->individual_number;
$individual_post_code = $row->individual_post_code;
$document_id = $row->document_id;
$individual_document = $this->individual_model->get_document_name($document_id);
$document_number = $row->document_number;
$document_place = $row->document_place;
$individual_email2 = $row->individual_email2;
$individual_type_id = $row->individual_type_id;
$individual_type = $this->individual_model->get_individual_type_name($individual_type_id);
$kra_pin = $row->kra_pin;
$outstanding_loan = $row->outstanding_loan;
$total_savings = $row->total_savings;

//repopulate data if validation errors occur
$validation_error = validation_errors();
                
if(!empty($validation_error))
{
    $individual_onames = set_value('individual_onames');
    $individual_fname = set_value('individual_fname');
    $individual_dob = set_value('individual_dob');
    $individual_email = set_value('individual_email');
    $individual_phone = set_value('individual_phone');
    $individual_phone2 = set_value('individual_phone2');
    $civil_status_id = set_value('civil_status_id');
    $individual_locality = set_value('individual_locality');
    $title_id = set_value('title_id');
    $gender_id = set_value('gender_id');
    $individual_number = set_value('individual_number');
    $individual_address = set_value('individual_address');
    $individual_city = set_value('individual_city');
    $individual_post_code = set_value('individual_post_code');
    $document_id = set_value('document_id');
    $document_number = set_value('document_number');
    $document_place = set_value('document_place');
    $individual_email2 = set_value('individual_email2');
    $individual_type_id = set_value('individual_type_id');
    $kra_pin = set_value('kra_pin');
    $branch_id = set_value('branch_id');
    $outstanding_loan = set_value('outstanding_loan');
    $total_savings = set_value('total_savings');
}

?>





<?php
    if($emergency_contacts->num_rows() > 0)
    {
        $count = 0;
            
        $result = 
        '
        <table class="table table-bordered table-striped table-condensed">
            <thead>
                <tr>
                    <th>#</th>
                    <th>First name</a></th>
                    <th>Other names</a></th>
                    <th>Relationship</a></th>
                    <th>Phone</a></th>
                    <th>Share Percentage</a></th>
                </tr>
            </thead>
              <tbody>
              
        ';
        
        foreach ($emergency_contacts->result() as $row)
        {
            $individual_emergency_id = $row->individual_emergency_id;
            $individual_emergency_fname = $row->individual_emergency_fname;
            $individual_emergency_onames = $row->individual_emergency_onames;
            $individual_emergency_phone = $row->individual_emergency_phone;
            $relationship_name = $row->relationship_name;
            $share_percentage = $row->share_percentage;
            $individual_emergency_status = $row->individual_emergency_status;
            $created = date('jS M Y H:i a',strtotime($row->created));
            $last_modified = date('jS M Y H:i a',strtotime($row->last_modified));
            
            //create deactivated status display
            if($individual_emergency_status == 0)
            {
                $status = '<span class="label label-important">Deactivated</span>';
                $button = '<a class="btn btn-info" href="'.site_url().'human-resource/activate-position/'.$individual_emergency_id.'" onclick="return confirm(\'Do you want to activate '.$individual_emergency_fname.'?\');" title="Activate '.$individual_emergency_fname.'"><i class="fa fa-thumbs-up"></i></a>';
            }
            //create activated status display
            else if($individual_emergency_status == 1)
            {
                $status = '<span class="label label-success">Active</span>';
                $button = '<a class="btn btn-default" href="'.site_url().'human-resource/deactivate-position/'.$individual_emergency_id.'" onclick="return confirm(\'Do you want to deactivate '.$individual_emergency_fname.'?\');" title="Deactivate '.$individual_emergency_fname.'"><i class="fa fa-thumbs-down"></i></a>';
            }
            
            $count++;
            $result .= 
            '
                <tr>
                    <td>'.$count.'</td>
                    <td>'.$individual_emergency_fname.'</td>
                    <td>'.$individual_emergency_onames.'</td>
                    <td>'.$relationship_name.'</td>
                    <td>'.$individual_emergency_phone.'</td>
                    <td>'.$share_percentage.'</td>
                </tr> 
            ';
        }
        
        $result .= 
        '
                      </tbody>
                    </table>
        ';
    }
    
    else
    {
        $result = "<p>No next of kin have been added</p>";
    }
    

//repopulate data if validation errors occur
$validation_error = validation_errors();
                
if(!empty($validation_error))
{
    $relationship_id = set_value('relationship_id');
    $individual_emergency_fname = set_value('individual_emergency_fname');
    $individual_emergency_onames = set_value('individual_emergency_onames');
    $individual_emergency_phone = set_value('individual_emergency_phone');
    $individual_emergency_phone2 = set_value('individual_emergency_phone2');
    $individual_emergency_email = set_value('individual_emergency_email');
    $individual_emergency_email2 = set_value('individual_emergency_email2');
    $document_id = set_value('document_id');
    $document_number = set_value('document_number');
    $document_place = set_value('document_place');
    $individual_emergency_address = set_value('individual_emergency_address');
    $individual_emergency_city = set_value('individual_emergency_city');
    $individual_emergency_post_code = set_value('individual_emergency_post_code');
    $individual_emergency_dob = set_value('individual_emergency_dob');
}

else
{
    $relationship_id = '';
    $individual_emergency_fname = '';
    $individual_emergency_onames = '';
    $individual_emergency_phone = '';
    $individual_emergency_phone2 = '';
    $individual_emergency_email = '';
    $individual_emergency_email2 = '';
    $document_id = '';
    $document_number = '';
    $document_place = '';
    $individual_emergency_address = '';
    $individual_emergency_city = '';
    $individual_emergency_dob = '';
    $individual_emergency_post_code = '';
}
    ?>
<section class="panel">
    <div class="panel-body">
                    <div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <a href="<?php echo site_url();?>savings-management/member-savings" class="btn btn-info pull-right">Back to Member Savings</a>
                        </div>
                    </div>
                        
                    <!-- Adding Errors -->
                    <?php
                    if(isset($error)){
                        echo '<div class="alert alert-danger"> Oh snap! Change a few things up and try submitting again. </div>';
                    }
                    
                    $validation_errors = validation_errors();
                    
                    if(!empty($validation_errors))
                    {
                        echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
                    }
                    ?>
                    
                    <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
        <header class="panel-heading">                      
            <h2 class="panel-title">Member Details </h2>
        </header>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12"> 
                    <div class="col-md-4"> 
                        <section class="panel">
                            <header class="panel-heading">                      
                                <h2 class="panel-title">Member Personal Information </h2>
                            </header>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-condensed">
                                        <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Detail</th>
                                            </tr>
                                        </thead>
                                       <tbody>
                                            <tr><td><span>Member Name:</span></td><td><?php echo $individual_title.' '.$individual_fname.' '.$individual_mname.' '.$individual_lname;?></td></tr>
                                            <tr><td><span>Member Number:</span></td><td><?php echo $individual_number;?></td></tr>
                                            <tr><td><span>Member Gender:</span></td><td><?php echo $individual_gender;?></td></tr>
                                            <tr><td><span>Member Civil Status:</span></td><td><?php echo $individual_civil_status;?></td></tr>
                                            <tr><td><span>Member Document:</span></td><td><?php echo $individual_document.' '.$document_number;?></td></tr>
                                            <tr><td><span>Member City:</span></td><td><?php echo $document_place;?></td></tr>
                                            <tr><td><span>Member Type:</span></td><td><?php echo $individual_type;?></td></tr>
                                            
                                        </tbody>
                                      </table>
                                </div>
                            </div>
                        </section>
                    </div>
                        <div class="col-md-4"> 
                        <section class="panel">
                            <header class="panel-heading">                      
                                <h2 class="panel-title">Member Contact Information </h2>
                            </header>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-condensed">
                                        <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Detail</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                            <tr><td><span>Member Phone Number:</span></td><td><?php echo $individual_phone;?></td></tr>
                                            <tr><td><span>Member Email Address:</span></td><td><?php echo $individual_email;?></td></tr>
                                            <tr><td><span>Member 2nd Phone Number:</span></td><td><?php echo $individual_phone2;?></td></tr>
                                            <tr><td><span>Member Address:</span></td><td><?php echo $individual_address;?></td></tr>
                                            <tr><td><span>Member Postal Address:</span></td><td><?php echo $individual_post_code;?></td></tr>
                                            <tr><td><span>Member Residence:</span></td><td><?php echo $individual_city;?></td></tr>
                                            
                                        </tbody>
                                      </table>
                                </div>
                            </div>
                        </section>
                    </div>
                       <div class="col-md-4"> 
                        <section class="panel">
                            <header class="panel-heading">                      
                                <h2 class="panel-title">Member Photographic Evidence </h2>
                            </header>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="max-width:200px; max-height:200px;">
                                            <img src="<?php echo $image_location;?>" class="img-responsive">
                                        </div>
                                    </div>
                                     <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="max-width:200px; max-height:200px;">
                                        <img src="<?php echo $signature_location;?>" class="img-responsive">
                                    </div>
                                    
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
            <?php echo form_close();?>
    </div>
</section>
                     

<?php
        $count = 0; 
        $result = '';

        
        //if users exist display them
        if ($individual_savings->num_rows() > 0)

        {   
            $result .= 
            '
            <table class="table table-bordered table-striped table-condensed">
                <thead>
                    <tr>
                        <th>#</th>
                        <th> Savings Plan Name</th>
                        <th> Start Date</th>
                        <th> End Date</th>
                        <th> Starting Amount</th>
                        <th> Status</th>
                        <th> Saving Payments</th>
                        <th> Saving Withdrawals</th>
                        <th> Saving Balances</th>
                        <th> Saving Dividends</th>
                        <th colspan="5">Actions</th>
                    </tr>
                </thead>
                  <tbody>
                  
            ';
            
           
            foreach ($individual_savings->result() as $row)
            {
                $individual_savings_id = $row->individual_savings_id;

                $savings_plan_id = $row->savings_plan_id;
                $savings_plan_name = $this->member_savings_model->get_savings_plan_name($savings_plan_id);
                $individual_savings_status = $row->individual_savings_status;
                $savings_start_amount = $row->individual_savings_amount;
                $savings_start_date = $row->start_date;
                $savings_end_date = $row->end_date;
                
                
                
                
                //status
                if($individual_savings_status == 1)
                {
                    $status = 'Active';
                }
                else
                {
                    $status = 'Disabled';
                }
                
                //create deactivated status display
                if($individual_savings_status == 0)
                {
                    $status = '<span class="label label-default">Deactivated</span>';
                    $button = '<a class="btn btn-info" href="'.site_url().'microfinance/activate-individual/'.$individual_savings_id.'" onclick="return confirm(\'Do you want to activate '.$savings_plan_name.'?\');" title="Activate '.$savings_plan_name.'"><i class="fa fa-thumbs-up"></i></a>';
                }
                //create activated status display
                else if($individual_savings_status == 1)
                {
                    $status = '<span class="label label-success">Active</span>';
                    $button = '<a class="btn btn-default" href="'.site_url().'microfinance/deactivate-individual/'.$individual_savings_id.'" onclick="return confirm(\'Do you want to deactivate '.$savings_plan_name.'?\');" title="Deactivate '.$savings_plan_name.'"><i class="fa fa-thumbs-down"></i></a>';
                }
                
                $count++;
                $balance = '0';
                $member_payments = $this->member_savings_model->get_actual_individual_savings_payments($individual_savings_id);
                $member_withdrawals = $this->member_savings_model->get_actual_individual_savings_withdrawals($individual_savings_id);
                $member_balances = $member_payments - $member_withdrawals;
                $member_dividends = $this->member_savings_model->get_actual_individual_savings_dividends($individual_savings_id);
                $result .= 
                '
                    <tr>
                        <td>'.$count.'</td>
                        <td>'.$savings_plan_name.'</td>
                        <td>'.$savings_start_date.'</td>
                        <td>'.$savings_start_date.'</td>
                        <td>'.$savings_start_amount.'</td>
                        <td>'.$status.'</td>
                        <td>'.$member_payments.'</td>
                        <td>'.$member_withdrawals.'</td>
                        <td>'.$member_balances.'</td>
                        <td>'.$member_dividends.'</td>
                        
                         <td><a href="'.site_url().'savings-management/member-savings/account-detail/'.$individual_id.'/'.$individual_savings_id.'" class="btn btn-warning">Account Detail</a></td>
                        
                        <td><a href="'.site_url().'send-dividend-statement/'.$individual_savings_id.'" class="btn btn-primary">SMS Dividend</a></td>

                        <td><a href="'.site_url().'microfinance/individual/download_statement/'.$individual_savings_id.'" class="btn btn-danger">Download</a></td>
                        <td><a href="'.site_url().'microfinance/send-money/'.$individual_savings_id.'" class="btn btn-sm btn-success" title="Money '.$savings_plan_name.'"><i class="fa fa-money"></i></a></td>
                        <td>'.$button.'</td>
                    </tr> 
                ';
            }
            
            $result .= 
            '
                          </tbody>
                        </table>
            ';
        }
        
        else
        {
            $result .= "There are no savings plans";
        }

?>


<section class="panel">
    <header class="panel-heading">                      
        <h2 class="panel-title"><?php echo $title;?></h2>
    </header>
    <div class="panel-body">
        <div class="table-responsive">
            
            <?php echo $result;?>
    
        </div>
    </div>
    <div class="panel-footer">
        <?php if(isset($links)){echo $links;}?>
    </div>
</section>



                      