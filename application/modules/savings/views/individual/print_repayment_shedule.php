<?php
$result = '';
$individual = $this->individual_model->get_individual($individual_id);
$row = $individual->row();

$individual_lname = $row->individual_lname;
$individual_mname = $row->individual_mname;
$individual_fname = $row->individual_fname;
$individual_loan = $this->individual_model->get_individual_loans($individual_loan_id);
if($individual_loan->num_rows() > 0)
{
	$count = 0;
	foreach ($individual_loan->result() as $row)
	{
		$count++;
		$loans_plan_name = $row->loans_plan_name;
		$proposed_amount = $row->proposed_amount;
		$approved_amount = $row->approved_amount;
		$disbursed_amount = $row->disbursed_amount;
		$purpose = $row->purpose;
		$installment_type_duration = $row->installment_type_duration;
		$no_of_repayments = $row->no_of_repayments;
		$interest_rate = $row->interest_rate;
		$interest_id = $row->interest_id;
		$grace_period = $row->grace_period;
		$application_date = date('jS M Y',strtotime($row->application_date));
		$approved_date = date('jS M Y',strtotime($row->approved_date));
		$disbursed_date = date('jS M Y',strtotime($row->disbursed_date));
		$created_by = $row->created_by;
		$approved_by = $row->approved_by;
		$disbursed_by = $row->disbursed_by;
		$individual_loan_status = $row->individual_loan_status;
		$first_date = $row->application_date;
	}
}
$loan_amount = $this->individual_model->get_loan_amount($individual_loan_id);
if($loan_amount > 0)
{
	$result .= '
	<div class = "row" align="center">
		 <h4>Repayment Details for '.$individual_lname.' '.$individual_fname.' '.$individual_mname.' </h4>
	</div>
	</br>
	<table class="table table-condensed table-striped table-hover table-bordered">
		<tr>
			<th>#</th>
			<th>Dates of repayment</th>
			<th>Start bal.</th>
			<th>Interest</th>
			<th>Principal payment</th>
			<th>End bal.</th>
			<th>Cummulative interest</th>
			<th>Cummulative payments</th>
		</tr>
	';
	$cummulative_interest = 0;
	$cummulative_principal = 0;
	$start_balance = $loan_amount;
	$total_days = 0;
	
	//display all payment dates
	for($r = 0; $r < $no_of_repayments; $r++)
	{
		$total_days += $installment_type_duration;
		$count = $r+1;
		$payment_date = date('jS M Y', strtotime($first_date. ' + '.$total_days.' days'));
		
		//straight line
		if($interest_id == 1)
		{
			//$interest_payment = ($loan_amount * ($interest_rate/100)) / $no_of_repayments;
			$interest_payment = ($loan_amount * ($interest_rate/100));
		}
		
		//reducing balance
		else
		{
			//$interest_payment = ($start_balance * ($interest_rate/100)) / $no_of_repayments;
			$interest_payment = ($start_balance * ($interest_rate/100));
		}
		$principal_payment = round(($loan_amount / $no_of_repayments),-3);
		$end_balance = $start_balance - $principal_payment;
		$cummulative_interest += $interest_payment;
		$cummulative_principal += $principal_payment;
		
		if ($count == $no_of_repayments)
		{
			$principal_payment = $start_balance;
			$end_balance = $start_balance - $principal_payment;
			$cummulative_principal = $loan_amount;
		}
		
		$result .= '
		<tr>
			<td>'.$count.'</td>
			<td>'.$payment_date.'</td>
			<td>'.number_format($start_balance, 2).'</td>
			<td>'.number_format($interest_payment, 2).'</td>
			<td>'.number_format($principal_payment, 2).'</td>
			<td>'.number_format($end_balance, 2).'</td>
			<td>'.number_format($cummulative_interest, 2).'</td>
			<td>'.number_format($cummulative_principal, 2).'</td>
		</tr>';
		$start_balance -= $principal_payment;
	}	
	$total_repayment_per_month = 0;
	for($i=1;$i<=$no_of_repayments;$i++)
	{
		$total_start_balance = $cummulative_principal + $cummulative_interest;
		$monthly_repayment = round(($total_start_balance/ $no_of_repayments),-2);
		if($i==$no_of_repayments)
		{//echo $total_monthly_repayment;die();
			for($i=1;$i<$no_of_repayments;$i++)
			{
				$month_fee = round(($total_start_balance/ $no_of_repayments),-2);
				$total_repayment_per_month += $month_fee;
				$monthly_repayment = $total_start_balance - $total_repayment_per_month;
			}
		}
		$result .= '
	<tr>
		<td colspan="7">Monthly repayment '.$i.'</td>
		<td>'.number_format($monthly_repayment,2).'</td>
	</tr>
	';
	}
	$result .= '
	</table>
	';
}
?><html>
    <head>
        <title>Repayment Schedule</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css">
		<style type="text/css">
			.receipt_spacing{letter-spacing:0px; font-size: 0.8em !important;}
			.center-align{margin:0 auto; text-align:center;}
			
			.receipt_bottom_border{border-bottom: #888888 medium solid;}
			.row .col-md-12 table {
				border:solid #000 !important;
				border-width:1px 0 0 1px !important;
				font-size:10px;
			}
			.row .col-md-12 th, .row .col-md-12 td {
				border:solid #000 !important;
				border-width:0 1px 1px 0 !important;
			}
			.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
			{
				 padding: 2px;
			}
			
			.row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
			.title-img{float:left; padding-left:30px;}
			img.logo{max-height:70px; margin:0 auto;}
		</style>
        <script type="text/javascript" src="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/jquery/jquery.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>tableExport.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>jquery.base64.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>html2canvas.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>libs/sprintf.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>jspdf.js"></script>
        <script type="text/javascript" src="<?php echo base_url()."assets/jspdf/";?>libs/base64.js"></script>
    </head>
    <body class="receipt_spacing">
        <?php echo $result;?>
        <a href="#" onClick ="$('#customers').tableExport({type:'excel',escape:'false'});">EXCEL DOWNLOAD</a>
    </body>
</html>