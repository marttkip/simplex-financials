<?php
$result = '';

if($loan_amount > 0)
{
	$result .= '
	<table class="table table-condensed table-striped table-hover table-bordered">
		<tr>
			<th>#</th>
			<th>Dates of repayment</th>
			<th>Start bal.</th>
			<th>Interest</th>
			<th>Principal payment</th>
			<th>End bal.</th>
			<th>Cummulative interest</th>
			<th>Cummulative payments</th>
		</tr>
	';
	$cummulative_interest = 0;
	$cummulative_principal = 0;
	$start_balance = $loan_amount;
	$total_days = 0;
	
	//display all payment dates
	for($r = 0; $r < $no_of_repayments; $r++)
	{
		$total_days += $installment_type_duration;
		$count = $r+1;
		$payment_date = date('jS M Y', strtotime($first_date. ' + '.$total_days.' days'));
		
		//straight line
		if($interest_id == 1)
		{
			//$interest_payment = ($loan_amount * ($interest_rate/100)) / $no_of_repayments;
			$interest_payment = ($loan_amount * ($interest_rate/100));
		}
		
		//reducing balance
		else
		{
			//$interest_payment = ($start_balance * ($interest_rate/100)) / $no_of_repayments;
			$interest_payment = ($start_balance * ($interest_rate/100));
		}
		$principal_payment = round(($loan_amount / $no_of_repayments),-3);
		$end_balance = $start_balance - $principal_payment;
		$cummulative_interest += $interest_payment;
		$cummulative_principal += $principal_payment;
		
		if ($count == $no_of_repayments)
		{
			$principal_payment = $start_balance;
			$end_balance = $start_balance - $principal_payment;
			$cummulative_principal = $loan_amount;
		}
		
		//for each month, insert the principal and interest expected for that loan;
		//$this->payments_model->update_amortization_table($count,$interest_payment,$principal_payment,$individual_loan_id);
		
		$result .= '
		<tr>
			<td>'.$count.'</td>
			<td>'.$payment_date.'</td>
			<td>'.number_format($start_balance, 2).'</td>
			<td>'.number_format($interest_payment, 2).'</td>
			<td>'.number_format($principal_payment, 2).'</td>
			<td>'.number_format($end_balance, 2).'</td>
			<td>'.number_format($cummulative_interest, 2).'</td>
			<td>'.number_format($cummulative_principal, 2).'</td>
		</tr>';
		$start_balance -= $principal_payment;
	}	
	$total_repayment_per_month = 0;
	for($i=1;$i<=$no_of_repayments;$i++)
	{
		$total_start_balance = $cummulative_principal + $cummulative_interest;
		$monthly_repayment = round(($total_start_balance/ $no_of_repayments),-2);
		if($i==$no_of_repayments)
		{//echo $total_monthly_repayment;die();
			for($i=1;$i<$no_of_repayments;$i++)
			{
				$month_fee = round(($total_start_balance/ $no_of_repayments),-2);
				$total_repayment_per_month += $month_fee;
				$monthly_repayment = $total_start_balance - $total_repayment_per_month;
			}
		}
		$result .= '
	<tr>
		<td colspan="7">Monthly repayment '.$i.'</td>
		<td>'.number_format($monthly_repayment,2).'</td>
	</tr>
	';
	}
	$result .= '
	</table>
	';
	
}
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Amortization table</h2>
    </header>
    <div class="panel-body">
        
        <?php 
            echo $result;
        ?>
        
    </div>
	</br>
	<div class="row" style="margin-bottom:20px;">
		<div class="col-lg-offset-10 col-lg-2">
			<a href="<?php echo site_url().'microfinance/export-repayment-schedule/'.$individual_id.'/'.$individual_loan_id ;?>" title = "Download Repayment Schedule" class="btn btn-success">Export</a>
		</div>
	</div>
</section>