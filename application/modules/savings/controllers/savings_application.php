<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/microfinance/controllers/microfinance.php";
		
// include autoloader
//require_once "./application/libraries/dompdf/autoload.inc.php";
	
// reference the Dompdf namespace
//use Dompdf\Dompdf;

class Savings_application extends microfinance 
{
	var $individual_path;
	var $individual_location;
	var $signature_path;
	var $signature_location;
	
	function __construct()
	{
		parent:: __construct();
		
		$this->load->library('image_lib');

		$this->load->model('member_savings_model');
		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/users_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/file_model');
		$this->load->model('admin/admin_model');
		$this->load->model('individual_model');
		$this->load->model('group_model');
		$this->load->model('savings_plan_model');
		$this->load->model('microfinance/loans_plan_model');
		$this->load->model('payments_model');
		$this->load->model('withdrawals_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('microfinance_model');
		
		//path to image directory
		$this->individual_path = realpath(APPPATH . '../assets/img/individuals');
		$this->individual_location = base_url().'assets/img/individuals/';
		$this->signature_path = realpath(APPPATH . '../assets/img/signatures');
		$this->signature_location = base_url().'assets/img/signatures/';
		$this->identification_document_path = realpath(APPPATH . '../assets/img/identification_documents');
		$this->identification_document_location = base_url().'assets/img/identification_documents/';

		$this->document_upload_path = realpath(APPPATH . '../assets/img/document_uploads');
		$this->document_upload_location = base_url().'assets/img/document_uploads/';
	}
    
	/*
	*
	*	Default action is to show all the individual
	*
	*/
	public function index($order = 'individual_lname', $order_method = 'ASC') 
	{
		$individual_search = $this->session->userdata('individual_search');
		//$where = '(visit_type_id <> 2 OR visit_type_id <> 1) AND individual_delete = '.$delete;
		$where = 'individual_id > 0';
		if(!empty($individual_search))
		{
			$where .= $individual_search;
		}
		
		$table = 'individual';
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'savings-management/savings-application/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#"';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->member_savings_model->get_all_individual($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Members';
		
		$search_title = $this->session->userdata('individual_search_title');
			
		if(!empty($search_title))
		{
			$v_data['title'] = 'Members filtered by :'.$search_title;
		}
		
		else
		{
			$v_data['title'] = $data['title'];
		}
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['all_individual'] = $this->member_savings_model->all_individual();
		$v_data['individual_types'] = $this->member_savings_model->get_individual_types();
			
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('savings/savings/all_members', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
    
	public function apply_savings_plan($individual_id, $image_location = NULL, $signature_location = NULL) 
	{
		//open the add new individual
		$data['title'] = 'Savings Plan Application';
		$v_data['title'] = $data['title'];
		$v_data['withdrawal_type'] = $this->withdrawals_model->get_withdrawal_type();
		$v_data['individual'] = $this->individual_model->get_individual($individual_id);
		$row = $v_data['individual']->row();
		
		if($image_location == NULL)
		{
			$img = $row->image;
			
			if((empty($img)) || ($img == '0'))
			{
				$image_location = 'http://placehold.it/200x200?text=image';
			}
			
			else
			{
				$image_location = $this->individual_location.$img;
			}
		}
		
		else
		{
			$image_location = $this->individual_location.$image_location;
		}
		
		if($signature_location == NULL)
		{
			$img = $row->signature;
			
			if((empty($img)) || ($img == '0'))
			{
				$signature_location = 'http://placehold.it/200x100?text=signature';
			}
			
			else
			{
				$signature_location = $this->signature_location.$img;
			}
		}
		
		else
		{
			$signature_location = $this->signature_location.$signature_location;
		}
		
		$v_data['image_location'] = $image_location;
		$v_data['signature_location'] = $signature_location;
		$v_data['individual_id'] = $individual_id;
		$v_data['personnel'] = $this->personnel_model->retrieve_personnel();
		$v_data['relationships'] = $this->individual_model->get_relationship();
		$v_data['religions'] = $this->individual_model->get_religion();
		$v_data['civil_statuses'] = $this->individual_model->get_civil_status();
		$v_data['titles'] = $this->individual_model->get_title();
		$v_data['genders'] = $this->individual_model->get_gender();
		$v_data['job_titles_query'] = $this->individual_model->get_job_titles();
		$v_data['payments'] = $this->individual_model->get_loan_payments($individual_id);
		$v_data['savings_payments'] = $this->individual_model->get_savings_payments($individual_id);
		$v_data['emergency_contacts'] = $this->individual_model->get_emergency_contacts($individual_id);
		$v_data['dependants'] = $this->individual_model->get_individual_dependants($individual_id);
		$v_data['jobs'] = $this->individual_model->get_individual_jobs($individual_id);
		$v_data['individual_savings'] = $this->individual_model->get_individual_savings_plans($individual_id);
		$v_data['savings_withdrawal_amount']= $this->individual_model->get_loan_repayment_amount($individual_id);

		$v_data['individual_loan'] = $this->individual_model->get_individual_loan_plan($individual_id);
		$v_data['individual_identifications'] = $this->individual_model->get_individual_identifications($individual_id);
		$v_data['individual_other_documents'] = $this->individual_model->get_document_uploads($individual_id);
		$v_data['all_savings_payments'] = $this->individual_model->get_all_savings_payments($individual_id);
		$v_data['disbursments'] = $this->individual_model->get_disbursments($individual_id);
		$v_data['savings_plans'] = $this->savings_plan_model->all_savings_plan();
		$v_data['loans_plans'] = $this->loans_plan_model->all_loans_plan();
		$v_data['payment_methods'] = $this->loans_plan_model->all_payment_methods();
		$v_data['individual_types'] = $this->individual_model->get_individual_types();
		
		$v_data['parent_sections'] = $this->sections_model->all_parent_sections('section_position');
		$v_data['savings_withdrawals'] = $this->individual_model->get_savings_withdrawals($individual_id);
		$v_data['withdrawal_type'] = $this->withdrawals_model->get_withdrawal_type();
		$v_data['individual_types'] = $this->individual_model->get_individual_types();
		$v_data['savings_plan'] = $this->member_savings_model->get_savings_plans();
		$data['content'] = $this->load->view('savings/savings/savings_tab', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function calculate_dividend($savings_plan_id, $opening_balance, $monthly_payments)
	{   

		$savings_plan = $this->savings_plan_model->get_savings_plan($savings_plan_id);
		$result = '';
		
		if($savings_plan->num_rows() > 0)
		{
			$row = $savings_plan->row();
			$savings_plan_id = $row->savings_plan_id;
			$savings_plan_name = $row->savings_plan_name;
			$savings_plan_min_opening_balance = number_format($row->savings_plan_opening_balance, 0);
			$savings_plan_min_interest_balance = number_format($row->savings_plan_interest_balance, 0);
			$savings_plan_total_period = $row->period;
			$interest_percentage = $row->interest;

			$charge_withdrawal = $row->charge_withdrawal;
			$interest_type_id = $row->interest_type;
			$transactional_withdrawal_charge = $row->withdrawal_charge;

			$compounding_period_id = $row->interest_period_id;
			$compounding_period_name = $this->savings_plan_model->get_the_compounding_period($compounding_period_id);

			$savings_plan_status = $row->savings_plan_status;
			
			//status
			if($charge_withdrawal == 1)
			{
				$charge_withdrawal = 'Yes';
			}
			else
			{
				$charge_withdrawal = 'No';
			}

			if($opening_balance < $savings_plan_min_opening_balance)
			{
				$result = 'The Opening Balance is less than the Minimum Opening Balance of Savings Plan';
			}
			else
			{
				if($interest_type_id == 1)
				{
					$annual_amount = ($opening_balance + ($monthly_payments * 12));
				    $annual_interest = $interest_percentage/100;
				    $annual_dividend = $annual_amount * $annual_interest;
				    $result = '<table class="table table-bordered table-striped table-condensed">
							        <thead>
							            <tr>
							               
							                <th>Annual Amount</th>
							                <th>Opening Balance</th>
							                <th>Monthly Payments</th>
							                <th>% Percentage Interest</th>
							                <th>Annual Dividend</th>
							            </tr>
							        </thead>
							          <tbody>';
				    $result.= '
					                <tr>
					                    <td>'.$annual_amount.'</td>
					                    <td>'.$opening_balance.'</td>
					                    <td>'.$monthly_payments.'</td>
					                    <td>'.$interest_percentage.'</td>
					                    <td>'.$annual_dividend.'</td>
					                </tr> 
					                </tbody>
                                    </table> 
					            ';


				}
				else
				{
					$annual_amount = ($opening_balance + ($monthly_payments * 12));
					$annual_interest = $interest_percentage/100;
					$annual_dividend = $annual_amount * $annual_interest;
					$annual_amount = number_format($annual_amount, 0);
					$opening_balance = number_format($opening_balance, 0);
					$monthly_payments = number_format($monthly_payments, 0);
					$annual_dividend = number_format($annual_dividend, 0);
					  $result = '<table class="table table-bordered table-striped table-condensed">
							        <thead>
							            <tr>
							               
							                <th>Annual Amount</th>
							                <th>Opening Balance</th>
							                <th>Average Monthly Payments</th>
							                <th>% Percentage Interest</th>
							                <th>Estimated Annual Dividend</th>
							            </tr>
							        </thead>
							          <tbody>';
				    $result.= '
					                <tr>
					                    <td>'.$annual_amount.'</td>
					                    <td>'.$opening_balance.'</td>
					                    <td>'.$monthly_payments.'</td>
					                    <td>'.$interest_percentage.'</td>
					                    <td>'.$annual_dividend.'</td>
					                </tr> 
					                </tbody>
                                    </table> 
					            ';


					}

					

		  }
			
		}
	echo $result;
	}
	public function get_savings_plan_details($savings_plan_id)
	{
		$savings_plan = $this->savings_plan_model->get_savings_plan($savings_plan_id);
		$result = '';
		
		if($savings_plan->num_rows() > 0)
		{
			$row = $savings_plan->row();
			$savings_plan_id = $row->savings_plan_id;
			$savings_plan_name = $row->savings_plan_name;
			$savings_plan_min_opening_balance = number_format($row->savings_plan_opening_balance, 0);
			$savings_plan_min_interest_balance = number_format($row->savings_plan_interest_balance, 0);
			$savings_plan_total_period = $row->period;
			$interest_percentage = $row->interest;

			$charge_withdrawal = $row->charge_withdrawal;
			$interest_type_id = $row->interest_type;
			$transactional_withdrawal_charge = $row->withdrawal_charge;

			$compounding_period_id = $row->interest_period_id;
			$compounding_period_name = $this->savings_plan_model->get_the_compounding_period($compounding_period_id);

			$savings_plan_status = $row->savings_plan_status;
			
			//status
			if($charge_withdrawal == 1)
			{
				$charge_withdrawal = 'Yes';
			}
			else
			{
				$charge_withdrawal = 'No';
			}
			
			$result = 
			'
						<button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#more'.$savings_plan_id.'">
						<i class="fa fa-plus"></i> View details
						</button>
					<!-- Modal -->
					<div class="modal fade" id="more'.$savings_plan_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
										<h4 class="modal-title">'.$savings_plan_name.'</h4>
									</div>
									<div class="modal-body">
										<table class="table table-bordered table-striped table-condensed">
											<tr>
												<th>Savings Plan Name</th>
												<td>'.$savings_plan_name.'</td>
											</tr>
											<tr>
												<th>Minimimum Opening Balance</th>
												<td>'.$savings_plan_min_opening_balance.'</td>
											</tr>
											<tr>
												<th>Minimum Interest Earning Balance</th>
												<td>'.$savings_plan_min_interest_balance.'</td>
											</tr>
											<tr>
												<th>Total Period</th>
												<td>'.$savings_plan_total_period.'</td>
											</tr>
											<tr>
												<th>Dividend Interest</th>
												<td>'.$interest_percentage.'</td>
											</tr>
											<tr>
												<th>Charge Withdrawal</th>
												<td>'.$charge_withdrawal.'</td>
											</tr>
											<tr>
												<th>Transactional Withdrawal Charge</th>
												<td>'.$transactional_withdrawal_charge.'</td>
											</tr>
											<tr>
												<th>Compounding Period Name</th>
												<td>'.$compounding_period_name.'</td>
											</tr>
											
										</table>
									</div>
								</div>
							</div>

						</div>
			';
		}
		
		echo $result;
	}


	public function actual_savings_plan_record($individual_id) 
	{
		
		//form validation rules
		$this->form_validation->set_rules('actual_savings_plan_id', 'Savings Plan', 'required|xss_clean');
		$this->form_validation->set_rules('application_date', 'Application Date', 'required|xss_clean');
		$this->form_validation->set_rules('first_month_payment', 'First Month Payment', 'required|xss_clean');
		$this->form_validation->set_rules('actual_opening_balance', 'Opening Balance', 'required|xss_clean');

		$savings_plan_id = $this->input->post('actual_savings_plan_id');
		$opening_balance = $this->input->post('actual_opening_balance');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
				$savings_plan = $this->savings_plan_model->get_savings_plan($savings_plan_id);
				if($savings_plan->num_rows() > 0)
				{
					$row = $savings_plan->row();
					$savings_plan_id = $row->savings_plan_id;
					$savings_plan_name = $row->savings_plan_name;
					$savings_plan_min_opening_balance = number_format($row->savings_plan_opening_balance, 0);
					$savings_plan_min_interest_balance = number_format($row->savings_plan_interest_balance, 0);
					$savings_plan_total_period = $row->period;
					$interest_percentage = $row->interest;

					$charge_withdrawal = $row->charge_withdrawal;
					$interest_type_id = $row->interest_type;
					$transactional_withdrawal_charge = $row->withdrawal_charge;

					$compounding_period_id = $row->interest_period_id;
					$compounding_period_name = $this->savings_plan_model->get_the_compounding_period($compounding_period_id);

					$savings_plan_status = $row->savings_plan_status;
					
					//status
					if($charge_withdrawal == 1)
					{
						$charge_withdrawal = 'Yes';
					}
					else
					{
						$charge_withdrawal = 'No';
					}

					if($opening_balance < $savings_plan_min_opening_balance)
					{
						$this->session->set_userdata("error_message","The Opening Balance is less than the Minimum Opening Balance of Savings Plan");
					}
					else
					{
						$individual_savings_plan = $this->savings_plan_model->add_savings_plan_record($individual_id);
							if($individual_savings_plan != FALSE)
							{
								$this->session->set_userdata("success_message", "Member Savings Plan added successfully");
								redirect('savings-management/allocate-savings-plan/'.$individual_id);
							}
							
							else
							{
								$this->session->set_userdata("error_message","Could not add Savings Plan. Please try again");
							}	

					  }
						
					}
					else
					{
						$this->session->set_userdata("error_message","The Savings Plan has been Deleted");
					}
				
			}
			else
			{
				$this->session->set_userdata("error_message","Could not add Savings Plan. Please try again");

			}
		$this->apply_savings_plan($individual_id);
	}
	public function record_savings_plan_approval($individual_id, $individual_savings_id)
	{
		$this->form_validation->set_rules('approved_date', 'Approval Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('approved_interest', 'Approval Interest', 'required|xss_clean');


		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			
			
			if($this->individual_model->record_savings_plan_approval($individual_savings_id))
			{

				$this->session->set_userdata("success_message", "You have approved the Savings Application");
			}
			
			else
			{
				$this->session->set_userdata("error_message", "Oops something went wrong. Please try again");

			}
			
			
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			
		}
		
		$this->apply_savings_plan($individual_id);

 }

}
?>