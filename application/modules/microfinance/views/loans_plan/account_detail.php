<?php

$all_individual_savings = $this->loans_plan_model->get_individual_loans_plans_details($individual_loan_id);

	foreach ($all_individual_savings->result() as $leases_row)
	{
		
		$individual_loan_id = $leases_row->individual_loan_id;
		$individual_id = $leases_row->individual_id;
		$loans_plan_id = $leases_row->loans_plan_id;
		$individual_loan_status = $leases_row->individual_loan_status;
		$start_date = $leases_row->start_date;
		$savings_plan = $this->loans_plan_model->get_singular_loans_plan($loans_plan_id);
		foreach ($savings_plan->result() as $sp_row)
	    {
		    $loans_plan_name = $sp_row->loans_plan_name;
			$interest_rate = $sp_row->interest_rate;
			
	    }
		$proposed_amount = $leases_row->proposed_amount;
		$no_of_repayments = $leases_row->no_of_repayments;
		
		$individual_lname = $leases_row->individual_lname;
		$individual_mname = $leases_row->individual_mname;
		$individual_fname = $leases_row->individual_fname;
		$individual_dob = $leases_row->individual_dob;
		$individual_email = $leases_row->individual_email;
		$individual_phone = $leases_row->individual_phone;
		$individual_phone2 = $leases_row->individual_phone2;
		$individual_address = $leases_row->individual_address;
		$individual_status = $leases_row->individual_status;
		$civil_status_id = $leases_row->civilstatus_id;
		$individual_locality = $leases_row->individual_locality;
		$individual_name = $individual_fname.' '. $individual_mname;

		

		$individual_loan_start_date = date('jS M Y',strtotime($start_date));
		
	   
		
		//create deactivated status display
		if($individual_loan_status == 0)
		{
			$status = '<span class="label label-default"> Deactivated</span>';

			$button = '';
			$delete_button = '';
		}
		//create activated status display
		else if($individual_loan_status == 1)
		{
			$status = '<span class="label label-success">Active</span>';
			$button = '<td><a class="btn btn-default" href="'.site_url().'deactivate-rental-unit/'.$individual_loan_id.'" onclick="return confirm(\'Do you want to deactivate '.$individual_loan_id.'?\');" title="Deactivate '.$individual_loan_id.'"><i class="fa fa-thumbs-down"></i></a></td>';
			$delete_button = '<td><a href="'.site_url().'deactivate-rental-unit/'.$individual_loan_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$individual_loan_id.'?\');" title="Delete '.$individual_loan_id.'"><i class="fa fa-trash"></i></a></td>';

		}

		//create deactivated status display
		if($individual_status == 0)
		{
			$status_individual = '<span class="label label-default">Deactivated</span>';
		}
		//create activated status display
		else if($individual_status == 1)
		{
			$status_individual = '<span class="label label-success">Active</span>';
		}
	}






?>


<?php
$statement_response = $this->loans_plan_model->get_individual_loan_statement($individual_loan_id);






$grand_rent_bill = 0;
$grand_water_bill =0;
$grand_electricity_bill = 0;
$grand_service_charge_bill = 0;
$grand_penalty_bill = 0;
$inputs = ' ';


$inputs .='<div class="form-group">
				<label class="col-md-4 control-label">Amount: </label>
			  
				<div class="col-md-7">
					<input type="number" class="form-control" name="amount_paid" placeholder="" autocomplete="off">
				</div>
				<label class="col-md-4 control-label">Legal Fee: </label>
			  
				<div class="col-md-7">
					<input type="number" class="form-control" name="legal_fee" placeholder="" autocomplete="off">
				</div>
			</div>';





?>
<div class="row">
	<div class="col-md-12 pull-right">
		<a href="<?php echo site_url();?>loan-management/member-loans" class="btn btn-sm btn-warning pull-right"  ><i class="fa fa-arrow-left"></i> Back to Member Loans</a> 
	</div>
</div>
 <section class="panel">
	<header class="panel-heading">
		<h2 class="panel-title"><?php echo $title;?> </h2>
	</header>
	
	<!-- Widget content -->
	
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
			<?php
				$error = $this->session->userdata('error_message');
				$success = $this->session->userdata('success_message');
				
				if(!empty($error))
				{
				  echo '<div class="alert alert-danger">'.$error.'</div>';
				  $this->session->unset_userdata('error_message');
				}
				
				if(!empty($success))
				{
				  echo '<div class="alert alert-success">'.$success.'</div>';
				  $this->session->unset_userdata('success_message');
				}
			 ?>
			</div>
		</div>
		
		
		
        <!--<div class="row">
        	<div class="col-sm-3 col-sm-offset-3">
            	<a href="<?php echo site_url().'doctor/print_prescription'.$individual_id;?>" class="btn btn-warning">Print prescription</a>
            </div>
            
        	<div class="col-sm-3">
            	<a href="<?php echo site_url().'doctor/print_lab_tests'.$individual_id;?>" class="btn btn-danger">Print lab tests</a>
            </div>
        </div>-->
        
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-4">
					<div class="row">
						<div class="col-md-12">
							<section class="panel panel-featured panel-featured-info">
								<header class="panel-heading">
									
									<h2 class="panel-title">Individual's Details</h2>
								</header>
								<div class="panel-body">
									<table class="table table-hover table-bordered col-md-12">
										<thead>
											<tr>
												<th>Title</th>
												<th>Detail</th>
											</tr>
										</thead>
									  	<tbody>
									  		<tr><td><span>Individual Name :</span></td><td><?php echo $individual_name;?></td></tr>
									  		<tr><td><span>Individual Phone :</span></td><td><?php echo $individual_phone;?></td></tr>
									  		<tr><td><span>Individual Email :</span></td><td><?php echo $individual_email;?></td></tr>
									  		<tr><td><span>Account Status :</span></td><td><?php echo $status_individual;?></td></tr>
									  		
									  	</tbody>
									</table>
								</div>
							</section>
						</div>
					</div>
				</div>
				<!-- END OF THE SPAN 7 -->
				 <div class="col-md-3">
					<div class="row">
						<div class="col-md-12">
							<section class="panel panel-featured panel-featured-info">
								<header class="panel-heading">
									
									<h2 class="panel-title">Loans Plan Details</h2>
								</header>
								<div class="panel-body">
                                	<div class="row">
                                    	<div class="col-md-12">
                                         </div>
                                    </div>
									<table class="table table-hover table-bordered col-md-12">
										<thead>
											<tr>
												<th>Title</th>
												<th>Detail</th>
											</tr>
										</thead>
									  	<tbody>
									  		<tr><td><span>Loans Plan Name :</span></td><td><?php echo $loans_plan_name;?></td></tr>
									  		<tr><td><span>Loans Plan Interest:</span></td><td><?php echo $interest_rate;?></td></tr>
									  										  		
									  	</tbody>
									</table>

									 <div class="col-md-12">
										<div class="form-actions center-align">
								            <a href="<?php echo site_url();?>lease-detail/<?php echo $individual_loan_id?>" class="submit btn btn-warning btn-sm">
								                Lease Detail 
								            </a>
								        </div>
									</div>
								</div>
							</section>
						</div>
					</div>
				</div>
				<!-- START OF THE SPAN 5 -->
				<div class="col-md-5">
					<div class="row">
						<div class="col-md-12">
							<section class="panel panel-featured panel-featured-info">
								
                                
								<div class="panel-body">

									<div class="tabs">
										<ul class="nav nav-tabs nav-justified">
											<li class="active">
												<a class="text-center" data-toggle="tab" href="#general"><i class="fa fa-money"></i> Payments</a>
											</li>
											
											
											
									
										</ul>
										<div class="tab-content">
											<div class="tab-pane active" id="general">
												<?php echo form_open("loan-management/member-loan-details/make-loan-payments/".$individual_id."/".$individual_loan_id, array("class" => "form-horizontal"));?>
													<input type="hidden" name="type_of_account" value="1">
													<input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
													<div class="form-group" id="payment_method_id">
														<label class="col-md-4 control-label">Payment Method: </label>
														  
														<div class="col-md-7">
															<select class="form-control" name="payment_method_id" onchange="check_payment_type(this.value)" required>
																<option value="0">Select a payment method</option>
			                                                	<?php
																  $method_rs = $this->loans_plan_model->get_payment_methods();
																	
																	foreach($method_rs->result() as $res)
																	{
																	  $payment_method_id = $res->payment_method_id;
																	  $payment_method_name = $res->payment_method_name;
																	  
																		echo '<option value="'.$payment_method_id.'">'.$payment_method_name.'</option>';
																	  
																	}
																  
															  ?>
															</select>
														  </div>
													</div>
													<div id="mpesa_div" class="form-group" style="display:none;" >
														<label class="col-md-4 control-label"> Mpesa TX Code: </label>

														<div class="col-md-7">
															<input type="text" class="form-control" name="mpesa_code" placeholder="">
														</div>
													</div>
												  
												  
													<div id="cheque_div" class="form-group" style="display:none;" >
														<label class="col-md-4 control-label"> Bank Name: </label>
													  
														<div class="col-md-7">
															<input type="text" class="form-control" name="bank_name" placeholder="Barclays">
														</div>
													</div>
													

													<div class="form-group">
														<label class="col-md-4 control-label">Total Amount: </label>
													  
														<div class="col-md-7">
															<input type="number" class="form-control" name="total_amount_paid" placeholder=""  autocomplete="off" required>
														</div>
													</div>

													<div class="form-group">
														<label class="col-md-4 control-label">Net Savings Amount: </label>
													  
														<div class="col-md-7">
															<input type="number" class="form-control" name="net_amount_paid" placeholder=""  autocomplete="off" required>
														</div>
													</div>
													<div class="form-group">
														<label class="col-md-4 control-label">Other Fees: </label>
													  
														<div class="col-md-7">
															<input type="number" class="form-control" name="legal_fee" placeholder=""  autocomplete="off" required>
														</div>
													</div>


													<!-- where you put all the item that are to be paid for in this property -->
													
													<!-- end of the items to be paid for in this property -->
													<div class="form-group">
														<label class="col-md-4 control-label">Paid in By: </label>
													  
														<div class="col-md-7">
															<input type="text" class="form-control" name="paid_by" placeholder="<?php echo $individual_name;?>" value="<?php echo $individual_name;?>" autocomplete="off" >
														</div>
													</div>

													<div class="form-group">
														<label class="col-md-4 control-label">Payment Date: </label>
													  
														<div class="col-md-7">
															 <div class="input-group">
								                                <span class="input-group-addon">
								                                    <i class="fa fa-calendar"></i>
								                                </span>
								                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="payment_date" placeholder="Payment Date" required>
								                            </div>
														</div>
													</div>
													
													<div class="center-align">
														<button class="btn btn-info btn-sm" type="submit">Add Payment Information</button>
													</div>
													<?php echo form_close();?>
											</div>
											<div class="tab-pane" id="account">
											<?php echo form_open("savings-management/member-savings/make-savings-withdrawals/".$individual_id."/".$individual_loan_id, array("class" => "form-horizontal"));?>
													<input type="hidden" name="type_of_account" value="2">
													<input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
													<div class="form-group">
														<label class="col-md-4 control-label">Total Amount: </label>
													  
														<div class="col-md-7">
															<input type="number" class="form-control" name="total_withdrawal_amount" placeholder="" autocomplete="off" required>
														</div>
													</div>
													
													<div class="form-group">
														<label class="col-md-4 control-label">Withdrawal Reason: </label>
													  
														<div class="col-md-7">
															<textarea class="form-control" name="withdrawal_reason" autocomplete="off"></textarea>
														</div>
													</div>

													<div class="form-group">
														<label class="col-md-4 control-label">Withdrawal Date: </label>
													  
														<div class="col-md-7">
															 <div class="input-group">
								                                <span class="input-group-addon">
								                                    <i class="fa fa-calendar"></i>
								                                </span>
								                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="withdrawal_date" placeholder="Withdrawal Date" required>
								                            </div>
														</div>
													</div>
													
													<div class="center-align">
														<button class="btn btn-info btn-sm" type="submit">Add Withdrawal Information</button>
													</div>
													<?php echo form_close();?>
												  </div>
												  <div class="tab-pane" id="dividend">
											<?php echo form_open("savings-management/member-savings/make-savings-dividends/".$individual_id."/".$individual_loan_id, array("class" => "form-horizontal"));?>
													<input type="hidden" name="type_of_account" value="3">
													<input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string()?>">
													<div class="form-group">
														<label class="col-md-4 control-label">Dividend Amount: </label>
													  
														<div class="col-md-7">
															<input type="number" class="form-control" name="dividend_amount" placeholder="" autocomplete="off" required>
														</div>
													</div>

													<div class="form-group">
														<label class="col-md-4 control-label">Dividend Date: </label>
													  
														<div class="col-md-7">
															 <div class="input-group">
								                                <span class="input-group-addon">
								                                    <i class="fa fa-calendar"></i>
								                                </span>
								                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="dividend_date" placeholder="Dividend Date" required>
								                            </div>
														</div>
													</div>
													
													<div class="center-align">
														<button class="btn btn-info btn-sm" type="submit">Add Dividend Information</button>
													</div>
													<?php echo form_close();?>
												  </div>
									</div>

									
								</div>
							</section>
						</div>
					</div>

				</div>
				<!-- END OF THE SPAN 5 -->
			</div>
		</div>
		<hr>


			<div class="row">
			<div class="col-md-12">
				<section class="panel panel-featured panel-featured-info">
					<header class="panel-heading">
						
						<h2 class="panel-title">Loans Plan Payments Details <?php echo date('Y');?></h2>
						<!-- <a href="<?php echo site_url();?>lease-payments/<?php echo $individual_loan_id;?>" target="_blank" class="btn btn-sm btn-warning pull-right"  style="margin-top:-25px;"> <i class="fa fa-pencil"></i> Tenant's Payments</a> -->

					</header>
					<div class="panel-body">
                    	<div class="row">
                        	<div class="col-md-12">
                             </div>
                        </div>
						<table class="table table-hover table-bordered col-md-12">
							<thead>
								<tr>
									<th>#</th>
									<th>Payment Date</th>
									<th>Receipt Number</th>
									<th>Amount Paid</th>
									<th>Paid By</th>
									<th>Receipted Date</th>
									<th>Reference No.</th>
									<th>Confirmation Status</th>
									<th colspan="3">Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if($loans_plan_payments->num_rows() > 0)
								{
									$y = 0;
									foreach ($loans_plan_payments->result() as $key) {
										# code...
										$receipt_number = $key->receipt_number;
										$amount_paid = $key->amount_paid;
										$payment_id = $key->payment_id;
										$paid_by = $key->paid_by;
										$payment_date = $key->payment_date;
										$payment_created = $key->payment_created;
										$payment_created_by = $key->payment_created_by;
										$confirmation_status = $key->confirmation_status;
										$transaction_code = $key->transaction_code;

										if($confirmation_status == 0)
										{
											$confirmation_status_text = "Payment Confirmed";
											$button = '<a class="btn btn-danger" href="'.site_url().'accounts/deactivate_payments/'.$payment_id.'/'.$individual_loan_id.'" onclick="return confirm(\'Do you want to Deactivate this payment Kes.'.$amount_paid.'?\');" title="Deactivate For Company Return'.$amount_paid.'"> Deactivate </a>';
			

										}
										else
										{
											$confirmation_status_text = "Payment Not Confirmed";
											$button = '<a class="btn btn-info" href="'.site_url().'accounts/activate_payments/'.$payment_id.'/'.$individual_loan_id.'" onclick="return confirm(\'Do you want to Activate this payment Kes.'.$amount_paid.'?\');" title="Activate For Company Return'.$amount_paid.'"> Activate</a>';
			

										}
										$payment_explode = explode('-', $payment_date);

										$payment_date = date('jS M Y',strtotime($payment_date));
										$payment_created = date('jS M Y',strtotime($payment_created));
										$y++;
										$message = $this->site_model->create_web_name('Your payment of Ksh. '.number_format($amount_paid, 0).' has been received for Hse No. '.$loans_plan_name);
										
										?>
										<tr>
											<td><?php echo $y?></td>
											<td><?php echo $payment_date;?></td>
											<td><?php echo $receipt_number?></td>
											<td><?php echo number_format($amount_paid,2);?></td>
											<td><?php echo $paid_by;?></td>
											<td><?php echo $payment_created;?></td>
											<td><?php echo $transaction_code;?></td>
											<td><?php echo $confirmation_status_text;?></td>
											<td><?php echo $button; ?></td>

										
											<td><a href="<?php echo site_url().'cash-office/print-receipt/'.$payment_id.'/'.$individual_id.'/'.$individual_loan_id;?>" class="btn btn-sm btn-primary" target="_blank">Receipt</a></td>
                                            <td><a href="<?php echo site_url().'send-tenants-receipt/'.$payment_id.'/'.$individual_id.'/'.$individual_loan_id;?>" class="btn btn-sm btn-success">Send SMS</a></td>
                                            <td>
                                          		<button type="button" class="btn btn-small btn-default" data-toggle="modal" data-target="#refund_payment<?php echo $payment_id;?>"><i class="fa fa-times"></i></button>
                                            	
                                            	<div class="modal fade" id="refund_payment<?php echo $payment_id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
												    <div class="modal-dialog" role="document">
												        <div class="modal-content">
												            <div class="modal-header">
												            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												            	<h4 class="modal-title" id="myModalLabel">Cancel payment</h4>
												            </div>
												            <div class="modal-body">
												            	<?php echo form_open("accounts/cancel_payment/".$payment_id, array("class" => "form-horizontal"));?>

												            	<input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string();?>">
												                <div class="form-group">
												                    <label class="col-md-4 control-label">Action: </label>
												                    
												                    <div class="col-md-8">
												                        <select class="form-control" name="cancel_action_id">
												                        	<option value="">-- Select action --</option>
												                            <?php
												                                if($cancel_actions->num_rows() > 0)
												                                {
												                                    foreach($cancel_actions->result() as $res)
												                                    {
												                                        $cancel_action_id = $res->cancel_action_id;
												                                        $cancel_action_name = $res->cancel_action_name;
												                                        
												                                        echo '<option value="'.$cancel_action_id.'">'.$cancel_action_name.'</option>';
												                                    }
												                                }
												                            ?>
												                        </select>
												                    </div>
												                </div>
												                
												                <div class="form-group">
												                    <label class="col-md-4 control-label">Description: </label>
												                    
												                    <div class="col-md-8">
												                        <textarea class="form-control" name="cancel_description"></textarea>
												                    </div>
												                </div>
												                
												                <div class="row">
												                	<div class="col-md-8 col-md-offset-4">
												                    	<div class="center-align">
												                        	<button type="submit" class="btn btn-primary">Save action</button>
												                        </div>
												                    </div>
												                </div>
												                <?php echo form_close();?>
												            </div>
												            <div class="modal-footer">
												                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												            </div>
												        </div>
												    </div>
												</div>
                                            </td>
										</tr>
										<?php

									}
								}
								?>
								
							</tbody>
						</table>
					</div>
				</section>
			</div>
		</div>



			<div class="row">
			<div class="col-md-12">
				<section class="panel panel-featured panel-featured-info">
					<header class="panel-heading">
						
						<h2 class="panel-title">Loans Plans Amortization Details <?php echo date('Y');?></h2>
						<!-- <a href="<?php echo site_url();?>lease-payments/<?php echo $individual_loan_id;?>" target="_blank" class="btn btn-sm btn-warning pull-right"  style="margin-top:-25px;"> <i class="fa fa-pencil"></i> Tenant's Payments</a> -->

					</header>
					<div class="panel-body">
                    	<div class="row">
                        	<div class="col-md-12">
                             </div>
                        </div>
						<table class="table table-hover table-bordered col-md-12">
							<thead>
								
								<tr>
									<th>#</th>
									<th>Payment Date</th>
									<th>Starting Balance</th>
									<th>Calculated Amount</th>
									<th>Interest</th>
									<th>Principal Payment</th>
									<th>End Balance</th>
									<th colspan="3">Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if($loans_plan_invoices->num_rows() > 0)
								{
									$y = 0;
									foreach ($loans_plan_invoices->result() as $key) {
										# code...
										$amortization_id = $key->amortization_id;
										$starting_balance = $key->starting_balance;
										$amount = $key->amount;
										$interest = $key->interest;
										$principal_payment = $key->principal_payment;
										$ending_balance = $key->end_balance;
										$date = $key->date;
										$y++;
										
										?>
										<tr>
											<td><?php echo $y?></td>
											<td><?php echo $date;?></td>
											<td><?php echo number_format($starting_balance,2);?></td>
											<td><?php echo number_format($amount,2);?></td>
											<td><?php echo number_format($interest,2);?></td>
											<td><?php echo number_format($principal_payment,2);?></td>
											<td><?php echo number_format($ending_balance,2);?></td>
							
											<td><a href="<?php echo site_url().'cash-office/print-receipt/'.$amortization_id.'/'.$individual_id.'/'.$individual_loan_id;?>" class="btn btn-sm btn-primary" target="_blank">Receipt</a></td>
                                            <td><a href="<?php echo site_url().'send-tenants-receipt/'.$amortization_id.'/'.$individual_id.'/'.$individual_loan_id;?>" class="btn btn-sm btn-success">Send SMS</a></td>
                                            <td>
                                          		<button type="button" class="btn btn-small btn-default" data-toggle="modal" data-target="#refund_payment<?php echo $amortization_id;?>"><i class="fa fa-times"></i></button>
                                            	
                                            	<div class="modal fade" id="refund_payment<?php echo $amortization_id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
												    <div class="modal-dialog" role="document">
												        <div class="modal-content">
												            <div class="modal-header">
												            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												            	<h4 class="modal-title" id="myModalLabel">Cancel payment</h4>
												            </div>
												            <div class="modal-body">
												            	<?php echo form_open("accounts/cancel_payment/".$amortization_id, array("class" => "form-horizontal"));?>

												            	<input type="hidden" name="redirect_url" value="<?php echo $this->uri->uri_string();?>">
												                <div class="form-group">
												                    <label class="col-md-4 control-label">Action: </label>
												                    
												                    <div class="col-md-8">
												                        <select class="form-control" name="cancel_action_id">
												                        	<option value="">-- Select action --</option>
												                            <?php
												                                if($cancel_actions->num_rows() > 0)
												                                {
												                                    foreach($cancel_actions->result() as $res)
												                                    {
												                                        $cancel_action_id = $res->cancel_action_id;
												                                        $cancel_action_name = $res->cancel_action_name;
												                                        
												                                        echo '<option value="'.$cancel_action_id.'">'.$cancel_action_name.'</option>';
												                                    }
												                                }
												                            ?>
												                        </select>
												                    </div>
												                </div>
												                
												                <div class="form-group">
												                    <label class="col-md-4 control-label">Description: </label>
												                    
												                    <div class="col-md-8">
												                        <textarea class="form-control" name="cancel_description"></textarea>
												                    </div>
												                </div>
												                
												                <div class="row">
												                	<div class="col-md-8 col-md-offset-4">
												                    	<div class="center-align">
												                        	<button type="submit" class="btn btn-primary">Save action</button>
												                        </div>
												                    </div>
												                </div>
												                <?php echo form_close();?>
												            </div>
												            <div class="modal-footer">
												                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												            </div>
												        </div>
												    </div>
												</div>
                                            </td>
										</tr>
										<?php

									}
								}
								?>
								
							</tbody>
						</table>
					</div>
				</section>
			</div>
		</div>




		<div class="row">
			<div class="col-md-12">
				<section class="panel panel-featured panel-featured-info">
					<header class="panel-heading">
						
						<h2 class="panel-title">Payment Statement <?php echo date('Y');?>
							<!-- <a href="<?php echo site_url();?>lease-statement/<?php echo $individual_loan_id;?>" target="_blank" class="btn btn-sm btn-success pull-right"  style="margin-top:-5px;">Statement</a> -->
							

						</h2>
						<!-- <a href="<?php echo site_url();?>lease-invoices/<?php echo $individual_loan_id;?>" target="_blank" class="btn btn-sm btn-warning pull-right"  style="margin-top:-25px;"> <i class="fa fa-pencil"></i> Tenant's Invoices</a> -->

					</header>
					<div class="panel-body">
                    	<div class="row">
                        	<div class="col-md-12">
                             </div>
                        </div>


						<table class="table table-hover table-bordered col-md-12">
							<thead>
								<tr>
									<th>Date</th>
									<th>Description</th>
									<th>Invoices</th>
									<th>Payments</th>
									<th>Arrears</th>
									<th></th>
								</tr>
							</thead>
						  	<tbody>
						  		<?php echo $statement_response['result'];?>
						  	</tbody>
						</table>
					</div>
				</section>
			</div>
		</div> 
	
		<!-- END OF PADD -->
	</div>
		
</section>
  <!-- END OF ROW -->
<script type="text/javascript">
  function getservices(id){

        var myTarget1 = document.getElementById("service_div");
        var myTarget2 = document.getElementById("username_div");
        var myTarget3 = document.getElementById("password_div");
        var myTarget4 = document.getElementById("service_div2");
        var myTarget5 = document.getElementById("payment_method_id");
		
        if(id == 1)
        {
          myTarget1.style.display = 'none';
          myTarget2.style.display = 'none';
          myTarget3.style.display = 'none';
          myTarget4.style.display = 'block';
          myTarget5.style.display = 'block';
        }
        else
        {
          myTarget1.style.display = 'block';
          myTarget2.style.display = 'block';
          myTarget3.style.display = 'block';
          myTarget4.style.display = 'none';
          myTarget5.style.display = 'none';
        }
        
  }
  function check_payment_type(payment_type_id){
   
    var myTarget1 = document.getElementById("cheque_div");

    var myTarget2 = document.getElementById("mpesa_div");

    var myTarget3 = document.getElementById("insuarance_div");

    if(payment_type_id == 1)
    {
      // this is a check
     
      myTarget1.style.display = 'block';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 2)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 3)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';
    }
    else if(payment_type_id == 4)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'none';
    }
    else if(payment_type_id == 5)
    {
      myTarget1.style.display = 'none';
      myTarget2.style.display = 'block';
      myTarget3.style.display = 'none';
    }
    else
    {
      myTarget2.style.display = 'none';
      myTarget3.style.display = 'block';  
    }

  }
</script>