<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Loans Plan</th>
						<th>Approved Amount</th>
						<th>Approved Date</th>
						<th>Repayment Months</th>
						<th>Interest Rate</th>
						<th>Processing Fee</th>
						<th>Loan Status</th>
						<th colspan="2">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->users_model->get_active_users();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				
				$individual_loan_id = $row->individual_loan_id;
				$individual_id = $row->individual_id;
				$start_date = $row->start_date;
				$purpose = $row->purpose;
				$proposed_amount = $row->proposed_amount;
				$loans_plan_id = $row->loans_plan_id;
				$loans_plan_name = $this->loans_plan_model->get_loans_plan_name($loans_plan_id);
				$interest_rate = $row->interest_rate;
				$processing_fee = $row->processing_fee;
				$no_of_repayments = $row->no_of_repayments;
				$individual_loan_status = $row->individual_loan_status;
				
				
				
				//status
				if($individual_loan_status == 1)
				{
					$status = 'Active';
				}
				else
				{
					$status = 'Disabled';
				}
				
				//create deactivated status display
				if($individual_loan_status == 0)
				{
					$status = '<span class="label label-default">Not Approved</span>';
					
				}
				//create activated status display
				else if($individual_loan_status == 1)
				{
					$status = '<span class="label label-success">Disbursed</span>';
					
				}
				else if($individual_loan_status == 2)
				{
					$status = '<span class="label label-warning">Complete</span>';
					
				}
				
				$count++;
				
				
				
				$zero = 0;
				$result .= 

				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$loans_plan_name.'</td>
						<td>'.$proposed_amount.'</td>
						<td>'.$start_date.'</td>
						<td>'.$no_of_repayments.'</td>
						<td>'.$interest_rate.'</td>
						<td>'.$processing_fee.'</td>
						<td>'.$status.'</td>
						
						
						<td><a href="'.site_url().'loan-management/member-loan-details/account-details/'.$individual_id.'/'.$individual_loan_id.'" class="btn btn-xs btn-primary">Account Details</a></td>
						
						
					    
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no Member Loans";
		}
?>

 <section class="panel">
    
    
  
</section>

						<section class="panel">
							<header class="panel-heading">
						        <h2 class="panel-title">Member Long Term Loan Details</h2>
						    </header>
							<div class="panel-body">
                            	
								<div class="table-responsive">
                                	
									<?php echo $result;?>
							
                                </div>
							</div>
                            <div class="panel-footer">
                            	<?php if(isset($links)){echo $links;}?>
                            </div>
						</section>