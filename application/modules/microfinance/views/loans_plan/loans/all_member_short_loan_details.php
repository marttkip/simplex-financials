<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Loans Plan</th>
						<th>Approved Amount</th>
						<th>Approved Date</th>
						<th>Repayment Months</th>
						<th>Interest Rate</th>
						<th>Processing Fee</th>
						<th>phone Number</th>
						<th>Sent Transaction Code</th>
						<th>Loan Status</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->users_model->get_active_users();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				
				$short_loan_id = $row->short_loan_id;
				$individual_id = $row->individual_id;
				$phone_number = $row->phone_number;
				$proposed_amount = $row->proposed_amount;
				$proposed_no_of_repayments = $row->proposed_no_of_repayments;
				$loans_plan_id = $row->loans_plan_id;
				$loans_plan_name = $this->loans_plan_model->get_loans_plan_name($loans_plan_id);
				$interest_rate = $row->interest_rate;
				$processing_fee = $row->processing_fee;
				$application_date = $row->application_date;
				$short_loan_status = $row->short_loan_status;
				$transaction_code = $row->transaction_code;
				
				
				
				//status
				if($short_loan_status == 1)
				{
					$status = 'Active';
				}
				else
				{
					$status = 'Disabled';
				}
				
				//create deactivated status display
				if($short_loan_status == 0)
				{
					$status = '<span class="label label-default">Not Disbursed</span>';
					$button = '<a href="'.site_url().'loan-management/member-loan-details/short-loan-disburment/'.$individual_id.'/'.$short_loan_id.'/'.$phone_number.'/'.$proposed_amount.'" class="btn btn-xs btn-success">Disburse Money</a>';
					
				}
				//create activated status display
				else
				{
					$status = '<span class="label label-success">Disbursed</span>';
					$button = '<a href="'.site_url().'loan-management/member-loan-details/short-account-details/'.$individual_id.'/'.$short_loan_id.'" class="btn btn-xs btn-primary">Account Details</a>';
					
				}
				
				$count++;
				
				
				
				$zero = 0;
				$result .= 

				'
					<tr>
					
						
						<td>'.$count.'</td>
						<td>'.$loans_plan_name.'</td>
						<td>'.$proposed_amount.'</td>
						<td>'.$application_date.'</td>
						<td>'.$proposed_no_of_repayments.'</td>
						<td>'.$interest_rate.'</td>
						<td>'.$processing_fee.'</td>
						<td>'.$phone_number.'</td>
						<td>'.$transaction_code.'</td>
						<td>'.$status.'</td>
						
						
						
						<td></td>
						
						
					    
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no Member Short Loans";
		}
?>

 <section class="panel">
    
    
  
</section>

						<section class="panel">
							<header class="panel-heading">
						        <h2 class="panel-title">Member Short Term Loan Payment Details</h2>
						        <h2 class="panel-title">Payment Batch 001 Amount Kes.500,000</h2>
						    </header>
							<div class="panel-body">
                            	
								<div class="table-responsive">
                                	
									<?php echo $result;?>
									<h2 class="panel-title">Payment Batch 001 Balance <?php echo $balance;?></h2>
							
                                </div>
							</div>
                            <div class="panel-footer">
                            	<?php if(isset($links)){echo $links;}?>
                            </div>
						</section>