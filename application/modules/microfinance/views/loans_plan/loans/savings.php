<?php
//individual data
$row = $individual->row();

$individual_id = $row->individual_id;
$individual_lname = $row->individual_lname;
$individual_mname = $row->individual_mname;
$individual_fname = $row->individual_fname;
$individual_dob = $row->individual_dob;
$individual_email = $row->individual_email;
$individual_phone = $row->individual_phone;
$individual_phone2 = $row->individual_phone2;
$individual_address = $row->individual_address;
$civil_status_id = $row->civilstatus_id;
$individual_locality = $row->individual_locality;
$title_id = $row->title_id;
$branch_id = $row->branch_id;
$gender_id = $row->gender_id;
$individual_city = $row->individual_city;
$individual_number = $row->individual_number;
$individual_post_code = $row->individual_post_code;
$document_id = $row->document_id;
$document_number = $row->document_number;
$document_place = $row->document_place;
$individual_email2 = $row->individual_email2;
$individual_type_id = $row->individual_type_id;
$kra_pin = $row->kra_pin;
$outstanding_loan = $row->outstanding_loan;
$total_savings = $row->total_savings;

$starting_amount = 0;
$savings_plan_id = 0;

$individual_name = $individual_fname.' '. $individual_mname;
//repopulate data if validation errors occur
$validation_error = validation_errors();
                
if(!empty($validation_error))
{
    $individual_onames = set_value('individual_onames');
    $individual_fname = set_value('individual_fname');
   
    $individual_dob = set_value('individual_dob');
    $individual_email = set_value('individual_email');
    $individual_phone = set_value('individual_phone');
    $individual_phone2 = set_value('individual_phone2');
    $civil_status_id = set_value('civil_status_id');
    $individual_locality = set_value('individual_locality');
    $title_id = set_value('title_id');
    $gender_id = set_value('gender_id');
    $individual_number = set_value('individual_number');
    $individual_address = set_value('individual_address');
    $individual_city = set_value('individual_city');
    $individual_post_code = set_value('individual_post_code');
    $document_id = set_value('document_id');
    $document_number = set_value('document_number');
    $document_place = set_value('document_place');
    $individual_email2 = set_value('individual_email2');
    $individual_type_id = set_value('individual_type_id');
    $kra_pin = set_value('kra_pin');
    $branch_id = set_value('branch_id');
    $outstanding_loan = set_value('outstanding_loan');
    $total_savings = set_value('total_savings');

}
?>
<section class="panel">
    <div class="panel-body">
        <header class="panel-heading">                      
            <h2 class="panel-title">Savings Plans Registered </h2>
        </header>
        
            <?php echo form_close();?>
    </div>
</section>
<?php
        $count = 0; 
        $result = '';

        
        //if users exist display them
        if ($individual_savings->num_rows() > 0)

        {   
            $result .= 
            '
            <table class="table table-bordered table-striped table-condensed">
                <thead>
                    <tr>
                        <th>#</th>
                        <th> Savings Plan Name</th>
                        <th> Start Date</th>
                        <th> End Date</th>
                        <th> Starting Amount</th>
                        <th> Status</th>
                        <th> Saving Payments</th>
                        <th> Saving Withdrawals</th>
                        <th> Saving Balances</th>
                        <th> Saving Dividends</th>
                    </tr>
                </thead>
                  <tbody>
                  
            ';
            
           
            foreach ($individual_savings->result() as $row)
            {
                $individual_savings_id = $row->individual_savings_id;

                $savings_plan_id = $row->savings_plan_id;
                $savings_plan_name = $this->member_savings_model->get_savings_plan_name($savings_plan_id);
                $individual_savings_status = $row->individual_savings_status;
                $savings_start_amount = $row->individual_savings_amount;
                $savings_start_date = $row->start_date;
                $savings_end_date = $row->end_date;
                
                
                
                
                //status
                if($individual_savings_status == 1)
                {
                    $status = 'Active';
                }
                else
                {
                    $status = 'Disabled';
                }
                
                //create deactivated status display
                if($individual_savings_status == 0)
                {
                    $status = '<span class="label label-default">Deactivated</span>';
                    $button = '<a class="btn btn-info" href="'.site_url().'microfinance/activate-individual/'.$individual_savings_id.'" onclick="return confirm(\'Do you want to activate '.$savings_plan_name.'?\');" title="Activate '.$savings_plan_name.'"><i class="fa fa-thumbs-up"></i></a>';
                }
                //create activated status display
                else if($individual_savings_status == 1)
                {
                    $status = '<span class="label label-success">Active</span>';
                    $button = '<a class="btn btn-default" href="'.site_url().'microfinance/deactivate-individual/'.$individual_savings_id.'" onclick="return confirm(\'Do you want to deactivate '.$savings_plan_name.'?\');" title="Deactivate '.$savings_plan_name.'"><i class="fa fa-thumbs-down"></i></a>';
                }
                
                $count++;
                $balance = '0';
                $member_payments = $this->member_savings_model->get_actual_individual_savings_payments($individual_savings_id);
                $member_withdrawals = $this->member_savings_model->get_actual_individual_savings_withdrawals($individual_savings_id);
                $member_balances = $member_payments - $member_withdrawals;
                $member_dividends = $this->member_savings_model->get_actual_individual_savings_dividends($individual_savings_id);
                $result .= 
                '
                    <tr>
                        <td>'.$count.'</td>
                        <td>'.$savings_plan_name.'</td>
                        <td>'.$savings_start_date.'</td>
                        <td>'.$savings_start_date.'</td>
                        <td>'.$savings_start_amount.'</td>
                        <td>'.$status.'</td>
                        <td>'.$member_payments.'</td>
                        <td>'.$member_withdrawals.'</td>
                        <td>'.$member_balances.'</td>
                        <td>'.$member_dividends.'</td>
                    
                    </tr> 
                ';
            }
            
            $result .= 
            '
                          </tbody>
                        </table>
            ';
        }
        
        else
        {
            $result .= "There are no savings plans";
        }

?>


<section class="panel">
    <header class="panel-heading">                      
        <h2 class="panel-title"><?php echo $title;?></h2>
    </header>
    <div class="panel-body">
        <div class="table-responsive">
            
            <?php echo $result;?>
    
        </div>
    </div>
    <div class="panel-footer">
        <?php if(isset($links)){echo $links;}?>
    </div>
</section>



                      