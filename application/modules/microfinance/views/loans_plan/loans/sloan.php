<?php	

			
$validation_error = validation_errors();

				
$loans_plan_id = set_value('loans_plan_id');
$proposed_amount = set_value('proposed_amount');
$actual_application_date = set_value('actual_application_date');
$actual_proposed_purpose = set_value('actual_proposed_purpose');
$no_of_repayments = set_value('no_of_repayments');





?>
	<section class="panel">
		<header class="panel-heading">
			<h2 class="panel-title"> Short Term Loans Application</h2>
		</header>
		<div class="panel-body">
                	<div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <a href="<?php echo site_url();?>loan-management/setup" class="btn btn-info pull-right">Back to loans plans</a>
                        </div>
                    </div>
                        
                    <!-- Adding Errors -->
                    <?php
                    if(isset($error)){
                        echo '<div class="alert alert-danger"> Oh snap! Change a few things up and try submitting again. </div>';
                    }
                    
                    $validation_errors = validation_errors();
                    
                    if(!empty($validation_errors))
                    {
                        echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
                    }
                    ?>
		


            <div class="row">
						
					<div class="col-md-12 center-align ">
						<h4><strong>Record Short Loan Application</strong></h4>
					</div>
			</div>
            <?php echo form_open('microfinance/short-loan/'.$individual_id, array("class" => "form-horizontal", "role" => "form"));?>
				<div class="row">
						
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-lg-5 control-label">Loan type: </label>
							
							<div class="col-lg-7">
								<select class="form-control" name="loans_plan_id" id="update_loan_plan">
									<option value="">--Select loan type--</option>
									<?php
										if($sloans_plans->num_rows() > 0)
										{
											$loans_plan = $sloans_plans->result();
											
											foreach($loans_plan as $res)
											{
												$db_loans_plan_id = $res->loans_plan_id;
												$loans_plan_name = $res->loans_plan_name;
												
												if($db_loans_plan_id == $loans_plan_id)
												{
													echo '<option value="'.$db_loans_plan_id.'" selected>'.$loans_plan_name.'</option>';
												}
												
												else
												{
													echo '<option value="'.$db_loans_plan_id.'">'.$loans_plan_name.'</option>';
												}
											}
										}
									?>
								</select>
								<br/>
								<div id="loan_details"></div>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-lg-5 control-label">Requested loan amount: </label>
							
							<div class="col-lg-7">
								<input type="text" class="form-control" name="proposed_amount" id="proposed_amount" placeholder="Requested loan amount" value="<?php echo $proposed_amount;?>">
							</div>
						</div>
						
						
						
					</div>
				   
					<div class="col-md-6">
						
						
						<div class="form-group">
							<label class="col-lg-5 control-label">Number of repayments: </label>
							
							<div class="col-lg-7">
								<input type="text" class="form-control" id="no_of_repayments" name="no_of_repayments" placeholder="Number of repayments" value="<?php echo $no_of_repayments;?>">
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-5 control-label">Loan Application Date: </label>
							
							<div class="col-lg-7">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="actual_application_date" placeholder="Application date" id="actual_application_date" value="<?php echo $actual_application_date;?>">
                                </div>
                            </div>
						</div>
						<div class="form-group">
							<label class="col-lg-5 control-label">Proposed Loan Purpose: </label>
							
							<div class="col-lg-7">
								<input type="text" class="form-control" id="actual_proposed_purpose" name="actual_proposed_purpose"  placeholder="Requested loan purpose" value="<?php echo $actual_proposed_purpose;?>">
							</div>
						</div>

						
						
						
					</div>
				</div>
				<div class="row" style="margin:10px 0 10px;">
					<div class="col-md-12">
					    <div class="form-actions center-align">
					        <button class="submit btn btn-primary" type="submit">
					            Add Short Loan Request
					        </button>
					    </div>
					</div>
				</div>

			  <?php echo form_close();?>
           
		</div>           	
	</section>

<script type="text/javascript">
	$(document).on("change","select#update_loan_plan",function(e)
	{
		$( "#loan_details" ).html('');
		var loans_plan_id = $(this).val();
		
		//get department services
		$.get( "<?php echo site_url();?>microfinance/loans_plan/get_loan_details/"+loans_plan_id, function( data ) 
		{
			$( "#loan_details" ).html( data );
		});
	});
	
</script>