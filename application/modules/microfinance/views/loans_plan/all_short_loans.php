<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th><a href="'.site_url().'microfinance/loans/loans_plan_name/'.$order_method.'/'.$page.'">Member Name</a></th>
						<th><a href="'.site_url().'microfinance/loans/interest_rate/'.$order_method.'/'.$page.'">Phone Number</a></th>
						<th><a href="'.site_url().'microfinance/loans/interest_name/'.$order_method.'/'.$page.'">Savings Plan</a></th>
						<th><a href="'.site_url().'microfinance/loans/interest_rate/'.$order_method.'/'.$page.'">% Interest Rate</a></th>
						<th><a href="'.site_url().'microfinance/loans/installment_type_name/'.$order_method.'/'.$page.'">Proposed Amount</a></th>
						<th><a href="'.site_url().'microfinance/loans/minimum_loan_amount/'.$order_method.'/'.$page.'">Repayment Months</a></th>
						<th><a href="'.site_url().'microfinance/loans/maximum_loan_amount/'.$order_method.'/'.$page.'">Application Date</a></th>
						<th><a href="'.site_url().'microfinance/loans/custom_loan_amount/'.$order_method.'/'.$page.'">Transaction Code</a></th>
						<th><a href="'.site_url().'microfinance/loans/custom_loan_amount/'.$order_method.'/'.$page.'">Processing Fees</a></th>
						<th><a href="'.site_url().'microfinance/loans/loans_plan_status/'.$order_method.'/'.$page.'">Status</a></th>
						<th colspan="5">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->users_model->get_active_users();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				
				$short_loan_id = $row->short_loan_id;
				$individual_id = $row->individual_id;

				$individual_names = $this->loans_plan_model->get_individual_names($individual_id);
				$phone_number = $row->phone_number;
				$proposed_amount = $row->proposed_amount;
				$proposed_no_of_repayments = $row->proposed_no_of_repayments;
				$application_date = $row->application_date;
				$loans_plan_id =$row->loans_plan_id;
				$loans_plan_name = $this->loans_plan_model->get_loans_plan_name($loans_plan_id);
				$interest_rate =$row->interest_rate;
				$processing_fee =$row->processing_fee;
				$created =$row->created;
				$short_loan_status =$row->short_loan_status;
				$transaction_code =$row->transaction_code;
				$transaction_id =$row->transaction_id;
				$disbused_by =$row->disbused_by;
				$amount =$row->amount;
				$mpesa_response =$row->mpesa_response;
				
				//create deactivated status display
				if($short_loan_status == 0)
				{
					$status = '<span class="label label-default">Not Disbursed</span>';
					$button = '<a class="btn btn-success" href="'.site_url().'microfinance/activate-loans-plan/'.$loans_plan_id.'" onclick="return confirm(\'Do you want to send the money'.$loans_plan_name.'?\');" title="Activate '.$individual_names.'"><i class="fa fa-money"></i></a>';
				}
				//create activated status display
				else if($short_loan_status == 1)
				{
					$status = '<span class="label label-success">Disbursed</span>';
					$button = ' ';
				}
				
				
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$individual_names.'</td>
						<td>'.$phone_number.'</td>
						<td>'.$loans_plan_name.'</td>
						<td>'.$interest_rate.'</td>
						<td>'.number_format($proposed_amount, 2).'</td>
						<td>'.$proposed_no_of_repayments.'</td>
						<td>'.$application_date.'</td>
						<td>'.number_format($processing_fee, 2).'</td>
						<td>'.$transaction_code.'</td>
						<td>'.$status.'</td>
					
						<td>'.$button.'</td>
						
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no loans plans";
		}
?>

						<section class="panel">
							<header class="panel-heading">						
								<h2 class="panel-title"><?php echo $title;?></h2>
							</header>
							<div class="panel-body">
                            	<?php
                                $success = $this->session->userdata('success_message');
		
								if(!empty($success))
								{
									echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
									$this->session->unset_userdata('success_message');
								}
								
								$error = $this->session->userdata('error_message');
								
								if(!empty($error))
								{
									echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
									$this->session->unset_userdata('error_message');
								}
								?>
                            	
								<div class="table-responsive">
                                	
									<?php echo $result;?>
							
                                </div>
							</div>
                            <div class="panel-footer">
                            	<?php if(isset($links)){echo $links;}?>
                            </div>
						</section>
