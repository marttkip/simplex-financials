<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count_individual = 0;
			$total_interest = 0;
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Loan number</th>
						<th>Member number</th>
						<th>Member Name</th>
						<th>Applied Loan Amount</th>
						<th>Disbursed Amount</th>
						<th>Loan Repayment Amount</th>
						<th>Interest Repaid Amount</th>
						<th>Loan Balance</th>

					</tr>
				</thead>
				  <tbody>
				  
			';
			foreach ($query->result() as $row)
			{
				$individual_id = $row->individual_id;
				$individual_number = $row->individual_number;
				$individual_fname = $row->individual_fname;
				$individual_mname = $row->individual_mname;
				$individual_loan_id = $row->individual_loan_id;
				$individual_lname = $row->individual_lname;
				$individual_loan_applied = $row->proposed_amount;
				$loan_number = $row->individual_loan_number;
				$loan_application_date = $row->application_date;
				$individual_loan_approved = $row->approved_amount;
				$loan_disbursed_amount = $this->reports_model->get_total_amount_disbursed($individual_loan_id);
				$individual_repayment_made = $this->reports_model->get_total_loan_repayment($individual_loan_id);
				$individual_interst_paid = $this->reports_model->get_total_interest_repayment($individual_loan_id);
				$loan_balance = $loan_disbursed_amount - $individual_repayment_made;
				$count_individual++;
				$total_interest  += $individual_interst_paid;
				$result .= 
				'
					<tr>
						<td>'.$count_individual.'</td>
						<td>'.$loan_number.'</td>
						<td>'.$individual_number.'</td>
						<td>'.$individual_lname.' '.$individual_fname.' '.$individual_mname.'</td>
						<td>'.number_format($individual_loan_applied,2).'</td>
						<td>'.number_format($loan_disbursed_amount,2).'</td>
						<td>'.number_format($individual_repayment_made,2).'</td>
						<td>'.number_format($individual_interst_paid,2).'</td>
						<td>'.number_format($loan_balance,2).'</td>
					</tr>  
				';
			}
			
			$result .= 
			'
					<tr>
						<th colspan="7">Total Interest</th>
						<th>'.$total_interest.'</th>
					</tr>
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no payments made";
		}
?>


<section class="panel">
	<header class="panel-heading">						
		<h2 class="panel-title"><?php echo $title;?></h2>
	</header>
	<div class="panel-body">
    	<?php
        $success = $this->session->userdata('success_message');

		if(!empty($success))
		{
			echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
			$this->session->unset_userdata('success_message');
		}
		
		$error = $this->session->userdata('error_message');
		
		if(!empty($error))
		{
			echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
			$this->session->unset_userdata('error_message');
		}
		?>
    	<div class="row " style="margin-bottom:20px;">
            <div class="col-lg-2 col-lg-offset-8 pull-right">
                <a href="<?php echo site_url();?>export-interest-revenue" class="btn btn-sm btn-success pull-right" target="_blank">Export Interest Revenue</a>
            </div>
        </div>
		<div class="table-responsive">
        	
			<?php echo $result;?>
	
        </div>
	</div>
    <div class="panel-footer">
    	<?php if(isset($links)){echo $links;}?>
    </div>
</section>