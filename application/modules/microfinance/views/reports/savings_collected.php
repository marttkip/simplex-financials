<?php
		
		$result = '';
		
		//if users exist display them
		if ($all_individual_savings->num_rows() > 0)
		{
			$count_individual = 0;
			$total_balance = $total_savings = $total_withdrawals = 0;
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Member number</th>
						<th>Member Name</th>
						<th>Savings Amount</th>
						<th>Withdrawal Amount</th>
						<th>Balance</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			foreach ($all_individual_savings->result() as $row)
			{
				$individual_id = $row->individual_id;
				$individual_number = $row->individual_number;
				$individual_fname = $row->individual_fname;
				$individual_mname = $row->individual_mname;
				$individual_lname = $row->individual_lname;
				$individual_savings = $this->reports_model->get_total_amount_saved($individual_id);
				$individual_savings_withdrawals = $this->reports_model->get_total_withdrawan($individual_id);
				$savings_balance = $individual_savings - $individual_savings_withdrawals;
				$count_individual++;
				$total_savings  += $individual_savings;
				$total_withdrawals =+ $individual_savings_withdrawals;
				$total_balance = $total_savings - $total_withdrawals;
				$result .= 
				'
					<tr>
						<td>'.$count_individual.'</td>
						<td>'.$individual_number.'</td>
						<td>'.$individual_lname.' '.$individual_fname.' '.$individual_mname.'</td>
						<td>'.number_format($individual_savings,2).'</td>
						<td>'.number_format($individual_savings_withdrawals,2).'</td>
						<td>'.number_format($savings_balance,2).'</td>
					</tr>  
				';
			}
			
			$result .= 
			'
					<tr>
						<th colspan="3">Totals</th>
						<th>'.number_format($total_savings,2).'</th>
						<th>'.number_format($total_withdrawals,2).'</th>
						<th>'.number_format($total_balance,2).'</th>
					</tr>
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no savings made";
		}
?>


<section class="panel">
	<header class="panel-heading">						
		<h2 class="panel-title"><?php echo $title;?></h2>
	</header>
	<div class="panel-body">
    	<?php
        $success = $this->session->userdata('success_message');

		if(!empty($success))
		{
			echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
			$this->session->unset_userdata('success_message');
		}
		
		$error = $this->session->userdata('error_message');
		
		if(!empty($error))
		{
			echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
			$this->session->unset_userdata('error_message');
		}
		?>
    	<div class="row " style="margin-bottom:20px;">
            <div class="col-lg-2 col-lg-offset-8 pull-right">
                <a href="<?php echo site_url();?>export-savings" class="btn btn-sm btn-success pull-right" target="_blank">Export Savings</a>
            </div>
        </div>
		<div class="table-responsive">
        	
			<?php echo $result;?>
	
        </div>
	</div>
    <div class="panel-footer">
    	<?php if(isset($links)){echo $links;}?>
    </div>
</section>