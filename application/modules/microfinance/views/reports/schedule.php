<?php
$schedule_result = '';
$count = 0;
if($query->num_rows() > 0)
{
	$schedule_result .='
					<table class="table table-bordered table-striped table-condensed">
						<thead>
							<tr>
								<th>#</th>
								<th>Individual Name</th>
								<th>Number</th>
								<th>Pricipal</th>
								<th>Interest</th>
								<th>Total</th>
							</tr>
						</thead>
						  <tbody>
				  
						';
	foreach($query->result() as $query_result)
	{
		$individual_loan_id = $query_result->individual_loan_id;
		$loan_details = $this->reports_model->get_schedule($individual_loan_id);
		$interest_amount = $principal_amount = 0;
		$individual_fname = $query_result->individual_fname;
		$individual_mname = $query_result->individual_mname;
		$individual_lname = $query_result->individual_lname;
		$individual_name = $individual_fname.' '.$individual_mname.' '.$individual_lname;
		$individual_number= $query_result->individual_number;
		
		if($loan_details->num_rows() > 0)
		{
			$row = $loan_details->row();
			$interest_amount = $row->interest_amount;
			$principal_amount = $row->principal_amount;
		}
		
		$deduction = $interest_amount + $principal_amount;
		if($deduction > 0)
		{
			$count++;
			$schedule_result .='
							<tr>
								<th>'.$count.'</th>
								<th>'.$individual_name.'</th>
								<th>'.$individual_number.'</th>
								<th>'.$principal_amount.'</th>
								<th>'.$interest_amount.'</th>
								<th>'.$deduction.'</th>
							</tr>
						';
		}
		
	}
	$schedule_result .='
						</tbody>
					</table>
	';
}
else
{
	$schedule_result .='No deductions';
}
?>
<section class="panel">
	<header class="panel-heading">						
		<h2 class="panel-title"><?php echo $title;?></h2>
	</header>
	<div class="panel-body">
	<?php echo $schedule_result;?>
	</div>
</section>