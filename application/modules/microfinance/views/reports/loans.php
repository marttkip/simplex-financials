<?php
		
		$result = '';
		
		//if users exist display them
		if ($all_loans->num_rows() > 0)
		{
			$count_individual = $page;
			$total_disbursed = 0;
			$total_repaid = 0;
			$total_balance = 0;
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Member number</th>
						<th>Member Name</th>
						<th>Loan Amount Applied</th>
						<th>Application Date</th>
						<th>Disbursed</th>
						<th>Repayment</th>
						<th>Loan Balance</th>
						<th colspan="2">Loan Status</th>

					</tr>
				</thead>
				  <tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->users_model->get_active_users();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($all_loans->result() as $row)
			{
				$individual_id = $row->individual_id;
				$individual_fname = $row->individual_fname;
				$individual_mname = $row->individual_mname;
				$individual_lname = $row->individual_lname;
				$individual_email = $row->individual_email;
				$individual_status = $row->individual_status;
				$individual_number = $row->individual_number;
				$individual_applied_loan_amount = $row->proposed_amount;
				$individual_loan_id = $row->individual_loan_id;
				$individual_loan_cleared = $row->individual_loan_cleared;
				$last_transaction_date = $row->application_date;
				$individual_name = $individual_fname.' '.$individual_lname;
				$outstanding_loan = $row->outstanding_loan;
				$total_savings = $row->total_savings;
				
				//last transaction date
				$last_transaction_date = '';
				$individual_loan = $this->individual_model->get_individual_loans($individual_id);
				$loan_disbursed_amount = $this->reports_model->get_total_amount_disbursed($individual_loan_id);
				$individual_repayment_made = $this->reports_model->get_total_loan_repayment($individual_loan_id);
				$individual_interst_paid = $this->reports_model->get_total_interest_repayment($individual_loan_id);
				$loan_balance = number_format($loan_disbursed_amount - $individual_repayment_made, 0);
				$total_disbursed =+ $loan_disbursed_amount ;
				$total_repaid =+ $individual_repayment_made;
				$total_balance = $total_disbursed - $total_repaid;
				$count_individual++;
				
				//display option to clear loans
				if(($individual_loan_cleared == 0) &&($loan_balance ==0.00))
				{
					$status = '<span class="label label-success">Not Cleared</span>';
					$button = '<a class="btn btn-default" href="'.site_url().'microfinance/reports/clear_loan/'.$individual_loan_id.'" onclick="return confirm(\'Do you want to clear the loan for '.$individual_number.'?\');" title="Clear Loan '.$individual_number.'"><i class="fa fa-thumbs-up"></i></a>';
				}
				elseif($individual_loan_cleared == 1)
				{
					$status = '<span class="label label-success">Cleared</span>';
					$button = '';
				}
				else
				{
					$status = '<span class="label label-success">Not Cleared</span>';
					$button = '';
				}
				$result .= 
				'
					<tr>
						<td>'.$count_individual.'</td>
						<td>'.$individual_number.'</td>
						<td>'.$individual_lname.' '.$individual_fname.' '.$individual_mname.'</td>
						<td>'.number_format($individual_applied_loan_amount, 2).'</td>
						<td>'.$last_transaction_date.'</td>
						<td>'.number_format($loan_disbursed_amount,2).'</td>
						<td>'.number_format($individual_repayment_made,2).'</td>
						<td>'.$loan_balance.'</td>
						<td>'.$status.'</td>
						<td>'.$button.'</td>
					</tr> 
				';
			}
			
			$result .= 
			'
					<tr>
						<th colspan="5">Total</th>
						<th>'.number_format($total_disbursed,2).'</th>
						<th>'.number_format($total_repaid,2).'</th>
						<th>'.number_format($total_balance,2).'</th>
					</tr>
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no individuals";
		}
?>


<section class="panel">
	<header class="panel-heading">						
		<h2 class="panel-title"><?php echo $title;?></h2>
	</header>
	<div class="panel-body">
    	<?php
        $success = $this->session->userdata('success_message');

		if(!empty($success))
		{
			echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
			$this->session->unset_userdata('success_message');
		}
		
		$error = $this->session->userdata('error_message');
		
		if(!empty($error))
		{
			echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
			$this->session->unset_userdata('error_message');
		}
		?>
    	<div class="row " style="margin-bottom:20px;">
            <div class="col-lg-2 col-lg-offset-8 pull-right">
                <a href="<?php echo site_url();?>export-all-loans" target="_blank" class="btn btn-sm btn-success pull-right">Export Loan</a>
            </div>
        </div>
		<div class="table-responsive">
        	
			<?php echo $result;?>
	
        </div>
	</div>
    <div class="panel-footer">
    	<?php if(isset($links)){echo $links;}?>
    </div>
</section>