<?php $validation_error = validation_errors();?>
<div class="row">
    
	<div class="col-md-12">
    	<section class="panel">
            <header class="panel-heading">						
                <h2 class="panel-title">Edit Processing fee amounts</h2>
            </header>
            <div class="panel-body">
            	<?php
				$success = $this->session->userdata('success_message');

				if(!empty($success))
				{
					echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
					$this->session->unset_userdata('success_message');
				}
				
				$error = $this->session->userdata('error_message');
				
				if(!empty($error))
				{
					echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
					$this->session->unset_userdata('error_message');
				}
				if(!empty($validation_errors))
				{
					echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
				}
				?>
            	<table class='table table-striped table-hover table-condensed'>
                	<tr>
                    	<th>From (Ksh)</th>
                    	<th>To (Ksh)</th>
                    	<th>Amount (Ksh)</th>
                    	<th colspan="2"></th>
                    </tr>
                    <form action="<?php echo site_url("microfinance/add_new_processing_fee");?>" method="post">
                    <tr>
                        <td><input type='text' name='paye_from' class="form-control" value="<?php echo set_value('paye_from');?>"></td>
                        <td><input type='text' name='paye_to' class="form-control" value="<?php echo set_value('paye_to');?>"></td>
                        <td><input type='text' name='paye_amount' class="form-control" value="<?php echo set_value('paye_amount');?>"></td>
                        <td colspan="2"><button class='btn btn-info btn-sm' type='submit'>Add</button></td>
                    </tr>
                    </form>
                <?php
                
                if($paye->num_rows() > 0)
                {
                    foreach ($paye->result() as $row2)
                    {
                        $processing_fee_id = $row2->processing_fee_id;
                        $processing_fee_from = $row2->processing_fee_from;
                        $processing_fee_to = $row2->processing_fee_to;
                        $processing_fee_amount = $row2->processing_fee_amount;
                        ?>
                        <form action="<?php echo site_url("microfinance/config/edit-processing-fee/".$processing_fee_id);?>" method="post">
                        <tr>
                            <td><input type='text' name='processing_fee_from<?php echo $processing_fee_id;?>' value='<?php echo number_format($processing_fee_from,0);?>' class="form-control"></td>
                            <td><input type='text' name='processing_fee_to<?php echo $processing_fee_id;?>' value='<?php echo number_format($processing_fee_to,0);?>' class="form-control"></td>
                            <td><input type='text' name='processing_fee_amount<?php echo $processing_fee_id;?>' value='<?php echo $processing_fee_amount;?>' class="form-control"></td>
                            <td><button class='btn btn-success btn-xs' type='submit'><i class='fa fa-pencil'></i> Edit</button></td>
                            <td><a href="<?php echo site_url("microfinance/delete-processing_fee/".$processing_fee_id);?>" onclick="return confirm('Do you want to delete <?php echo $processing_fee_amount;?>?');" title="Delete <?php echo $processing_fee_amount;?>"><button class='btn btn-danger btn-xs' type='button'><i class='fa fa-trash'></i> Delete</button></a></td>
                        </tr>
                        </form>
                        <?php
                    }
                }
                ?>
                </table>
            </div>
        </section>
    </div>
</div>