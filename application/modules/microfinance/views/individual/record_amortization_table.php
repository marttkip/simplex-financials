<?php
$result = '';
$total_interest = 0;
$actual_first_date = date('M Y', strtotime($first_date));
$individual_loan_id = 0;

if(empty($error))
{
	$cummulative_interest = 0;
	$cummulative_principal = 0;
	$start_balance = $loan_amount;
	$total_days = 0;
	$count = 0;
	
	$interest_per_year = $interest_rate/1200;
	$calculation_top_one = 1 + $interest_per_year;
	$calculation_top_two = pow($calculation_top_one, $no_of_repayments);
	$calculation_top_three = $calculation_top_two * $interest_per_year;


	$calculation_bottom_one = $calculation_top_two - 1;

	$calculation_one = $calculation_top_three/$calculation_bottom_one;
	$total_amount = $calculation_one * $loan_amount;
	$main_amount = $total_amount;
	$installment_type_duration = 1;
	
	$approved_date = date('M Y', strtotime($first_date. ' + '.$installment_type_duration.' months'));
	$end_date = date('M Y', strtotime($first_date. ' + '.$no_of_repayments.' months'));


	$individual_loan_id = $this->loans_plan_model->update_individual_loan_table($individual_id,$first_date,$loans_plan_id,$loan_amount,$interest_id,$interest_rate,$processing_fees,$approved_date,$end_date,$no_of_repayments);

	// var_dump($main_amount);die();
	//display all payment dates
	for($r = 0; $r < $no_of_repayments; $r++)
	{
		
		$total_days += $installment_type_duration;
		
        //$main_day = date_parse_from_format("Y-m-d", $first_date);
        //$month = $main_day["month"];
   
		$payment_date = date('M Y', strtotime($first_date. ' + '.$total_days.' months'));
		$month = date("m", strtotime($payment_date));
		$year = date("Y", strtotime($payment_date));
		$day = '01';
		$date = $year.'-'.$month.'-'.$day;
		//Amount Calculation

		$interest_per_year = $interest_rate/1200;
		$calculation_top_one = 1 + $interest_per_year;
		$calculation_top_two = pow($calculation_top_one, $no_of_repayments);
		$calculation_top_three = $calculation_top_two * $interest_per_year;


		$calculation_bottom_one = $calculation_top_two - 1;

		$calculation_one = $calculation_top_three/$calculation_bottom_one;
		$total_amount = $calculation_one * $loan_amount;
		$total_amount = number_format($total_amount, 2);

		// interest Calculation
		$interest_cal = $interest_per_year * $loan_amount;

		//Principal Calculation
		$principal = $main_amount - $interest_cal;

		// balance
		$balance = $loan_amount - $principal;
		$loan_amount = $balance;
		$count++;
		$total_interest = $total_interest + $interest_cal;

		
	
		$this->loans_plan_model->update_amortization_table($individual_id, $individual_loan_id,$count,$payment_date, $loan_amount, $main_amount,$interest_cal,$principal,$balance,$month,$year,$date);
		
		
		
	}
	

}	
	
?>
<section class="panel">
    <header class="panel-heading">
        <div class="alert alert-success"> <strong>The Data Has Been Saved...</strong></div>
    </header>
    <div class="panel-body">
    	<?php
        if(!empty($error))
			{
				echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
				//$this->session->unset_userdata('error_message');
			}
		?>
      
     </div>
</section>


						
				