<?php
$individual_id = set_value('individual_id');
$total_guaranateed_amount = 0;	
	if($guarantors->num_rows() > 0)
	{
		$count = 0;
			
		$result = 
		'
		<table class="table table-bordered table-striped table-condensed">
			<thead>
				<tr>
					<th>#</th>
					<th>Guarantor name</th>
					<th>Guaranteed amount</th>
					<th>Loans guaranteed</th>
					<th>Total amount guaranteed</th>
					<th>Added on</th>
					<th>Added by</th>
					<th colspan="2">Actions</th>
				</tr>
			</thead>
			  <tbody>
			  
		';
		$total_guarnateed_amount = 0;
		$count = 0;
		foreach ($guarantors->result() as $row)
		{
			$loan_guarantor_id = $row->loan_guarantor_id;
			$individual_loan_id = $row->individual_loan_id;
			$individual_id = $row->individual_id;
			$individual_fname = $row->individual_fname;
			$individual_mname = $row->individual_mname;
			$individual_lname  = $row->individual_lname;
			$personnel_fname = $row->personnel_fname;
			$personnel_onames = $row->personnel_onames;
			$guaranteed_amount = $row->guaranteed_amount;
			$total_guaranateed_amount += $guaranteed_amount;
			$guaranteed_amount = number_format($guaranteed_amount, 2);
			$created = date('jS M Y H:i:s',strtotime($row->created));
			
			$no_loans_guaranteed = $this->individual_model->get_no_loans_guaranteed($individual_id);
			$sum_guaranteed= $this->individual_model->get_total_sum_guaranteed($individual_id);
			
			//all the loans the guarantor has applied for
			$loans_guaranteed  = $this->individual_model->get_guaranteed_loans($individual_id);
			
			//loans that have been applied for by the guarantor
			$individual_loan = $this->individual_model->get_individual_loans($individual_id);
			$count++;
			$result1 = '';
			$result .= 
			'
				<tr>
					<td>'.$count.'</td>
					<td>'.$individual_fname.' '.$individual_mname.' '.$individual_lname.'</td>
					<td>'.$guaranteed_amount.'</td>
					<td>'.$no_loans_guaranteed.'</td>
					<td>'.$sum_guaranteed.'</td>
					<td>'.$created.'</td>
					<td>'.$personnel_fname.' '.$personnel_onames.'</td>
					<td>
						<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#more'.$loan_guarantor_id.'">
							<i class="fa fa-plus"></i>
						</button>
						<!-- Modal -->
						<div class="modal fade" id="more'.$loan_guarantor_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog modal-dialog-wide" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
										<h4 class="modal-title">'.$individual_fname.' '.$individual_mname.' '.$individual_lname.'</h4>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-6">
												<section class="panel">
													<header class="panel-heading">						
														<h2 class="panel-title">Loans Applied</h2>
													</header>
													<div class="panel-body">';
														$result1 = '';
														if($individual_loan->num_rows()> 0)
														{
															$result .=  '
															<table class="table table-bordered table-striped table-condensed">
																<tr>
																	<th>#</th>
																	<th>Loan amount</th>
																	<th>Applied on</th>
																	<th>Approved amount</th>
																	<th>Loan Status</th>
																</tr>
															';
															$counts = 0;
															foreach($individual_loan->result() as $loans_by_guarantor)
															{
																$loan_guarantor_amount = $loans_by_guarantor->proposed_amount;
																$loan_application_date = $loans_by_guarantor->application_date;
																$loan_approved_amount = $loans_by_guarantor->approved_amount;
																$loan_status = $loans_by_guarantor->individual_loan_status;
																$counts++;
																if($loan_status == 0)
																{
																	$status = '<span class="label label-default">Pending approval</span>';
																}
																else if($loan_status == 1)
																{
																	$status = '<span class="label label-default">Approved</span>';
																}
																else if($loan_status == 2)
																{
																	$status = '<span class="label label-default">Disbursed</span>';
																}
															 
																$result .=  '
																	<tr>
																		<td>'.$counts.'</td>
																		<td>'.$loan_guarantor_amount.'</td>
																		<td>'.$loan_application_date.'</td>
																		<td>'.$loan_approved_amount.'</td>
																		<td>'.$status.'</td>
																	</tr>
																</table>';
															}
														}
														else
														{
															$result .=  'No loans applied for.';
														}
														$result .= '
													</div>
												</section>
											</div>
											<div class="col-md-6">
												<section class="panel">
													<header class="panel-heading">						
														<h2 class="panel-title">Loans Guaranteed</h2>
													</header>
													<div class="panel-body">';
														$counter = 0;
														if($loans_guaranteed->num_rows()>0)
														{
															$result .=  '
															<table class="table table-bordered table-striped table-condensed">
																<tr>
																	<th>#</th>
																	<th>Loan applicant</th>
																	<th>Applied amount</th>
																	<th>Guaranteed amount</th>
																</tr>
															';
															foreach($loans_guaranteed->result() as $all_guaranteed_loans)
															{
																$loan_applicant_fname = $all_guaranteed_loans->individual_fname;$loan_applicant_mname = $all_guaranteed_loans->individual_mname;$loan_applicant_lname = $all_guaranteed_loans->individual_lname;
																$loan_applied_amount = $all_guaranteed_loans->proposed_amount;
																$loan_guaranteed_amount = $all_guaranteed_loans->guaranteed_amount;
																$counter++;
																
																$result .=  '
																	<tr>
																		<td>'.$counter.'</td>
																		<td>'.$loan_applicant_fname.' '.$loan_applicant_mname.' '.$loan_applicant_lname.'</td>
																		<td>'.$loan_applied_amount.'</td>
																		<td>'.$loan_guaranteed_amount.'</td>
																	</tr>
																';
															}
															$result .='</table>';
														}
														else
														{
															$result .=  'No loans guaranteed';
														}
														$result .= '
													</div>
												</section>
											</div>
										</div>
									</div>
									
								</div>
							</div>
						</div>';
						
						$result .='
					</td>
					<td><a href="'.site_url().'microfinance/delete-guarantor/'.$individual_loan_id.'/'.$source_individual_id.'/'.$loan_guarantor_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$individual_fname.' '.$individual_lname.'?\');" title="Delete '.$individual_fname.' '.$individual_lname.'"><i class="fa fa-trash"></i></a></td>
				</tr> 
			';
		}
		$result .= 
		'
			<tr>
				<td></td>
				<td></td>
				<th>'.number_format($total_guaranateed_amount, 2).'</th>
				<td></td>
				<td></td>
			</tr>
		';
		
		$result .= 
		'
					  </tbody>
					</table>
		';
	}
	
	else
	{
		$result = "<p>No guarantors added</p>";
	}
//repopulate data if validation errors occur
$validation_error = validation_errors();
				
$individual_id = set_value('individual_id');
$guaranteed_amount = set_value('guaranteed_amount');

?>
          <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title">Loans guarantors</h2>
                </header>
                <div class="panel-body">
            
					<?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-lg-5 control-label">Guarantor: </label>
                                    
                                    <div class="col-lg-7">
                                        <select class="form-control custom-select" name="individual_id" id="individual_id">
                                            <option value="">--Select guarantor--</option>
                                            <?php
                                                if($individuals->num_rows() > 0)
                                                {
                                                    $query = $individuals->result();
                                                    
                                                    foreach($query as $res)
                                                    {
                                                        $individual_number = $res->individual_number;
                                                        $individual_fname = $res->individual_fname;
                                                        $individual_lname = $res->individual_lname;
                                                        $db_individual_id = $res->individual_id;
                                                        
                                                        if($db_individual_id == $individual_id)
                                                        {
                                                            echo '<option value="'.$db_individual_id.'" selected>'.$individual_fname.' '.$individual_lname.' - '.$individual_number.'</option>';
                                                        }
                                                        
                                                        else
                                                        {
                                                            echo '<option value="'.$db_individual_id.'">'.$individual_fname.' '.$individual_lname.' - '.$individual_number.'</option>';
                                                        }
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                             
                            <div class="col-md-6">
                                
                                
                                
                                <div class="form-group">
                                    <label class="col-lg-5 control-label">Guaranteed amount: </label>
                                    
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" name="guaranteed_amount" placeholder="Guaranteed amount" value="<?php echo $guaranteed_amount;?>">
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="row" style="margin-top:10px;">
                        	<div class="col-md-6">
                                
                                
                                
                                <div class="form-group">
                                    <label class="col-lg-5 control-label">Loan Amount: </label>
                                    
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" name="loan_amount" placeholder="Guaranteed amount" value="<?php echo $loan_amount;?>" readonly>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-lg-5 control-label">Total Guaranteed amount: </label>
                                    
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" name="total_guaranateed_amount" placeholder="Guaranteed amount" value="<?php echo $total_guaranateed_amount;?>" readonly>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="row" style="margin-top:10px;">
                            <div class="col-md-12">
                                <div class="form-actions center-align">
                                    <button class="btn btn-primary" type="submit">
                                        Add guarantor
                                    </button>
                                </div>
                            </div>
                        </div>
                    <?php echo form_close();?> 
					<div class= "row" style="margin-bottom:20px;">
						<div class="col-lg-3">
							 <a href="<?php echo site_url();?>microfinance/add-individual" class="btn btn-sm btn-success pull-right">New Guarantor</a>
						</div>
						<div class="col-lg-9">
							<a href="<?php echo site_url();?>microfinance/edit-individual/<?php echo $source_individual_id;?>" class="btn btn-sm btn-info pull-right">Back to loan</a>
						</div>
					</div>
					<h4>Guarantors</h4>
					<?php
					if($total_guaranateed_amount < $loan_amount)
                    {
						$this->session->set_userdata("error_message", "The amount applied for has not been fully covered by the guarantors");
					}
                    $success = $this->session->userdata('success_message');

                    if(!empty($success))
                    {
                        echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
                        $this->session->unset_userdata('success_message');
                    }
                    
                    $error = $this->session->userdata('error_message');
                    
                    if(!empty($error))
                    {
                        echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
                        $this->session->unset_userdata('error_message');
                    }
                    ?>
                    <?php 
                        echo $result;
                    ?>
                    
                </div>
            </section>

<script type="text/javascript">
$(document).ready(function(){

	$(function() {
		$("#individual_id").customselect();
	});
});
</script>