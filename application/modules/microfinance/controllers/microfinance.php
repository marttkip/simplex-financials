<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Microfinance extends MX_Controller 
{
	function __construct()
	{
		parent:: __construct();
		
		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/users_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/file_model');
		$this->load->model('admin/admin_model');
		$this->load->model('individual_model');
		$this->load->model('group_model');
		$this->load->model('savings_plan_model');
		$this->load->model('loans_plan_model');
		$this->load->model('payments_model');
		$this->load->model('withdrawals_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('microfinance_model');

		
		// if(!$this->auth_model->check_login())
		// {
		// 	redirect('login');
		// }
	}
    
	/*
	*
	*	Dashboard
	*
	*/
	public function dashboard() 
	{
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];
		
		$data['content'] = $this->load->view('dashboard', $v_data, true);
		
		$this->load->view('templates/general_page', $data);
	}
	public function microfinance_configuration()
	{
		$v_data['paye'] = $this->microfinance_model->get_processing_fee(); 
		$data['title'] = $v_data['title'] = ' Configuration';
		
		$data['content'] = $this->load->view("configuration/configuration", $v_data, TRUE);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function add_new_processing_fee()
	{
		$this->form_validation->set_rules('paye_from', 'From', 'required|numeric|xss_clean');
		$this->form_validation->set_rules('paye_to', 'To', 'numeric|xss_clean');
		$this->form_validation->set_rules('paye_amount', 'Amount', 'required|numeric|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$fee_id = $this->microfinance_model->add_new_fee();
			if($fee_id != FALSE)
			{
				$this->session->set_userdata("success_message", "Amount added successfully");
				redirect('microfinance/configuration');
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add amount. Please try again");
			}
		}
		
		$this->microfinance_configuration();
	}
	public function add_processing_fee_payment($individual_id,$individual_loan_id)
	{
		//form validation rules
		$this->form_validation->set_rules('fee_amount_paid', 'Payment_amount', 'required|xss_clean');
		$this->form_validation->set_rules('payment_method_id','Payment method','required|xss_clean');
		$this->form_validation->set_rules('transaction_code', ' Transaction Code', 'required|xss_clean');
		$this->form_validation->set_rules('payment_date', 'Payment date', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if($this->individual_model->add_processing_fee_payment($individual_loan_id))
			{
				$this->session->set_userdata("success_message", "Procesing fee Payment added successfully");
			}
			else
			{
				$this->session->set_userdata("error_message", "Could not add payment. Please try again");
			}
		}
		redirect('microfinance/edit-individual/'.$individual_id);
	}
	public function edit_processing_fee($processing_fee_id)
	{
		$this->form_validation->set_rules('paye_from'.$processing_fee_id, 'From', 'required|numeric|xss_clean');
		$this->form_validation->set_rules('paye_to'.$processing_fee_id, 'To', 'numeric|xss_clean');
		$this->form_validation->set_rules('paye_amount'.$processing_fee_id, 'Amount', 'required|numeric|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if($this->microfinance_model->edit_processing_fee($processing_fee_id))
			{
				$this->session->set_userdata("success_message", "Amount edited successfully");
				redirect('microfinance/configuration');
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not edit amount. Please try again");
			}
		}
		
		$this->microfinance_configuration();
	}
}
?>