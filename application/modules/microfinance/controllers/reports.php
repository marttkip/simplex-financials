<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/microfinance/controllers/microfinance.php";

class Reports extends microfinance 
{
	
	function __construct()
	{
		parent:: __construct();
		
		$this->csv_path = realpath(APPPATH . '../assets/csv');
		$this->load->model('reports_model');
		$this->load->model('individual_model');
	}
	
	public function dashboard()
	{
		$data['content'] = $this->load->view('reports/dashboard', '', TRUE);
		
		$data['title'] = 'Dashboard';
		$data['sidebar'] = 'admin_sidebar';
		$this->load->view('admin/templates/general_page', $data);
	}
	
	function interest_revenue_totals($timestamp)
	{
		$date = gmdate("Y-m-d", ($timestamp/1000));
		$month = date('m',strtotime($date));
		$year = date('Y',strtotime($date));
		
		//initialize required variables
		$highest_bar = 0;
		
		//get outpatient total
		$total_interest_revenue = $this->reports_model->month_interest_payments($month, $year);
		$total_loan_payments = $this->reports_model->month_loan_payments($month, $year);
		$total_savings_payments = $this->reports_model->month_savings_payments($month, $year);
		//mark the highest bar
		$highest_bar = $total_interest_revenue;
		
		//prep data for the particular application type
		$result[strtolower('interest')] = $total_interest_revenue;
		$result[strtolower('loans')] = $total_loan_payments;
		$result[strtolower('savings')] = $total_savings_payments;
		
		$result['highest_bar'] = $highest_bar;//var_dump($result['bars']);
		echo json_encode($result);
	}
	
	function financial_totals()
	{	
		//get all loan_type types
		$loans_result = $this->reports_model->get_all_loan_types();
		
		//initialize required variables
		$totals = '';
		$names = '';
		$highest_bar = 0;
		$r = 1;
		
		if($loans_result->num_rows() > 0)
		{
			$result = $loans_result->result();
			
			foreach($result as $res)
			{
				$loan_type_status = $res->loan_type_status;
				$loan_type_name = $res->loan_type_name;
				
				//get loan_type total
				$total = $this->reports_model->get_loan_type_total($loan_type_status);
				
				//mark the highest bar
				if($total > $highest_bar)
				{
					$highest_bar = $total;
				}
				
				if($r == $loans_result->num_rows())
				{
					$totals .= $total;
					$names .= $loan_type_name;
				}
				
				else
				{
					$totals .= $total.',';
					$names .= $loan_type_name.',';
				}
				$r++;
			}
		}
		
		$result['total_loan_types'] = $loans_result->num_rows();
		$result['names'] = $names;
		$result['bars'] = $totals;
		$result['highest_bar'] = $highest_bar;
		echo json_encode($result);
	}
	
	function get_loan_applications()
	{	
		//get all appointments
		$applications_result = $this->reports_model->get_all_applications();
		
		//initialize required variables
		$totals = '';
		$highest_bar = 0;
		$r = 0;
		$data = array();
		
		if($applications_result->num_rows() > 0)
		{
			$result = $applications_result->result();
			
			foreach($result as $res)
			{
				$application_date = date('D M d Y',strtotime($res->application_date)); 
				$time_start = $application_date.' 00:00:00 GMT+0300'; 
				$time_end = $application_date.' 00:00:00 GMT+0300';
				$loan_application_status = $res->individual_loan_status;
				if($loan_application_status == 0)
				{
					$loan_application_status = 'Pending approval';
				}
				else
				{
					$loan_application_status = 'Approved';
				}
				
				$individual_id = $res->individual_id;
				//$color = $this->reception_model->random_color();
				$color = '#0088CC';
				
				$data['title'][$r] = $res->individual_fname.' '.$res->individual_mname.' '.$res->individual_lname.' '.$loan_application_status.' '.$res->proposed_amount;
				$data['start'][$r] = $time_start;
				$data['end'][$r] = $time_end;
				$data['backgroundColor'][$r] = $color;
				$data['borderColor'][$r] = $color;
				$data['allDay'][$r] = FALSE;
				$data['url'][$r] = site_url().'microfinance/edit-individual/'.$individual_id;
				$r++;
			}
		}
		
		$data['total_events'] = $r;
		echo json_encode($data);
	}
	
	public function loans()
	{
		$where = 'individual.individual_id = individual_loan.individual_id';
		
		$table = 'individual, individual_loan';
		//pagination
		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'mfi-reports/loans';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$all_loans = $this->reports_model->get_all_loans($table, $where, $config["per_page"], $page, $order='individual_id', $order_method = 'ASC');
		
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$v_data['title'] = 'Loans';
		
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['all_loans'] = $all_loans;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('reports/loans', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function member_balances()
	{

		$individual_search = $this->session->userdata('individual_search');
		//$where = '(visit_type_id <> 2 OR visit_type_id <> 1) AND individual_delete = '.$delete;
		$where = 'individual.individual_id > 0 AND individual_type.individual_type_id = individual.individual_type_id  AND individual_loan.individual_id = individual.individual_id';

		$table = 'individual,individual_type,individual_loan';
		//pagination
		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'mfi-reports/member-balances';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#"';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->individual_model->get_all_individual($table, $where, $config["per_page"], $page, $order='individual_lname', $order_method = 'ASC');
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Member Balances';
		
		$search_title = $this->session->userdata('individual_search_title');
		$v_data['title'] = $data['title'];
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['all_individual'] = $this->individual_model->all_individual();
		$v_data['individual_types'] = $this->individual_model->get_individual_types();
			
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('reports/loan_balances', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
		
	}

	public function export_balances()
	{
		$this->reports_model->export_member_balances();
	}
	public function loan_repayments()
	{
		$title = 'Loan Repayments';
		$v_data['title'] = $title;
		$v_data['all_loan_repayments'] = $this->reports_model->get_all_loan_repayments();
		//var_dump($v_data['all_loan_repayments'] );die();
		$data['content'] = $this->load->view('reports/loan_repayments', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function loan_repayment_export()
	{
		$title = 'Loan Repayments';
		$v_data['title'] = $title;
		$v_data['all_loan_repayments'] = $this->reports_model->get_all_loan_repayments();
		$html = $this->load->view('reports/loan_repayments', $v_data, true);
		//load mPDF library
        $this->load->library('mpdf');
		
		$this->mpdf->WriteHTML($html);
		$this->mpdf->Output();
	}
	
	public function clear_loan($individual_loan_id)
	{
		$this->reports_model->clear_loan($individual_loan_id);
		$this->session->set_userdata('success_message', 'Loan cleared successfully');
		redirect('mfi-reports/loans');
	}
	public function interest_revenue()
	{
		$where = 'individual.individual_id = individual_loan.individual_id AND individual_loan.individual_loan_id = loan_payment.individual_loan_id';
		$table = 'individual,individual_loan,loan_payment';
		//pagination
		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'mfi-reports/interest-revenue';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reports_model->get_interest_revenue_loans($table, $where, $config["per_page"], $page, $order='individual.individual_id', $order_method = 'ASC');//		echo $where;
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		else
		{
			$order_method = 'DESC';
		}
		$v_data['title'] = 'Interest Revenue';
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('reports/interest_revenue', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	
	public function export_interest_revenue()
	{
		
		$where = 'individual.individual_id = individual_loan.individual_id AND individual_loan.individual_loan_id = loan_payment.individual_loan_id';
		$table = 'individual,individual_loan,loan_payment';
		//pagination
		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'mfi-reports/interest-revenue';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#"';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->individual_model->get_all_individual($table, $where, $config["per_page"], $page, $order='loan_payment.individual_loan_id', $order_method = 'ASC');
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		else
		{
			$order_method = 'DESC';
		}
		$v_data['title'] = 'Interest Revenue';
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$html = $this->load->view('reports/interest_revenue', $v_data, true);
		//load mPDF library
        $this->load->library('mpdf');
		
		$this->mpdf->WriteHTML($html);
		$this->mpdf->Output();
	}
	public function export_loans()
	{
		$where = 'individual.individual_id = individual_loan.individual_id';
		
		$table = 'individual, individual_loan';
		//pagination
		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'mfi-reports/loans';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#"';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$all_loans = $this->reports_model->get_all_loans($table, $where, $config["per_page"], $page, $order='individual_id', $order_method = 'ASC');
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		else
		{
			$order_method = 'DESC';
		}
		$v_data['title'] = 'Loans';
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['all_loans'] = $all_loans;
		$v_data['page'] = $page;
		$html = $this->load->view('reports/loans', $v_data, true);
		//load mPDF library
        $this->load->library('mpdf');
		
		$this->mpdf->WriteHTML($html);
		$this->mpdf->Output();
	}
	
	public function savings_collection()
	{
		$where = 'individual.individual_id = savings_payment.individual_id';
		$table = 'individual, savings_payment';
		//pagination
		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'mfi-reports/savings-collection';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#"';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$all_individual_savings = $this->reports_model->get_all_savings_individual($table, $where, $config["per_page"], $page, $order='individual_id', $order_method = 'ASC');
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		else
		{
			$order_method = 'DESC';
		}
		$v_data['title'] = 'Savings Collected';
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['all_individual_savings'] = $all_individual_savings;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('reports/savings_collected', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	
	public function export_savings()
	{
		$where = 'individual.individual_id = savings_payment.individual_id';
		$table = 'individual, savings_payment';
		//pagination
		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'mfi-reports/savings-collection';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#"';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$all_individual_savings = $this->reports_model->get_all_savings_individual($table, $where, $config["per_page"], $page, $order='individual_id', $order_method = 'ASC');
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		else
		{
			$order_method = 'DESC';
		}
		$v_data['title'] = 'Savings Collected';
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['all_individual_savings'] = $all_individual_savings;
		$v_data['page'] = $page;
		$html = $this->load->view('reports/savings_collected', $v_data, true);
		//load mPDF library
        $this->load->library('mpdf');
		
		$this->mpdf->WriteHTML($html);
		$this->mpdf->Output();
	}
	
	public function defaulters()
	{
		//check for all defaulters
		$v_data['defaulters'] = $this->reports_model->get_all_defaulters();
		$v_data['title'] = 'Defaulters';
		$data['content'] = $this->load->view('reports/defaulters', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function fees()
	{
		$where = 'individual.individual_id = individual_loan.individual_id AND individual_loan.individual_loan_id = individual_loan_processing_fee.individual_loan_id';
		
		$table = 'individual, individual_loan,individual_loan_processing_fee';
		//pagination
		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'processing-fee';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#"';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$all_fees = $this->reports_model->get_all_fees_paid($table, $where, $config["per_page"], $page, $order='individual.individual_id', $order_method = 'ASC');
		
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$v_data['title'] = 'Processing Fee';
		
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['all_fees'] = $all_fees;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('reports/processing', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function verify_processing_fee($individual_loan_processing_fee_id)
	{
		if(($this->reports_model->verify_processing_fee($individual_loan_processing_fee_id))==TRUE)
		{
			$this->session->set_userdata('success_message', 'Processing fee verified successfully');
		}
		
		else
		{
			$this->session->set_userdata("error_message", "Could not verify fee. Please try again");
		}
		redirect('processing-fee');
	}
	public function edit_processing_fee($individual_loan_processing_fee_id)
	{
		//form validation
		$this->form_validation->set_rules('processing_amount','Processing Amount','required|xss_clean');
		$this->form_validation->set_rules('transaction_code','Code','required|xss_clean');
		if ($this->form_validation->run())
		{
			//update the processing fee amount
			if(($this->reports_model->update_processing_fee($individual_loan_processing_fee_id))== TRUE)
			{
				$this->session->set_userdata('success_message', 'Processing fee updated successfully');
				redirect('processing-fee');
			}
			else
			{
				$this->session->set_userdata("error_message", "Could not update fee. Please try again");
			}
		}
		$v_data['processing_fee_details'] = $this->reports_model->get_fee_details($individual_loan_processing_fee_id);//var_dump($processing_fee_details);die();
		$data['title'] = 'Edit Procesing Fee';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('reports/edit_fee', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function create_guarantor_loan($loan_guarantor_id,$deduction_amount,$individual_loan_id)
	{
		if(($this->reports_model->create_non_interest_loan_for_guarantor($loan_guarantor_id,$deduction_amount,$individual_loan_id))==TRUE)
		{
			$this->session->set_userdata('success_message', 'Interest free loan created for guarantor created successfully');
		}
		else
		{
			$this->session->set_userdata("error_message", "Could not create guarantor loan. Please try again");
		}
		redirect('mfi-reports/defaulters');
	}
	public function guarantor_loans()
	{
		$where = 'guarantor_loan.loan_guarantor_id = loan_guarantor.loan_guarantor_id AND guarantor_loan.individual_loan_id = individual_loan.individual_loan_id AND loan_guarantor.individual_id = individual.individual_id ';
		$table = 'guarantor_loan,loan_guarantor,individual_loan,individual';
		//pagination
		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'mfi-reports/defaulter-loans';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#"';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->reports_model->get_all_defaulter_loans($table, $where, $config["per_page"], $page, $order='guarantor_loan.guarantor_loan_id', $order_method = 'ASC');
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		else
		{
			$order_method = 'DESC';
		}
		$v_data['title'] = 'Guarantor Non Interest Loans';
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('reports/guarantor_loans', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function deactivate_individual_guarantor_loan($guarantor_loan_id)
	{
		if(($this->reports_model->deactivate_individual_guarantor_loan($guarantor_loan_id))==TRUE)
		{
			$this->session->set_userdata("success_message", "Non interest guarantor loan deactivated added successfully");
		}
		else
		{
			$this->session->set_userdata("error_message", "Could not deactivator guarantor non interest loan. Please try again");
		}
		redirect('mfi-reports/guarantor-loans');
	}
	public function activate_individual_guarantor_loan($guarantor_loan_id)
	{
		if(($this->reports_model->activate_individual_guarantor_loan($guarantor_loan_id))==TRUE)
		{
			$this->session->set_userdata("success_message", "Non interest guarantor loan activated added successfully");
		}
		else
		{
			$this->session->set_userdata("error_message", "Could not activator guarantor non interest loan. Please try again");
		}
		redirect('mfi-reports/guarantor-loans');
	}
	public function guarantor_loan_payment($guarantor_loan_id)
	{
		//form validation
		$this->form_validation->set_rules('fee_amount_paid','Amount','required|xss_clean');
		$this->form_validation->set_rules('transaction_code','Transaction code','required|xss_clean');
		$this->form_validation->set_rules('payment_date','Date','required|xss_clean');
		
		if($this->form_validation->run())
		{
			if($this->payments_model->add_guarantor_loan_payment($guarantor_loan_id))
			{
				$this->session->set_userdata("success_message", "Guarantor loan payment made successfully");
			}
			else
			{
				$this->session->set_userdata("error_message", "Could not add guarantor loan payment. Please try again");
			}
		}
		redirect('mfi-reports/guarantor-loans');
	}
	public function deduction_schedule()
	{
		$data['title'] = 'Deduction Schedule';
		$v_data['title'] = ' Generate Deduction Schedule';
		$data['content'] = $this->load->view('reports/deduction_schedule', $v_data, TRUE);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function generate_schedule()
	{
		//form validation rules
		$this->form_validation->set_rules('branch_id', 'Site', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$branch_id = $this->input->post('branch_id');
			if(!empty($branch_id))
			{
				$v_data['query'] = $this->reports_model->get_all_uncleared_loan($branch_id);
			}
		}
		$data['title'] = 'Schedule For month 3';
		$v_data['title'] = 'Schedule';
		$data['content'] = $this->load->view('reports/schedule', $v_data, TRUE);
		$this->load->view('admin/templates/general_page', $data);
	}
	//penalties
	public function get_month_penalties()
	{
		//get all loans that have been disbursed
		$disbursed_loans = $this->get_disbursed_loans();//var_dump($disbursed_loans);die();
		if($disbursed_loans->num_rows() > 0)
		{
			$penalty_fee = $new_month_repayment = 0;
			//get each disbursement date
			foreach($disbursed_loans->result() as $loans_disbursed)
			{
				$initial_disbursement_date = $loans_disbursed->disbursed_date;
				$individual_loan_id = $loans_disbursed->individual_loan_id;
				$no_of_repayments = $loans_disbursed->no_of_repayments;
				
				$initial_disbursement_date_explode = explode('-',$initial_disbursement_date);
				$initial_disbursement_date_year = $initial_disbursement_date_explode[0];
				$initial_disbursement_date_month = $initial_disbursement_date_explode[1];
				$initial_disbursement_date_day = $initial_disbursement_date_explode[2];
				
				//get current month
				$current_date = date('Y-m-d');
				$current_date_explode = explode('-',$current_date);
				$current_year = $current_date_explode[0];
				$current_month = $current_date_explode[1];
				
				//check that disbursement date and current dates are the same to impose penalties
				if(($initial_disbursement_date_year==$current_year) &&($initial_disbursement_date_month==$current_month) &&($initial_disbursement_date_day <= 20))
				{
					//check if payments have been made for that month
					if(($this->loan_repaid($individual_loan_id,$current_month,$current_year))==FALSE)
					{
						//then apply for penalty
						$repayment_no = $this->payments_model->get_repayment_number($individual_loan_id);
						$interest_expected_for_that_month = $this->payments_model->get_interest_expected($individual_loan_id, $repayment_no);
						$principal_expected_for_that_month = $this->payments_model->get_principal_expected($individual_loan_id, $repayment_no);
						$total_month_loan = $interest_expected_for_that_month + $principal_expected_for_that_month;
						$penalty_fee = (0.10 * $total_month_loan);
						$new_month_repayment = $total_month_loan + $penalty_fee;
					}
				}
				echo $individual_loan_id.'-'.$new_month_repayment;
			}
		}
		
	}
	function get_disbursed_loans()
	{
		$this->db->select('*');
		$this->db->where('individual_loan_status = 2 AND disbursed_amount > 0');
		$disbursed_loans = $this->db->get('individual_loan');
		return $disbursed_loans;
	}
	function loan_repaid($individual_loan_id,$current_month,$current_year)
	{
		$this->db->select('*');
		$this->db->where('individual_loan_id = '.$individual_loan_id.' AND payment_date BETWEEN "'.$current_year.'-'.$current_month.'-01" AND "'.$current_year.'-'.$current_month.'-31"');
		$loan_paid = $this->db->get('loan_payment');
		if($loan_paid->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}