<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/microfinance/controllers/microfinance.php";

class Loans_application extends microfinance 
{
	function __construct()
	{
		parent:: __construct();
		
		$this->load->library('image_lib');

		$this->load->model('savings/member_savings_model');
		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/users_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/file_model');
		$this->load->model('admin/admin_model');
		$this->load->model('individual_model');
		$this->load->model('group_model');
		$this->load->model('savings_plan_model');
		$this->load->model('loans_plan_model');
		$this->load->model('payments_model');
		$this->load->model('withdrawals_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('microfinance_model');
		
		//path to image directory
		$this->individual_path = realpath(APPPATH . '../assets/img/individuals');
		$this->individual_location = base_url().'assets/img/individuals/';
		$this->signature_path = realpath(APPPATH . '../assets/img/signatures');
		$this->signature_location = base_url().'assets/img/signatures/';
		$this->identification_document_path = realpath(APPPATH . '../assets/img/identification_documents');
		$this->identification_document_location = base_url().'assets/img/identification_documents/';

		$this->document_upload_path = realpath(APPPATH . '../assets/img/document_uploads');
		$this->document_upload_location = base_url().'assets/img/document_uploads/';
	}
    
	/*
	*
	*	Default action is to show all the loans_plan
	*
	*/
	public function index($order = 'individual_lname', $order_method = 'ASC') 
	{
		$individual_search = $this->session->userdata('individual_search');
		//$where = '(visit_type_id <> 2 OR visit_type_id <> 1) AND individual_delete = '.$delete;
		$where = 'individual_id > 0';
		if(!empty($individual_search))
		{
			$where .= $individual_search;
		}
		
		$table = 'individual';
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'loan-management/loan-application/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#"';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->member_savings_model->get_all_individual($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Members';
		
		$search_title = $this->session->userdata('individual_search_title');
			
		if(!empty($search_title))
		{
			$v_data['title'] = 'Members filtered by :'.$search_title;
		}
		
		else
		{
			$v_data['title'] = $data['title'];
		}
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['all_individual'] = $this->member_savings_model->all_individual();
		$v_data['individual_types'] = $this->member_savings_model->get_individual_types();
			
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('loans_plan/all_member_loans', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
    
	/*
	*
	*	Add a new loans_plan
	*
	*/
	public function add_loans_plan() 
	{
		//form validation rules
		$this->form_validation->set_rules('loan_type', 'Loan Type', 'required|xss_clean');
		$this->form_validation->set_rules('interest_id', 'Interest type', 'required|xss_clean');
		$this->form_validation->set_rules('interest_rate', 'Interest rate', 'required|xss_clean');
		$this->form_validation->set_rules('loans_plan_name', 'Name', 'required|xss_clean');
		$this->form_validation->set_rules('installment_type_id', 'Installment type', 'required|xss_clean');
		$this->form_validation->set_rules('grace_period_minimum', 'Minimum grace period', 'numeric|xss_clean');
		$this->form_validation->set_rules('grace_period_maximum', 'Maximum grace period', 'numeric|xss_clean');
		$this->form_validation->set_rules('grace_period_default', 'Default grace period', 'numeric|xss_clean');
		$this->form_validation->set_rules('charge_interest_over_grace_period', 'Charge interest over grace period', 'xss_clean');
		$this->form_validation->set_rules('maximum_loan_amount', 'Maximum loan amount', 'numeric|xss_clean');
		$this->form_validation->set_rules('minimum_loan_amount', 'Minimum loan amount', 'numeric|xss_clean');
		$this->form_validation->set_rules('custom_loan_amount', 'Custom loan amount', 'numeric|xss_clean');
		$this->form_validation->set_rules('maximum_number_of_installments', 'Maximum number of installments', 'numeric|xss_clean');
		$this->form_validation->set_rules('minimum_number_of_installments', 'Minimum number of installments', 'numeric|xss_clean');
		$this->form_validation->set_rules('custom_number_of_installments', 'Custom number of installments', 'numeric|xss_clean');
		$this->form_validation->set_rules('minimum_late_fee_on_total_loan', 'Minimum late fee on total loan', 'numeric|xss_clean');
		$this->form_validation->set_rules('maximum_late_fee_on_total_loan', 'Maximum late fee on total loan', 'numeric|xss_clean');
		$this->form_validation->set_rules('custom_late_fee_on_total_loan', 'Custom late fee on total loan', 'numeric|xss_clean');
		$this->form_validation->set_rules('minimum_late_fee_on_overdue_principal', 'Minimum late fee on overdue principal', 'numeric|xss_clean');
		$this->form_validation->set_rules('maximum_late_fee_on_overdue_principal', 'Maximum late fee on overdue principal', 'numeric|xss_clean');
		$this->form_validation->set_rules('custom_late_fee_on_overdue_principal', 'Custom late fee on overdue principal', 'numeric|xss_clean');
		$this->form_validation->set_rules('minimum_late_fee_on_overdue_interest', 'Minimum late fee on overdue interest', 'numeric|xss_clean');
		$this->form_validation->set_rules('maximum_late_fee_on_overdue_interest', 'Maximum late fee on overdue interest', 'numeric|xss_clean');
		$this->form_validation->set_rules('custom_late_fee_on_overdue_interest', 'Custom late fee on overdue interest', 'numeric|xss_clean');
		$this->form_validation->set_rules('maximum_number_of_guarantors', 'Maximum number of guarantors', 'numeric|xss_clean');
		$this->form_validation->set_rules('minimum_number_of_guarantors', 'Minimum_ number of guarantors', 'numeric|xss_clean');
		$this->form_validation->set_rules('custom_number_of_guarantors', 'Custom number of guarantors', 'numeric|xss_clean');
		 
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$loans_plan_id = $this->loans_plan_model->add_loans_plan();
			if($loans_plan_id != FALSE)
			{
				$this->session->set_userdata("success_message", "Loan plan added successfully");
				redirect('loan-management/setup');
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add loan plan. Please try again");
			}
		}
		
		$v_data['interest_scheme'] = $this->loans_plan_model->get_interest_scheme();
		$v_data['installments'] = $this->loans_plan_model->get_installment_types();
		$data['title'] = 'Add loans plan';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('loans_plan/add_loans_plan', $v_data, true);
		// print_r($v_data['interest_scheme']);
		$this->load->view('admin/templates/general_page', $data);
	}
    
	/*
	*
	*	Edit an existing loans_plan
	*	@param int $loans_plan_id
	*
	*/
	public function edit_loans_plan($loans_plan_id) 
	{	
		//form validation rules
		$this->form_validation->set_rules('interest_id', 'Interest type', 'required|xss_clean');
		$this->form_validation->set_rules('interest_rate', 'Interest rate', 'required|xss_clean');
		$this->form_validation->set_rules('loans_plan_name', 'Name', 'required|xss_clean');
		$this->form_validation->set_rules('installment_type_id', 'Installment type', 'required|xss_clean');
		$this->form_validation->set_rules('grace_period_minimum', 'Minimum grace period', 'numeric|xss_clean');
		$this->form_validation->set_rules('grace_period_maximum', 'Maximum grace period', 'numeric|xss_clean');
		$this->form_validation->set_rules('grace_period_default', 'Default grace period', 'numeric|xss_clean');
		$this->form_validation->set_rules('charge_interest_over_grace_period', 'Charge interest over grace period', 'xss_clean');
		$this->form_validation->set_rules('maximum_loan_amount', 'Maximum loan amount', 'numeric|xss_clean');
		$this->form_validation->set_rules('minimum_loan_amount', 'Minimum loan amount', 'numeric|xss_clean');
		$this->form_validation->set_rules('custom_loan_amount', 'Custom loan amount', 'numeric|xss_clean');
		$this->form_validation->set_rules('maximum_number_of_installments', 'Maximum number of installments', 'numeric|xss_clean');
		$this->form_validation->set_rules('minimum_number_of_installments', 'Minimum number of installments', 'numeric|xss_clean');
		$this->form_validation->set_rules('custom_number_of_installments', 'Custom number of installments', 'numeric|xss_clean');
		$this->form_validation->set_rules('minimum_late_fee_on_total_loan', 'Minimum late fee on total loan', 'numeric|xss_clean');
		$this->form_validation->set_rules('maximum_late_fee_on_total_loan', 'Maximum late fee on total loan', 'numeric|xss_clean');
		$this->form_validation->set_rules('custom_late_fee_on_total_loan', 'Custom late fee on total loan', 'numeric|xss_clean');
		$this->form_validation->set_rules('minimum_late_fee_on_overdue_principal', 'Minimum late fee on overdue principal', 'numeric|xss_clean');
		$this->form_validation->set_rules('maximum_late_fee_on_overdue_principal', 'Maximum late fee on overdue principal', 'numeric|xss_clean');
		$this->form_validation->set_rules('custom_late_fee_on_overdue_principal', 'Custom late fee on overdue principal', 'numeric|xss_clean');
		$this->form_validation->set_rules('minimum_late_fee_on_overdue_interest', 'Minimum late fee on overdue interest', 'numeric|xss_clean');
		$this->form_validation->set_rules('maximum_late_fee_on_overdue_interest', 'Maximum late fee on overdue interest', 'numeric|xss_clean');
		$this->form_validation->set_rules('custom_late_fee_on_overdue_interest', 'Custom late fee on overdue interest', 'numeric|xss_clean');
		$this->form_validation->set_rules('maximum_number_of_guarantors', 'Maximum number of guarantors', 'numeric|xss_clean');
		$this->form_validation->set_rules('minimum_number_of_guarantors', 'Minimum_ number of guarantors', 'numeric|xss_clean');
		$this->form_validation->set_rules('custom_number_of_guarantors', 'Custom number of guarantors', 'numeric|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			//update loans_plan
			if($this->loans_plan_model->edit_loans_plan($loans_plan_id))
			{
				$this->session->set_userdata('success_message', 'Loan plan updated successfully');
				redirect('microfinance/loans_plan');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update loan plan. Please try again');
			}
		}
		
		//open the add new loans_plan
		$data['title'] = 'Edit loans plan';
		$v_data['title'] = $data['title'];
		
		$v_data['loans_plan_id'] = $loans_plan_id;
		$v_data['interest_scheme'] = $this->loans_plan_model->get_interest_scheme();
		$v_data['installments'] = $this->loans_plan_model->get_installment_types();
		$v_data['loans_plan'] = $this->loans_plan_model->get_loans_plan($loans_plan_id);
		$data['content'] = $this->load->view('loans_plan/edit_loans_plan', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
    
	/*
	*
	*	Delete an existing loans_plan
	*	@param int $loans_plan_id
	*
	*/
	public function delete_loans_plan($loans_plan_id)
	{
		if($this->loans_plan_model->delete_loans_plan($loans_plan_id))
		{
			$this->session->set_userdata('success_message', 'Individual has been deleted');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Individual could not deleted');
		}
		redirect('microfinance/loans_plan');
	}
    
	/*
	*
	*	Activate an existing loans_plan
	*	@param int $loans_plan_id
	*
	*/
	public function activate_loans_plan($loans_plan_id)
	{
		$this->loans_plan_model->activate_loans_plan($loans_plan_id);
		$this->session->set_userdata('success_message', 'Individual activated successfully');
		redirect('microfinance/loans_plan');
	}
    
	/*
	*
	*	Deactivate an existing loans_plan
	*	@param int $loans_plan_id
	*
	*/
	public function deactivate_loans_plan($loans_plan_id)
	{
		$this->loans_plan_model->deactivate_loans_plan($loans_plan_id);
		$this->session->set_userdata('success_message', 'Individual disabled successfully');
		redirect('microfinance/loans_plan');
	}
	
	function add_leave()
	{
		$this->form_validation->set_rules('loans_plan_id', 'Individual', 'trim|numeric|required|xss_clean');
		$this->form_validation->set_rules('start_date', 'Start Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('end_date', 'End Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('leave_type_id', 'Leave Type', 'trim|numeric|required|xss_clean');

		if ($this->form_validation->run() == FALSE)//if there is an invalid item
		{
			$this->calender($_SESSION['navigation_id'], $_SESSION['sub_navigation_id']);
		}
		
		else//if the input is valid
		{
			$items = array(
						'loans_plan_id' => $this->input->post("loans_plan_id"),
						'start_date' => $this->input->post("start_date"),
						'end_date' => $this->input->post("end_date"),
						'leave_type_id' => $this->input->post("leave_type_id")
					);
			$result = $this->db->insert("leave_duration", $items);
			
			redirect("administration/calender/".$_SESSION['navigation_id']."/".$_SESSION['sub_navigation_id']);
		}
	}
	
	public function get_section_children($section_id)
	{
		$sub_sections = $this->sections_model->get_sub_sections($section_id);
		
		$children = '';
		
		if($sub_sections->num_rows() > 0)
		{
			foreach($sub_sections->result() as $res)
			{
				$section_id = $res->section_id;
				$section_name = $res->section_name;
				
				$children .= '<option value="'.$section_id.'" >'.$section_name.'</option>';
			}
		}
		
		else
		{
			$children = '<option value="" >--No sub sections--</option>';
		}
		
		echo $children;
	}
	
	public function get_loan_details($loans_plan_id)
	{
		$loans_plan = $this->loans_plan_model->get_loans_plan($loans_plan_id);
		$result = '';
		
		if($loans_plan->num_rows() > 0)
		{
			$row = $loans_plan->row();
			$loans_plan_id = $row->loans_plan_id;
			$loans_plan_status = $row->loans_plan_status;
			$installment_type_name = $row->installment_type_name;
			$interest_name = $row->interest_name;
			$interest_id =$row->interest_id;
			$loans_plan_name =$row->loans_plan_name;
			$maximum_loan_amount =$row->maximum_loan_amount;
			$minimum_loan_amount =$row->minimum_loan_amount;
			$custom_loan_amount =$row->custom_loan_amount;
			$installment_type_id =$row->installment_type_id;
			$grace_period_minimum =$row->grace_period_minimum;
			$grace_period_maximum =$row->grace_period_maximum;
			$grace_period_default =$row->grace_period_default;
			$charge_interest_over_grace_period =$row->charge_interest_over_grace_period;
			$maximum_number_of_installments =$row->maximum_number_of_installments;
			$minimum_number_of_installments =$row->minimum_number_of_installments;
			$custom_number_of_installments =$row->custom_number_of_installments;
			$minimum_late_fee_on_total_loan =$row->minimum_late_fee_on_total_loan;
			$maximum_late_fee_on_total_loan =$row->maximum_late_fee_on_total_loan;
			$custom_late_fee_on_total_loan =$row->custom_late_fee_on_total_loan;
			$minimum_late_fee_on_overdue_principal =$row->minimum_late_fee_on_overdue_principal;
			$maximum_late_fee_on_overdue_principal =$row->maximum_late_fee_on_overdue_principal; 
			$custom_late_fee_on_overdue_principal =$row->custom_late_fee_on_overdue_principal;  
			$minimum_late_fee_on_overdue_interest =$row->minimum_late_fee_on_overdue_interest;
			$maximum_late_fee_on_overdue_interest =$row->maximum_late_fee_on_overdue_interest;
			$custom_late_fee_on_overdue_interest =$row->custom_late_fee_on_overdue_interest;
			$maximum_number_of_guarantors =$row->maximum_number_of_guarantors;
			$minimum_number_of_guarantors =$row->minimum_number_of_guarantors;
			$custom_number_of_guarantors =$row->custom_number_of_guarantors;
			$interest_rate =$row->interest_rate;
			
			if($charge_interest_over_grace_period == 1)
			{
				$charge_interest_over_grace_period = 'Yes';
			}
			else
			{
				$charge_interest_over_grace_period = 'No';
			}
			
			$result = 
			'
						<button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#more'.$loans_plan_id.'">
						<i class="fa fa-plus"></i> View details
						</button>
					<!-- Modal -->
					<div class="modal fade" id="more'.$loans_plan_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
										<h4 class="modal-title">'.$loans_plan_name.'</h4>
									</div>
									<div class="modal-body">
										<table class="table table-bordered table-striped table-condensed">
											<tr>
												<th>Loan name</th>
												<td>'.$loans_plan_name.'</td>
											</tr>
											<tr>
												<th>Interest type</th>
												<td>'.$interest_name.'</td>
											</tr>
											<tr>
												<th>Interest rate</th>
												<td>'.$interest_rate.'</td>
											</tr>
											<tr>
												<th>Installment type</th>
												<td>'.$installment_type_name.'</td>
											</tr>
											<tr>
												<th>Minimum loan amount</th>
												<td>'.number_format($minimum_loan_amount, 2).'</td>
											</tr>
											<tr>
												<th>Maximum loan amount</th>
												<td>'.number_format($maximum_loan_amount, 2).'</td>
											</tr>
											<tr>
												<th>Fixed loan amount</th>
												<td>'.number_format($custom_loan_amount, 2).'</td>
											</tr>
											<tr>
												<th>Minimum grace period</th>
												<td>'.$grace_period_minimum.'</td>
											</tr>
											<tr>
												<th>Maximum grace period</th>
												<td>'.$grace_period_maximum.'</td>
											</tr>
											<tr>
												<th>Default grace period</th>
												<td>'.$grace_period_default.'</td>
											</tr>
											<tr>
												<th>Charge interest over grace period</th>
												<td>'.$charge_interest_over_grace_period.'</td>
											</tr>
											<tr>
												<th>Minimum number of installments</th>
												<td>'.$minimum_number_of_installments.'</td>
											</tr>
											<tr>
												<th>Maximum number of installments</th>
												<td>'.$maximum_number_of_installments.'</td>
											</tr>
											<tr>
												<th>Fixed number of installments</th>
												<td>'.$custom_number_of_installments.'</td>
											</tr>
											<tr>
												<th>Minimum late fee on total loan</th>
												<td>'.number_format($minimum_late_fee_on_total_loan, 2).'</td>
											</tr>
											<tr>
												<th>Maximum late fee on total loan</th>
												<td>'.number_format($maximum_late_fee_on_total_loan, 2).'</td>
											</tr>
											<tr>
												<th>Fixed late fee on total loan</th>
												<td>'.number_format($custom_late_fee_on_total_loan, 2).'</td>
											</tr>
											<tr>
												<th>Minimum late fee on overdue principal</th>
												<td>'.number_format($minimum_late_fee_on_overdue_principal, 2).'</td>
											</tr>
											<tr>
												<th>Maximum late fee on overdue principal</th>
												<td>'.number_format($maximum_late_fee_on_overdue_principal, 2).'</td>
											</tr>
											<tr>
												<th>Fixed late fee on overdue principal</th>
												<td>'.number_format($custom_late_fee_on_overdue_principal, 2).'</td>
											</tr>
											<tr>
												<th>Minimum late fee on overdue interest</th>
												<td>'.number_format($minimum_late_fee_on_overdue_interest, 2).'</td>
											</tr>
											<tr>
												<th>Maximum late fee on overdue interest</th>
												<td>'.number_format($maximum_late_fee_on_overdue_interest, 2).'</td>
											</tr>
											<tr>
												<th>Fixed late fee on overdue interest</th>
												<td>'.number_format($custom_late_fee_on_overdue_interest, 2).'</td>
											</tr>
											<tr>
												<th>Minimum number of guarantors</th>
												<td>'.$minimum_number_of_guarantors.'</td>
											</tr>
											<tr>
												<th>Maximum number of guarantors</th>
												<td>'.$maximum_number_of_guarantors.'</td>
											</tr>
											<tr>
												<th>Fixed number of guarantors</th>
												<td>'.$custom_number_of_guarantors.'</td>
											</tr>
										</table>
									</div>
								</div>
							</div>

						</div>
			';
		}
		
		echo $result;
	}
	
	public function amortize($individual_id)
	{
		$v_data['individual_loan_id'] = 1;
		$v_data['individual_id'] = 95;
		$v_data['loan_amount'] = 20000;
		$v_data['no_of_repayments'] = 10;
		$v_data['first_date'] = '2015-11-16';
		$v_data['interest_id'] = '1';
		$v_data['interest_rate'] = 7.5;
		$v_data['installment_type_duration'] = '30';
		$data['title'] = 'Amortization Table';
		$v_data['title'] = $data['title'];
		//echo $this->load->view('individual/get_amortization_table', $v_data, true);
		$data['content'] = $this->load->view('individual/get_amortization_table', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	public function get_individual_loans()
	{
		$json = file_get_contents('php://input');

		$decoded = json_decode($json,true);

		$phone = $decoded['phone_number'];
		$individual_id = $decoded['individual_id'];

		if (substr($phone, 0, 1) === '0') 
		{
			$phone = ltrim($phone, '0');
		}
		else if(substr($phone, 0, 4) === '+254') 
		{
			$phone = ltrim($phone, '+254');
		}
		else if(substr($phone, 0, 3) === '254') 
		{
			$phone = ltrim($phone, '254');
		}
		else if(substr($phone, 0, 1) === '7') 
		{
			$phone = $phone;
		}
		$phone = '254'.$phone;

		$this->db->where('individual_phone = "'.$phone.'" AND individual.individual_id = individual_loan.individual_id AND loans_plan.loans_plan_id = individual_loan.loans_plan_id');
		$query = $this->db->get('individual,individual_loan,loans_plan');

		
		// $v_data['individual_id'] = $individual_id;
		// $v_data['phone'] = $phone;
		$v_data['query'] = $query;



		$content = $this->load->view('individual/get_all_loans', $v_data,true);


		$response['status'] = 'success';
		$response['loans'] = $content;
		echo json_encode($response);


	}
	
	public function get_my_loan_details()
	{

		$json = file_get_contents('php://input');

		$decoded = json_decode($json,true);

		$phone = $decoded['phone_number'];
		$individual_id = $decoded['individual_id'];
		$limit = $decoded['loan_id'];
		$limit_one = $limit-1;
		if (substr($phone, 0, 1) === '0') 
		{
			$phone = ltrim($phone, '0');
		}
		else if(substr($phone, 0, 4) === '+254') 
		{
			$phone = ltrim($phone, '+254');
		}
		else if(substr($phone, 0, 3) === '254') 
		{
			$phone = ltrim($phone, '254');
		}
		else if(substr($phone, 0, 1) === '7') 
		{
			$phone = $phone;
		}
		$phone = '254'.$phone;

		// $this->db->where('individual_phone = "'.$phone.'" AND individual.individual_id = individual_loan.individual_id AND loans_plan.loans_plan_id = individual_loan.loans_plan_id');
		$query = $this->db->query('SELECT * FROM individual,individual_loan,loans_plan WHERE individual_phone = "'.$phone.'" AND individual.individual_id = individual_loan.individual_id AND loans_plan.loans_plan_id = individual_loan.loans_plan_id ORDER BY individual_loan.individual_loan_id LIMIT '.$limit_one.', '.$limit.'');

		
		// $v_data['individual_id'] = $individual_id;
		// $v_data['phone'] = $phone;
		$v_data['query'] = $query;

		$result = '';
		if($query->num_rows() > 0)
		{
			
			foreach ($query->result() as $key => $value) {
				# code...
				$individual_id = $value->individual_id;
				$approved_amount = $value->approved_amount;
				$loans_plan_name = $value->loans_plan_name;
				$individual_loan_id = $value->individual_loan_id;
			}
			$data_insert['phone_number'] = $phone;
			$data_insert['individual_id'] = $individual_id;
			$data_insert['individual_loan_id'] = $individual_loan_id;
			$this->db->insert('ussd_session',$data_insert);

		}

		$result .= "Loan: ".$loans_plan_name." : ".number_format($approved_amount,2)."\n";
		$result .= '1. Pay ';

		
		// $content = $this->load->view('individual/get_loan_detail', $v_data,true);


		$response['status'] = 'success';
		$response['loan_detail'] = $result;
		$response['individual_loan_id'] = $individual_loan_id;


		echo json_encode($response);
	}


	public function get_short_loan()
	{
		// $json = file_get_contents('php://input');

		// $decoded = json_decode($json,true);

		// $phone = $decoded['phone_number'];
		// $individual_id = $decoded['individual_id'];

		// if (substr($phone, 0, 1) === '0') 
		// {
		// 	$phone = ltrim($phone, '0');
		// }
		// else if(substr($phone, 0, 4) === '+254') 
		// {
		// 	$phone = ltrim($phone, '+254');
		// }
		// else if(substr($phone, 0, 3) === '254') 
		// {
		// 	$phone = ltrim($phone, '254');
		// }
		// else if(substr($phone, 0, 1) === '7') 
		// {
		// 	$phone = $phone;
		// }
		$phone = '254704808007';

		$this->db->select('SUM(proposed_amount) AS total_amount');
		$this->db->where('phone_number = "'.$phone.'" ');
		$query = $this->db->get('short_loan');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount = $value->total_amount;
			}
		}

		
		$this->db->select('SUM(amount_paid) AS total_amount_paid');
		$this->db->where('individual_phone = "'.$phone.'" AND individual.individual_id = individual_id.individual_id');
		$query = $this->db->get('short_loan_payments,individual');

		$total_amount_paid = 0;
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$total_amount_paid = $value->total_amount_paid;
			}
		}
		if(empty($total_amount_paid))
		{
			$total_amount_paid = 0;
		}

		$balance = $total_amount - $total_amount_paid;
		
		if($balance > 0)
		{
			$content = 'Your current short loan balance is Kes. '.number_format($balance,2).'.';
			$response['status'] = 'success';
			$response['loans'] = $content;
		}
		else
		{
			$content = 'Sorry you do not have short loans.';
			$response['status'] = 'fail';
			$response['loans'] = $content;
		}

		

		
		echo json_encode($response);


	}



	public function get_short_individual_loans()
	{
		$json = file_get_contents('php://input');

		$decoded = json_decode($json,true);

		$phone = $decoded['phone_number'];
		$individual_id = $decoded['individual_id'];

		if (substr($phone, 0, 1) === '0') 
		{
			$phone = ltrim($phone, '0');
		}
		else if(substr($phone, 0, 4) === '+254') 
		{
			$phone = ltrim($phone, '+254');
		}
		else if(substr($phone, 0, 3) === '254') 
		{
			$phone = ltrim($phone, '254');
		}
		else if(substr($phone, 0, 1) === '7') 
		{
			$phone = $phone;
		}
		$phone = '254'.$phone;
		// $phone = '254704808007';

		$this->db->where('individual_phone = "'.$phone.'" AND individual.individual_id = short_loan.individual_id');
		$query = $this->db->get('individual,short_loan');

		
		// $v_data['individual_id'] = $individual_id;
		// $v_data['phone'] = $phone;
		$v_data['query'] = $query;



		$content = $this->load->view('individual/get_short_loan', $v_data,true);


		$response['status'] = 'success';
		$response['loans'] = $content;
		echo json_encode($response);


	}
	
	public function get_short_loan_details()
	{

		$json = file_get_contents('php://input');

		$decoded = json_decode($json,true);

		$phone = $decoded['phone_number'];
		$individual_id = 1;//$decoded['individual_id'];
		$limit = $decoded['loan_id'];
		$limit_one = $limit-1;
		if (substr($phone, 0, 1) === '0') 
		{
			$phone = ltrim($phone, '0');
		}
		else if(substr($phone, 0, 4) === '+254') 
		{
			$phone = ltrim($phone, '+254');
		}
		else if(substr($phone, 0, 3) === '254') 
		{
			$phone = ltrim($phone, '254');
		}
		else if(substr($phone, 0, 1) === '7') 
		{
			$phone = $phone;
		}
		$phone = '254'.$phone;

		// $this->db->where('individual_phone = "'.$phone.'" AND individual.individual_id = individual_loan.individual_id AND loans_plan.loans_plan_id = individual_loan.loans_plan_id');
		$query = $this->db->query('SELECT * FROM individual,short_loan WHERE individual_phone = "'.$phone.'" AND individual.individual_id = short_loan.individual_id  ORDER BY short_loan.short_loan_id LIMIT '.$limit_one.', '.$limit.'');

		
		// $v_data['individual_id'] = $individual_id;
		// $v_data['phone'] = $phone;
		$v_data['query'] = $query;

		$result = '';
		if($query->num_rows() > 0)
		{
			
			foreach ($query->result() as $key => $value) {
				# code...
				$individual_id = $value->individual_id;
				$amount = $value->amount;
				$short_loan_id = $value->short_loan_id;
			}
			$data_insert['phone_number'] = $phone;
			$data_insert['individual_id'] = $individual_id;
			$data_insert['short_loan_id'] = $short_loan_id;
			$this->db->insert('ussd_session',$data_insert);

		}

		$result .= "Loan: ".number_format($amount,2)."\n";
		$result .= '1. Pay ';

		
		// $content = $this->load->view('individual/get_loan_detail', $v_data,true);


		$response['status'] = 'success';
		$response['loan_detail'] = $result;
		$response['short_loan_id'] = $short_loan_id;


		echo json_encode($response);
	}

	
}
?>