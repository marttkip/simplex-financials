<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/microfinance/controllers/microfinance.php";

class Loans_plan extends microfinance 
{
	function __construct()
	{
		parent:: __construct();
		$this->load->model('savings/member_savings_model');
		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/users_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/file_model');
		$this->load->model('admin/admin_model');
		$this->load->model('individual_model');
		$this->load->model('group_model');
		$this->load->model('savings_plan_model');
		$this->load->model('loans_plan_model');
		$this->load->model('payments_model');
		$this->load->model('withdrawals_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('microfinance_model');
		$this->load->model('savings/member_savings_model');
		$this->load->model('mpesa/mpesa_model');
		

		$this->document_upload_location = base_url().'assets/img/document_uploads/';
	}

    
	/*
	*
	*	Default action is to show all the loans_plan
	*
	*/
	public function index($order = 'loans_plan_name', $order_method = 'ASC') 
	{
		$where = 'loans_plan.installment_type_id = installment_type.installment_type_id AND loans_plan.interest_id = interest.interest_id';
		$table = 'loans_plan, installment_type, interest';
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'microfinance/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->loans_plan_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $data["links"] = $this->pagination->create_links();
		$query = $this->loans_plan_model->get_all_loans_plan($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Setup Loan plan';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('loans_plan/all_loans_plan', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
    
	/*
	*
	*	Add a new loans_plan
	*
	*/
	public function add_loans_plan() 
	{
		//form validation rules
		$this->form_validation->set_rules('processing_fees', 'Processing Fees', 'required|xss_clean');
		$this->form_validation->set_rules('loan_type', 'Loan Type', 'required|xss_clean');
		$this->form_validation->set_rules('interest_id', 'Interest type', 'required|xss_clean');
		$this->form_validation->set_rules('interest_rate', 'Interest rate', 'required|xss_clean');
		$this->form_validation->set_rules('loans_plan_name', 'Name', 'required|xss_clean');
		$this->form_validation->set_rules('installment_type_id', 'Installment type', 'required|xss_clean');
		$this->form_validation->set_rules('grace_period_minimum', 'Minimum grace period', 'numeric|xss_clean');
		$this->form_validation->set_rules('grace_period_maximum', 'Maximum grace period', 'numeric|xss_clean');
		$this->form_validation->set_rules('grace_period_default', 'Default grace period', 'numeric|xss_clean');
		//$this->form_validation->set_rules('charge_interest_over_grace_period', 'Charge interest over grace period', 'xss_clean');
		$this->form_validation->set_rules('maximum_loan_amount', 'Maximum loan amount', 'numeric|xss_clean');
		$this->form_validation->set_rules('minimum_loan_amount', 'Minimum loan amount', 'numeric|xss_clean');
		//$this->form_validation->set_rules('custom_loan_amount', 'Custom loan amount', 'numeric|xss_clean');
		$this->form_validation->set_rules('maximum_number_of_installments', 'Maximum number of installments', 'numeric|xss_clean');
		$this->form_validation->set_rules('minimum_number_of_installments', 'Minimum number of installments', 'numeric|xss_clean');
		//$this->form_validation->set_rules('custom_number_of_installments', 'Custom number of installments', 'numeric|xss_clean');
		$this->form_validation->set_rules('minimum_late_fee_on_total_loan', 'Minimum late fee on total loan', 'numeric|xss_clean');
		$this->form_validation->set_rules('maximum_late_fee_on_total_loan', 'Maximum late fee on total loan', 'numeric|xss_clean');
		//$this->form_validation->set_rules('custom_late_fee_on_total_loan', 'Custom late fee on total loan', 'numeric|xss_clean');
		$this->form_validation->set_rules('minimum_late_fee_on_overdue_principal', 'Minimum late fee on overdue principal', 'numeric|xss_clean');
		$this->form_validation->set_rules('maximum_late_fee_on_overdue_principal', 'Maximum late fee on overdue principal', 'numeric|xss_clean');
		//$this->form_validation->set_rules('custom_late_fee_on_overdue_principal', 'Custom late fee on overdue principal', 'numeric|xss_clean');
		$this->form_validation->set_rules('minimum_late_fee_on_overdue_interest', 'Minimum late fee on overdue interest', 'numeric|xss_clean');
		$this->form_validation->set_rules('maximum_late_fee_on_overdue_interest', 'Maximum late fee on overdue interest', 'numeric|xss_clean');
		//$this->form_validation->set_rules('custom_late_fee_on_overdue_interest', 'Custom late fee on overdue interest', 'numeric|xss_clean');
		$this->form_validation->set_rules('maximum_number_of_guarantors', 'Maximum number of guarantors', 'numeric|xss_clean');
		$this->form_validation->set_rules('minimum_number_of_guarantors', 'Minimum_ number of guarantors', 'numeric|xss_clean');
		//$this->form_validation->set_rules('custom_number_of_guarantors', 'Custom number of guarantors', 'numeric|xss_clean');
		 
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$loans_plan_id = $this->loans_plan_model->add_loans_plan();
			if($loans_plan_id != FALSE)
			{
				$this->session->set_userdata("success_message", "Loan plan added successfully");
				redirect('loan-management/setup');
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add loan plan. Please try again");
			}
		}
		
		$v_data['interest_scheme'] = $this->loans_plan_model->get_interest_scheme();
		$v_data['installments'] = $this->loans_plan_model->get_installment_types();
		$data['title'] = 'Add loans plan';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('loans_plan/add_loans_plan', $v_data, true);
		// print_r($v_data['interest_scheme']);
		$this->load->view('admin/templates/general_page', $data);
	}
    
	/*
	*
	*	Edit an existing loans_plan
	*	@param int $loans_plan_id
	*
	*/
	public function edit_loans_plan($loans_plan_id) 
	{	
		//form validation rules
		$this->form_validation->set_rules('interest_id', 'Interest type', 'required|xss_clean');
		$this->form_validation->set_rules('interest_rate', 'Interest rate', 'required|xss_clean');
		$this->form_validation->set_rules('loans_plan_name', 'Name', 'required|xss_clean');
		$this->form_validation->set_rules('installment_type_id', 'Installment type', 'required|xss_clean');
		$this->form_validation->set_rules('grace_period_minimum', 'Minimum grace period', 'numeric|xss_clean');
		$this->form_validation->set_rules('grace_period_maximum', 'Maximum grace period', 'numeric|xss_clean');
		$this->form_validation->set_rules('grace_period_default', 'Default grace period', 'numeric|xss_clean');
		$this->form_validation->set_rules('charge_interest_over_grace_period', 'Charge interest over grace period', 'xss_clean');
		$this->form_validation->set_rules('maximum_loan_amount', 'Maximum loan amount', 'numeric|xss_clean');
		$this->form_validation->set_rules('minimum_loan_amount', 'Minimum loan amount', 'numeric|xss_clean');
		$this->form_validation->set_rules('custom_loan_amount', 'Custom loan amount', 'numeric|xss_clean');
		$this->form_validation->set_rules('maximum_number_of_installments', 'Maximum number of installments', 'numeric|xss_clean');
		$this->form_validation->set_rules('minimum_number_of_installments', 'Minimum number of installments', 'numeric|xss_clean');
		$this->form_validation->set_rules('custom_number_of_installments', 'Custom number of installments', 'numeric|xss_clean');
		$this->form_validation->set_rules('minimum_late_fee_on_total_loan', 'Minimum late fee on total loan', 'numeric|xss_clean');
		$this->form_validation->set_rules('maximum_late_fee_on_total_loan', 'Maximum late fee on total loan', 'numeric|xss_clean');
		$this->form_validation->set_rules('custom_late_fee_on_total_loan', 'Custom late fee on total loan', 'numeric|xss_clean');
		$this->form_validation->set_rules('minimum_late_fee_on_overdue_principal', 'Minimum late fee on overdue principal', 'numeric|xss_clean');
		$this->form_validation->set_rules('maximum_late_fee_on_overdue_principal', 'Maximum late fee on overdue principal', 'numeric|xss_clean');
		$this->form_validation->set_rules('custom_late_fee_on_overdue_principal', 'Custom late fee on overdue principal', 'numeric|xss_clean');
		$this->form_validation->set_rules('minimum_late_fee_on_overdue_interest', 'Minimum late fee on overdue interest', 'numeric|xss_clean');
		$this->form_validation->set_rules('maximum_late_fee_on_overdue_interest', 'Maximum late fee on overdue interest', 'numeric|xss_clean');
		$this->form_validation->set_rules('custom_late_fee_on_overdue_interest', 'Custom late fee on overdue interest', 'numeric|xss_clean');
		$this->form_validation->set_rules('maximum_number_of_guarantors', 'Maximum number of guarantors', 'numeric|xss_clean');
		$this->form_validation->set_rules('minimum_number_of_guarantors', 'Minimum_ number of guarantors', 'numeric|xss_clean');
		$this->form_validation->set_rules('custom_number_of_guarantors', 'Custom number of guarantors', 'numeric|xss_clean');
		$this->form_validation->set_rules('processing_fees', 'Processing Fees', 'numeric|xss_clean');
		$this->form_validation->set_rules('loan_type', 'Loan Type', 'numeric|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			//update loans_plan
			if($this->loans_plan_model->edit_loans_plan($loans_plan_id))
			{
				$this->session->set_userdata('success_message', 'Loan plan updated successfully');
				redirect('microfinance/loans_plan');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update loan plan. Please try again');
			}
		}
		
		//open the add new loans_plan
		$data['title'] = 'Edit loans plan';
		$v_data['title'] = $data['title'];
		
		$v_data['loans_plan_id'] = $loans_plan_id;
		$v_data['interest_scheme'] = $this->loans_plan_model->get_interest_scheme();
		$v_data['installments'] = $this->loans_plan_model->get_installment_types();
		$v_data['loans_plan'] = $this->loans_plan_model->get_loans_plan($loans_plan_id);
		$data['content'] = $this->load->view('loans_plan/edit_loans_plan', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
    
	/*
	*
	*	Delete an existing loans_plan
	*	@param int $loans_plan_id
	*
	*/
	public function delete_loans_plan($loans_plan_id)
	{
		if($this->loans_plan_model->delete_loans_plan($loans_plan_id))
		{
			$this->session->set_userdata('success_message', 'Individual has been deleted');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Individual could not deleted');
		}
		redirect('microfinance/loans_plan');
	}
	public function short_loan_disbursement($individual_id,$short_loan_id,$phone_number,$proposed_amount)
	{
	  $this->mpesa_model->send_money($phone_number,$proposed_amount,$short_loan_id);
	  redirect('loan-management/member-short-loan-details/'.$individual_id);

	}
    
	/*
	*
	*	Activate an existing loans_plan
	*	@param int $loans_plan_id
	*
	*/
	public function activate_loans_plan($loans_plan_id)
	{
		$this->loans_plan_model->activate_loans_plan($loans_plan_id);
		$this->session->set_userdata('success_message', 'Individual activated successfully');
		redirect('microfinance/loans_plan');
	}
    
	/*
	*
	*	Deactivate an existing loans_plan
	*	@param int $loans_plan_id
	*
	*/
	public function deactivate_loans_plan($loans_plan_id)
	{
		$this->loans_plan_model->deactivate_loans_plan($loans_plan_id);
		$this->session->set_userdata('success_message', 'Individual disabled successfully');
		redirect('microfinance/loans_plan');
	}
	
	function add_leave()
	{
		$this->form_validation->set_rules('loans_plan_id', 'Individual', 'trim|numeric|required|xss_clean');
		$this->form_validation->set_rules('start_date', 'Start Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('end_date', 'End Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('leave_type_id', 'Leave Type', 'trim|numeric|required|xss_clean');

		if ($this->form_validation->run() == FALSE)//if there is an invalid item
		{
			$this->calender($_SESSION['navigation_id'], $_SESSION['sub_navigation_id']);
		}
		
		else//if the input is valid
		{
			$items = array(
						'loans_plan_id' => $this->input->post("loans_plan_id"),
						'start_date' => $this->input->post("start_date"),
						'end_date' => $this->input->post("end_date"),
						'leave_type_id' => $this->input->post("leave_type_id")
					);
			$result = $this->db->insert("leave_duration", $items);
			
			redirect("administration/calender/".$_SESSION['navigation_id']."/".$_SESSION['sub_navigation_id']);
		}
	}
	
	public function get_section_children($section_id)
	{
		$sub_sections = $this->sections_model->get_sub_sections($section_id);
		
		$children = '';
		
		if($sub_sections->num_rows() > 0)
		{
			foreach($sub_sections->result() as $res)
			{
				$section_id = $res->section_id;
				$section_name = $res->section_name;
				
				$children .= '<option value="'.$section_id.'" >'.$section_name.'</option>';
			}
		}
		
		else
		{
			$children = '<option value="" >--No sub sections--</option>';
		}
		
		echo $children;
	}
	
	public function get_loan_details($loans_plan_id)
	{
		$loans_plan = $this->loans_plan_model->get_loans_plan($loans_plan_id);
		$result = '';
		
		if($loans_plan->num_rows() > 0)
		{
			$row = $loans_plan->row();
			$loans_plan_id = $row->loans_plan_id;
			$loans_plan_status = $row->loans_plan_status;
			$installment_type_name = $row->installment_type_name;
			$interest_name = $row->interest_name;
			$interest_id =$row->interest_id;
			$loans_plan_name =$row->loans_plan_name;
			$maximum_loan_amount =$row->maximum_loan_amount;
			$minimum_loan_amount =$row->minimum_loan_amount;
			$custom_loan_amount =$row->custom_loan_amount;
			$installment_type_id =$row->installment_type_id;
			$grace_period_minimum =$row->grace_period_minimum;
			$grace_period_maximum =$row->grace_period_maximum;
			$grace_period_default =$row->grace_period_default;
			$charge_interest_over_grace_period =$row->charge_interest_over_grace_period;
			$maximum_number_of_installments =$row->maximum_number_of_installments;
			$minimum_number_of_installments =$row->minimum_number_of_installments;
			$custom_number_of_installments =$row->custom_number_of_installments;
			$minimum_late_fee_on_total_loan =$row->minimum_late_fee_on_total_loan;
			$maximum_late_fee_on_total_loan =$row->maximum_late_fee_on_total_loan;
			$custom_late_fee_on_total_loan =$row->custom_late_fee_on_total_loan;
			$minimum_late_fee_on_overdue_principal =$row->minimum_late_fee_on_overdue_principal;
			$maximum_late_fee_on_overdue_principal =$row->maximum_late_fee_on_overdue_principal; 
			$custom_late_fee_on_overdue_principal =$row->custom_late_fee_on_overdue_principal;  
			$minimum_late_fee_on_overdue_interest =$row->minimum_late_fee_on_overdue_interest;
			$maximum_late_fee_on_overdue_interest =$row->maximum_late_fee_on_overdue_interest;
			$custom_late_fee_on_overdue_interest =$row->custom_late_fee_on_overdue_interest;
			$maximum_number_of_guarantors =$row->maximum_number_of_guarantors;
			$minimum_number_of_guarantors =$row->minimum_number_of_guarantors;
			$custom_number_of_guarantors =$row->custom_number_of_guarantors;
			$interest_rate =$row->interest_rate;
			
			if($charge_interest_over_grace_period == 1)
			{
				$charge_interest_over_grace_period = 'Yes';
			}
			else
			{
				$charge_interest_over_grace_period = 'No';
			}
			
			$result = 
			'
						<button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#more'.$loans_plan_id.'">
						<i class="fa fa-plus"></i> View details
						</button>
					<!-- Modal -->
					<div class="modal fade" id="more'.$loans_plan_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
										<h4 class="modal-title">'.$loans_plan_name.'</h4>
									</div>
									<div class="modal-body">
										<table class="table table-bordered table-striped table-condensed">
											<tr>
												<th>Loan name</th>
												<td>'.$loans_plan_name.'</td>
											</tr>
											<tr>
												<th>Interest type</th>
												<td>'.$interest_name.'</td>
											</tr>
											<tr>
												<th>Interest rate</th>
												<td>'.$interest_rate.'</td>
											</tr>
											<tr>
												<th>Installment type</th>
												<td>'.$installment_type_name.'</td>
											</tr>
											<tr>
												<th>Minimum loan amount</th>
												<td>'.number_format($minimum_loan_amount, 2).'</td>
											</tr>
											<tr>
												<th>Maximum loan amount</th>
												<td>'.number_format($maximum_loan_amount, 2).'</td>
											</tr>
											<tr>
												<th>Fixed loan amount</th>
												<td>'.number_format($custom_loan_amount, 2).'</td>
											</tr>
											<tr>
												<th>Minimum grace period</th>
												<td>'.$grace_period_minimum.'</td>
											</tr>
											<tr>
												<th>Maximum grace period</th>
												<td>'.$grace_period_maximum.'</td>
											</tr>
											<tr>
												<th>Default grace period</th>
												<td>'.$grace_period_default.'</td>
											</tr>
											<tr>
												<th>Charge interest over grace period</th>
												<td>'.$charge_interest_over_grace_period.'</td>
											</tr>
											<tr>
												<th>Minimum number of installments</th>
												<td>'.$minimum_number_of_installments.'</td>
											</tr>
											<tr>
												<th>Maximum number of installments</th>
												<td>'.$maximum_number_of_installments.'</td>
											</tr>
											<tr>
												<th>Fixed number of installments</th>
												<td>'.$custom_number_of_installments.'</td>
											</tr>
											<tr>
												<th>Minimum late fee on total loan</th>
												<td>'.number_format($minimum_late_fee_on_total_loan, 2).'</td>
											</tr>
											<tr>
												<th>Maximum late fee on total loan</th>
												<td>'.number_format($maximum_late_fee_on_total_loan, 2).'</td>
											</tr>
											<tr>
												<th>Fixed late fee on total loan</th>
												<td>'.number_format($custom_late_fee_on_total_loan, 2).'</td>
											</tr>
											<tr>
												<th>Minimum late fee on overdue principal</th>
												<td>'.number_format($minimum_late_fee_on_overdue_principal, 2).'</td>
											</tr>
											<tr>
												<th>Maximum late fee on overdue principal</th>
												<td>'.number_format($maximum_late_fee_on_overdue_principal, 2).'</td>
											</tr>
											<tr>
												<th>Fixed late fee on overdue principal</th>
												<td>'.number_format($custom_late_fee_on_overdue_principal, 2).'</td>
											</tr>
											<tr>
												<th>Minimum late fee on overdue interest</th>
												<td>'.number_format($minimum_late_fee_on_overdue_interest, 2).'</td>
											</tr>
											<tr>
												<th>Maximum late fee on overdue interest</th>
												<td>'.number_format($maximum_late_fee_on_overdue_interest, 2).'</td>
											</tr>
											<tr>
												<th>Fixed late fee on overdue interest</th>
												<td>'.number_format($custom_late_fee_on_overdue_interest, 2).'</td>
											</tr>
											<tr>
												<th>Minimum number of guarantors</th>
												<td>'.$minimum_number_of_guarantors.'</td>
											</tr>
											<tr>
												<th>Maximum number of guarantors</th>
												<td>'.$maximum_number_of_guarantors.'</td>
											</tr>
											<tr>
												<th>Fixed number of guarantors</th>
												<td>'.$custom_number_of_guarantors.'</td>
											</tr>
										</table>
									</div>
								</div>
							</div>

						</div>
			';
		}
		
		echo $result;
	}
	public function amortize_loan($individual_id)
	{
		$this->form_validation->set_rules('loans_plan_id', 'Loans Plan', 'trim|numeric|required|xss_clean');
		$this->form_validation->set_rules('loan_amount', 'Requested Amount', 'trim|required|xss_clean');
		$this->form_validation->set_rules('application_date', 'Application Date', 'trim|required|xss_clean');
		$this->form_validation->set_rules('grace_period', 'Grace Period', 'trim|numeric|required|xss_clean');
		$this->form_validation->set_rules('no_of_repayments', 'Number of repayments', 'trim|numeric|required|xss_clean');
        


        $loans_plan_id = $this->input->post('loans_plan_id');
        $loan_amount = $this->input->post('loan_amount');
        $application_date = $this->input->post('application_date');
        $grace_period = $this->input->post('grace_period');
        $no_of_repayments = $this->input->post('no_of_repayments');


       //if form conatins invalid data
		if ($this->form_validation->run())
		{
			//update loans_plan
			if($this->loans_plan_model->check_loans_plan_details($individual_id, $loans_plan_id))
			{
				$this->session->set_userdata('success_message', 'Loan plan updated successfully');
				redirect('microfinance/loans_plan');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update loan plan. Please try again');
			}
		}
		
		$v_data['individual_id'] = $individual_id;
		$v_data['loan_amount'] = $loan_amount;
		$v_data['no_of_repayments'] = $no_of_repayments;
		$v_data['first_date'] = $application_date;
		$v_data['interest_id'] = '1';
		$v_data['interest_rate'] = '20';
		$v_data['installment_type_duration'] = '30';
		echo $this->load->view('loans_plan/loans/loans', $v_data, true);
	}

	public function record_amortization($loans_plan_id, $no_of_repayments,$proposed_amount,$actual_application_date,$actual_proposed_purpose,$individual_id)
	{
		
	
	      	$loans_plan = $this->loans_plan_model->get_loans_plan($loans_plan_id);
			$result = '';
			
			if($loans_plan->num_rows() > 0)
			{
				$row = $loans_plan->row();
				$loans_plan_id = $row->loans_plan_id;
				$loans_plan_status = $row->loans_plan_status;
				$installment_type_name = $row->installment_type_name;
				$interest_name = $row->interest_name;
				$interest_id =$row->interest_id;
				$loans_plan_name =$row->loans_plan_name;
				$maximum_loan_amount =$row->maximum_loan_amount;
				$minimum_loan_amount =$row->minimum_loan_amount;
				$custom_loan_amount =$row->custom_loan_amount;
				$installment_type_id =$row->installment_type_id;
				$grace_period_minimum =$row->grace_period_minimum;
				$grace_period_maximum =$row->grace_period_maximum;
				$grace_period_default =$row->grace_period_default;
				$charge_interest_over_grace_period =$row->charge_interest_over_grace_period;
				$maximum_number_of_installments =$row->maximum_number_of_installments;
				$minimum_number_of_installments =$row->minimum_number_of_installments;
				$custom_number_of_installments =$row->custom_number_of_installments;
				$minimum_late_fee_on_total_loan =$row->minimum_late_fee_on_total_loan;
				$maximum_late_fee_on_total_loan =$row->maximum_late_fee_on_total_loan;
				$custom_late_fee_on_total_loan =$row->custom_late_fee_on_total_loan;
				$minimum_late_fee_on_overdue_principal =$row->minimum_late_fee_on_overdue_principal;
				$maximum_late_fee_on_overdue_principal =$row->maximum_late_fee_on_overdue_principal; 
				$custom_late_fee_on_overdue_principal =$row->custom_late_fee_on_overdue_principal;  
				$minimum_late_fee_on_overdue_interest =$row->minimum_late_fee_on_overdue_interest;
				$maximum_late_fee_on_overdue_interest =$row->maximum_late_fee_on_overdue_interest;
				$custom_late_fee_on_overdue_interest =$row->custom_late_fee_on_overdue_interest;
				$maximum_number_of_guarantors =$row->maximum_number_of_guarantors;
				$minimum_number_of_guarantors =$row->minimum_number_of_guarantors;
				$custom_number_of_guarantors =$row->custom_number_of_guarantors;
				$interest_rate =$row->interest_rate;
				$processing_fees =$row->processing_fees;

				$v_data['individual_id'] = $individual_id;
				$v_data['loans_plan_id'] = $loans_plan_id;
				$v_data['interest_id'] = $interest_id;
				$v_data['interest_rate'] = $interest_rate;
				$v_data['processing_fees'] = $processing_fees;
				


				
				if($charge_interest_over_grace_period == 1)
				{
					$charge_interest_over_grace_period = 'Yes';
				}
				else
				{
					$charge_interest_over_grace_period = 'No';
				}
			}

			if($proposed_amount<=$maximum_loan_amount && $proposed_amount>=$minimum_loan_amount)
			{
				if($no_of_repayments<=$maximum_number_of_installments && $no_of_repayments>=$minimum_number_of_installments)
				{
					$v_data['loan_amount'] = $proposed_amount;
					$v_data['no_of_repayments'] = $no_of_repayments;
					$v_data['first_date'] = $actual_application_date;
					$v_data['interest_id'] = $interest_id;
					$v_data['interest_rate'] = $interest_rate;
				}
				else
				{
					$v_data['error'] = 'Number of Repayments: '.$no_of_repayments.' Has an issue based on Loan Plan Choosen';
				}
			}
			else
			{
			
			 $v_data['error'] = 'The Proposed Amount:  Kes.'.$proposed_amount.' Has an issue based on Loan Plan Choosen';
			}

			echo $this->load->view('individual/record_amortization_table', $v_data, true);


	}
	
	public function calculate_amortization($loans_plan_id, $no_of_repayments,$proposed_amount,$actual_application_date,$actual_proposed_purpose)
	{

		$loans_plan = $this->loans_plan_model->get_loans_plan($loans_plan_id);
		$result = '';
		
		if($loans_plan->num_rows() > 0)
		{
			$row = $loans_plan->row();
			$loans_plan_id = $row->loans_plan_id;
			$loans_plan_status = $row->loans_plan_status;
			$installment_type_name = $row->installment_type_name;
			$interest_name = $row->interest_name;
			$interest_id =$row->interest_id;
			$loans_plan_name =$row->loans_plan_name;
			$maximum_loan_amount =$row->maximum_loan_amount;
			$minimum_loan_amount =$row->minimum_loan_amount;
			$custom_loan_amount =$row->custom_loan_amount;
			$installment_type_id =$row->installment_type_id;
			$grace_period_minimum =$row->grace_period_minimum;
			$grace_period_maximum =$row->grace_period_maximum;
			$grace_period_default =$row->grace_period_default;
			$charge_interest_over_grace_period =$row->charge_interest_over_grace_period;
			$maximum_number_of_installments =$row->maximum_number_of_installments;
			$minimum_number_of_installments =$row->minimum_number_of_installments;
			$custom_number_of_installments =$row->custom_number_of_installments;
			$minimum_late_fee_on_total_loan =$row->minimum_late_fee_on_total_loan;
			$maximum_late_fee_on_total_loan =$row->maximum_late_fee_on_total_loan;
			$custom_late_fee_on_total_loan =$row->custom_late_fee_on_total_loan;
			$minimum_late_fee_on_overdue_principal =$row->minimum_late_fee_on_overdue_principal;
			$maximum_late_fee_on_overdue_principal =$row->maximum_late_fee_on_overdue_principal; 
			$custom_late_fee_on_overdue_principal =$row->custom_late_fee_on_overdue_principal;  
			$minimum_late_fee_on_overdue_interest =$row->minimum_late_fee_on_overdue_interest;
			$maximum_late_fee_on_overdue_interest =$row->maximum_late_fee_on_overdue_interest;
			$custom_late_fee_on_overdue_interest =$row->custom_late_fee_on_overdue_interest;
			$maximum_number_of_guarantors =$row->maximum_number_of_guarantors;
			$minimum_number_of_guarantors =$row->minimum_number_of_guarantors;
			$custom_number_of_guarantors =$row->custom_number_of_guarantors;
			$interest_rate =$row->interest_rate;
			
			if($charge_interest_over_grace_period == 1)
			{
				$charge_interest_over_grace_period = 'Yes';
			}
			else
			{
				$charge_interest_over_grace_period = 'No';
			}
		}

		if($proposed_amount<=$maximum_loan_amount && $proposed_amount>=$minimum_loan_amount)
		{
			if($no_of_repayments<=$maximum_number_of_installments && $no_of_repayments>=$minimum_number_of_installments)
			{
				$v_data['loan_amount'] = $proposed_amount;
				$v_data['no_of_repayments'] = $no_of_repayments;
				$v_data['first_date'] = $actual_application_date;
				$v_data['interest_id'] = $interest_id;
				$v_data['interest_rate'] = $interest_rate;
				$v_data['save'] = 1;

				

			}
			else
			{
				$v_data['error'] = 'Number of Repayments: '.$no_of_repayments.' Has an issue based on Loan Plan Choosen';
				$v_data['save'] = 0;
			
				
			}



		}
		else
		{
		
		 $v_data['error'] = 'The Proposed Amount:  Kes.'.$proposed_amount.' Has an issue based on Loan Plan Choosen';
		 $v_data['save'] = 0;


		}

		echo $this->load->view('individual/get_amortization_table', $v_data, true);

		//$v_data['individual_loan_id'] = 1;
		//$v_data['individual_id'] = 95;
		
	}
	public function calculate_short_loan_amortization($loans_plan_id, $no_of_repayments,$proposed_amount,$actual_application_date,$actual_proposed_purpose)
	{

		$loans_plan = $this->loans_plan_model->get_loans_plan($loans_plan_id);
		$result = '';
		
		if($loans_plan->num_rows() > 0)
		{
			$row = $loans_plan->row();
			$loans_plan_id = $row->loans_plan_id;
			$loans_plan_status = $row->loans_plan_status;
			$installment_type_name = $row->installment_type_name;
			$interest_name = $row->interest_name;
			$interest_id =$row->interest_id;
			$loans_plan_name =$row->loans_plan_name;
			$maximum_loan_amount =$row->maximum_loan_amount;
			$minimum_loan_amount =$row->minimum_loan_amount;
			$custom_loan_amount =$row->custom_loan_amount;
			$installment_type_id =$row->installment_type_id;
			$grace_period_minimum =$row->grace_period_minimum;
			$grace_period_maximum =$row->grace_period_maximum;
			$grace_period_default =$row->grace_period_default;
			$charge_interest_over_grace_period =$row->charge_interest_over_grace_period;
			$maximum_number_of_installments =$row->maximum_number_of_installments;
			$minimum_number_of_installments =$row->minimum_number_of_installments;
			$custom_number_of_installments =$row->custom_number_of_installments;
			$minimum_late_fee_on_total_loan =$row->minimum_late_fee_on_total_loan;
			$maximum_late_fee_on_total_loan =$row->maximum_late_fee_on_total_loan;
			$custom_late_fee_on_total_loan =$row->custom_late_fee_on_total_loan;
			$minimum_late_fee_on_overdue_principal =$row->minimum_late_fee_on_overdue_principal;
			$maximum_late_fee_on_overdue_principal =$row->maximum_late_fee_on_overdue_principal; 
			$custom_late_fee_on_overdue_principal =$row->custom_late_fee_on_overdue_principal;  
			$minimum_late_fee_on_overdue_interest =$row->minimum_late_fee_on_overdue_interest;
			$maximum_late_fee_on_overdue_interest =$row->maximum_late_fee_on_overdue_interest;
			$custom_late_fee_on_overdue_interest =$row->custom_late_fee_on_overdue_interest;
			$maximum_number_of_guarantors =$row->maximum_number_of_guarantors;
			$minimum_number_of_guarantors =$row->minimum_number_of_guarantors;
			$custom_number_of_guarantors =$row->custom_number_of_guarantors;
			$interest_rate =$row->interest_rate;
			
			if($charge_interest_over_grace_period == 1)
			{
				$charge_interest_over_grace_period = 'Yes';
			}
			else
			{
				$charge_interest_over_grace_period = 'No';
			}
		}

		if($proposed_amount<=$maximum_loan_amount && $proposed_amount>=$minimum_loan_amount)
		{
			if($no_of_repayments<=$maximum_number_of_installments && $no_of_repayments>=$minimum_number_of_installments)
			{
				$v_data['loan_amount'] = $proposed_amount;
				$v_data['no_of_repayments'] = $no_of_repayments;
				$v_data['first_date'] = $actual_application_date;
				$v_data['interest_id'] = $interest_id;
				$v_data['interest_rate'] = $interest_rate;
				$v_data['save'] = 1;

				

			}
			else
			{
				$v_data['error'] = 'Number of Repayments: '.$no_of_repayments.' Has an issue based on Loan Plan Choosen';
				$v_data['save'] = 0;
			
				
			}



		}
		else
		{
		
		 $v_data['error'] = 'The Proposed Amount:  Kes.'.$proposed_amount.' Has an issue based on Loan Plan Choosen';
		 $v_data['save'] = 0;


		}

		echo $this->load->view('individual/get_amortization_table', $v_data, true);

		//$v_data['individual_loan_id'] = 1;
		//$v_data['individual_id'] = 95;
		
	}
	
	public function apply_loan($individual_id, $image_location = NULL, $signature_location = NULL) 
	{
		//open the add new individual
		$data['title'] = 'Loan Application';
		$v_data['title'] = $data['title'];
		$v_data['withdrawal_type'] = $this->withdrawals_model->get_withdrawal_type();
		$v_data['individual'] = $this->individual_model->get_individual($individual_id);
		$row = $v_data['individual']->row();
		
		if($image_location == NULL)
		{
			$img = $row->image;
			
			if((empty($img)) || ($img == '0'))
			{
				$image_location = 'http://placehold.it/200x200?text=image';
			}
			
			else
			{
				$image_location = $this->individual_location.$img;
			}
		}
		
		else
		{
			$image_location = $this->individual_location.$image_location;
		}
		
		if($signature_location == NULL)
		{
			$img = $row->signature;
			
			if((empty($img)) || ($img == '0'))
			{
				$signature_location = 'http://placehold.it/200x100?text=signature';
			}
			
			else
			{
				$signature_location = $this->signature_location.$img;
			}
		}
		
		else
		{
			$signature_location = $this->signature_location.$signature_location;
		}
		
		$v_data['image_location'] = $image_location;
		$v_data['signature_location'] = $signature_location;
		$v_data['individual_id'] = $individual_id;
		$v_data['personnel'] = $this->personnel_model->retrieve_personnel();
		$v_data['relationships'] = $this->individual_model->get_relationship();
		$v_data['religions'] = $this->individual_model->get_religion();
		$v_data['civil_statuses'] = $this->individual_model->get_civil_status();
		$v_data['titles'] = $this->individual_model->get_title();
		$v_data['genders'] = $this->individual_model->get_gender();
		$v_data['job_titles_query'] = $this->individual_model->get_job_titles();
		$v_data['payments'] = $this->individual_model->get_loan_payments($individual_id);
		$v_data['savings_payments'] = $this->individual_model->get_savings_payments($individual_id);
		$v_data['emergency_contacts'] = $this->individual_model->get_emergency_contacts($individual_id);
		$v_data['dependants'] = $this->individual_model->get_individual_dependants($individual_id);
		$v_data['jobs'] = $this->individual_model->get_individual_jobs($individual_id);
		$v_data['individual_savings'] = $this->individual_model->get_individual_savings_plans($individual_id);
		$v_data['savings_withdrawal_amount']= $this->individual_model->get_loan_repayment_amount($individual_id);
		$v_data['individual_loan'] = $this->individual_model->get_individual_loans($individual_id);
		$v_data['individual_identifications'] = $this->individual_model->get_individual_identifications($individual_id);
		$v_data['individual_other_documents'] = $this->individual_model->get_document_uploads($individual_id);
		$v_data['all_savings_payments'] = $this->individual_model->get_all_savings_payments($individual_id);
		$v_data['disbursments'] = $this->individual_model->get_disbursments($individual_id);
		$v_data['savings_plans'] = $this->savings_plan_model->all_savings_plan();
		$v_data['loans_plans'] = $this->loans_plan_model->all_loans_plan();

		$v_data['sloans_plans'] = $this->loans_plan_model->all_short_term_loans_plan();
		$v_data['lloans_plans'] = $this->loans_plan_model->all_long_term_loans_plan();

		$v_data['payment_methods'] = $this->loans_plan_model->all_payment_methods();
		$v_data['individual_types'] = $this->individual_model->get_individual_types();
		$v_data['parent_sections'] = $this->sections_model->all_parent_sections('section_position');
		$v_data['savings_withdrawals'] = $this->individual_model->get_savings_withdrawals($individual_id);
		$v_data['withdrawal_type'] = $this->withdrawals_model->get_withdrawal_type();
		$v_data['individual_types'] = $this->individual_model->get_individual_types();
		$v_data['savings_plan'] = $this->member_savings_model->get_savings_plans();
		$data['content'] = $this->load->view('loans_plan/loan_tab', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function short_loan($individual_id)
	{
		$this->form_validation->set_rules('loans_plan_id', 'Loans Plan', 'required|xss_clean');
		$this->form_validation->set_rules('proposed_amount', 'Proposed Amount', 'required|xss_clean');
		$this->form_validation->set_rules('no_of_repayments', 'Number Of Repayments', 'required|xss_clean');
		$this->form_validation->set_rules('actual_application_date', 'Application Date', 'required|xss_clean');
		$this->form_validation->set_rules('actual_proposed_purpose', 'Proposed Purpose', 'required|xss_clean');

		$phone_number = $this->individual_model->get_individual_phone_number($individual_id);
		$proposed_amount = $this->input->post('proposed_amount');
		$no_of_repayments =  $this->input->post('no_of_repayments');
		$loans_plan_id =  $this->input->post('loans_plan_id');
		//$processing_fees = 0;
		//$processing_fees = $this->loans_plan_model->get_processing_fees($loans_plan_id);
		
		$loans_plan = $this->loans_plan_model->get_loans_plan($loans_plan_id);
		$result = '';
		if($loans_plan->num_rows() > 0)
		{
			$row = $loans_plan->row();
			$loans_plan_status = $row->loans_plan_status;
			$installment_type_name = $row->installment_type_name;
			$interest_name = $row->interest_name;
			$interest_id =$row->interest_id;
			$loans_plan_name =$row->loans_plan_name;
			$maximum_loan_amount =$row->maximum_loan_amount;
			$minimum_loan_amount =$row->minimum_loan_amount;
			$custom_loan_amount =$row->custom_loan_amount;
			$installment_type_id =$row->installment_type_id;
			$grace_period_minimum =$row->grace_period_minimum;
			$grace_period_maximum =$row->grace_period_maximum;
			$grace_period_default =$row->grace_period_default;
			$charge_interest_over_grace_period =$row->charge_interest_over_grace_period;
			$maximum_number_of_installments =$row->maximum_number_of_installments;
			$minimum_number_of_installments =$row->minimum_number_of_installments;
			$custom_number_of_installments =$row->custom_number_of_installments;
			$minimum_late_fee_on_total_loan =$row->minimum_late_fee_on_total_loan;
			$maximum_late_fee_on_total_loan =$row->maximum_late_fee_on_total_loan;
			$custom_late_fee_on_total_loan =$row->custom_late_fee_on_total_loan;
			$minimum_late_fee_on_overdue_principal =$row->minimum_late_fee_on_overdue_principal;
			$maximum_late_fee_on_overdue_principal =$row->maximum_late_fee_on_overdue_principal; 
			$custom_late_fee_on_overdue_principal =$row->custom_late_fee_on_overdue_principal;  
			$minimum_late_fee_on_overdue_interest =$row->minimum_late_fee_on_overdue_interest;
			$maximum_late_fee_on_overdue_interest =$row->maximum_late_fee_on_overdue_interest;
			$custom_late_fee_on_overdue_interest =$row->custom_late_fee_on_overdue_interest;
			$maximum_number_of_guarantors =$row->maximum_number_of_guarantors;
			$minimum_number_of_guarantors =$row->minimum_number_of_guarantors;
			$custom_number_of_guarantors =$row->custom_number_of_guarantors;
			$interest_rate =$row->interest_rate;
			$processing_fees =$row->processing_fees;
		}
		else
		{
			$this->session->set_userdata("validation_errors","Loans Plan cannot be found");
			$v_data['validation_errors']  = 'Loans Plan cannot be found';

		}

		if($proposed_amount<=$maximum_loan_amount && $proposed_amount>=$minimum_loan_amount)
		{
			if($no_of_repayments<=$maximum_number_of_installments && $no_of_repayments>=$minimum_number_of_installments)
			{
				if ($this->form_validation->run())
				{
					$short_loan_id = $this->loans_plan_model->add_short_loan($individual_id,$interest_rate,$no_of_repayments,$phone_number,$processing_fees);
					if($short_loan_id != FALSE)
					{
						$this->session->set_userdata("success_message", "Short Loan added successfully");
						//redirect('loan-management/setup');
					}
					
					else
					{
						$this->session->set_userdata("validation_errors","Could Not Add Short Loan Number. Please try again");
						$v_data['validation_errors']  = 'Could Not Add Short Loan Number. Please try again';
					}
				}
				else
				{
					$this->session->set_userdata("validation_errors","Could Not Add Short Loan. Please try again");
					$v_data['validation_errors']  = 'Could Not Add Short Loan. Please try again';
				}

			}
			else
			{
				$v_data['validation_errors'] = 'Number of Repayments: '.$no_of_repayments.' Has an issue based on Loan Plan Choosen';
			}
		}
		else
		{
		 $v_data['validation_errors'] = 'The Proposed Amount:  Kes.'.$proposed_amount.' Has an issue based on Loan Plan Choosen';
		}
		
		$data['title'] = 'Short Loan Application';
		$v_data['title'] = $data['title'];
		$v_data['individual_id'] = $individual_id;
		$v_data['sloans_plans'] = $this->loans_plan_model->all_short_term_loans_plan();
		$data['content'] = $this->load->view('loans_plan/loans/sloan', $v_data, true);
		
		$this->apply_loan($individual_id);

	}
	public function member_short_loans($order = 'short_loan_id', $order_method = 'ASC') 
	{
		$where = 'short_loan_id > 0';
		$table = 'short_loan';
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'microfinance/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->loans_plan_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $data["links"] = $this->pagination->create_links();
		$query = $this->loans_plan_model->get_all_short_loans($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Short Loan Details';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('loans_plan/all_short_loans', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function member_long_loans($order = 'individual_loan_id', $order_method = 'ASC') 
	{
		$where = 'individual_loan_id > 0';
		$table = 'individual_loan';
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'microfinance/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->loans_plan_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $data["links"] = $this->pagination->create_links();
		$query = $this->loans_plan_model->get_all_long_term_loans($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Short Loan Details';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('loans_plan/all_short_loans', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function member_list($order = 'individual_lname', $order_method = 'ASC') 
	{
		$individual_search = $this->session->userdata('individual_search');
		//$where = '(visit_type_id <> 2 OR visit_type_id <> 1) AND individual_delete = '.$delete;
		$where = 'individual_id > 0';
		if(!empty($individual_search))
		{
			$where .= $individual_search;
		}
		
		$table = 'individual';
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'loan-management/member-loans/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#"';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->loans_plan_model->get_all_individual($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Member Loans Management';
		
		$search_title = $this->session->userdata('individual_search_title');
			
		if(!empty($search_title))
		{
			$v_data['title'] = 'Members filtered by :'.$search_title;
		}
		
		else
		{
			$v_data['title'] = $data['title'];
		}
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['all_individual'] = $this->member_savings_model->all_individual();
		$v_data['individual_types'] = $this->member_savings_model->get_individual_types();
			
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('loans_plan/member_loans', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function member_loan_details($individual_id, $order = 'individual_loan_id', $order_method = 'ASC') 
	{
		
		$table = 'individual_loan';
		$where = 'individual_id = '.$individual_id;
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'loan-management/member-loan-details/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#"';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();

		$query = $this->loans_plan_model->get_all_individual($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Member Loans Details';
		
		
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['all_individual'] = $this->member_savings_model->all_individual();
		$v_data['individual_types'] = $this->member_savings_model->get_individual_types();
			
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('loans_plan/member_loan_details', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function member_short_loan_details($individual_id, $order = 'short_loan_id', $order_method = 'ASC') 
	{
		
		$table = 'short_loan';
		$where = 'individual_id = '.$individual_id;
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'loan-management/member-short-loan-details/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#"';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();

		$query = $this->loans_plan_model->get_short_loans($individual_id);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Member Short Term Loans Details';
		
		
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['all_individual'] = $this->member_savings_model->all_individual();
		$v_data['individual_types'] = $this->member_savings_model->get_individual_types();
			
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('loans_plan/member_short_loan_details', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function all_member_short_loan_details($order = 'short_loan_id', $order_method = 'ASC') 
	{
		
		$table = 'short_loan';
		$where = 'short_loan_id > 0 ';
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'payments-manager/member-short-term-loans/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#"';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();

		$query = $this->loans_plan_model->get_all_member_short_loans();
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Member Short Term Loans Details';
		
		
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['all_individual'] = $this->member_savings_model->all_individual();
		$v_data['individual_types'] = $this->member_savings_model->get_individual_types();
		$v_data['balance'] = $this->loans_plan_model->get_sum_short_loan_disbursed();
		
			
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('loans_plan/loans/all_member_short_loan_details', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	 public function account_details($individual_id, $individual_loan_id)
	{
		$v_data = array('individual_loan_id'=>$individual_loan_id);
		$v_data['title'] = 'Loans Plan Details';
		$v_data['cancel_actions'] = $this->loans_plan_model->get_cancel_actions();
		// get lease payments for this year

		$v_data['loans_plan_payments'] = $this->loans_plan_model->get_loans_plan_payments($individual_loan_id,$individual_id);
		$v_data['loans_plan_invoices'] = $this->loans_plan_model->get_loans_plan_invoices($individual_loan_id,$individual_id);
		
		
		$v_data['individual_id'] = $individual_id;
		//$v_data['individual_loan_id'] = $individual_loan_id;
		


		$data['content'] = $this->load->view('loans_plan/account_detail', $v_data, true);
		
		$data['title'] = 'Saving Plan Details';
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function  short_account_details($individual_id, $short_loan_id)
	{
		$v_data = array('short_loan_id'=>$short_loan_id);
		$v_data['title'] = 'Short Loans Plan Details';
		$v_data['cancel_actions'] = $this->loans_plan_model->get_cancel_actions();
		// get lease payments for this year

		$v_data['short_loans_plan_payments'] = $this->loans_plan_model->get_short_loans_plan_payments($short_loan_id,$individual_id);
		$v_data['short_loans_plan_invoices'] = $this->loans_plan_model->get_short_loans_plan_invoices($short_loan_id,$individual_id);
		$v_data['total_payments'] = $this->loans_plan_model->get_total_short_loan_payments($short_loan_id);
		
		
		
		$v_data['individual_id'] = $individual_id;
		//$v_data['short_loan_id'] = $short_loan_id;
		


		$data['content'] = $this->load->view('loans_plan/short_account_detail', $v_data, true);
		
		$data['title'] = 'Saving Plan Details';
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function make_loan_payments($individual_id, $individual_loan_id, $close_page = NULL)
	{

		$this->form_validation->set_rules('payment_method_id', 'Payment Method', 'trim|required|xss_clean');
		$this->form_validation->set_rules('total_amount_paid', 'Total Amount', 'trim|required|xss_clean');
		$this->form_validation->set_rules('net_amount_paid', 'Amount Paid', 'trim|required|xss_clean');
		$this->form_validation->set_rules('legal_fee', 'Legal Fees', 'trim|required|xss_clean');
		$this->form_validation->set_rules('payment_date', 'Date Receipted', 'trim|required|xss_clean');
		$this->form_validation->set_rules('paid_by', 'Date Paid By', 'trim|required|xss_clean');
		
		
		


		$payment_method_id = $this->input->post('payment_method_id');
		$total_amount_paid = $this->input->post('total_amount_paid');
		$net_amount_paid = $this->input->post('net_amount_paid');
		$legal_fee = $this->input->post('legal_fee');
		$payment_date = $this->input->post('payment_date');
		$amount_paid = $this->input->post('amount_paid');
		$paid_by = $this->input->post('paid_by');
		


		if(empty($net_amount_paid))
		{
			$net_amount_paid = 0;
		}
		if(empty($legal_fee))
		{
			$legal_fee = 0;
		}
		if(empty($total_amount_paid))
		{
			$total_amount_paid = 0;
		}
		
		//  add all this items 
		$total = $legal_fee + $net_amount_paid;

		// var_dump($total); die();
		// Normal
		
		if(!empty($payment_method_id))
		{
			if($payment_method_id == 1)
			{
				// check for cheque number if inserted
				$this->form_validation->set_rules('bank_name', 'Bank Name', 'trim|required|xss_clean');
			
			}
			else if($payment_method_id == 5)
			{
				//  check for mpesa code if inserted
				$this->form_validation->set_rules('mpesa_code', 'Mpesa Code', 'is_unique[loans_plan_payments.transaction_code]|trim|required|xss_clean');
			}
		}
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$transaction_code = $this->input->post('mpesa_code');

			if($payment_method_id == 5)
			{

				$check_transaction_code = $this->loans_plan_model->check_transaction_code($transaction_code);
			}
			else
			{
				$check_transaction_code = TRUE;
			}
		
			if($total_amount_paid == $total AND $check_transaction_code)
			{
				$this->loans_plan_model->receipt_payment($individual_id, $individual_loan_id);

				$this->session->set_userdata("success_message", 'Payment successfully added');
			}
			else
			{
				if($check_transaction_code == FALSE)
				{
					$this->session->set_userdata("error_message", 'Sorry seems like the transaction code is not unique');
				}
				else
				{
					$this->session->set_userdata("error_message", 'The amounts of payment do not add up to the total amount paid');
				}
			}
			
			
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			
		}

		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);
	}
	public function make_short_loan_payments($individual_id, $short_loan_id, $close_page = NULL)
	{

		$this->form_validation->set_rules('payment_method_id', 'Payment Method', 'trim|required|xss_clean');
		$this->form_validation->set_rules('total_amount_paid', 'Total Amount', 'trim|required|xss_clean');
		$this->form_validation->set_rules('net_amount_paid', 'Amount Paid', 'trim|required|xss_clean');
		$this->form_validation->set_rules('legal_fee', 'Legal Fees', 'trim|required|xss_clean');
		$this->form_validation->set_rules('payment_date', 'Date Receipted', 'trim|required|xss_clean');
		$this->form_validation->set_rules('paid_by', 'Date Paid By', 'trim|required|xss_clean');
		
		
		


		$payment_method_id = $this->input->post('payment_method_id');
		$total_amount_paid = $this->input->post('total_amount_paid');
		$net_amount_paid = $this->input->post('net_amount_paid');
		$legal_fee = $this->input->post('legal_fee');
		$payment_date = $this->input->post('payment_date');
		$amount_paid = $this->input->post('amount_paid');
		$paid_by = $this->input->post('paid_by');
		


		if(empty($net_amount_paid))
		{
			$net_amount_paid = 0;
		}
		if(empty($legal_fee))
		{
			$legal_fee = 0;
		}
		if(empty($total_amount_paid))
		{
			$total_amount_paid = 0;
		}
		
		//  add all this items 
		$total = $legal_fee + $net_amount_paid;

		// var_dump($total); die();
		// Normal
		
		if(!empty($payment_method_id))
		{
			if($payment_method_id == 1)
			{
				// check for cheque number if inserted
				$this->form_validation->set_rules('bank_name', 'Bank Name', 'trim|required|xss_clean');
			
			}
			else if($payment_method_id == 5)
			{
				//  check for mpesa code if inserted
				$this->form_validation->set_rules('mpesa_code', 'Mpesa Code', 'is_unique[short_loan_payments.transaction_code]|trim|required|xss_clean');
			}
		}
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$transaction_code = $this->input->post('mpesa_code');

			if($payment_method_id == 5)
			{

				$check_transaction_code = $this->loans_plan_model->check_short_transaction_code($transaction_code);
			}
			else
			{
				$check_transaction_code = TRUE;
			}
		
			if($total_amount_paid == $total AND $check_transaction_code)
			{
				$this->loans_plan_model->receipt_short_loan_payment($individual_id, $short_loan_id);

				$this->session->set_userdata("success_message", 'Payment successfully added');
			}
			else
			{
				if($check_transaction_code == FALSE)
				{
					$this->session->set_userdata("error_message", 'Sorry seems like the transaction code is not unique');
				}
				else
				{
					$this->session->set_userdata("error_message", 'The amounts of payment do not add up to the total amount paid');
				}
			}
			
			
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
			
		}

		$redirect_url = $this->input->post('redirect_url');
		redirect($redirect_url);
	}
	
}
?>