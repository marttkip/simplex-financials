<?php

class Loans_plan_model extends CI_Model 
{	



	/*
	*	Count all items from a table
	*	@param string $table
	* 	@param string $where
	*
	*/

	public function count_items($table, $where, $limit = NULL)
	{
		if($limit != NULL)
		{
			$this->db->limit($limit);
		}
		$this->db->from($table);
		$this->db->where($where);
		return $this->db->count_all_results();
	}
	
	/*
	*	Retrieve all loans_plan
	*
	*/
	public function all_loans_plan()
	{
		$this->db->where('loans_plan_status = 1');
		$this->db->order_by('loans_plan_name');
		$query = $this->db->get('loans_plan');
		
		return $query;
	}
	public function all_short_term_loans_plan()
	{
		$this->db->where('loans_plan_status = 1 AND loan_type = 1');
		$this->db->order_by('loans_plan_name');
		$query = $this->db->get('loans_plan');
		
		return $query;
	}
	public function all_long_term_loans_plan()
	{
		$this->db->where('loans_plan_status = 1  AND loan_type = 0');
		$this->db->order_by('loans_plan_name');
		$query = $this->db->get('loans_plan');
		
		return $query;
	}
	public function all_payment_methods()
	{
		$this->db->order_by('payment_method_name');
		$query = $this->db->get('payment_method');
		
		return $query;
	}
	/*
	*	Retrieve all compounding_periods
	*
	*/
	public function get_compounding_periods()
	{
		$query = $this->db->get('compounding_period');
		
		return $query;
	}

	/*
	*	Retrieve all interest scheme
	*
	*/
	public function get_interest_scheme()
	{
		$this->db->order_by('interest_name');
		$query = $this->db->get('interest');
		return $query;
	}
	
	public function get_installment_types()
	{
		$this->db->order_by('installment_type_name');
		$query = $this->db->get('installment_type');
		return $query;
	}
	public function get_cancel_actions()
	{
		$this->db->where('cancel_action_status', 1);
		$this->db->order_by('cancel_action_name');
		
		return $this->db->get('cancel_action');
	}
	/*
	*	Retrieve all parent loans_plan
	*
	*/
	public function all_parent_loans_plan($order = 'loans_plan_name')
	{
		$this->db->where('loans_plan_status = 1 AND loans_plan_parent = 0');
		$this->db->order_by($order, 'ASC');
		$query = $this->db->get('loans_plan');
		
		return $query;
	}
	/*
	*	Retrieve all children loans_plan
	*
	*/
	public function all_child_loans_plan()
	{
		$this->db->where('loans_plan_status = 1 AND loans_plan_parent > 0');
		$this->db->order_by('loans_plan_name', 'ASC');
		$query = $this->db->get('loans_plan');
		
		return $query;
	}
	
	/*
	*	Retrieve all loans_plan
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_long_term_loans($table, $where, $per_page, $page, $order = 'loans_plan_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function get_all_short_loans($table, $where, $per_page, $page, $order = 'loans_plan_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}

	public function get_all_loans_plan($table, $where, $per_page, $page, $order = 'loans_plan_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('loans_plan.*, installment_type.installment_type_name, interest.interest_name');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	
	/*
	*	Add a new loans_plan
	*	@param string $image_name
	*
	*/
	public function add_loans_plan()
	{
		$data = array(
			'loan_type'                             =>$this->input->post('loan_type'),
			'interest_rate'                         =>$this->input->post('interest_rate'),
			'interest_id'                           =>$this->input->post('interest_id'),
			'loans_plan_name'               		=>$this->input->post('loans_plan_name'),
			'installment_type_id'               	=>$this->input->post('installment_type_id'),
			'grace_period_minimum'               	=>$this->input->post('grace_period_minimum'),
			'grace_period_maximum'           		=>$this->input->post('grace_period_maximum'),
			'grace_period_default'          		=>$this->input->post('grace_period_default'),
			'maximum_loan_amount'                   =>$this->input->post('maximum_loan_amount'),
			'minimum_loan_amount'					=>$this->input->post('minimum_loan_amount'),
			'maximum_number_of_installments'		=>$this->input->post('maximum_number_of_installments'),
			'minimum_number_of_installments'		=>$this->input->post('minimum_number_of_installments'),
			'minimum_late_fee_on_total_loan'		=>$this->input->post('minimum_late_fee_on_total_loan'),
			'maximum_late_fee_on_total_loan'		=>$this->input->post('maximum_late_fee_on_total_loan'),
			'minimum_late_fee_on_overdue_principal'	=>$this->input->post('minimum_late_fee_on_overdue_principal'),
			'maximum_late_fee_on_overdue_principal'	=>$this->input->post('maximum_late_fee_on_overdue_principal'),
			'minimum_late_fee_on_overdue_interest'	=>$this->input->post('minimum_late_fee_on_overdue_interest'),
			'maximum_late_fee_on_overdue_interest'	=>$this->input->post('maximum_late_fee_on_overdue_interest'),
			'maximum_number_of_guarantors'			=>$this->input->post('maximum_number_of_guarantors'),
			'minimum_number_of_guarantors'			=>$this->input->post('minimum_number_of_guarantors'),
			'processing_fees'			            =>$this->input->post('processing_fees'),
			'loans_plan_status'			            => 1
		);
		
		if($this->db->insert('loans_plan', $data))
		{
			return $this->db->insert_id();
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Update an existing loans_plan
	*	@param string $image_name
	*	@param int $loans_plan_id
	*
	*/
	public function edit_loans_plan($loans_plan_id)
	{
		$data = array(
			'interest_rate'                         =>$this->input->post('interest_rate'),
			'interest_id'                           =>$this->input->post('interest_id'),
			'loans_plan_name'               		=>$this->input->post('loans_plan_name'),
			'installment_type_id'               	=>$this->input->post('installment_type_id'),
			'grace_period_minimum'               	=>$this->input->post('grace_period_minimum'),
			'grace_period_maximum'           		=>$this->input->post('grace_period_maximum'),
			'grace_period_default'          		=>$this->input->post('grace_period_default'),
			'charge_interest_over_grace_period'		=>$this->input->post('charge_interest_over_grace_period'),
			'maximum_loan_amount'                   =>$this->input->post('maximum_loan_amount'),
			'minimum_loan_amount'					=>$this->input->post('minimum_loan_amount'),
			'custom_loan_amount'					=>$this->input->post('custom_loan_amount'),
			'maximum_number_of_installments'		=>$this->input->post('maximum_number_of_installments'),
			'minimum_number_of_installments'		=>$this->input->post('minimum_number_of_installments'),
			'custom_number_of_installments'			=>$this->input->post('custom_number_of_installments'),
			'minimum_late_fee_on_total_loan'		=>$this->input->post('minimum_late_fee_on_total_loan'),
			'maximum_late_fee_on_total_loan'		=>$this->input->post('maximum_late_fee_on_total_loan'),
			'custom_late_fee_on_total_loan'			=>$this->input->post('custom_late_fee_on_total_loan'),
			'minimum_late_fee_on_overdue_principal'	=>$this->input->post('minimum_late_fee_on_overdue_principal'),
			'maximum_late_fee_on_overdue_principal'	=>$this->input->post('maximum_late_fee_on_overdue_principal'),
			'custom_late_fee_on_overdue_principal'	=>$this->input->post('custom_late_fee_on_overdue_principal'),
			'minimum_late_fee_on_overdue_interest'	=>$this->input->post('minimum_late_fee_on_overdue_interest'),
			'maximum_late_fee_on_overdue_interest'	=>$this->input->post('maximum_late_fee_on_overdue_interest'),
			'custom_late_fee_on_overdue_interest'	=>$this->input->post('custom_late_fee_on_overdue_interest'),
			'maximum_number_of_guarantors'			=>$this->input->post('maximum_number_of_guarantors'),
			'minimum_number_of_guarantors'			=>$this->input->post('minimum_number_of_guarantors'),
			'custom_number_of_guarantors'			=>$this->input->post('custom_number_of_guarantors'),
			'processing_fees'			            =>$this->input->post('processing_fees'),
			'loan_type'			                    =>$this->input->post('loan_type')

		);
		
		$this->db->where('loans_plan_id', $loans_plan_id);
		if($this->db->update('loans_plan', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	get a single loans_plan's children
	*	@param int $loans_plan_id
	*
	*/
	public function get_sub_loans_plan($loans_plan_id)
	{
		//retrieve all users
		$this->db->from('loans_plan');
		$this->db->select('*');
		$this->db->where('loans_plan_parent = '.$loans_plan_id);
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	get a single loans_plan's details
	*	@param int $loans_plan_id
	*
	*/
	public function get_loans_plan($loans_plan_id)
	{
		//retrieve all users
		$this->db->from('loans_plan');
		$this->db->select('*');
		$this->db->join('interest', 'interest.interest_id = loans_plan.interest_id', 'left');
		$this->db->join('installment_type', 'installment_type.installment_type_id = loans_plan.installment_type_id', 'left');
		$this->db->where('loans_plan_id = '.$loans_plan_id);
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	Delete an existing loans_plan
	*	@param int $loans_plan_id
	*
	*/
	public function delete_loans_plan($loans_plan_id)
	{
		//delete children
		if($this->db->delete('loans_plan', array('loans_plan_parent' => $loans_plan_id)))
		{
			//delete parent
			if($this->db->delete('loans_plan', array('loans_plan_id' => $loans_plan_id)))
			{
				return TRUE;
			}
			else{
				return FALSE;
			}
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Activate a deactivated loans_plan
	*	@param int $loans_plan_id
	*
	*/
	public function activate_loans_plan($loans_plan_id)
	{
		$data = array(
				'loans_plan_status' => 1
			);
		$this->db->where('loans_plan_id', $loans_plan_id);
		

		if($this->db->update('loans_plan', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Deactivate an activated loans_plan
	*	@param int $loans_plan_id
	*
	*/
	public function deactivate_loans_plan($loans_plan_id)
	{
		$data = array(
				'loans_plan_status' => 0
			);
		$this->db->where('loans_plan_id', $loans_plan_id);
		
		if($this->db->update('loans_plan', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Retrieve gender
	*
	*/
	public function get_gender()
	{
		$this->db->order_by('gender_name');
		$query = $this->db->get('gender');
		
		return $query;
	}
	
	/*
	*	Retrieve title
	*
	*/
	public function get_title()
	{
		$this->db->order_by('title_name');
		$query = $this->db->get('title');
		
		return $query;
	}
	
	/*
	*	Retrieve civil_status
	*
	*/
	public function get_civil_status()
	{
		$this->db->order_by('civil_status_name');
		$query = $this->db->get('civil_status');
		
		return $query;
	}
	
	/*
	*	Retrieve religion
	*
	*/
	public function get_religion()
	{
		$this->db->order_by('religion_name');
		$query = $this->db->get('religion');
		
		return $query;
	}
	
	/*
	*	Retrieve relationship
	*
	*/
	public function get_relationship()
	{
		$this->db->order_by('relationship_name');
		$query = $this->db->get('relationship');
		
		return $query;
	}
	
	/*
	*	Select get_job_titles
	*
	*/
	public function get_job_titles()
	{
		$this->db->select('*');
		$this->db->order_by('job_title_name', 'ASC');
		$query = $this->db->get('job_title');
		
		return $query;
	}
	
	/*
	*	get a single loans_plan's details
	*	@param int $loans_plan_id
	*
	*/
	public function get_emergency_contacts($loans_plan_id)
	{
		//retrieve all users
		$this->db->from('loans_plan_emergency');
		$this->db->select('*');
		$this->db->where(array('loans_plan_emergency.loans_plan_id' => $loans_plan_id, 'loans_plan_emergency.relationship_id' => 'relationship.relationship_id'));
		$this->db->order_by('loans_plan_emergency_fname');
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	get a single loans_plan's details
	*	@param int $loans_plan_id
	*
	*/
	public function get_loans_plan_dependants($loans_plan_id)
	{
		//retrieve all users
		$this->db->from('loans_plan_dependant');
		$this->db->select('*');
		$this->db->where(array('loans_plan_dependant.loans_plan_id' => $loans_plan_id, 'loans_plan_dependant.relationship_id' => 'relationship.relationship_id'));
		$this->db->order_by('loans_plan_dependant_fname');
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	get a single loans_plan's details
	*	@param int $loans_plan_id
	*
	*/
	public function get_loans_plan_jobs($loans_plan_id)
	{
		//retrieve all users
		$this->db->from('loans_plan_job');
		$this->db->select('loans_plan_job.*');
		$this->db->order_by('employment_date', 'DESC');
		$this->db->where(array('loans_plan_job.loans_plan_id' => $loans_plan_id));
		$query = $this->db->get();
		
		return $query;
	}
	
	public function get_leave_types()
	{
		$table = "leave_type";
		$where = "leave_type_status = 0";
		$items = "leave_type_id, leave_type_name";
		$order = "leave_type_name";
		
		$this->db->where($where);
		$this->db->order_by($order);
		$result = $this->db->get($table);
		
		return $result;
	}
	
	/*
	*	get a single loans_plan's leave details
	*	@param int $loans_plan_id
	*
	*/
	public function get_loans_plan_leave($loans_plan_id)
	{
		//retrieve all users
		$this->db->from('leave_duration, leave_type');
		$this->db->select('leave_duration.*, leave_type.leave_type_name');
		$this->db->order_by('start_date', 'DESC');
		$this->db->where(array('leave_duration.loans_plan_id' => $loans_plan_id, 'leave_duration.leave_type_id' => 'leave_type.leave_type_id'));
		$query = $this->db->get();
		
		return $query;
	}
	
	
	/*
	*	get a single loans_plan's roles
	*	@param int $loans_plan_id
	*
	*/
	public function get_loans_plan_roles($loans_plan_id)
	{
		//retrieve all users
		$this->db->from('loans_plan_section, section');
		$this->db->select('loans_plan_section.*, section.section_name, section.section_position');
		$this->db->order_by('section_position', 'ASC');
		$this->db->where(array('loans_plan_section.loans_plan_id' => $loans_plan_id, 'loans_plan_section.section_id' => 'section.section_id'));
		$query = $this->db->get();
		
		return $query;
	}

	public function get_all_individual($table, $where, $per_page, $page, $order, $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	public function get_short_loans($individual_id)
	{
		//retrieve all users
		$this->db->from('short_loan');
		$this->db->select('*');
		$this->db->where('individual_id = '.$individual_id);
		$this->db->order_by('individual_id');
		$query = $this->db->get('');
		
		return $query;
	}
	public function get_all_member_short_loans()
	{
		//retrieve all users
		$this->db->from('short_loan');
		$this->db->select('*');
		$this->db->where('short_loan_status > 0');
		$this->db->order_by('short_loan_id');
		$query = $this->db->get('');
		
		return $query;
	}
	public function sum_loans_plan()
	{
		//retrieve all users
		$this->db->select('loans_plan_name');
		$this->db->where('loans_plan_status = 1');
		$this->db->order_by('loans_plan_name', 'DESC');
		$query = $this->db->get('loans_plan');
		
		return $query->num_rows(); 
	}
	public function sum_member()
	{
		//retrieve all users
		$this->db->select('individual_id');
		$this->db->where('individual_status = 1');
		$this->db->order_by('individual_id', 'DESC');
		$query = $this->db->get('individual');
		
		return $query->num_rows(); 
	}
	public function sum_savings_plan()
	{
		//retrieve all users
		$this->db->select('savings_plan_id');
		$this->db->where('savings_plan_status = 1');
		$this->db->order_by('savings_plan_id', 'DESC');
		$query = $this->db->get('savings_plan');
		
		return $query->num_rows(); 
	}
	public function get_loan_gender_name($gender_id)
	{
		//retrieve all users
		$this->db->select('gender_name');
		$this->db->where('gender_id ='.$gender_id);
		$this->db->order_by('gender_name', 'DESC');
		$query = $this->db->get('gender');
		
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $db_sp_name)
			{
				$sp_name = $db_sp_name->gender_name;
			} 
		}
		return $sp_name; 
	}
	public function get_civil_status_name($civil_status_id)
	{
		//retrieve all users
		$this->db->select('civil_status_name');
		$this->db->where('civil_status_id ='.$civil_status_id);
		$this->db->order_by('civil_status_name', 'DESC');
		$query = $this->db->get('civil_status');
		
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $db_sp_name)
			{
				$sp_name = $db_sp_name->civil_status_name;
			} 
		}
		return $sp_name; 
	}
	public function get_loan_individual_title($title_id)
	{
		//retrieve all users
		$this->db->select('title_name');
		$this->db->where('title_id ='.$title_id);
		$this->db->order_by('title_name', 'DESC');
		$query = $this->db->get('title');
		
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $db_sp_name)
			{
				$sp_name = $db_sp_name->title_name;
			} 
		}
		return $sp_name; 
	}
	public function get_processing_fees($loans_plan_id)
	{
		//retrieve all users
		$this->db->select('processing_fees');
		$this->db->where('loans_plan_id ='.$loans_plan_id);
		$this->db->order_by('loans_plan_id', 'DESC');
		$query = $this->db->get('loans_plan');
		
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $db_sp_name)
			{
				$sp_name = $db_sp_name->processing_fees;
			} 
		}
		return $sp_name; 
	}
	public function get_individual_names($individual_id)
	{
		//retrieve all users
		$this->db->select('individual_mname as mname, individual_lname as lname, individual_fname as fname');
		$this->db->where('individual_id ='.$individual_id);
		$this->db->order_by('individual_id', 'DESC');
		$query = $this->db->get('individual');
		
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $db_sp_name)
			{
				$lsp_name = $db_sp_name->lname;
				$fsp_name = $db_sp_name->fname;
				$msp_name = $db_sp_name->mname;

				$sp_name = $fsp_name.' '.$msp_name.' '.$lsp_name;
			} 
		}
		return $sp_name; 
	}
	public function get_loans_plan_name($loans_plan_id)
	{
		$lsp_name = ' ';
		//retrieve all users
		$this->db->select('loans_plan_name');
		$this->db->where('loans_plan_id ='.$loans_plan_id);
		$this->db->order_by('loans_plan_id', 'DESC');
		$query = $this->db->get('loans_plan');
		
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $db_sp_name)
			{
				$lsp_name = $db_sp_name->loans_plan_name;
				
			} 
		}
		return $lsp_name; 
	}
	public function Check_loans_plan_amount($loans_plan_id, $loan_amount)
	{
		$maximum_loan_amount = ' ';
		//retrieve all users
		$this->db->select('maximum_loan_amount');
		$this->db->where('loans_plan_id ='.$loans_plan_id);
		$this->db->order_by('loans_plan_id', 'DESC');
		$query = $this->db->get('loans_plan');
		
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $db_sp_name)
			{
				$maximum_loan_amount = $db_sp_name->maximum_loan_amount;
			} 
		}

		$minimum_loan_amount = ' ';
		//retrieve all users
		$this->db->select('minimum_loan_amount');
		$this->db->where('loans_plan_id ='.$loans_plan_id);
		$this->db->order_by('loans_plan_id', 'DESC');
		$query = $this->db->get('loans_plan');
		
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $db_sp_name)
			{
				$minimum_loan_amount = $db_sp_name->minimum_loan_amount;
			} 
		}
		
		if($loan_amount<=$maximum_loan_amount && $loan_amount>$minimum_loan_amount)
		{
			return true; 

		}
		else{
			return false;
		}
		
	}
	
	public function update_individual_loan_table($individual_id,$first_date,$loans_plan_id,$loan_amount,$interest_id,$interest_rate,$processing_fees,$approved_date,$end_date,$no_of_repayments)
	{
     	$purpose = 'Loan Management';
		$response = array();

	
		$individiual_loan_array = array(
				                        
				                        'individual_id'=>$individual_id,
										'individual_loan_status'=> 1,
										'start_date'=>$first_date,
										'no_of_repayments'=>$no_of_repayments,
										'application_date'=>$first_date,
										'purpose'=> $purpose,
										'proposed_amount'=>$loan_amount,
										'approved_amount'=>$loan_amount,
										'loans_plan_id'=>$loans_plan_id,
										'interest_id'=>$interest_id,
										'interest_rate'=>$interest_rate,
										'processing_fee'=>$processing_fees
										);
			if($this->db->insert('individual_loan',$individiual_loan_array))
			{
				$individual_loan_id = $this->db->insert_id();	
			}
			else
			{
				$individual_loan_id = 0;
			}
			return $individual_loan_id;
	}

	
	public function update_amortization_table($individual_id, $individual_loan_id,$count,$payment_date, $loan_amount, $main_amount,$interest_cal,$principal,$balance,$month,$year,$date)
	{
		$response = array();
				//check if for that loan repayment schedule has been added
				//var_dump($this->check_if_recovery_schedule_exists($individual_loan_id,$count));die();
				

					
					$repayment_schedule = array(
						                        'individual_loan_id'=>$individual_loan_id,
						                        'individual_id'=>$individual_id,
						                        'count'=>$count,
												'starting_balance'=>$loan_amount,
												'amount'=>$main_amount,
												'interest'=>$interest_cal,
												'principal_payment'=>$principal,
												'end_balance'=>$balance,
												'date'=>$payment_date,
												'invoice_month'=>$month,
												'invoice_year'=>$year,
												'invoice_date'=>$date
												
												);
					if($this->db->insert('amortization',$repayment_schedule))
					{
						//var_dump(1);die();
						$response['result'] = 1;
						$response['message'] = "Amortization Update done";
					}
					else
					{
						$response['result'] = 0;
						$response['message'] = "Amortization Update not Done";
					}

		
			return $response;
	}
	public function add_short_loan($individual_id,$interest_rate,$no_of_repayments,$phone_number,$processing_fees)
	{
	
		
		$data = array(

			'loans_plan_id'                           =>$this->input->post('loans_plan_id'),
			'proposed_amount'                         =>$this->input->post('proposed_amount'),
			'proposed_no_of_repayments'               =>$this->input->post('no_of_repayments'),
			'application_date'               		  =>$this->input->post('actual_application_date'),
			'interest_rate'               	          => $interest_rate,
			'processing_fee'               	          => $processing_fees,
			'individual_id'          		          =>$individual_id,
			'phone_number'          		          =>$phone_number,
			'short_loan_status'                       =>0
		);
		
		if($this->db->insert('short_loan', $data))
		{
			return $this->db->insert_id();
		}
		else{
			return FALSE;
		}
	}
	public function get_loans_plan_payments($individual_loan_id,$individual_id)
	{

		$this->db->where('individual_id = '.$individual_id.' AND individual_loan_id = '.$individual_loan_id.' AND payment_status = 1 AND cancel = 0');
		
		return $this->db->get('loans_plan_payments');
	}
	public function get_loans_plan_invoices($individual_loan_id,$individual_id)
	{
		$this->db->where('individual_id = '.$individual_id.' AND individual_loan_id = '.$individual_loan_id);
		
		return $this->db->get('amortization');
	}
	public function get_short_loans_plan_payments($short_loan_id,$individual_id)
	{

		$this->db->where('individual_id = '.$individual_id.' AND short_loan_id = '.$short_loan_id.' AND payment_status = 1 AND cancel = 0');
		
		return $this->db->get('short_loan_payments');
	}
	public function get_short_loans_plan_invoices($short_loan_id,$individual_id)
	{
		$this->db->where('individual_id = '.$individual_id.' AND short_loan_id = '.$short_loan_id);
		
		return $this->db->get('short_loan');
	}
	public function get_individual_short_loans_plans_details($short_loan_id)
	{
		$this->db->select('*');
		$this->db->where('short_loan.short_loan_id = '.$short_loan_id.' AND short_loan.individual_id = individual.individual_id');
		
		return $this->db->get('short_loan,individual');
	}
	public function get_individual_loans_plans_details($individual_loan_id)
	{
		$this->db->select('*');
		$this->db->where('individual_loan.individual_loan_id = '.$individual_loan_id.' AND individual_loan.individual_id = individual.individual_id');
		
		return $this->db->get('individual_loan,individual');
	}
	public function get_singular_loans_plan($loans_plan_id)
	{

		//retrieve all users
		$this->db->from('loans_plan');
		$this->db->select('*');
		$this->db->where('loans_plan_delete = 0 AND loans_plan_status = 1 AND loans_plan_id = '.$loans_plan_id);
		$this->db->order_by('loans_plan_name', 'DESC');
		$query = $this->db->get();
		
		return $query;
	}
	public function get_payment_methods()
	{
		$this->db->where('payment_method_id > 0');
		return $this->db->get('payment_method');
	}
	public function receipt_payment($individual_id, $individual_loan_id,$personnel_id = NULL)
	{
		$payment_method_id = $this->input->post('payment_method_id');
		$total_amount_paid = $this->input->post('total_amount_paid');
		$net_amount_paid = $this->input->post('net_amount_paid');
		$legal_fee = $this->input->post('legal_fee');
		$payment_date = $this->input->post('payment_date');
		$amount_paid = $this->input->post('amount_paid');
		$paid_by = $this->input->post('paid_by');

		$receipt_number = $this->create_receipt_number();
		
		
		
		if($payment_method_id == 1)
		{
			$transaction_code = $this->input->post('bank_name');
		}
		else if($payment_method_id == 5)
		{
			//  check for mpesa code if inserted
			$transaction_code = $this->input->post('mpesa_code');
		}
		else
		{
			$transaction_code = '';
		}

		// calculate the points to get 
		$payment_date = $this->input->post('payment_date');


		// $this->get_points_to_award($individual_loan_id,$payment_date);
		// end of point calculation
		$date_check = explode('-', $payment_date);
		$month = $date_check[1];
		$year = $date_check[0];

		
		
		$data = array(
			'individual_id'=>$individual_id,
			'payment_method_id'=>$payment_method_id,
			'amount_paid'=>$total_amount_paid,
			'personnel_id'=>$this->session->userdata("personnel_id"),
			'transaction_code'=>$transaction_code,
			'payment_date'=>$this->input->post('payment_date'),
			'receipt_number'=>$receipt_number,
			'paid_by'=>$this->input->post('paid_by'),
			'payment_created'=>date("Y-m-d"),
			'year'=>$year,
			'month'=>$month,
			'confirm_number'=> $receipt_number,
			'payment_created_by'=>$this->session->userdata("personnel_id"),
			'approved_by'=>$personnel_id,'date_approved'=>date('Y-m-d')
		);

		$type_of_account = 1;

		if($type_of_account == 1)
		{
			$data['individual_loan_id'] = $individual_loan_id;

			if($this->db->insert('loans_plan_payments', $data))
			{

				$payment_id = $this->db->insert_id();

				// send email and sms for receipting
				// $this->send_receipt_notification($individual_loan_id,$confirm_number,$payment_id,$amount);

				$net_amount_paid = $this->input->post('net_amount_paid');
				$legal_fee = $this->input->post('legal_fee');


				$invoice_number = $this->get_invoice_number(); 

				if(!empty($net_amount_paid))
				{
					// insert for service charge
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $net_amount_paid,
										'invoice_type_id' => 1,
										'payment_item_status' => 1,
										'individual_loan_id' => $individual_loan_id,
										'individual_id' => $individual_id,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('loans_plan_payment_item',$service);
				}

				if(!empty($legal_fee))
				{
					$service_two = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $legal_fee,
										'invoice_type_id' => 2,
										'payment_item_status' => 1,
										'individual_loan_id' => $individual_loan_id,
										'individual_id' => $individual_id,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('loans_plan_payment_item',$service_two);
					// update the invoice table 
					// using invoice_type_id
					//$invoice_id = $this->get_last_invoice_id(2);

					

				}
				

				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		
		
	}
function create_receipt_number()
	{
		//select product code
		$preffix = "LP-LOAN-PAY-";
		$this->db->from('loans_plan_payments');
		$this->db->where("receipt_number LIKE '".$preffix."%' AND payment_status = 1");
		$this->db->select('MAX(receipt_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();
		
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number);
			$real_number++;//go to the next number
			$number = $preffix.sprintf('%03d', $real_number);
		}
		else{//start generating receipt numbers
			$number = $preffix.sprintf('%03d', 1);
		}
		
		return $number;
	}
	function check_transaction_code($transaction_code)
	{
		$this->db->where('transaction_code = "'.$transaction_code.'" AND payment_status <= 2 AND cancel = 0');
		$query = $this->db->get('loans_plan_payments');

		if($query->num_rows() > 0)
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	function check_short_transaction_code($transaction_code)
	{
		$this->db->where('transaction_code = "'.$transaction_code.'" AND payment_status <= 2 AND cancel = 0');
		$query = $this->db->get('short_loan_payments');

		if($query->num_rows() > 0)
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	public function get_all_invoice_month($individual_loan_id,$type= null)
	{
		// var_dump($individual_loan_id); die();
		
		$this->db->from('amortization');
		$this->db->select('SUM(amount) AS total_invoice, invoice_date, invoice_month,invoice_year,created');
		$this->db->where('individual_loan_id = '.$individual_loan_id.' AND created <> "0000-00-00" ');
		$this->db->order_by('created','ASC');
		$this->db->group_by('invoice_month,invoice_year');
		$query = $this->db->get();
		return $query;
	}
	public function get_all_payments_loans($individual_loan_id,$type)
	{

		$this->db->from('loans_plan_payments,loans_plan_payment_item');
		$this->db->select('loans_plan_payments.*');
		$this->db->where('loans_plan_payments.individual_loan_id = '.$individual_loan_id.'  AND loans_plan_payments.payment_status = 1 AND cancel = 0 AND loans_plan_payments.payment_id = loans_plan_payment_item.payment_id ');
		$this->db->order_by('loans_plan_payments.payment_date','ASC');
		$this->db->group_by('loans_plan_payments.payment_id','ASC');
		$query = $this->db->get();
		return $query;
	}
	public function get_all_invoice_loans($individual_loan_id,$type)
	{
		
		$this->db->from('amortization');
		$this->db->select('*');
		$this->db->where('amortization.individual_loan_id = '.$individual_loan_id);
		$this->db->order_by('amortization.amortization_id','ASC');
		$this->db->group_by('amortization.amortization_id','ASC');
		$query = $this->db->get();
		return $query;
	}
	public function get_individual_loan_statement($individual_loan_id)
	{
		$bills = $this->loans_plan_model->get_all_invoice_month($individual_loan_id,1);
		$payments = $this->loans_plan_model->get_all_payments_loans($individual_loan_id,1);
		$pardons = $this->loans_plan_model->get_all_invoice_loans($individual_loan_id,1);

		// var_dump($payments); die();
		$x=0;

		$bills_result = '';
		$last_date = '';
		$current_year = date('Y');
		$total_invoices = $bills->num_rows();
		$invoices_count = 0;
		$total_invoice_balance = 0;
		$total_arrears = 0;
		$total_payment_amount = 0;
		$result = '';
		$total_pardon_amount = 0;
		if($bills->num_rows() > 0)
		{
			foreach ($bills->result() as $key_bills) {
				# code...
				$invoice_month = $key_bills->invoice_month;
			    $invoice_year = $key_bills->invoice_year;
				$invoice_date = $key_bills->invoice_date;
				$invoice_amount = $key_bills->total_invoice;
				$invoices_count++;
				if($payments->num_rows() > 0)
				{
					foreach ($payments->result() as $payments_key) {
						# code...
						$payment_date = $payments_key->payment_date;
						$payment_year = $payments_key->year;
						$payment_month = $payments_key->month;
						$payment_amount = $payments_key->amount_paid;

						if(($payment_date <= $invoice_date) && ($payment_date > $last_date) && ($payment_amount > 0))
						{
							$total_arrears -= $payment_amount;
							// var_dump($payment_year); die();
							// if($payment_year >= $current_year)
							// {
								$result .= 
								'
									<tr>
										<td>'.date('d M Y',strtotime($payment_date)).' </td>
										<td>Payment</td>
										<td></td>
										<td>'.number_format($payment_amount, 2).'</td>
										<td>'.number_format($total_arrears, 2).'</td>
										<td></td>
									</tr> 
								';
							// }
							
							$total_payment_amount += $payment_amount;

						}
					}
				}
				
				//display disbursment if cheque amount > 0
				if($invoice_amount != 0)
				{
					$total_arrears += $invoice_amount;
					$total_invoice_balance += $invoice_amount;
						
					// if($invoice_year >= $current_year)
					// {
						$result .= 
						'
							<tr>
								<td>'.date('d M Y',strtotime($invoice_date)).' </td>
								<td>'.$invoice_month.' '.$invoice_year.' Invoice</td>
								<td>'.number_format($invoice_amount, 2).'</td>
								<td></td>
								<td>'.number_format($total_arrears, 2).'</td>
								<td><a href="'.site_url().'invoice/'.$individual_loan_id.'/'.$invoice_month.'/'.$invoice_year.'" target="_blank" class="btn btn-sm btn-warning">Invoice</a></td>
							</tr> 
						';
					// }
				}
						
				//check if there are any more payments
				if($total_invoices == $invoices_count)
				{
					//get all loan deductions before date
					if($payments->num_rows() > 0)
					{
						foreach ($payments->result() as $payments_key) {
							# code...
							$payment_date = $payments_key->payment_date;
							$payment_year = $payments_key->year;
							$payment_month = $payments_key->month;
							$payment_amount = $payments_key->amount_paid;

							if(($payment_date > $invoice_date) &&  ($payment_amount > 0))
							{
								$total_arrears -= $payment_amount;
								// if($payment_year >= $current_year)
								// {
									$result .= 
									'
										<tr>
											<td>'.date('d M Y',strtotime($payment_date)).' </td>
											<td>Payment</td>
											<td></td>
											<td>'.number_format($payment_amount, 2).'</td>
											<td>'.number_format($total_arrears, 2).'</td>
											<td></td>
										</tr> 
									';
								// }
								
								$total_payment_amount += $payment_amount;

							}
						}
					}

					
				}
						$last_date = $invoice_date;
			}
		}	
		else
		{
			//get all loan deductions before date
			if($payments->num_rows() > 0)
			{
				foreach ($payments->result() as $payments_key) {
					# code...
					$payment_date = $payments_key->payment_date;
					$payment_year = $payments_key->year;
					$payment_month = $payments_key->month;
					$payment_amount = $payments_key->amount_paid;

					if(($payment_amount > 0))
					{
						$total_arrears -= $payment_amount;
						// if($payment_year >= $current_year)
						// {
							$result .= 
							'
								<tr>
									<td>'.date('d M Y',strtotime($payment_date)).' </td>
									<td>Payment</td>
									<td></td>
									<td>'.number_format($payment_amount, 2).'</td>
									<td>'.number_format($total_arrears, 2).'</td>
									<td></td>
								</tr> 
							';
						// }
						
						$total_payment_amount += $payment_amount;

					}
				}
			}
			

		}
						
		//display loan
		$result .= 
		'
			<tr>
				<th colspan="2">Total</th>
				<th>'.number_format($total_invoice_balance, 2).'</th>
				<th>'.number_format($total_payment_amount, 2).'</th>
				<th>'.number_format($total_arrears, 2).'</th>
				<td></td>
			</tr> 
		';

		$invoice_date = $this->get_max_invoice_date($individual_loan_id);

		$account_invoice_amount = $this->loans_plan_model->get_loans_plan_invoice_brought_forward($individual_loan_id,$invoice_date);
		$account_paid_amount = $this->loans_plan_model->get_payment_loans_brought_forward($individual_loan_id,$invoice_date);
		$account_todays_payment =  $this->loans_plan_model->get_todays_loans_current_payments($individual_loan_id,$invoice_date);
		$total_brought_forward_account = $account_invoice_amount - $account_paid_amount;
	


		$response['total_arrears'] = $total_arrears;
		$response['invoice_date'] = $invoice_date;
		$response['result'] = $result;
		$response['total_invoice_balance'] = $total_brought_forward_account;
		$response['total_payment_amount'] = $total_payment_amount;
		$response['total_pardon_amount'] = $total_pardon_amount;

		// var_dump($response); die();

		return $response;
	}
	public function get_loans_plan_invoice_brought_forward($individual_loan_id, $invoice_date=NULL,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND invoice_type = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}
		// $date = $invoice_year.'-'.$invoice_month.'-30';

		if(!empty($invoice_date))
		{

			$date_add = ' AND invoice_date < "'.$invoice_date.'"';	
					
		}
		else
		{
			$date_add = '';
		}

		$where = 'individual_loan_id = '.$individual_loan_id.' '.$date_add.' '.$add;
		// var_dump($where); die();

		
		$this->db->from('amortization');
		$this->db->select('SUM(amount) AS total_invoice');
		$this->db->where($where);
		$query = $this->db->get();
		$total_invoice = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_invoice = $row->total_invoice;
		}
		return $total_invoice;
	}
	public function get_max_invoice_date($individual_loan_id,$invoice_month = NULL,$invoice_year=NULL)
	{
		if($invoice_month != NULL || $invoice_year != NULL)
		{
			$add = 'AND invoice_month = "'.$invoice_month.'" AND invoice_year = '.$invoice_year.'';
		}
		else
		{
			$add = '';
		}
		$where = 'individual_loan_id = '.$individual_loan_id.' '.$add;

		$this->db->from('amortization');
		$this->db->select('MAX(invoice_date) AS invoice_date');
		$this->db->where($where);
		$query = $this->db->get();
		$invoice_date = '';
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$invoice_date = $row->invoice_date;
		}
		return $invoice_date;
	}
	public function get_payment_loans_brought_forward($individual_loan_id,$invoice_date=NULL,$invoice_type_id = NULL,$confirm =null)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND loans_plan_payments.payment_id = loans_plan_payment_item.payment_id AND loans_plan_payment_item.invoice_type_id = '.$invoice_type_id;
			$table_add = 'loans_plan_payments,loans_plan_payment_item';
			$amount_cal = 'SUM(loans_plan_payment_item.amount_paid) AS total_paid';
		}
		else
		{
			$add = ' AND loans_plan_payments.payment_status = 1 AND cancel = 0 ';
			$table_add = 'loans_plan_payments';
			$amount_cal = 'SUM(loans_plan_payments.amount_paid) AS total_paid';
		}

		if(!empty($invoice_date))
		{
			$date_add = '  AND (loans_plan_payments.payment_date < "'.$invoice_date.'")';
		}
		else
		{
			$date_add = '';
		}
		if($confirm == 1)
		{
			$status = 'AND loans_plan_payments.confirmation_status = 0';
		}
		else
		{
			$status = '';
		}
		

		$where = 'loans_plan_payments.individual_loan_id = '.$individual_loan_id.'   '.$date_add.' '.$add.' '.$status;

		

		$this->db->from($table_add);
		$this->db->select($amount_cal);
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}
	public function get_todays_loans_current_payments($individual_loan_id,$invoice_date,$invoice_type_id = NULL)
	{
		if($invoice_type_id != NULL)
		{
			$add = 'AND loans_plan_payment_item.invoice_type_id = '.$invoice_type_id;
		}
		else
		{
			$add = '';
		}

		// $date = $invoice_year.'-'.$invoice_month.'-30';
		// $date = $invoice_year.'-08-30';
		// var_dump($date); die();
		$where = 'loans_plan_payments.individual_loan_id = '.$individual_loan_id.' AND loans_plan_payments.payment_id = loans_plan_payment_item.payment_id AND loans_plan_payments.payment_status = 1 AND cancel = 0 AND (loans_plan_payments.payment_date = "'.$invoice_date.'")'.$add;

		// var_dump($where); die();

		$this->db->from('loans_plan_payment_item,loans_plan_payments');
		$this->db->select('SUM(loans_plan_payment_item.amount_paid) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}
	public function get_invoice_number()
	{
		//select product code
		$preffix = $this->session->userdata('branch_code');
		$this->db->from('amortization');
		$this->db->where("amortization_id LIKE '".$preffix."%'");
		$this->db->select('MAX(amortization_id) AS number');
		$query = $this->db->get();//echo $query->num_rows();
		
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number);
			$real_number++;//go to the next number
			$number = $preffix.sprintf('%03d', $real_number);
		}
		else{//start generating receipt numbers
			$number = $preffix.sprintf('%03d', 1);
		}
		
		return $number;
	}
	public function receipt_short_loan_payment($individual_id, $short_loan_id,$personnel_id = NULL){

		$payment_method_id = $this->input->post('payment_method_id');
		$total_amount_paid = $this->input->post('total_amount_paid');
		$net_amount_paid = $this->input->post('net_amount_paid');
		$legal_fee = $this->input->post('legal_fee');
		$payment_date = $this->input->post('payment_date');
		$amount_paid = $this->input->post('amount_paid');
		$paid_by = $this->input->post('paid_by');

		$receipt_number = $this->create_short_receipt_number();
		
		
		
		if($payment_method_id == 1)
		{
			$transaction_code = $this->input->post('bank_name');
		}
		else if($payment_method_id == 5)
		{
			//  check for mpesa code if inserted
			$transaction_code = $this->input->post('mpesa_code');
		}
		else
		{
			$transaction_code = '';
		}

		// calculate the points to get 
		$payment_date = $this->input->post('payment_date');


		// $this->get_points_to_award($short_loan_id,$payment_date);
		// end of point calculation
		$date_check = explode('-', $payment_date);
		$month = $date_check[1];
		$year = $date_check[0];

		
		
		$data = array(
			'individual_id'=>$individual_id,
			'payment_method_id'=>$payment_method_id,
			'amount_paid'=>$total_amount_paid,
			'personnel_id'=>$this->session->userdata("personnel_id"),
			'transaction_code'=>$transaction_code,
			'payment_date'=>$this->input->post('payment_date'),
			'receipt_number'=>$receipt_number,
			'paid_by'=>$this->input->post('paid_by'),
			'payment_created'=>date("Y-m-d"),
			'year'=>$year,
			'month'=>$month,
			'confirm_number'=> $receipt_number,
			'payment_created_by'=>$this->session->userdata("personnel_id"),
			'approved_by'=>$personnel_id,'date_approved'=>date('Y-m-d')
		);

		$type_of_account = 1;

		if($type_of_account == 1)
		{
			$data['short_loan_id'] = $short_loan_id;

			if($this->db->insert('short_loan_payments', $data))
			{

				$payment_id = $this->db->insert_id();

				// send email and sms for receipting
				// $this->send_receipt_notification($short_loan_id,$confirm_number,$payment_id,$amount);

				$net_amount_paid = $this->input->post('net_amount_paid');
				$legal_fee = $this->input->post('legal_fee');


				$invoice_number = $this->get_invoice_number(); 

				if(!empty($net_amount_paid))
				{
					// insert for service charge
					$service = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $net_amount_paid,
										'invoice_type_id' => 1,
										'payment_item_status' => 1,
										'short_loan_id' => $short_loan_id,
										'individual_id' => $individual_id,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('short_loan_payment_item',$service);
				}

				if(!empty($legal_fee))
				{
					$service_two = array(
										'payment_id'=>$payment_id,
										'amount_paid'=> $legal_fee,
										'invoice_type_id' => 2,
										'payment_item_status' => 1,
										'short_loan_id' => $short_loan_id,
										'individual_id' => $individual_id,
										'payment_item_created' => date('Y-m-d')
									);
					$this->db->insert('short_loan_payment_item',$service_two);
					// update the invoice table 
					// using invoice_type_id
					//$invoice_id = $this->get_last_invoice_id(2);

					

				}
				

				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		
		
	}
function create_short_receipt_number()
	{
		//select product code
		$preffix = "SLP-LOAN-PAY-";
		$this->db->from('short_loan_payments');
		$this->db->where("receipt_number LIKE '".$preffix."%' AND payment_status = 1");
		$this->db->select('MAX(receipt_number) AS number');
		$query = $this->db->get();//echo $query->num_rows();
		
		if($query->num_rows() > 0)
		{
			$result = $query->result();
			$number =  $result[0]->number;
			$real_number = str_replace($preffix, "", $number);
			$real_number++;//go to the next number
			$number = $preffix.sprintf('%03d', $real_number);
		}
		else{//start generating receipt numbers
			$number = $preffix.sprintf('%03d', 1);
		}
		
		return $number;
	}
	public function get_total_short_loan_payments($short_loan_id)
	{
		
		$where = 'short_loan_id = '.$short_loan_id.' AND payment_status = 1 AND cancel = 0';

		
		$this->db->from('short_loan_payments');
		$this->db->select('SUM(amount_paid) AS total_paid');
		$this->db->where($where);
		$query = $this->db->get();
		$total_paid = 0;
		if($query->num_rows() >0)
		{
			$row=$query->row();
			$total_paid = $row->total_paid;
		}
		return $total_paid;
	}
	public function get_sum_short_loan_disbursed()
	{
		$balance = 500000;
		//retrieve all users
		$this->db->select('sum(proposed_amount) as total_amount_paid');
		$this->db->where('short_loan_status > 0');
		$this->db->order_by('short_loan_id', 'DESC');
		$query = $this->db->get('short_loan');
		
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $db_sp_name)
			{
				$sp_name = $db_sp_name->total_amount_paid;
				$out_balance = $balance - $sp_name;
			} 
		}
		
		return $out_balance; 

	}

}
?>