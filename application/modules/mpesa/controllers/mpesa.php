<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// header('Content-type: application/json;');
error_reporting(E_ALL);
class Mpesa  extends MX_Controller
{
	function __construct()
	{
		parent:: __construct();
		
		$this->load->model('microfinance/loans_plan_model');
    	$this->load->model('messaging/messaging_model');
    	$this->load->model('mpesa/mpesa_model');
    	$this->load->model('admin/admin_model');
		
	}

	public function mpesa_receive_transaction($type=NULL)
	{
		
		
		$json = file_get_contents('php://input');
		// $json = '{"TransactionType":"Pay Bill","TransID":"QA352S7LE3","TransTime":"20220103130836","TransAmount":"10.00","BusinessShortCode":"4022029","BillRefNumber":"P3KAK4046","InvoiceNumber":"","OrgAccountBalance":"641.00","ThirdPartyTransID":"","MSISDN":"254704808007","FirstName":"MARTIN","MiddleName":"KIPROP","LastName":"TARUS"}';

		$insert_array['test'] = $json;

		$this->db->insert('mpesa_test',$insert_array);
		$decoded = json_decode($json);

	    $serial_number = null;
	    if(isset($decoded))
	    {
	      $serial_number = $decoded->TransID;
	    }

	    if(!empty($serial_number))
		{
			$TransAmount = $decoded->TransAmount;
			$TransTime = $decoded->TransTime;
			$TransID = $decoded->TransID;
			$BusinessShortCode = $decoded->BusinessShortCode;
			$BillRefNumber = $decoded->BillRefNumber;
			$MSISDN = $decoded->MSISDN;
			$FirstName = $decoded->FirstName;
			$MiddleName = $decoded->MiddleName;
			$LastName = $decoded->LastName;
			$OrgAccountBalance = $decoded->OrgAccountBalance;
			$sender_name = $FirstName.' '.$MiddleName.' '.$LastName;
	    	$sender_name = str_replace('%20', ' ', $sender_name);
			$insert_array2['serial_number'] = $TransID;
			$insert_array2['account_number'] = $BillRefNumber;
			$insert_array2['mpesa_status'] = $type;
			$insert_array2['sender_name'] = $sender_name;
			$insert_array2['sender_phone'] = $MSISDN;
			$insert_array2['amount'] = $TransAmount;
			$insert_array2['created'] = date('Y-m-d H:i:s');

		

			if($this->db->insert('mpesa_transactions',$insert_array2))
			{
				
				$url = 'https://simplex-financials.com/rcp/mpesa/mpesa_receive_transaction/'.$type;
				//Encode the array into JSON.
				$patient_details = $decoded;
				//The JSON data.

				$data_string = json_encode($patient_details);

				try{                                                                                                         

					$ch = curl_init($url);
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
					curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($ch, CURLOPT_HTTPHEADER, array(
						'Content-Type: application/json',
						'Content-Length: ' . strlen($data_string))
					);
					$result = curl_exec($ch);


					$response['ResultCode'] = 0;
					$response['ResultDesc'] = 'Could not process payment';
					$response['ThirdPartyTransID'] = date('YmdHis');
					
					
					curl_close($ch);
				}
				catch(Exception $e)
				{
					
					$response['ResultCode'] = 1;
					$response['ResultDesc'] = 'Could not process payment';
					$response['ThirdPartyTransID'] = date('YmdHis');
			
				}


			}
			else
			{
				$response['ResultCode'] = 0;
				$response['ResultDesc'] = 'Could not process payment';
				$response['ThirdPartyTransID'] = date('YmdHis');
			}
		}
		else
		{
			$response['ResultCode'] = 1;
			$response['ResultDesc'] = 'Could not process payment';
			$response['ThirdPartyTransID'] = date('YmdHis');
		}
		echo json_encode($response);


		
	

	}
  	public function mpesa_receive_transaction_old($type=NULL)
	{
		
		

		$json = file_get_contents('php://input');

		$insert_array['test'] = $json;
		$this->db->insert('mpesa_test',$insert_array);
		$decoded = json_decode($json);
	    $serial_number = null;
	    if(isset($decoded))
	    {
	      $serial_number = $decoded->TransID;
	    }

		if(!empty($serial_number))
		{
			$TransAmount = $decoded->TransAmount;
			$TransTime = $decoded->TransTime;
			$TransID = $decoded->TransID;
			$BusinessShortCode = $decoded->BusinessShortCode;
			$BillRefNumber = $decoded->BillRefNumber;
			$MSISDN = $decoded->MSISDN;
			$FirstName = $decoded->FirstName;
			$MiddleName = $decoded->MiddleName;
			$LastName = $decoded->LastName;
			$OrgAccountBalance = $decoded->OrgAccountBalance;
			$sender_name = $FirstName.' '.$MiddleName.' '.$LastName;
	    	$sender_name = str_replace('%20', ' ', $sender_name);
			$insert_array2['serial_number'] = $TransID;
			$insert_array2['account_number'] = $BillRefNumber;
			$insert_array2['mpesa_status'] = $type;
			$insert_array2['sender_name'] = $sender_name;
			$insert_array2['sender_phone'] = $MSISDN;
			$insert_array2['amount'] = $TransAmount;
			$insert_array2['created'] = date('Y-m-d H:i:s');

		

			if($this->db->insert('mpesa_transactions',$insert_array2))
			{
				


				$mpesa_id = $this->db->insert_id();


				$decoded = json_decode($json);

				$message = 'Dear '.$sender_name.' we have received your tithe in our Parish St. Francis Xavier. Peace be with you';

			   	$this->messaging_model->sms($MSISDN,$message);

			   	
				$transaction_identifier = $BillRefNumber;

				$exploded = explode("-", $transaction_identifier);

				// send_notifications();
				

				// if(is_array($exploded))
				// {


				// 	$first_character = $exploded[0];


				// 	if($first_character == "LN" AND !is_numeric($first_character) AND !empty($first_character))
				// 	{
				// 		$individual_loan_id = $exploded[1];
				// 		$individual_id = $exploded[2];

				// 		// save into individual loan payment

				// 		$payment_method_id = 5;
				// 		$total_amount_paid = $TransAmount;
				// 		$net_amount_paid = $TransAmount;
				// 		$legal_fee = 0;
				// 		$payment_date = date('Y-m-d');
				// 		$amount_paid = $TransAmount;
				// 		$paid_by = $mpesa_id;
						

				// 		$receipt_number = $this->loans_plan_model->create_receipt_number();
						
						
				// 		$transaction_code = $TransID;
						
				// 		$date_check = explode('-', $payment_date);
				// 		$month = $date_check[1];
				// 		$year = $date_check[0];

						
						
				// 		$data = array(
				// 						'individual_id'=>$individual_id,
				// 						'payment_method_id'=>$payment_method_id,
				// 						'amount_paid'=>$total_amount_paid,
				// 						'personnel_id'=>0,
				// 						'transaction_code'=>$transaction_code,
				// 						'payment_date'=>$payment_date,
				// 						'receipt_number'=>$receipt_number,
				// 						'paid_by'=>$mpesa_id,
				// 						'payment_created'=>date("Y-m-d"),
				// 						'year'=>$year,
				// 						'month'=>$month,
				// 						'confirm_number'=> $receipt_number,
				// 						'payment_created_by'=>$this->session->userdata("personnel_id"),
				// 						'approved_by'=>$individual_id,'date_approved'=>date('Y-m-d')
				// 					);

				// 		$type_of_account = 1;

				// 		if($type_of_account == 1)
				// 		{
				// 			$data['individual_loan_id'] = $individual_loan_id;

				// 			if($this->db->insert('loans_plan_payments', $data))
				// 			{

				// 				$payment_id = $this->db->insert_id();
				// 				$service = array(
				// 									'payment_id'=>$payment_id,
				// 									'amount_paid'=> $total_amount_paid,
				// 									'invoice_type_id' => 1,
				// 									'payment_item_status' => 1,
				// 									'individual_loan_id' => $individual_loan_id,
				// 									'individual_id' => $individual_id,
				// 									'payment_item_created' => date('Y-m-d')
				// 								);
				// 				$this->db->insert('loans_plan_payment_item',$service);

				// 			}
							
				// 		}

				// 	}
				// 	else if($first_character == "SL" AND !is_numeric($first_character) AND !empty($first_character))
				// 	{
				// 		$short_loan_id = $exploded[1];
				// 		$individual_id = $exploded[2];

				// 		// save into individual loan payment

				// 		$payment_method_id = 5;
				// 		$total_amount_paid = $TransAmount;
				// 		$net_amount_paid = $TransAmount;
				// 		$legal_fee = 0;
				// 		$payment_date = date('Y-m-d');
				// 		$amount_paid = $TransAmount;
				// 		$paid_by = $mpesa_id;
						

				// 		$receipt_number = 'BR25612';//$this->loans_plan_model->create_receipt_number();
						
						
				// 		$transaction_code = $TransID;
						
				// 		$date_check = explode('-', $payment_date);
				// 		$month = $date_check[1];
				// 		$year = $date_check[0];

						
						
				// 		$data = array(
				// 						'individual_id'=>$individual_id,
				// 						'payment_method_id'=>$payment_method_id,
				// 						'amount_paid'=>$total_amount_paid,
				// 						'personnel_id'=>0,
				// 						'transaction_code'=>$transaction_code,
				// 						'payment_date'=>$payment_date,
				// 						'receipt_number'=>$receipt_number,
				// 						'paid_by'=>$mpesa_id,
				// 						'payment_created'=>date("Y-m-d"),
				// 						'year'=>$year,
				// 						'month'=>$month,
				// 						'confirm_number'=> $receipt_number,
				// 						'payment_created_by'=>$this->session->userdata("personnel_id"),
				// 						'approved_by'=>$individual_id,'date_approved'=>date('Y-m-d')
				// 					);

				// 		$type_of_account = 1;

				// 		if($type_of_account == 1)
				// 		{
				// 			$data['short_loan_id'] = $short_loan_id;

				// 			if($this->db->insert('short_loan_payments', $data))
				// 			{

				// 				$payment_id = $this->db->insert_id();
				// 				$service = array(
				// 									'payment_id'=>$payment_id,
				// 									'amount_paid'=> $total_amount_paid,
				// 									'invoice_type_id' => 1,
				// 									'payment_item_status' => 1,
				// 									'short_loan_id' => $short_loan_id,
				// 									'individual_id' => $individual_id,
				// 									'payment_item_created' => date('Y-m-d')
				// 								);
				// 				$this->db->insert('short_loan_payment_item',$service);

				// 			}
							
				// 		}

				// 	}
				// }
				

			}
			else
			{
				$response['ResultCode'] = 0;
				$response['ResultDesc'] = 'Could not process payment';
				$response['ThirdPartyTransID'] = date('YmdHis');
			}
		}
		else
		{
			$response['ResultCode'] = 1;
			$response['ResultDesc'] = 'Could not process payment';
			$response['ThirdPartyTransID'] = date('YmdHis');
		}
		echo json_encode($response);
		
	}

	public function send_notifications()
	{
		$this->db->where('notified = 0');
		$query = $this->db->get('mpesa_transactions');
		// var_dump($query->num_rows());die();
		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$sender_phone = $value->sender_phone;
				$sender_name = $value->sender_name;
				$mpesa_id = $value->mpesa_id;

				
				
			   // var_dump($response);die();
				// $array['notified'] = 1;
				// $this->db->where('mpesa_id',$mpesa_id);
				// $this->db->update('mpesa_transactions',$array);
			}
		}
	}
	public function loan_request()
	{

		$json = file_get_contents('php://input');

		$decoded = json_decode($json,true);

		$phone = $decoded['phone_number'];
		$amount = $decoded['amount'];
		$individual_id =$decoded['individual_id'];


		// save request 
		// $phone = $decoded['phone_number'];

		if (substr($phone, 0, 1) === '0') 
		{
			$phone = ltrim($phone, '0');
		}
		else if(substr($phone, 0, 4) === '+254') 
		{
			$phone = ltrim($phone, '+254');
		}
		else if(substr($phone, 0, 3) === '254') 
		{
			$phone = ltrim($phone, '254');
		}
		else if(substr($phone, 0, 1) === '7') 
		{
			$phone = $phone;
		}
		$phone = '254'.$phone;

		$this->db->where('individual_phone = "'.$phone.'"');
		$query = $this->db->get('individual');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$individual_id = $value->individual_id;
			}
		}

		$short_loan_request['individual_id'] = $individual_id;
		$short_loan_request['amount'] = $amount;
		$short_loan_request['proposed_amount'] = $amount;
		$short_loan_request['proposed_no_of_repayments'] = 1;
		$short_loan_request['interest_rate'] = 10;
		$short_loan_request['processing_fee'] = 100;
		$short_loan_request['phone_number'] = $phone;
		$short_loan_request['application_date'] = date('Y-m-d');
		$short_loan_request['created'] = date('Y-m-d');
		$short_loan_request['short_loan_status'] = 0;
		$short_loan_request['transaction_code'] = NULL;
		$short_loan_request['transaction_id'] = NULL;


		if($this->db->insert('short_loan',$short_loan_request))
		{
			$short_loan_id = $this->db->insert_id();


			// send sms to person 

			$message = 'Your account has been successfully credited with Ksh. '.$amount.'. Thank you.';

			// $this->messaging_model->sms($phone,$message);


			$mpesa_response = $this->mpesa_model->send_money($phone,$amount,$short_loan_id);

			$data_insert['phone_number'] = $phone;
			$data_insert['individual_id'] = $individual_id;
			$data_insert['short_loan_id'] = $short_loan_id;
			$data_insert['short_loan_amount'] = $amount;
			$this->db->insert('ussd_session',$data_insert);


			$response['status'] ='success';
			$response['message'] ='You request is being processed';
		}
		else
		{	
			$response['status'] ='fail';
			$response['message'] ='Sorry, kindly try again';

		}




		echo json_encode($response);
	}

	public function pay_loan_older()
	{

			$json = file_get_contents('php://input');

			$decoded = json_decode($json,true);

			$phone = $decoded['phone_number'];
			// $individual_id = $decoded['individual_id'];
			// $individual_loan_id = $decoded['individual_loan_id'];
			$amount = $decoded['amount'];
		
			if (substr($phone, 0, 1) === '0') 
			{
				$phone = ltrim($phone, '0');
			}
			else if(substr($phone, 0, 4) === '+254') 
			{
				$phone = ltrim($phone, '+254');
			}
			else if(substr($phone, 0, 3) === '254') 
			{
				$phone = ltrim($phone, '254');
			}
			else if(substr($phone, 0, 1) === '7') 
			{
				$phone = $phone;
			}
			// $phone = ;
			$phone = '254'.$phone;
			// $amount = 10;

			$this->db->where('phone_number',$phone);
			$this->db->order_by('session_id','DESC');
			$this->db->limit(1);
			$query = $this->db->get('ussd_session');

			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $key => $value) {
					# code...
					$individual_loan_id = $value->individual_loan_id;
					$individual_id = $value->individual_id;
				}
			}



		  $consumer_key = 'rgfADfcCfrHuW4LwhC2t89DoRvhUyh0j';
		  $consumer_secret = 'QrvpfeTSzgOa4Fwq';
		  $headers = ['Content-Type:application/json; charset=utf8'];


		  $access_token_url = 'https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';

		  $curl = curl_init($access_token_url);
		  curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		  curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);

		  curl_setopt($curl, CURLOPT_HEADER, FALSE);

		  curl_setopt($curl, CURLOPT_USERPWD, $consumer_key.':'.$consumer_secret);

		  $result = curl_exec($curl);
		  $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		  $result = json_decode($result);

		  $access_token = $result->access_token;

		  // echo $access_token;

		  curl_close($curl);

		  $url = 'https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest';
		  
		  $curl = curl_init();
		  curl_setopt($curl, CURLOPT_URL, $url);
		  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$access_token)); 

		  $BusinessShortCode = '4022029';
		  $Timestamp = date('YmdHis');
		  $Amount = $amount;
		  $PartyA = $phone;
		  $AccountReference = 'LN-'.$individual_loan_id.'-'.$individual_id;
		  $TransactionDesc = 'CAT Payment';

		  $Passkey = '67723e58c18bd7dcecf3535e9eb549738e1bfd2bd2d92d97eb045fdffafd9d92';
		  $Password = base64_encode($BusinessShortCode.$Passkey.$Timestamp);
		  
		  
		   //setting custom header

		  
		  
		  $curl_post_data = array(
		    //Fill in the request parameters with valid values
		    'BusinessShortCode' => $BusinessShortCode,
		    'Password' => $Password,
		    'Timestamp' => $Timestamp,
		    'TransactionType' => 'CustomerPayBillOnline',
		    'Amount' => $Amount,
		    'PartyA' => $PartyA,
		    'PartyB' => $BusinessShortCode,
		    'PhoneNumber' => $PartyA,
		    'CallBackURL' => 'http://simplex-financials.com/transaction/stk_callback.php',
		    'AccountReference' => $AccountReference,
		    'TransactionDesc' => $TransactionDesc
		  );
		  
		  $data_string = json_encode($curl_post_data);
		  
		  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		  curl_setopt($curl, CURLOPT_POST, true);
		  curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
		  
		  $curl_response = curl_exec($curl);

		  $response['status'] = 'success';

		  echo json_encode($response);
	}


	public function pay_short_loan()
	{

			$json = file_get_contents('php://input');

			$decoded = json_decode($json,true);

			$phone = $decoded['phone_number'];
			// $individual_id = $decoded['individual_id'];
			// $individual_loan_id = $decoded['individual_loan_id'];
			$amount = $decoded['amount'];
		
			if (substr($phone, 0, 1) === '0') 
			{
				$phone = ltrim($phone, '0');
			}
			else if(substr($phone, 0, 4) === '+254') 
			{
				$phone = ltrim($phone, '+254');
			}
			else if(substr($phone, 0, 3) === '254') 
			{
				$phone = ltrim($phone, '254');
			}
			else if(substr($phone, 0, 1) === '7') 
			{
				$phone = $phone;
			}
			// $phone = ;
			$phone = '254'.$phone;
			// $amount = 10;

			$this->db->where('phone_number',$phone);
			$this->db->order_by('session_id','DESC');
			$this->db->limit(1);
			$query = $this->db->get('ussd_session');

			if($query->num_rows() > 0)
			{
				foreach ($query->result() as $key => $value) {
					# code...
					$short_loan_id = $value->short_loan_id;
					$individual_id = $value->individual_id;
				}
			}



		  $consumer_key = 'rgfADfcCfrHuW4LwhC2t89DoRvhUyh0j';
		  $consumer_secret = 'QrvpfeTSzgOa4Fwq';
		  $headers = ['Content-Type:application/json; charset=utf8'];


		  $access_token_url = 'https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';

		  $curl = curl_init($access_token_url);
		  curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		  curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);

		  curl_setopt($curl, CURLOPT_HEADER, FALSE);

		  curl_setopt($curl, CURLOPT_USERPWD, $consumer_key.':'.$consumer_secret);

		  $result = curl_exec($curl);
		  $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		  $result = json_decode($result);

		  $access_token = $result->access_token;

		  // echo $access_token;

		  curl_close($curl);

		  $url = 'https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest';
		  
		  $curl = curl_init();
		  curl_setopt($curl, CURLOPT_URL, $url);
		  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$access_token)); 

		  $BusinessShortCode = '4022029';
		  $Timestamp = date('YmdHis');
		  $Amount = $amount;
		  $PartyA = $phone;
		  $AccountReference = 'SL-'.$short_loan_id.'-'.$individual_id;
		  $TransactionDesc = 'CAT Payment';

		  $Passkey = '67723e58c18bd7dcecf3535e9eb549738e1bfd2bd2d92d97eb045fdffafd9d92';
		  $Password = base64_encode($BusinessShortCode.$Passkey.$Timestamp);
		  
		  
		   //setting custom header

		  
		  
		  $curl_post_data = array(
		    //Fill in the request parameters with valid values
		    'BusinessShortCode' => $BusinessShortCode,
		    'Password' => $Password,
		    'Timestamp' => $Timestamp,
		    'TransactionType' => 'CustomerPayBillOnline',
		    'Amount' => $Amount,
		    'PartyA' => $PartyA,
		    'PartyB' => $BusinessShortCode,
		    'PhoneNumber' => $PartyA,
		    'CallBackURL' => 'http://simplex-financials.com/transaction/stk_callback.php',
		    'AccountReference' => $AccountReference,
		    'TransactionDesc' => $TransactionDesc
		  );
		  
		  $data_string = json_encode($curl_post_data);
		  
		  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		  curl_setopt($curl, CURLOPT_POST, true);
		  curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
		  
		  $curl_response = curl_exec($curl);

		  $response['status'] = 'success';

		  echo json_encode($response);
	}

	public function pay_loan()
	{
		$json = file_get_contents('php://input');

		$decoded = json_decode($json,true);

		$phone = $decoded['phone_number'];
		// $individual_id = $decoded['individual_id'];
		// $individual_loan_id = $decoded['individual_loan_id'];
		$amount = $decoded['amount'];
	
		if (substr($phone, 0, 1) === '0') 
		{
			$phone = ltrim($phone, '0');
		}
		else if(substr($phone, 0, 4) === '+254') 
		{
			$phone = ltrim($phone, '+254');
		}
		else if(substr($phone, 0, 3) === '254') 
		{
			$phone = ltrim($phone, '254');
		}
		else if(substr($phone, 0, 1) === '7') 
		{
			$phone = $phone;
		}
		$phone = '254'.$phone;


		


		$this->db->where('phone_number',$phone);
		$this->db->order_by('session_id','DESC');
		$this->db->limit(1);
		$query = $this->db->get('ussd_session');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $key => $value) {
				# code...
				$individual_loan_id = $value->individual_loan_id;
				$individual_id = $value->individual_id;
			}
		}
		// $individual_loan_id = 10;
		// $individual_id = 1;

		  $consumer_key = 'rgfADfcCfrHuW4LwhC2t89DoRvhUyh0j';
		  $consumer_secret = 'QrvpfeTSzgOa4Fwq';
		  $headers = ['Content-Type:application/json; charset=utf8'];


		  $access_token_url = 'https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';

		  $curl = curl_init($access_token_url);
		  curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		  curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);

		  curl_setopt($curl, CURLOPT_HEADER, FALSE);

		  curl_setopt($curl, CURLOPT_USERPWD, $consumer_key.':'.$consumer_secret);

		  $result = curl_exec($curl);
		  $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		  $result = json_decode($result);

		  $access_token = $result->access_token;

		  // echo $access_token;

		  curl_close($curl);

		  $url = 'https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest';
		  
		  $curl = curl_init();
		  curl_setopt($curl, CURLOPT_URL, $url);
		  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$access_token)); 

		  $BusinessShortCode = '4022029';
		  $Timestamp = date('YmdHis');
		  $Amount = $amount;
		  $PartyA = $phone;
		  $AccountReference = 'LN-'.$individual_loan_id.'-'.$individual_id;
		  $TransactionDesc = 'CAT Payment';

		  $Passkey = '67723e58c18bd7dcecf3535e9eb549738e1bfd2bd2d92d97eb045fdffafd9d92';
		  $Password = base64_encode($BusinessShortCode.$Passkey.$Timestamp);
		  
		  
		   //setting custom header

		  
		  
		  $curl_post_data = array(
		    //Fill in the request parameters with valid values
		    'BusinessShortCode' => $BusinessShortCode,
		    'Password' => $Password,
		    'Timestamp' => $Timestamp,
		    'TransactionType' => 'CustomerPayBillOnline',
		    'Amount' => $Amount,
		    'PartyA' => $PartyA,
		    'PartyB' => $BusinessShortCode,
		    'PhoneNumber' => $PartyA,
		    'CallBackURL' => 'http://simplex-financials.com/transaction/stk_callback.php',
		    'AccountReference' => $AccountReference,
		    'TransactionDesc' => $TransactionDesc
		  );
		  
		  $data_string = json_encode($curl_post_data);
		  
		  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		  curl_setopt($curl, CURLOPT_POST, true);
		  curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
		  
		  $curl_response = curl_exec($curl);
	  
	  // echo $curl_response;
	  $checked_response['status'] ='success';
	  echo json_encode($checked_response);

	}

}
?>
