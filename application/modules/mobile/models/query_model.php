<?php

class Query_model extends CI_Model 
{	
	/*
	*	Retrieve all group
	*
	*/
	public function clean_phone_number($phone_number)
	{
		//remove forward slash
		$numbers = explode("/",$phone_number);
		$phone_number = $numbers[0];

		//remove hyphens
		$phone_number = str_replace("-","",$phone_number);

		//remove spaces
		$phone_number = str_replace(" ","",$phone_number);

		if (substr($phone_number, 0, 1) === '0') 
		{
			$phone_number = ltrim($phone_number, '0');
		}
		
		if (substr($phone_number, 0, 3) === '254') 
		{
			$phone_number = ltrim($phone_number, '254');
		}
		
	
		return $phone_number;
	}
	public function confirm_phone_number($phone_number)
	{
		$this->db->where('individual_status = 1 AND (individual_phone = '.$phone_number.' OR individual_phone = +254'.$phone_number.' OR individual_phone = 254'.$phone_number.' OR individual_phone = 0'.$phone_number.')');
		$query = $this->db->get('individual');
		
		return $query;
	}
	public function get_query_type($query_type_sent)
	{
		$query_type_id=" ";
		$this->db->select('query_type_id');
		$this->db->where('query_type_status = 1 AND query_type_name LIKE "%'.$query_type_sent.'%"');
		$query2 = $this->db->get('query_type');
		$total2 = $query2->row();
		$query_type_id = $total2->query_type_id;
		
		return $query_type_id;
	}
	public function add_message_recieved($phone_number,$message,$amount,$query_type,$individual_id)
	{

		$interest_id = 1;
		$data = array(
			
			'query_message'=>ucwords(strtolower($message)),
			'query_phone'=>$phone_number,
			'query_amount'=>$amount,
			'query_type'=>$query_type,
			'individual_id'=>$individual_id,
			'query_status'=>1
		);
		$loan_data = array(
				'individual_id'=>$individual_id,
				'loans_plan_id'=>1,
				'created'=>date('Y-m-d'),
				'created_by'=>1,
				'individual_loan_status'=>1,
				'proposed_amount'=>$amount,
				'interest_id' =>$interest_id,//straight line interest calculation
				'application_date'=>date('Y-m-d')
							);
		if($this->db->insert('query', $data))
		{
			//if successfull insert in query,  add the loan to individual_loan table
			if($this->db->insert('individual_loan',$loan_data))
			{
				$individual_loan_id = $this->db->insert_id();
				
				return $individual_loan_id;
			}
			else
			{
				return FALSE;
			}
		}
		else{
			return FALSE;
		}
	}
	public function get_amortization($individual_loan_id,$amount,$individual_id,$phone)
	{
		$interest_rate = 5;
		$disbursement_data = array(
						'individual_id'=>$individual_id,
						'individual_loan_id'=>$individual_loan_id,
						'cheque_amount'=>$amount,
						'created_by'=>$this->session->userdata('personnel_id'),
						'modified_by'=>$this->session->userdata('personnel_id'),
						'created'=>date('Y-m-d'),
						'dibursement_date'=>date('Y-m-d H:i:s')
					);
		//save loan ammortization
		$no_of_repayments = 1;
		$interest_id = 1;
		$start_balance = $loan_amount = $amount;

		
		for($p = 0; $p < $no_of_repayments; $p++)
		{
			$count = $p + 1;
			//straight line
			if($interest_id == 1)
			{
				$interest_payment = ($loan_amount * ($interest_rate/100));
			}
			
			$principal_payment = round(($loan_amount / $no_of_repayments),-3);
			//var_dump($principal_payment);die();
			
			if ($count == $no_of_repayments)
			{
				//var_dump($no_of_repayments);die();
				$principal_payment = $start_balance;
			}
			$end_balance = $start_balance - $principal_payment;
			
			//var_dump($this->payments_model->update_amortization_table($count,$interest_payment,$principal_payment,$individual_loan_id));die();
			
			if($this->payments_model->update_amortization_table($count,$interest_payment,$principal_payment,$individual_loan_id))
			{
				//insert into the disburement table
				
				if($this->db->insert('disbursement', $disbursement_data))
				{
					$disbursement_id = $this->db->insert_id();
					//var_dump($disbursement_id);die();
					
					//disburse money
					$fields_ser = array
					(
						'api_key' => urlencode("1000"),
						'phone_number' => urlencode($phone),
						'transaction_id' => urlencode($disbursement_id),
						'amount' => urlencode("10")
					);

					$base_url = 'https://www.omnis.co.ke/omnis_gateway/';
					$service_url = $base_url.'disburse-payment';
					$response2 = $this->rest_service($service_url, $fields_ser);
					$message = json_decode($response2);
					$message = json_decode($message);
					
					//var_dump($message);die();
					
					if($message->result == 0)
					{
						$data_insert = array(
								'member_id' => $individual_id,
								'amount_paid' => $amount,
								'date_transacted' => date('Y-m-d'),
							);
						$this->db->insert('receipt', $data_insert);
						
						//update member invoice
						$data_insert2 = array(
								'disbursement_status' => 1,
								'dibursement_recieve_date'=>date('Y-m-d H:i:s'),
							);
						$this->db->where('disbursement_id', $disbursement_id);
						$this->db->update('disbursement', $data_insert2);
						
						$return['message'] = 'success';
						$return['result'] = 'You will receive your money shortly via MPesa. Thank you for using Omnis Sacco';

						$delivery_message = "You will receive your money shortly via MPesa. Thank you for using Omnis Sacco";

						
					}
					else
					{
						$return['result'] = FALSE;
				 	    $return['message'] = "Could not add disbursement";

					}
					
				}
				else
				{
					$return['result'] = FALSE;
				 	$return['message'] = "Could not add disbursement";
				}
			}
			else
			{
				 $return['result'] = FALSE;
				 $return['message'] = "Could not update the amortization table";
			}
		}
		
		return $return;
	}
	public function rest_service($service_url, $fields)
	{
		$fields_string = '';
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string, '&');
		
		// a. initialize
		try{
			$ch = curl_init();
			
			// b. set the options, including the url
			curl_setopt($ch, CURLOPT_URL, $service_url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, count($fields));
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
			
			// c. execute and fetch the resulting HTML output
			$output = curl_exec($ch);
			
			//in the case of an error save it to the database
			if ($output === FALSE) 
			{
				$return['result'] = 0;
				$return['message'] = curl_error($ch);
				
				$return = json_encode($response);
			}
			
			else
			{
				$return = $output;
				$return = json_encode($output);
			}
		}
		
		//in the case of an exceptions save them to the database
		catch(Exception $e)
		{
			$response['result'] = 0;
			$response['message'] = $e->getMessage();
			
			$return = json_encode($response);
		}
		
		return $return;
	}
}
?>