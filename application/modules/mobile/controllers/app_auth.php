<?php
class App_auth extends MX_Controller 
{
	function __construct()
	{
		parent:: __construct();
		if (isset($_SERVER['HTTP_ORIGIN'])) {
			header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
			header('Access-Control-Allow-Credentials: true');
			header('Access-Control-Max-Age: 86400');    // cache for 1 day
		}
	
		// Access-Control headers are received during OPTIONS requests
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
	
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
	
			exit(0);
		}
		$this->load->model('auth/auth_model');
		$this->load->model('query_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/admin_model');
		$this->load->model('microfinance/payments_model');
		
	}
	
	public function index()
	{
		
	}
    
	
	public function sms_auth()
	{
		$phone = $this->input->post('phone');
		$message = $this->input->post('message');
		//$phone = '254711589113';
		//$message = 'pesa 2000';
		$phone_sent = $this->query_model->clean_phone_number($phone);
		//var_dump($phone_sent); die();
		$message_sent = (explode(" ",$message));
		//print_r($message_sent); die();
		$amount = $message_sent[1];
		if($amount > 0)
		{
            $query_type_sent = $message_sent[0];
            $query_type = $this->query_model->get_query_type($query_type_sent);

			$confirm_phone_number = $this->query_model->confirm_phone_number($phone);
			if($confirm_phone_number->num_rows() >0)
			{
				$row = $confirm_phone_number->row();
				$individual_id = $row->individual_id;
				$individual_fname = $row->individual_fname;
				$individual_lname = $row->individual_lname;
				$individual_name = $individual_fname." ".$individual_lname;
				

				$add_message = $this->query_model->add_message_recieved($phone,$message,$amount,$query_type,$individual_id);

				
				if($add_message > 0)
				{
					//get deductions expected
					$return = $this->query_model->get_amortization($add_message,$amount,$individual_id,$phone_sent);

					if($return['result'])
					{
						$response['result'] = 1;
						$response['message'] = 'Hi, '.$individual_name.' your query has been recieved and is being processed.';
					}
					
					else
					{
						$response['result'] = 0;
						$response['message'] = $return['message'];
					}
				}
				else
				{
					$response['result'] = 0;
					$response['message'] = 'Query recieved but processing is taking a while call admin';
				}
			}
			else
			{

			  $response['result'] = 0;
			  $response['message'] = 'The member does not exist';
			}
		}
		else
		{
			$response['result'] = 0;
			$response['message'] = 'The amount provided is wrong...';
		}
		
		echo json_encode($response);
	}
	
	
	
}
?>