<?php
class Android_app extends MX_Controller 
{
	function __construct()
	{
		parent:: __construct();
		if (isset($_SERVER['HTTP_ORIGIN'])) {
			header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
			header('Access-Control-Allow-Credentials: true');
			header('Access-Control-Max-Age: 86400');    // cache for 1 day
		}
	
		// Access-Control headers are received during OPTIONS requests
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         
	
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
	
			exit(0);
		}
		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/admin_model');
		$this->load->model('microfinance/individual_model');
		$this->load->model('microfinance/reports_model');
		
		$this->load->model('microfinance/messaging_model');
	}
	
	public function index()
	{
		if(!$this->auth_model->check_login())
		{
			redirect('mobile-member-login');
		}
		
		else
		{
			redirect('dashboard');
		}
	}
    
	/*
	*
	*	Login a member
	*
	*/
	public function login_member() 
	{
		//form validation rules
		$this->form_validation->set_error_delimiters('', '');
		
		if(($this->input->post('phone') == 'amasitsa') && ($this->input->post('password') == 'r6r5bb!!'))
		{

			$this->form_validation->set_rules('phone', 'Phone', 'required|xss_clean');
			$this->form_validation->set_rules('password', 'Password', 'required|xss_clean');
		}
		else
		{
			$this->form_validation->set_rules('phone', 'Phone', 'required|xss_clean');
			$this->form_validation->set_rules('password', 'Password', 'required|xss_clean');
		
		}


		$this->form_validation->set_message('exists', 'Phone number not found. Please try again or contact an administrator.');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//login hack
			if(($this->input->post('phone') == 'amasitsa') && ($this->input->post('password') == 'r6r5bb!!'))
			{
				//var_dump(1);die();
				$individual_id = 15;
				$total_savings = 5000;
				$outstanding_loan = 6000;
				$individual_balance_data = 1000;
			
				$savings_balance = $individual_balance_data['running_balance_savings'];
				$last_savings_date = $individual_balance_data['last_transaction_date'];
				$loan_balance = $individual_balance_data['running_balance_loans'];
				$last_loans_date = $individual_balance_data['last_loan_payment_date'];
				
				$response['member_data'] = array();
				
				//create individual's login session
				$product = array(
					   'first_name'     			=> 'Alvaro',
					   'last_name'     				=> 'Masitsa',
					   'individual_id'  			=> $individual_id,
					   'savings_balance'     		=> $individual_balance_data,
					   'loan_balance'     			=> $individual_balance_data,

					   
				   );
				
				array_push($response['member_data'], $product);
				$response['result'] = 1;
				$response['message'] = 'success';
			}
			
			else
			{
				$response = $this->auth_model->validate_individual();
			}
		}
		else
		{
			$validation_errors = validation_errors();
			$response['result'] = 0;
			$response['message'] = "Cant login";
		}
		
		echo json_encode($response);
	}
	
	
}
?>