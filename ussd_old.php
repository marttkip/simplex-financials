<?php
// Reads the variables sent via POST
$sessionId   = $_POST["sessionId"];
$serviceCode = $_POST["serviceCode"];
$phoneNumber = $phone = '0704808007';// $_POST["phoneNumber"];
$text        = $_POST["text"];


	


//set default level to zero
$level = 0;

/* Split text input based on asteriks(*)
 * Africa's talking appends asteriks for after every menu level or input
 * One needs to split the response from Africa's Talking in order to determine
 * the menu level and input for each level
 * */
$ussd_string_exploded = explode ("*",$text);

// Get menu level from ussd_string reply
$level = count($ussd_string_exploded);

if($level == 1 or $level == 0){
    
    // ussd_proceed($level);
    
    display_menu($phoneNumber); // show the home/first menu
}
else
{
	ussd_proceed($level);
}
if ($level > 1)
{
	
    // if ($ussd_string_exploded[0] == "1")
    // {
    //     // If user selected 1 send them to the registration menu
    // 	 confirm_password($ussd_string_exploded);
    //      // menu_one();
    //     // account_balance($ussd_string_exploded,$phone, $dbh);
    // }

    // else if ($ussd_string_exploded[0] == "2")
    // {
    //     //If user selected 2, send them to the about menu
    //     confirm_password($ussd_string_exploded);
    //     // request_loan($ussd_string_exploded,$phone, $dbh);
    // }
}

/* The ussd_proceed function appends CON to the USSD response your application gives.
 * This informs Africa's Talking USSD gateway and consecuently Safaricom's
 * USSD gateway that the USSD session is till in session or should still continue
 * Use this when you want the application USSD session to continue
*/
function ussd_proceed($ussd_text){
    echo "CON $ussd_text";
}

/* This ussd_stop function appends END to the USSD response your application gives.
 * This informs Africa's Talking USSD gateway and consecuently Safaricom's
 * USSD gateway that the USSD session should end.
 * Use this when you to want the application session to terminate/end the application
*/
function ussd_stop($ussd_text){
    echo "END $ussd_text";
}

function confirm_password($password)
{
	ussd_proceed($password);
}

//This is the home menu function
function display_menu($phone)
{
	$url = 'https://simplex-financials.com/simplex-financials/microfinance/individual/get_individual_details';
	//Encode the array into JSON.
	$patient_details['phone_number'] = $phone;
	//The JSON data.

	$data_string = json_encode($patient_details);

	try{                                                                                                         

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string))
		);
		$result = curl_exec($ch);
		curl_close($ch);
		

		$result = (array)json_decode($result);
		
		$status = $result['status'];
		if($status == "success")
		{
			$response  = "Welcome to back ".$result['message'].",\n";
		    $response .= "1. Enter passcode \n";
		    // $response .= "2. Forgot password ";
		    	// var_dump($response);die();
		    ussd_proceed($response);
		}
		else
		{
			$ussd_text = "To Sign up for microfinance. Kindly call us through 0720465220";
			ussd_stop($ussd_text); 
		}

		 
	}
	catch(Exception $e)
	{
		
		$ussd_text = "Sorry could not process the request now. Please try again";
		ussd_stop($ussd_text); 
	}


  
}


// Function that hanldles About menu
function account_balance($details,$phone, $dbh)
{
	if(count($details) == 2)
    {
	    $ussd_text =    "Account Number: ";
	    ussd_proceed($ussd_text);
	}

    if(count($details)== 3)
    {
        if (empty($details[1])){
                $ussd_text = "Sorry we do not accept blank values \n \n 0 go to previous menu \n 00 to exit";
                ussd_proceed($ussd_text);
        } else 
        {


        	$url = 'https://simplex-financials.com/simplex-financials/messaging/send_individual_statement';
			//Encode the array into JSON.
			$patient_details['account_number'] = $details[1];
			$patient_details['phone_number'] = $phone;
			//The JSON data.

			$data_string = json_encode($patient_details);

			try{                                                                                                         

				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'Content-Length: ' . strlen($data_string))
				);
				$result = curl_exec($ch);
				curl_close($ch);

				$ussd_text = "Sorry could not process the request now. Please try again";
            	ussd_stop($ussd_text); 
			}
			catch(Exception $e)
			{
				
				$ussd_text = "Sorry could not process the request now. Please try again";
            	ussd_stop($ussd_text); 
			}

          
        }
    }
}

// Function that handles Registration menu
function request_loan($details,$phone, $dbh){
    if(count($details) == 2)
    {
        
        $ussd_text = "Please enter your account number :";
        ussd_proceed($ussd_text); // ask user to enter registration details
    }
    if(count($details)== 3)
    {
        if (empty($details[1])){
                $ussd_text = "Sorry we do not accept blank values";
                ussd_proceed($ussd_text);
        } else 
        {
            // $input = explode(",",$details[1]);//store input values in an array
            // $full_name = $input[0];//store full name
            // $email = $input[1];//store email
            // $phone_number =$phone;//store phone number 

            $ussd_text = "Amount:";
            ussd_proceed($ussd_text); // ask user to enter registration details
        }
    }
    if(count($details)== 4)
    {
        if (empty($details[3])){
                $ussd_text = "Sorry we do not accept blank values";
                ussd_proceed($ussd_text);
        } else 
        {
         $ussd_text = "Passsword:";
         ussd_proceed($ussd_text); // ask user to enter registration details
        }
    }
    if(count($details)==5)
    {
        if (empty($details[3])){
                $ussd_text = "Sorry we do not accept blank values";
                ussd_proceed($ussd_text);
        } else 
        {
            $ussd_text = "Amount:";
            ussd_proceed($ussd_text); // ask user to enter registration details
        }
    }
}

header('Content-type: text/plain; charset=utf-8;');
// echo $response



?>

